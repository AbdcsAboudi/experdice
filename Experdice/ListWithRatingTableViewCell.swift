//
//  GenderTableViewCell.swift
//  BookDr
//
//  Created by Ayman Rawashdeh 📱^  🇯🇴 =  on 7/9/17.
//  Copyright © 2017 Emdadat. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ListWithRatingTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
