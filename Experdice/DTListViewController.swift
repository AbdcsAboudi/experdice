//
//  DTListViewController.swift
//  CoreDataTemplateApp
//
//  Created by Ayman Rawashdeh on 1/22/17.
//  Copyright © 2017 DreamTechs. All rights reserved.
//

import UIKit

class DTListViewController: DTViewController, UITableViewDelegate, UITableViewDataSource {
    
    weak var tableView: UITableView?
    var refreshControl: UIRefreshControl?
    
    var list = [Any]()
    
    // AR: Offset for requesting next page
    var requestNextPageOffset = 5
    
    var currentPage = 0
    var isHasMore =  false
    var isRequesting = false
    var isRefreshControlEnabled = true
    
    private var savedOldListCount = 0
    
    var limit = 10
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //AS: for hidding the side menu
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        refreshControl?.endRefreshing()
    }
    
    deinit {
        print("Deinitlizing List view controller")
    }
    
    // MARK: - Override Methods
    
    func tableViewSetup(){
        
        if tableView == nil {
            
            tableView = UITableView(frame: self.view.frame, style: .plain)
            tableView?.translatesAutoresizingMaskIntoConstraints = true
            tableView?.clipsToBounds = true
            tableView?.scrollsToTop = true
            self.view.addSubview(tableView!)
        }
        
        setRefreshControlEnabled(to: isRefreshControlEnabled)
        
        if refreshControl != nil {
            
            if #available(iOS 10, *) {
                
                tableView?.refreshControl = refreshControl
            } else {
                
                tableView?.addSubview(refreshControl!)
                tableView?.sendSubview(toBack: refreshControl!)
            }
        }
        
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.separatorStyle = .none
        tableView?.separatorColor = .clear
        
        initialRequest()
    }
    
    // AR: Mandatory override this function, and start your API request calls here
    func beginRequest(){
        fatalError("DTListViewController: beginRequest Function must be override..")
    }
    
    func endRequest(hasMore: Bool){
        
        // AR: Removes load more indicator in the tableview footer
        removeLoadMoreIndicator()
        
        isHasMore = hasMore
        isRequesting = false
        
        DispatchQueue.main.async {
             self.refreshControl?.endRefreshing()
             self.tableView?.reloadData()
        }
        
    }
    
    func endRequestWithFailure(){
        
        // AR: Reset latest saved count in case failure
        savedOldListCount = 0
        
        // AR: Removes load more indicator in the tableview footer
        removeLoadMoreIndicator()
        
        isRequesting = false
        
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
        }
    }
    
    func populateTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //fatalError("DTListViewController: populateTableViewCell Func must be override..")
        
        return UITableViewCell()
    }
    
    
    // MARK: - Selectors
    
    open func initialRequest(){
        
        guard !isRequesting else {
            
            return
        }

        list = [AnyObject]()
        isRequesting = true
        currentPage = 0
        
        refreshControl?.beginRefreshing()
        
        beginRequest()
    }
    
    func refreshListSelector(_ sender: Any){
        initialRequest()
    }
    
    func getNextPage(){
        
        guard isHasMore, !isRequesting, savedOldListCount != list.count else {
            
            return
        }
        
        isRequesting = true
        currentPage += 1
        
        refreshControl?.beginRefreshing()
        
        // AR: Fix repeated calls if there were no new data has been added in latest request
        savedOldListCount = list.count
        
        addLoadMoreIndicator()
        beginRequest()
    }
    
    func setRefreshControlEnabled(to isEnabled: Bool) {
        
        if isEnabled {
            
            refreshControl = UIRefreshControl()
            refreshControl?.addTarget(self, action: #selector(DTListViewController.refreshListSelector(_:)), for: .valueChanged)
        }else{
            
           refreshControl = nil
        }
        
        
        isRefreshControlEnabled = isEnabled
    }
    
    func addLoadMoreIndicator(){
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView!.bounds.width, height: 33))
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.tag = 100
        activityIndicator.startAnimating()
        activityIndicator.frame = footerView.frame
        footerView.addSubview(activityIndicator)
        
        tableView?.tableFooterView = footerView
    }
    
    func removeLoadMoreIndicator(){
        tableView?.tableFooterView = nil
    }
    
    
    // MARK: - TableView Delegates // nawaf was here
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return populateTableViewCell(tableView, cellForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == (list.count - requestNextPageOffset) {
            getNextPage()
        }
        
    }
    
    
    
}
