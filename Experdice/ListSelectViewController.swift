//
//  GenderSelectViewController.swift
//  BookDr
//
//  Created by Ayman Rawashdeh 📱^  🇯🇴 =  on 7/9/17.
//  Copyright © 2017 Emdadat. All rights reserved.
//

import UIKit
import SDWebImage

protocol ListSelectViewControllerDelegate: NSObjectProtocol {
    
    func listDidSelect(listViewController: ListSelectViewController, selectedIndex: Int, selectedTitle: String, selectedId: Int?)
}

class ListSelectViewController: DTViewController,UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: ListSelectViewControllerDelegate?
    
    var defaultArray = [String]()
    var listArray: [String]!
    var idsArray: [Int]?
    var imagesArray: [String]?
    
    var didSelect:((Int,String, Int?)->Void)?
    var willDisappear: ((_ isSelected: Bool) -> Void)?
    
    var isRatingCell = false
    var isSelected = false
    
    var firstSection: [String]?
    var firstSectionIsEnabled = true
    
    init(titlesArray: [String]!, idsArray: [Int]?, withSearch: Bool, withRatingCell: Bool, withOtherCell: Bool) {
        
        if withSearch {
            super.init(nibName: "ListSelectWithSearchViewController", bundle: nil)
        }else {
            super.init(nibName: "ListSelectViewController", bundle: nil)
        }
        
        if titlesArray != nil {
            
            self.listArray = titlesArray!
        }
        
        self.defaultArray = listArray
        self.idsArray = idsArray
        self.isRatingCell = withRatingCell
       
        // AR: Append other cell to end of lists with id = -1
        if withOtherCell {
            self.idsArray?.append(-1)
            self.listArray.append(DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Other"))
            self.defaultArray.append(DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Other"))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
//        willDisappear = nil
        print("Deintilizing list view")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        needSideBar = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName:"ListTableViewCell",bundle: nil), forCellReuseIdentifier: "ListTableViewCell")
        tableView.register(UINib(nibName:"ListWithRatingTableViewCell",bundle: nil), forCellReuseIdentifier: "ListWithRatingTableViewCell")
        tableView.register(UINib(nibName:"ListWithImageTableViewCell",bundle: nil), forCellReuseIdentifier: "ListWithImageTableViewCell")
        
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        tableView.backgroundColor = .clear
        
        searchBarStyle()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        willDisappear?(isSelected)
//        didSelect = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarStyle(){
        
        searchTextField?.delegate = self
        //searchTextField?.placeholder = DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Search Placeholder")
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 25))
        let imageSearchIcon = UIImageView(frame: CGRect(x: 5, y: 0, width: 25, height: 25))
        imageSearchIcon.image = #imageLiteral(resourceName: "search_icon").withRenderingMode(.alwaysTemplate)
        imageSearchIcon.tintColor = .black
        imageSearchIcon.contentMode = .scaleToFill
        
        leftView.addSubview(imageSearchIcon)
        
        searchTextField?.leftView = leftView
        searchTextField?.leftViewMode = .always
       
    }
    
    // MARK: - Table View Delegates 🚬
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isRatingCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListWithRatingTableViewCell", for: indexPath) as! ListWithRatingTableViewCell
            
            cell.titleLabel.text = listArray[indexPath.row]
            cell.ratingView.value = CGFloat(indexPath.row + 1)
            cell.ratingView.isUserInteractionEnabled = false
           // alignInputViewsForLanguage(view: cell.contentView)
            return cell
            
        }
        
        if let firstSection = firstSection, indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as! ListTableViewCell
            cell.titleLabel.text = firstSection[indexPath.row]
            if !firstSectionIsEnabled {
               // cell.titleLabel.textColor = DTConstants.textLightGrayColor
                cell.selectionStyle = .none
            }
          //  alignInputViewsForLanguage(view: cell.contentView)
            return cell
        }
        
        if let imagesArray = imagesArray {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListWithImageTableViewCell", for: indexPath) as! ListWithImageTableViewCell
            if indexPath.row < imagesArray.count {
                let imagePath = imagesArray[indexPath.row]
                cell.logoImageView.sd_setIndicatorStyle(.gray)
                cell.logoImageView.sd_addActivityIndicator()
                cell.logoImageView.sd_setImage(with: URL(string: imagePath), completed:{ [weak cell = cell](image, error, cacheType, imageUrl) in
                    cell?.logoImageView.sd_removeActivityIndicator()
                })                
            }
            cell.titleLabel.text = listArray[indexPath.row]
         //   alignInputViewsForLanguage(view: cell.contentView)
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as! ListTableViewCell
            cell.titleLabel.text = listArray[indexPath.row]
         //   alignInputViewsForLanguage(view: cell.contentView)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if let firstSection = firstSection, indexPath.section == 0 {
            
            if !firstSectionIsEnabled{
                return
            }
            
            didSelect?(indexPath.row, firstSection[indexPath.row], -1)
            delegate?.listDidSelect(listViewController: self, selectedIndex: indexPath.row, selectedTitle: firstSection[indexPath.row], selectedId: -1)
           
            return
        }
       
        // AR: get Id index incase autoComplete changed indexes between ids and title, figure out the orginal index for title
        let idIndex = getIndexID(from: listArray[indexPath.row])
        var id: Int!
        
        if let indexId = idIndex, let idsArray = idsArray {
            if indexId < idsArray.count {
                id = idsArray[indexId]
            }
        }
        
        isSelected = true
        didSelect?(indexPath.row,listArray[indexPath.row], id)
        
        // AR: Using delegate method in case list view controller will be retained for more selections 
        delegate?.listDidSelect(listViewController: self, selectedIndex: indexPath.row, selectedTitle: listArray[indexPath.row], selectedId: id)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return firstSection != nil ? 2 : 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if firstSection != nil, section == 0 {
            return firstSection!.count
        }
        return listArray.count
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return isRatingCell ? 66 : 55
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        if section == 0 {
            return 10
        }
        return 0.1
    }
    
    func getIndexID(from titleLabel: String) -> Int?{
        for (index, str) in defaultArray.enumerated() {
            if str == titleLabel {
                return index
            }
        }
        return nil
    }
    
    
    // MARK: - TextField delegates
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let substring = textField.text {
            
            if (range.location == 0 && string.isEmpty){
                
                listArray = defaultArray
                tableView.reloadData()
                
                return true
            }
            
            var substr = NSString(string: substring)
            substr = substr.replacingCharacters(in: range, with: string) as NSString
            
            searchAutoCompleteEntries(with: substr)
            
        }
        
        return true
    }
    
    
    func searchAutoCompleteEntries(with substr: NSString){
        
        listArray.removeAll()
        
        for str in defaultArray {
            
            let strValue = NSString(string: arabicReplaceCharacters(str: str).lowercased())
            
            let substrRange = strValue.range(of: substr.lowercased as String)
            
            if substrRange.length > 0 {
                
                listArray.append(str)
            }
            
        }
        
        tableView.reloadData()
        
    }
    
    func arabicReplaceCharacters(str: String) -> String{
        
        var str1  = str.replacingOccurrences(of: "أ", with: "ا")
        str1  = str1.replacingOccurrences(of: "إ", with: "ا")
        return str1
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
