//
//  DTHelper.swift
//
//
//  Created by Saad Albasha on 8/1/16.
//  Copyright © 2016 Dreamtechs. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import QuartzCore
import Foundation
import CoreLocation
import MapKit
import AFNetworking
import MBProgressHUD

public enum TextFieldValidationType: Int {
    case emailValidation,       // 0
    nameValidation,             // 1
    phoneNumberValidation,      // 2
    complexPasswordValidation,  // 3
    simplePasswordValidation,   // 4
    urlValidation,              // 5
    facebookValidation,         // 6
    twitterValidation,          // 7
    instagramValidation,        // 8
    snapchatValidation,         // 9
    noneValidation              // 10
}


public class DTHelper : NSObject  {
    
    public static let sharedInstance = DTHelper()
    public var hud = MBProgressHUD()
    
    
    fileprivate override init(){
        
    }
    
    
    // MARK: Connectivity check
    
    
    
    // MARK: Google Maps Helper
    
    //    public func getGoogleMapsImageFromCoordinates(longitude:Double , Latitude:Double , mapImage:UIImageView) ->UIImage {
    //
    ////        var staticMapUrl: String = "http://maps.google.com/maps/api/staticmap?markers=color:blue|\(35.3),\(35.3)&\("zoom=13&size=\(2 * Int(mapImage.size.width))\(2 * Int(mapImage.size.height))")&sensor=true"
    ////        var mapUrl: NSURL = NSURL(string: staticMapUrl.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding))!
    ////        var image: UIImage = UIImage.imageWithData(NSData.dataWithContentsOfURL(mapUrl))
    ////        var mapImage: UIImageView = UIImageView(frame: mapFrame)
    //
    //    }
    
    // MARK: Colors
    
    public func colorWithHexString (_ hex:String) -> UIColor {
        
        
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            let startIndex = cString.characters.index(cString.startIndex, offsetBy: 1)
            cString = cString.substring(from: startIndex)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = cString.substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        let gString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 2)).substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        let bString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 4)).substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string:gString).scanHexInt32(&g)
        Scanner(string:bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    //MARK: - Strings and Validation
    
    public func validateInputInTextField(_ textfield:UITextField, withType validationType:TextFieldValidationType)->Bool{
        
        switch (validationType.rawValue) {
        case 0:
            
            let emailRegex = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
            
            return emailTest.evaluate(with: textfield.text)
            
        case 1:
            
            let nameRegex = "^[A-Za-zا-ي\\s-0-9]{3,36}"
            let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegex)
            
            return nameTest.evaluate(with: textfield.text)
            
        case 2:
            
            let phoneRegex = "(([0]{2})|[+]{1})(\\d{9,14})"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            
            return phoneTest.evaluate(with: textfield.text)
            
        case 3:
            
            let passwordRegex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[!@#$%^&*])\\H{3,30}$"
            let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
            
            return passwordTest.evaluate(with: textfield.text)
            
        case 4:
            
            let passwordRegex = "^[A-Za-z0-9]{8,24}"
            let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
            
            return passwordTest.evaluate(with: textfield.text)
            
        case 5:
            
            let urlRegex = "(?i)https?://(?:www\\.)?\\S+(?:/|\\b)"
            let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegex)
            
            return urlTest.evaluate(with: textfield.text)
            
        case 6:
            
            let urlRegex = "((http|https)://)?(www[.])?facebook.com/.+"
            let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegex)
            
            return urlTest.evaluate(with: textfield.text)
            
        case 7:
            
            let urlRegex = "((http|https)://)?(www[.])?twitter.com/.+"
            let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegex)
            
            return urlTest.evaluate(with: textfield.text)
            
        case 8:
            
            let urlRegex = "((http|https)://)?(www[.])?instagram.com/.+"
            let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegex)
            
            return urlTest.evaluate(with: textfield.text)
            
        case 9:
            
            let urlRegex = "((http|https)://)?(www[.])?snapchat.com/.+"
            let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegex)
            
            return urlTest.evaluate(with: textfield.text)
            
        case 10:
            return true;
            
        default:
            return true;
        }
        //AA : Test
    }
    
    
    public func trimDeviceToken(_ deviceTokenData:Data)->NSString {
        
        return NSString(format: "%@", deviceTokenData as CVarArg).replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "") as NSString
        
    }
    
    
    //MARK: - Navigation
    
    public func prepareAnimationForPush(NavigationController navigation:UINavigationController,forDirectionRight rightDirection:Bool,orDirectionLeft leftDirection:Bool){
        
        var directionType = ""
        
        if leftDirection == true {
            directionType = kCATransitionFromLeft
        }
        
        if rightDirection == true {
            directionType = kCATransitionFromRight
        }
        
        let transition = CATransition()
        transition.duration = 0.45;
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn;
        transition.subtype = directionType;
        
        navigation.view.layer.add(transition, forKey: kCATransition)
        
        
    }
    
    public func prepareAnimationForPop(NavigationController navigation:UINavigationController,forDirectionRight rightDirection:Bool,orDirectionLeft leftDirection:Bool){
        
        var directionType = ""
        
        if leftDirection == true {
            directionType = kCATransitionFromLeft
        }
        
        if rightDirection == true {
            directionType = kCATransitionFromRight
        }
        
        let transition = CATransition()
        transition.duration = 0.40;
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        transition.type = kCATransitionMoveIn;
        transition.subtype = directionType;
        
        navigation.view.layer.add(transition, forKey: kCATransition)
        
        
    }
    
    public func popToViewController(targetClass:AnyClass){
        
        if let topController = UIApplication.shared.keyWindow!.visibleViewController() {
            
            print(topController)
            
            if !topController.isKind(of: targetClass){
                
                if topController.presentingViewController?.presentedViewController == topController {
                    
                    topController.dismiss(animated: false, completion: {
                        
                        _ = self.popToViewController(targetClass: targetClass)
                        
                    })
                    
                }else {
                    _ = topController.navigationController?.popViewController(animated: false)
                    _ = popToViewController(targetClass: targetClass)
                }
                
            }else {
                print(topController)
                
                topController.viewWillAppear(true)
                
            }
            
        }
    }
    
    public func topMostViewController()->UIViewController{
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
            // topController should now be your topmost view controller
        }
        
        return UIViewController()
    }
    
    //MARK: - Alert Display
    
    public func showAlertWithTitle(_ title:String,message:String,cancelButtonTitle:String){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.cancel) { (action) in
            
            
            
        }
        alertController.addAction(okButton)
        
        let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            self.topMostViewController().present(alertController, animated: true, completion: nil)
            
        }
        
        
    }
    
    public func showAlertWithTitle(_ title:String,message:String,cancelButtonTitle:String,cancelCompletionBlock: @escaping () -> (Void)){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.cancel) { (action) in
            
            cancelCompletionBlock()
            
        }
        alertController.addAction(okButton)
        
        let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            self.topMostViewController().present(alertController, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    public func showAlertWithTitle(_ title:String,message:String,okButtonTitle:String,okCompletionBlock: @escaping () -> (Void),cancelButtonTitle:String,cancelCompletionBlock: @escaping () -> (Void)){
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okButton = UIAlertAction(title: okButtonTitle, style: UIAlertActionStyle.default) { (action) in
            
            okCompletionBlock()
            
        }
        
        let cancelButton = UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.cancel) { (action) in
            
            cancelCompletionBlock()
            
        }
        
        alert.addAction(okButton)
        
        alert.addAction(cancelButton)
        
        let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            self.topMostViewController().present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    //MARK: - Views Helping
    
    public func showMBProgressForListWithBlackColor(forView view:UIView,withTitle title:String){
        
        self.hud = MBProgressHUD()
        self.hud.color = .black
        self.hud.activityIndicatorColor = .white
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.labelText = title
        view.addSubview(self.hud)
        self.hud.isUserInteractionEnabled = true
        self.hud.removeFromSuperViewOnHide = true
        self.hud.show(true)
        
    }
    
    
    public func showMBProgressForListWithClearColor(forView view:UIView,withTitle title:String){
        
        self.hud = MBProgressHUD()
        self.hud.color = .clear
        self.hud.activityIndicatorColor = .black
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.labelText = title
        view.addSubview(self.hud)
        self.hud.isUserInteractionEnabled = true
        self.hud.removeFromSuperViewOnHide = true
        self.hud.show(true)
        
    }
    
    public func showMBProgressForListWithClearColor(forView view:UIView,withTitle title:String,textColor:UIColor){
        
        self.hud = MBProgressHUD()
        self.hud.color = .clear
        self.hud.activityIndicatorColor = .black
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.detailsLabelText = title
        self.hud.detailsLabelColor = textColor
        view.addSubview(self.hud)
        self.hud.isUserInteractionEnabled = true
        self.hud.removeFromSuperViewOnHide = true
        self.hud.show(true)
        
    }
    
    public func showMBProgress(forView view:UIView,withTitle title:String,withBackgroundColor backgroundcolor:UIColor,andActivityIndicatorColor activityIndicatorColor:UIColor,interationEnabled flag:Bool){
        
        self.hud = MBProgressHUD()
        self.hud.color = backgroundcolor
        self.hud.activityIndicatorColor = activityIndicatorColor
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.labelText = title
        view.addSubview(self.hud)
        self.hud.isUserInteractionEnabled = flag
        self.hud.removeFromSuperViewOnHide = true
        self.hud.show(true)
        
    }
    
    public func showMBProgress(forView view:UIView,withTitle title:String,interationEnabled flag:Bool){
        
        self.hud = MBProgressHUD()
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.labelText = title
        view.addSubview(self.hud)
        self.hud.isUserInteractionEnabled = flag
        self.hud.removeFromSuperViewOnHide = true
        self.hud.show(true)
        
    }
    
    
    //    public func showSpinnerView(_ view:UIView){
    //
    //        view.addSubview(spinnerView)
    //        spinnerView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
    //        spinnerView.center = view.center
    //        spinnerView.circleLayer.lineWidth = 3.0
    //        spinnerView.circleLayer.strokeColor = UIColor(red: 3/255, green: 122/255, blue: 244/255, alpha: 1.0).cgColor
    //        spinnerView.beginRefreshing()
    //
    //    }
    //
    //    public func hideSpinnderView(_ view:UIView){
    //
    //        spinnerView.endRefreshing()
    //
    //    }
    
    public func showNoResultLabel(withTitle title:String,forView view:UITableView,withTextColor color:UIColor,withfont font:UIFont?){
        
        //* Display a message when the table is empty
        let noResultLabel = UILabel(frame: CGRect(x:0,y: 0,width: view.bounds.size.width,height: view.bounds.size.height))
        noResultLabel.text = title;
        noResultLabel.textColor = color
        noResultLabel.numberOfLines = 0
        noResultLabel.textAlignment = NSTextAlignment.center
        
        if let foundFont = font {
            
            noResultLabel.font = foundFont
            
        }else {
            
            noResultLabel.font = UIFont.systemFont(ofSize: 20)
            
        }
        
        noResultLabel.sizeToFit()
        
        view.backgroundView = noResultLabel
        
    }
    
    public func hideNoResultLabel(forView view:UITableView){
        
        //* Display a message when the table is empty
        let noResultLabel = UILabel(frame: CGRect(x:0,y: 0,width: view.bounds.size.width,height: view.bounds.size.height))
        noResultLabel.text = "";
        noResultLabel.textColor = UIColor.white
        noResultLabel.numberOfLines = 0
        noResultLabel.textAlignment = NSTextAlignment.center
        noResultLabel.font = UIFont.systemFont(ofSize: 20)
        noResultLabel.sizeToFit()
        
        view.backgroundView = noResultLabel
        
    }
    
    public func makePaddingForTextField(textField:UITextField){
        
        let paddingView = UIView(frame:CGRect(x:0,y: 0,width: 5,height: textField.frame.size.height))
        textField.leftView = paddingView;
        textField.leftViewMode = UITextFieldViewMode.always
        textField.rightView = paddingView
        textField.rightViewMode = UITextFieldViewMode.always
        
    }
    
    public func makeTopRoundedCornersForView(view:UIView) {
        let maskPAth1 = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [ .topLeft,.topRight], cornerRadii: CGSize(width: 10, height: 10)).cgPath
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = view.bounds
        maskLayer1.path = maskPAth1
        view.layer.mask = maskLayer1
        
    }
    
    public func makeRoundedCornersForView(view:UIView ,width:Double ,height:Double) {
        let maskPAth1 = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [ .topLeft,.topRight,.bottomLeft,.bottomRight], cornerRadii: CGSize(width: width, height: height)).cgPath
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = view.bounds
        maskLayer1.path = maskPAth1
        view.layer.mask = maskLayer1
        
    }
    
    public func makeLeftRoundedCornersForView(view:UIView ,width:Double ,height:Double) {
        let maskPAth1 = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [ .topLeft,.bottomLeft], cornerRadii: CGSize(width: width, height: height)).cgPath
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = view.bounds
        maskLayer1.path = maskPAth1
        view.layer.mask = maskLayer1
        
    }
    
    public func makeRightRoundedCornersForView(view:UIView ,width:Double ,height:Double) {
        let maskPAth1 = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [ .topRight,.bottomRight], cornerRadii: CGSize(width: width, height: height)).cgPath
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = view.bounds
        maskLayer1.path = maskPAth1
        view.layer.mask = maskLayer1
        
    }
    
    
    //MARK: PhotoLibrary and Camera Managment
    
    public func requestCameraPermissions(){
        
        
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized{
            // Already Authorized
            
        }else{
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true{
                    
                }else{
                    // access denied
                }
            });
        }
        
        
    }
    
    public func checkCameraRollPermissionAndRequestIfNeeded()->PHAuthorizationStatus {
        
        var rollStatus = PHPhotoLibrary.authorizationStatus()
        
        
        if(rollStatus == PHAuthorizationStatus.authorized) { // authorized
            print("Camera roll Permission authorized");
        } else if(rollStatus == PHAuthorizationStatus.denied){ // denied
            print("Camera roll Permission denied");
        } else if(rollStatus == PHAuthorizationStatus.restricted){ // restricted
            print("Camera roll Permission restricted");
        } else if(rollStatus == PHAuthorizationStatus.notDetermined){ // not determined
            
            
            print("Camera roll Permission not determined");
            
            
            PHPhotoLibrary.requestAuthorization({ (status) in
                
                
                rollStatus = status
                
                switch (rollStatus) {
                case PHAuthorizationStatus.authorized:
                    print("Camera roll Permission authorized");
                    break;
                case PHAuthorizationStatus.restricted:
                    print("Camera roll Permission restricted");
                    break;
                case PHAuthorizationStatus.denied:
                    print("Camera roll Permission denied");
                    break;
                case PHAuthorizationStatus.notDetermined:
                    print("Camera roll Permission not determined");
                    break;
                }
                
                
            })
            
        }
        
        return rollStatus;
    }
    
    
    public func fixOrientationForImage(_ src:UIImage)->UIImage {
        
        if src.imageOrientation == UIImageOrientation.up {
            return src
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: .pi)
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: .pi / 2)
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: .pi / 2)
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: src.cgImage!.bitsPerComponent, bytesPerRow: 0, space: src.cgImage!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    
    public func resizeImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    public func loadJSON(fileName: String, completion: ((_ jsonObject: Any?) -> Void)){
        
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "json")
        {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: .mappedIfSafe)
                
                let jsonDictionary = try JSONSerialization.jsonObject(with: jsonData as Data, options: .mutableContainers)
                
                completion(jsonDictionary)
                
            } catch  {
                
            }
            
        }
        
    }
    
    public func loadText(fileName: String, completion: ((_ txt: String?) -> Void)){
        
        if let filepath = Bundle.main.path(forResource: fileName, ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                
                completion(contents)
                
            } catch {
                // contents could not be loaded
            }
        }
        
    }
    
    
}


    
    

