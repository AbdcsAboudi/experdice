//
//  LoginViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/25/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class LoginViewController: DTViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var signInButton:UIButton!
    @IBOutlet weak var signInWithButton:UIButton!
    @IBOutlet weak var forgotPasswordButton:UIButton!
    @IBOutlet weak var signUpExperts:UILabel!
    @IBOutlet weak var signUpBusiness:UILabel!
    @IBOutlet weak var signInView:UIView!
    @IBOutlet weak var signInWithView:UIView!
    @IBOutlet weak var passwordTextField:DTtextField!
    @IBOutlet weak var emailTextField:DTtextField!
    
    
    
    var userArrayObject = [UserObject]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        needSideBar = false
        setup()
        setupTargetLabel()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //  DTCredentialManager.resetUserCredentials()
    }
    
    
    //MARK: - Helping Methods
    
    func setup(){
        
        signInView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        signInWithView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        
        //AS : Set title for navigationBar
        self.navigationItem.title = "Welcome To Experdice"
        
        emailTextField.textFieldType = .email
        passwordTextField.textFieldType = .password
        
        emailTextField.placeHolderText = "Enter Your Email Address"
        passwordTextField.placeHolderText = "Enter Your Password"
        
        
        forgotPasswordButton.addTarget(self, action: #selector(forgotPasswordSelector), for: .touchUpInside)
        signInWithButton.addTarget(self, action: #selector(loginWithLinkdinSelector), for: .touchUpInside)
        signInButton.addTarget(self, action: #selector(signInSuccessSelector), for: .touchUpInside)
        
        
    }
    
    
    
    func setupTargetLabel(){
        
        //AS: HERE FOR SET THE TAP GESTURE FOR SIGNUP LABEL
        let signUpExpertsTap = UITapGestureRecognizer(target: self, action: #selector(signUpExpertsSelector))
        let signUpBusinessTap = UITapGestureRecognizer(target: self, action: #selector(signUpBusinessSelector))
        
        signUpExperts.isUserInteractionEnabled = true
        signUpExperts.addGestureRecognizer(signUpExpertsTap)
        
        signUpBusiness.isUserInteractionEnabled = true
        signUpBusiness.addGestureRecognizer(signUpBusinessTap)
        
        
    }
    
    
    
    func checkTextFiledValidation()->Bool{
        
        if let emailTextField = emailTextField.text {
            
            if emailTextField.isEmpty {
                let alertMessage = "Please Fill E-Mail"
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                return false
                
            }else{
                if !DTValidationManager.isValidEmail(text: emailTextField) {
                    let alertMessage = "Please Enter Valid E-Mail"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
                
            }
            
        }
        
        if let passwordTextField = passwordTextField.text {
            
            if passwordTextField.isEmpty {
                
                let alertMessage = "Please Fill Password"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                
                return false
            }else{
                
                if !DTValidationManager.isValidPassword(text: passwordTextField) {
                    let alertMessage = "Please Enter Valid Password"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
            }
            
        }
        return true
    }
    
    //MARK: - Actions Methods
    
    func signUpExpertsSelector(){
        
        self.performSegue(withIdentifier: "signUpExpertSegue", sender: self)
        
    }
    func signUpBusinessSelector(){
        self.performSegue(withIdentifier: "signUpBusinessSegue", sender: self)
        
    }
    func signInSuccessSelector(){
        
        signInAPI()
        
    }
    
    
    func forgotPasswordSelector(){
        
        let alertMessage1 = "if you forgot your password, we can send you an email to reset it. Enter the email you signed up with."
        let alertMessage2 = "Check Your Email For The Link To Reset Your Password"
        
        let alertView1 = EXAlertViewController(title: "Password Password", twoButton: false, alertMessage:alertMessage1 ,buttonTitle: "Send Email",zeroHighView:true,centerTextAlertMessage:false,speratorViewEnable:true)
        
        let alertView2 = EXAlertViewController(title: "Password Recovery", twoButton: false, alertMessage:alertMessage2 , buttonTitle: "Done",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
        
        //AS : Present the alertView 1 with Email Address TextFiled
        self.present(alertView1, animated: true, completion: nil)
        
        alertView1.singalOkeyButtonClicked = {
            
            if #available(iOS 10.0, *) {
                Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false, block: { (timer) in
                    
                    self.present(alertView2, animated: true, completion: nil)
                    
                })
            } else {
                // Fallback on earlier versions
            }
            
        }
        //AS : After the email is send a new view is present to set the new password
        alertView2.singalOkeyButtonClicked = {
            
            self.performSegue(withIdentifier: "ForgotPasswordViewController", sender: self)
        }
        
    }
    
    func loginWithLinkdinSelector(){
        
        DTLinkedInManger.sharedInstance.loginWithLinkdein(showGoToAppStoreDialog: true) { (result,isSuccess) in
            
            if isSuccess!{
                self.performSegue(withIdentifier: "signInSuccess", sender: self)
                
            }
        }
    }
    //MARK: - API Calls to check userName and password for signIn ⚡️🥓🥓🍔⚡️
    
    
    func signInAPI(){
        
        //FOR SIGN IN AUTOMATIC
        //        self.performSegue(withIdentifier: "signInSuccess", sender: self)
        
        var parameters = [String: Any]()
        
        if let emailTextField = emailTextField.text {
            parameters["EmailAddress"] = emailTextField
        }
        
        if let passwordTextField = passwordTextField.text {
            parameters["Password"] = passwordTextField
        }
        
        //if all field empty it will return
        if !checkTextFiledValidation(){return}
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .signIn, requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [String:Any]{
                    
                    let user = DTParser.parseUserObjectFromResponse(response: response) as UserObject
                    
                    DTCredentialManager.credentialSharedInstance.user = user
                    DTCredentialManager.saveUserCredentialsForUsername(username: user.email ?? "", password: "", withAuthentication: .email)
                    DTCredentialManager.userID = user.userID
                    
                    if UserObject.getUserType() == .expert {
                        
                        //AS:  LOGIN FROM HERE
                        viewController.performSegue(withIdentifier: "signInSuccess", sender: self)
                        
                    }else{
                        
                        viewController.performSegue(withIdentifier: "signInSuccess", sender: self)
                    }
                    
                }
                
            }else{
                
                let alert = APIClient.sharedInstance.makeAlertViewForResponseDictionary(response: apiResponse)
                viewController.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func unwindToLoginViewController(segue:UIStoryboardSegue) { }
    
    
}


