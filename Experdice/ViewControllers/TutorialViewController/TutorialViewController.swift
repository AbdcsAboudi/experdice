//
//  TutorialViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/20/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class TutorialViewController: DTViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    deinit {
        NSLog("there leak!!")
    }

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl:UIPageControl!
    @IBOutlet weak var skipButton: UIButton!
    
    var currentPage = 0
    var tutorialsArray = [TutorialObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        needSideBar = false
        setupTutorials()
        setupCollectionView()
        setupButton()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    //MARK: - Setup Methods
    
    func setupCollectionView(){
                
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = UIEdgeInsets.zero
        
        let width = collectionView.frame.size.width
        let height = collectionView.frame.size.height
        
        flowLayout.itemSize = CGSize(width: width, height: height)
        collectionView.isScrollEnabled = true
        self.collectionView.bounces = true
        self.collectionView.isPagingEnabled = true
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.contentInset = UIEdgeInsets.zero
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(UINib(nibName:"TutorialImageCell",bundle:nil), forCellWithReuseIdentifier: "TutorialImageCell")
        
        self.pageControl.numberOfPages = tutorialsArray.count
        self.pageControl.addTarget(self, action: #selector(TutorialViewController.pageControlChanged), for: .valueChanged)
    }
    
    func setupTutorials(){
        
        //AS : FILL ARRAY WITH TUTORIAL OBJECT
        tutorialsArray.append(TutorialObject(image: #imageLiteral(resourceName: "Page 1"), text: "Experdice is all about connecting experts to businesses in MENA",subTitle:"Whether it’s for a simple business plan review or a team building event for 100 employees, Experdice has got you covered"))
        
        tutorialsArray.append(TutorialObject(image: #imageLiteral(resourceName: "Page 2"), text: "Get your project set up in minutes ",subTitle:" Describe your need and required expertise in a couple of steps to finally launch a new project to reach thousands of experts"))
        
        tutorialsArray.append(TutorialObject(image: #imageLiteral(resourceName: "Page 3"), text: "Connect with the right experts",subTitle:"Our system instantly connects you to experts who meet your requirements and you have the freedom to pick and choose from the biggest pool of verified experts in the MENA region"))
        
        tutorialsArray.append(TutorialObject(image: #imageLiteral(resourceName: "Page 4"), text: "You’re in control",subTitle:"From step one, you set the budget, the terms, pick the right expert and control every aspect of the project. This all happens within a centralised and secure environment, which ensures the highest level of service"))
    }
    
    
    func setupButton(){
        
        
        skipButton.addTarget(self, action: #selector(skipButtonSelector), for: .touchUpInside)
        
        //Style
        skipButton.tintColor = DTHelper.sharedInstance.colorWithHexString("253273")
        skipButton.layer.cornerRadius = 20
      
    }
    
    //MARK: -  UICollectionView Delegate and Datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
    return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tutorialsArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       // let cell = TutorialImageCell.populateTutorialCell(tutorialObject: tutorialsArray[indexPath.row], collectionView: collectionView, indexPath: indexPath as NSIndexPath)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TutorialImageCell", for: indexPath as IndexPath) as? TutorialImageCell
        
        let tutorialObject = tutorialsArray[indexPath.row]
        
        cell?.tutorialImageView.image = tutorialObject.image
        cell?.headerTextLabel.text = tutorialObject.text
        cell?.subtitleTextLabel.text = tutorialObject.subTitle
        
        //return cell!
        
        return cell!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageWidth = self.collectionView.frame.size.width as CGFloat
        self.pageControl.currentPage = NSInteger(self.collectionView.contentOffset.x/pageWidth)
        
        //AS : FOR SET THE TITLE OF "SKIP - FINISH" BUTTON
        if isLastSlide() == true {
            skipButton.setTitle("Finish", for: .normal)
            skipButton.addTarget(self, action: #selector(finishButtonSelctor), for: .touchUpInside)
        }else{
            skipButton.setTitle("Skip", for: .normal)
            skipButton.addTarget(self, action: #selector(skipButtonSelector), for: .touchUpInside)
        }
        
    }
    
    func pageControlChanged(){
        
        let scrollToPage = pageControl.currentPage
        collectionView.scrollToItem(at: IndexPath(row: scrollToPage, section: 0), at: .centeredHorizontally, animated: false)
        
    }
    
    // MARK: - Selectors And Actions
    
    func skipButtonSelector(){
        
        //AS: TO CHACK IF ITS NOT LAST PAGE
//        if  !isLastSlide() {
//            let scrollToPage = pageControl.currentPage
//            collectionView.scrollToItem(at:IndexPath(item: scrollToPage + 1 , section: 0) , at: .left, animated: true)
//        }else{
//             skipButton.addTarget(self, action: #selector(finishButtonSelctor), for: .touchUpInside)
//        }
        
        finishButtonSelctor()
        
    }
    
    func finishButtonSelctor(){
        
        self.performSegue(withIdentifier: "LoginViewController", sender: self)
        
    }
    
    func isLastSlide() -> Bool {
    
        let currentPage = pageControl.currentPage
        
      //AS : TO CHECK IF IS REACH TO LAST PAGE
        if currentPage == (self.pageControl.numberOfPages - 1){
            
            return true
        }
        
        return false
    }

    
    struct TutorialObject {
        
        var image:UIImage
        
        var text:String? , subTitle:String?
        
        init(image: UIImage!, text: String!, subTitle: String!) {
            
            self.image = image
            self.text = text
            self.subTitle = subTitle
        }
        
        
    }
    
    
  


}
