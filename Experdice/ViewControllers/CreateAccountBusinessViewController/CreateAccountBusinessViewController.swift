//
//  CreateAccountExpertViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/30/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class CreateAccountBusinessViewController: DTViewController,UIScrollViewDelegate,ListSelectViewControllerDelegate {
  
    var businessName = [String]()
    var businessID = [Int]()
    
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var joinNowButton:UIButton!
    @IBOutlet weak var connectWithButton:UIButton!
    @IBOutlet weak var signUpExpertLabel:UILabel!
    @IBOutlet weak var termsAndCondtion:UIButton!
    @IBOutlet weak var joinNowView:UIView!
    @IBOutlet weak var connectWithView:UIView!
    @IBOutlet weak var businessNameTextField:DTtextField!
    @IBOutlet weak var businessTypeTextField:DTtextField!
    @IBOutlet weak var emailAddressTextField:DTtextField!
    @IBOutlet weak var passwordTextField:DTtextField!
    @IBOutlet weak var businessTypeButton:UIButton!
    
    var selectedBusinessName:String?
    var selectedBusinessID:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //AS : Set title and back buuton for navigationBar
        title = "Create Account"
        
        setupTargetLabel()
        setupTextField()
        callBusinessTypeAPI()
        needSideBar = false
       
        setup()
      
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Helping Methods
    
    func setup(){
        
        //AS : Set title for navigationBar
        
        //Style
        joinNowView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        connectWithView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        joinNowButton.addTarget(self, action: #selector(joinNowSelector), for: .touchUpInside)
        
    }
    
    func setupTextField(){
        
        businessNameTextField.textFieldType = .string
        businessTypeButton.addTarget(self, action: #selector(openListViewController), for: .touchUpInside)
        emailAddressTextField.textFieldType = .email
        passwordTextField.textFieldType = .password
        
    }
    
    func setupBusinessName(){
        
        var index = 0
        for businessType in businessName {
            
            businessTypeTextField.domainOptions.append(DTDomainObject(id: businessID[index], display:businessType))
            
            index += 1
        }
    }
   
    func setupTargetLabel(){
        
        //A|S : Setup Target
        let signUpBusinessTap = UITapGestureRecognizer(target: self, action: #selector(signUpExpertSelector))
        
        signUpExpertLabel.isUserInteractionEnabled = true
        signUpExpertLabel.addGestureRecognizer(signUpBusinessTap)
    
    }
    
    func checkTextFiledValidation()->Bool{
        
        if let businessNameTextField = businessNameTextField.text {
            
            if businessNameTextField.isEmpty {
                
                let alertMessage = "Please Fill Business Name"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                
                return false
            }else{
                
                if !DTValidationManager.isVaildBusinessName(text:businessNameTextField) {
                    let alertMessage = "Please Enter Valid Business Name"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
            }
            
        }
        
        
        if let businessTypeTextField = businessTypeTextField.text {
            
            if businessTypeTextField.isEmpty {
                
                let alertMessage = "Please Fill Business Type"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                
                return false
            }else{
                
                if !DTValidationManager.isVaildBusinessType(text:businessTypeTextField) {
                    let alertMessage = "Please Enter Valid Business Type"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
            }
            
        }
        
        if let emailTextField = emailAddressTextField.text {
            
            if emailTextField.isEmpty {
                let alertMessage = "Please Fill E-Mail"
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                return false
                
            }else{
                if !DTValidationManager.isValidEmail(text: emailTextField) {
                    let alertMessage = "Please Enter Valid E-Mail"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
                
            }
            
        }
        
        if let passwordTextField = passwordTextField.text {
            
            if passwordTextField.isEmpty {
                
                let alertMessage = "Please Fill Password"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                
                return false
            }else{
                
                if !DTValidationManager.isValidPassword(text: passwordTextField) {
                    let alertMessage = "Please Enter Valid Password"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
            }
            
        }
        
        return true
    }
    
    
    //MARK: - Actions Methods
    
    func openListViewController(){
        
        let listViewController = ListSelectViewController(titlesArray: businessName, idsArray: businessID, withSearch: true, withRatingCell: false, withOtherCell: false)
        listViewController.delegate = self
        listViewController.navigationController?.title = "Business Type"
        self.navigationController?.pushViewController(listViewController, animated: true)
    }
    
    func signUpExpertSelector(){
        
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        self.performSegue(withIdentifier: "signUpExpertSegueNew", sender: self)
     
    }
    
    
    func joinNowSelector(){
        
        callEmailExistAPI()
        
    }
    
    //MARK: - List View Delegate
    
    func listDidSelect(listViewController: ListSelectViewController, selectedIndex: Int, selectedTitle: String, selectedId: Int?) {
        
        businessTypeTextField.text = selectedTitle
        selectedBusinessName = selectedTitle
        selectedBusinessID = selectedId
    }

     //MARK: - API Calls
    
    func callEmailExistAPI(){
       
        if !checkTextFiledValidation(){return}
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .emailExist , requestParams: ["email": emailAddressTextField.text])
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                let alertMessage = "Email Already Exist"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                viewController.present(alertView, animated: true, completion: nil)
                
            }else{
               self?.signUpBusinessAPI()
            }
        }
        
    }
    
    func alertView(alertMessag:String){
        
        
        let alertMessage = alertMessag
        
        
        let alertView = EXAlertViewController(title: "Warning", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
        
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    
    func callBusinessTypeAPI(){
       
        if let path = Bundle.main.path(forResource: "BusinessTypes", ofType: "json"){
            do {
                do{
                    let data = try Data(contentsOf: URL(fileURLWithPath: path))
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [Any]
                    
                    let businessArray = DTParser.parseBusinessTypeOjectArray(from: jsonResult)
                    
                    for business in businessArray  {
                        
                        if let businessName = business.name {
                            
                            self.businessName.append(businessName)
                            
                        }
                        
                        if let businessID = business.id {
                            
                            self.businessID.append(businessID)
                        }
                        
                    }
                    self.setupBusinessName()
                    
                } catch {}
            } catch {}
        }
        
//
//            MBProgressHUD.showAdded(to: self.view, animated: true)
//
//            let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .businessTypes, requestParams: nil)
//
//            DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
//
//
//            }) { [weak self](apiResponse,success) -> (Void) in
//
//                guard let viewController = self else{
//                    return
//                }
//
//                MBProgressHUD.hide(for: viewController.view, animated: true)
//
//                if success {
//
//                    if let response = apiResponse as? [Any] {
//
  //                      let businessArray = DTParser.parseBusinessTypeOjectArray(from: response)
//
//                        for business in businessArray  {
//
//                            if let businessName = business.name {
//
//                                self?.businessName.append(businessName)
//
//                            }
//
//                            if let businessID = business.id {
//
//                                self?.businessID.append(businessID)
//                            }
//
//                        }
//                        self?.setupBusinessName()
//                    }
//
//                }
//            }
    }
    
    
    
    func signUpBusinessAPI(){
        
        var parameters = [String: Any]()
        
        if let businessName = businessNameTextField.text {
            
            parameters["CompanyName"] = businessName
            
        }
        
        
        parameters["BusinessIndustryId"] = selectedBusinessID
        
        
        
        if let emailAddress = emailAddressTextField.text {
            
            parameters["EmailAddress"] = emailAddress
        }
        
        if let password = passwordTextField.text {
            
            parameters["Password"] = password
        }
        

        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .signUpBuisiness, requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                viewController.navigationController?.popToRootViewController(animated: true)
                
            }
            
        }
    }

}
