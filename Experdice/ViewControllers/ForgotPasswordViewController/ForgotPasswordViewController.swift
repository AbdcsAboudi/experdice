//
//  ForgotPasswordViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/31/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: DTViewController {

    @IBOutlet weak var enterPassword:UITextField!
    @IBOutlet weak var retypePassword:UITextField!
    @IBOutlet weak var saveButton:UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        needSideBar = false
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
    //MARK: - Helping Methods
    
    func setup(){
        
        //AS : Set title for navigationBar
        self.navigationItem.title = "Reset Password"
        
        //Style
        saveButton.makeCorneredViewWith(corenerRadius: 20, and: 0)

    }
    

}
