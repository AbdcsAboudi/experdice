//
//  SWAlertViewController.swift
//  7sWorld
//
//  Created by DTAboudi on 3/12/17.
//  Copyright © 2017 Saad Basha. All rights reserved.
//

import UIKit
import Spring

protocol EXAlertViewControllerDelegate: NSObjectProtocol {
    func alertDidConfirm(alertViewController: EXAlertViewController)
    func alertDidCancel(alertViewController: EXAlertViewController)
}


class EXAlertViewController: DTViewController {

    @IBOutlet weak var firstButton: UIButton!
    weak var delegate: EXAlertViewControllerDelegate?

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var alertTitleLabe: UILabel!
    @IBOutlet weak var okayButton: UIButton!
    @IBOutlet weak var singleOkayButton:UIButton!
    @IBOutlet var alertMessageLabel: UILabel!
    @IBOutlet var yesButtonView: UIView!
    @IBOutlet var noButtonView: UIView!
    @IBOutlet var oneButtonView:UIView!
    @IBOutlet var towButtonView:UIView!
    @IBOutlet var speratorView:UIView!
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var hightTextFiled:NSLayoutConstraint!
    
    var cancelClicked : (() -> Void)?
    var confirmClicked : (() -> Void)?
    var cancelButtonClicked: (()-> Void)?
    var okeyButtonClicked:(()-> Void)?
    var singalOkeyButtonClicked:(()->Void)?
    var zeroHighView:Bool?
    var alertMessage = ""
    var alertTitle = ""
    var centerTextAlertMessage:Bool?
    var cancel: String!
    var okay: String!
    var buttonTitle: String!
    var twoButton:Bool?
    var okayButtonBackgroundColor: UIColor!
    var speratorViewEnable:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //AA : To setup alertView
        self.setupAlertView()
        
        
    }
   
    
    
    //MARK: - New alert view used in EXPERDICE App
    
    init(title: String!, twoButton: Bool!,alertMessage:String!,buttonTitle:String!,zeroHighView:Bool!,centerTextAlertMessage:Bool!,speratorViewEnable:Bool!) {
        
        //AS: NEW Alert view
        
        super.init(nibName: "EXAlertView", bundle: nil)
        self.title = title
        self.twoButton = twoButton
        self.alertMessage = alertMessage
        self.buttonTitle = buttonTitle
        self.zeroHighView = zeroHighView
        self.speratorViewEnable = speratorViewEnable
        self.centerTextAlertMessage = centerTextAlertMessage
        
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .custom
    
    }
    
    
    
    
    
    
    //MARK: - NOT USED
    
    
    init(title: String!, firstButton: String!, okay: String!, cancel: String!) {
        
        super.init(nibName: "EXAlertView", bundle: nil)
        
        
        self.title = title
        self.okay = okay
        self.cancel = cancel
        self.buttonTitle = firstButton
        
    }
    
    init(title: String!, okay: String!, cancel: String!, vertical: Bool) {
        
        if vertical {
            super.init(nibName: "SWAlertJoinViewController", bundle: nil)
        }else{
            super.init(nibName: "SWAlertViewControllerWithCancel", bundle: nil)
        }
        
        self.title = title
        self.okay = okay
        self.cancel = cancel
        
        self.modalTransitionStyle = .crossDissolve
        
    }

    
    init(title: String!, okay:String!) {
        
        super.init(nibName: "SWAlertViewController", bundle: nil)
        
        self.title = title
        self.okay = okay
        
        self.modalTransitionStyle = .crossDissolve
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    // MARK: - Helping Methods
    
    func setupAlertView() {
        
        
        //AA : Add gesture to this view to dismiss it
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
       
        self.view.addGestureRecognizer(tap)

        //AA : To set alert meassge text
        self.alertMessageLabel.text = alertMessage
        
        //AA : To cornarize the container view and button 
        containerView?.makeCorneredViewWith(corenerRadius: 5, and: 0)
        cancelButton?.makeCorneredViewWith(corenerRadius: 20, and: 0)
        okayButton?.makeCorneredViewWith(corenerRadius: 20, and: 0)
        singleOkayButton?.makeCorneredViewWith(corenerRadius: 20, and: 0)
        
        //AS : To check if the flag of hight is true to set it to the UITextFiled
        if zeroHighView! {
             hightTextFiled.constant = 40
            updateViewConstraints()
        }else{
           hightTextFiled.constant = 0
            updateViewConstraints()
        }
        
        //AS : To check if the flag of towButton is true to add in the view one button or two
        if twoButton! {
            okayButton.setTitle(buttonTitle, for: .normal)
        }else{
            singleOkayButton.setTitle(buttonTitle, for: .normal)
        }
        
        //AS : To check if the flag of center Text is true to centerd the text in the UITextLabel
        if centerTextAlertMessage! {
            
            alertMessageLabel.textAlignment = .center
        }else{
            
            alertMessageLabel.textAlignment = .left
        }
        
       //AS : to show one or two view button based on the send flag
        if twoButton! {
            
            setShowTwoButton()
        
        }
        
        alertTitleLabe.text = title
        
        speratorView.isHidden = speratorViewEnable!
        
        
    }

    // MARK: - Actions

    func dismissView() {
        
        self.perform(#selector(self.dismissThisView), with: nil, afterDelay: 0.2)
    }

    @IBAction func yesButtonAction(_ sender: Any) {

         delegate?.alertDidConfirm(alertViewController: self)
        
        defer {
            dismiss(animated: false, completion: nil)
        }
        let onConfirm = self.confirmClicked
        // deliberately set to nil just in case there is a self reference
        self.confirmClicked = nil
        guard let block = onConfirm else { return }
        block()
        
    }
    @IBAction func signalYesButtonAction(_ sender: Any) {

        delegate?.alertDidConfirm(alertViewController: self)
        
        defer {
          dismiss(animated: false, completion: nil)
        }
        let onConfirm = self.singalOkeyButtonClicked
        // deliberately set to nil just in case there is a self reference
        self.singalOkeyButtonClicked = nil
        guard let block = onConfirm else { return }
        block()
        
    }
    
    @IBAction func noButtonAction(_ sender: Any) {
        
        delegate?.alertDidCancel(alertViewController: self)
        
        defer {
            dismiss(animated: false, completion: nil)
        }
        
        let onCancel = self.cancelClicked
        // deliberately set to nil just in case there is a self reference
        self.cancelClicked = nil
        guard let block = onCancel else { return }
        block()
}
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
         delegate?.alertDidCancel(alertViewController: self)

        defer {
            dismiss(animated: false, completion: nil)
        }
        
        self.okeyButtonClicked?()
    }

    func dismissThisView() {
        
        if !twoButton! {
            oneButtonView.isHidden = false
            towButtonView.isHidden = true
        }
        self.dismiss(animated: true, completion: nil)
       
        
    }
    
    func setShowTwoButton(){
        
        towButtonView.isHidden = !twoButton!
        oneButtonView.isHidden = true
        
    }
    
    
    

}


