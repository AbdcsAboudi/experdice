//
//  ContactUsViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class ContactUsViewController: DTViewController {

    @IBOutlet weak var contactusLabel: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        needSideBar = true
        setupContactusViewController()
        
        //AS: for hidding the side menu
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        submitButton.addTarget(self, action: #selector(submitButtonSelector), for: .touchUpInside)
    }

    
    func setupContactusViewController(){
        descriptionView.layer.borderWidth = 2
        descriptionView.layer.borderColor = UIColor.gray.cgColor
        descriptionView.layer.cornerRadius = 10
        descriptionView.clipsToBounds = true
        buttonView.makeCorneredViewWith(corenerRadius: buttonView.frame.height/2, and: 0)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Selector and Action
    
    func submitButtonSelector(){
        if !descriptionTextView.text.isEmpty{
            callGetProjectAPI(description: descriptionTextView.text)
        }else{
            let alertMessage = "Please Fill Description"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    //MARK: - API Call
    
    func callGetProjectAPI(description:String){
        
        var parameters = [String:Any]()
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        parameters["UserId"] = user?.userID
        parameters["Description"] = description
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .contactUs, requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any]{
                    
                }
            }
        }
    }
    


}
