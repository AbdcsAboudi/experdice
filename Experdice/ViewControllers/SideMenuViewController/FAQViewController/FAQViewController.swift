//
//  FAQViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class FAQViewController: DTViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var faqTableView: UITableView!
    
    var arrayOfFaq = [FAQObject]()
    var refferenceForSection = 0
    var fromSiriVc = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //AA : To setup FAQ TableView
        self.setupFAQTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Helping Methods
    
    func setupFAQTableView() {
        
        title = "FAQ"
        
        //AA : To remove the extra space above the tableView
        self.automaticallyAdjustsScrollViewInsets = false
        
        //AA : To add FAQ into faq array
        //self.setupFAQArray()
        
        self.faqTableView.delegate = self
        self.faqTableView.dataSource = self
        
        //AA : remove separtors and empty space under tableview
        self.faqTableView.tableFooterView = UIView()
        self.faqTableView.separatorStyle = .none
        
        self.faqTableView
            .register(UINib(nibName: "FAQTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "FAQTableViewCell")
        self.faqTableView.register(UINib.init(nibName: "FAQSectionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "TableHeader")
        
    }
    
    func setupFAQArray() {
        
        arrayOfFaq = [FAQObject(question: "why do i have to sign up via Facebook or Email ?", answer: "In the event that you lose or change your phone, your points and rewards will always be saved to your account."),
                      FAQObject(question: "I forgot my password - How can i reset it ?", answer: "Click on the i forgot my password link to request a reset code."),
                      FAQObject(question: "What is Let's Save ?", answer: "Expredice is the region's leading digital loyalty platform. Earn points and redeem rewards from your favorite venues directly from your smart phone."),
                      FAQObject(question: "What other services are you compatible with ?", answer: "Share your experience on social media(Facebook, Twitter) or via e-mail, text message and Whatsapp. Share the love and gift your friends their first free loyalty point at your favorite venues."),
                      FAQObject(question: "I have a technical problem - Who can help me?", answer: "Email us at info@letssave.net"),
                      FAQObject(question: "How can i see list of businesses ?", answer: "All of our partners are listed under discover. use the navigation feature in the map to get exact directions to a venue. Your favorite venue isn't a Let's Save partner yet?!Recommend them through our main menu."),
                      FAQObject(question: "I have a new phone - Did i lose all my points ?", answer: "Your points and rewards are always saved in your account. Sign into your sccount to access all your reward cards."),
                      FAQObject(question: "Can i redeem more than i card at once ?", answer: "Each of our partners has a different policy, so it depends on the venue.")]
        
    }
    
    func dismissThisView() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK: - TableViewDataSource
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FAQTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FAQTableViewCell") as! FAQTableViewCell
        
        cell.answerLabel?.text = arrayOfFaq[refferenceForSection].answer
        cell.answerLabel?.numberOfLines = 0
        cell.answerLabel?.sizeToFit()
        cell.selectionStyle = .none
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arrayOfFaq.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == refferenceForSection {
            
            if arrayOfFaq[section].isCollapsed {
                
                return 1
                
            }else {
                
                return 0
                
            }
            
        }else {
            
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header : FAQSectionHeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TableHeader") as! FAQSectionHeaderView
        
        header.cellExpandButton.addTarget(self, action: #selector(exapndCell(_:)), for: UIControlEvents.touchUpInside)
        header.cellExpandButton.tag = section
        header.titleLabel.text = arrayOfFaq[section].question
        header.titleLabel.numberOfLines = 0
        header.titleLabel.sizeToFit()
        return header
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 200
    }
    
    
    @objc private func exapndCell(_ button:UIButton){
        
        
        for object in arrayOfFaq {
            
            object.isCollapsed = false
            
        }
        
        refferenceForSection = button.tag
        
        if arrayOfFaq[button.tag].isCollapsed {
            
            arrayOfFaq[button.tag].isCollapsed = false
            
        }else {
            arrayOfFaq[button.tag].isCollapsed = true
            
        }
        
        
        DispatchQueue.main.async {
            self.faqTableView.reloadData()
        }
        
    }
    
    @objc private func collapseCell(_ button:UIButton){
        
        for object in arrayOfFaq {
            object.isCollapsed = true
        }
        
        refferenceForSection = button.tag
        
        arrayOfFaq[button.tag].isCollapsed = true
        
        
        DispatchQueue.main.async {
            self.faqTableView.reloadData()
        }
        
    }
    
}
