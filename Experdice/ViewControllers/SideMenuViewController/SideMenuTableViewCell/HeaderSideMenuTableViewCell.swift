//
//  SideMenuTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class HeaderSideMenuTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var specializationName:UILabel!
   

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
    //MARK :- Papulate Table View
    
    class func populateHeaderSideMenuCell(sideMenuObject : SideMenuObject, tableView : UITableView, indexPath: NSIndexPath) -> HeaderSideMenuTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderSideMenuTableViewCell", for: indexPath as IndexPath) as! HeaderSideMenuTableViewCell
        
        let user = DTCredentialManager.credentialSharedInstance.user as! UserObject
        
        if let imagePath = user.imageURL {
            let imageURL = "http://numidiajo.com/Experdice/API/images/\(imagePath)"
            
            cell.profileImage.sd_setImage(with: URL(string:imageURL), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
                
                if error != nil {
                    
                    print(error?.localizedDescription ?? "error found")
                    
                } else {
                    
                  // cell.profileImage?.image = image?.circleMaskBlue
                    cell.profileImage.layer.cornerRadius = 5
                    cell.profileImage?.contentMode = .scaleAspectFill
                    cell.profileImage?.clipsToBounds = true
                    
                }
            })
            
        }
        
        cell.userName.text = sideMenuObject.userName
        cell.specializationName.text = sideMenuObject.specializationName
        cell.selectionStyle = .none
        
        return cell
        
    }
    
}
