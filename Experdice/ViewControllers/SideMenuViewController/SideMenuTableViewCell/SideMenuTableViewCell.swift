//
//  SideMenuTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var bagdeImage:UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    //MARK :- Papulate Table View
    
    class func populateSideMenuCell(sideMenuObject : SideMenuObject, tableView : UITableView, indexPath: NSIndexPath) -> SideMenuTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath as IndexPath) as? SideMenuTableViewCell
        
        cell?.titleLabel.text = sideMenuObject.title
    
        if sideMenuObject.badgeLabel! {
           //TODO: HERE WE MUST CONVERT IMAGE TO NUMBER OF BADGE OR NOTIFICATIONS
            print("test")
        }
        cell?.selectionStyle = .none
        
        return cell!
        
        
    }

    
    
    
    
}
