//
//  AddSpeakersProjectViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class AddSpeakersProjectViewController: DTViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var addSpeakersProjectTableView: UITableView!
    
    var sectionsTitlesArray = [String]()
    var section0TextFieldsObjectsArray = [FormOptionObject]()
    var section1TextFieldsObjectsArray = [FormOptionObject]()
    var section2TextFieldsObjectsArray = [FormOptionObject]()
    var section3TextFieldsObjectsArray = [FormOptionObject]()
    var section4TextFieldsObjectsArray = [FormOptionObject]()
    var sectionsDictionary = [String : [FormOptionObject]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillSectionsArray()
        setupAddProjectTableView()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.needSideBar = false
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Helping Methods
    
    func setupAddProjectTableView() {
        
        addSpeakersProjectTableView.delegate = self
        addSpeakersProjectTableView.dataSource = self
        addSpeakersProjectTableView.estimatedRowHeight = 45
        addSpeakersProjectTableView.rowHeight = UITableViewAutomaticDimension
        
        addSpeakersProjectTableView.register(UINib(nibName: "ProjectSectionTitleTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSectionTitleTableViewCell")
        addSpeakersProjectTableView.register(UINib(nibName: "ProjectSharedTextFieldTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSharedTextFieldTableViewCell")
        addSpeakersProjectTableView.register(UINib(nibName: "ProjectSharedTexViewTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSharedTexViewTableViewCell")
        addSpeakersProjectTableView.register(UINib(nibName: "ProjectDropDownTextFieldTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectDropDownTextFieldTableViewCell")
        addSpeakersProjectTableView.register(UINib(nibName: "ProjectDropSownMultipleTextFieldsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectDropSownMultipleTextFieldsTableViewCell")
        addSpeakersProjectTableView.register(UINib(nibName: "ProjectOptionTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectOptionTableViewCell")
        addSpeakersProjectTableView.register(UINib(nibName: "CreateProjectTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "CreateProjectTableViewCell")
        
    }
    
    func fillSectionsArray() {
        
        sectionsTitlesArray.append(contentsOf: ["Main Details","Project Goals","Location & Time Frame","Budget","Candidate",""])
        section0TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Project Title", textFieldPlaceholder: "Enter Project Title", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:""),
            
            FormOptionObject(textFieldTitle: "Project Category", textFieldPlaceholder: "Choose Project Category", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")])])
        
        section1TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Main Objectives", textFieldPlaceholder: "Write the main objectives", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:""),
            FormOptionObject(textFieldTitle: "Description", textFieldPlaceholder: "", formOptionType: FormOptionType.textView, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:""),
            FormOptionObject(textFieldTitle: "Notes", textFieldPlaceholder: "", formOptionType: FormOptionType.textView, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:"")])
        
        section2TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Location", textFieldPlaceholder: "", formOptionType: FormOptionType.multipleDropDownsTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Event Date", textFieldPlaceholder: "Enter Event Date", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.birthDay, rowTag: 0,data:"",secondData:""),
            
            FormOptionObject(textFieldTitle: "Training Duration", textFieldPlaceholder: "Enter Training Duration", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:"")])
        
        section3TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Hourly / Daily Rate", textFieldPlaceholder: "Set Rate", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            FormOptionObject(textFieldTitle: "Amount", textFieldPlaceholder: "Set Amount", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")])])
        
        section4TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Field Of Experience", textFieldPlaceholder: "Select Field Of Experience", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Years Of Experience", textFieldPlaceholder: "Select Years Of Experience", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Education", textFieldPlaceholder: "Choose Education", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Language", textFieldPlaceholder: "FormOptionObject Language", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Preferred Coach Gender", textFieldPlaceholder: "Select Coach Gender", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Please describe your ideal Candidate for this project", textFieldPlaceholder: "", formOptionType: FormOptionType.textView, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:"")])
        
        sectionsDictionary["section0"] = section0TextFieldsObjectsArray
        sectionsDictionary["section1"] = section1TextFieldsObjectsArray
        sectionsDictionary["section2"] = section2TextFieldsObjectsArray
        sectionsDictionary["section3"] = section3TextFieldsObjectsArray
        sectionsDictionary["section4"] = section4TextFieldsObjectsArray
        
    }
    
    //MARK:- TableView Delegate && DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionsTitlesArray.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 || section == 3{
            
            return 3
            
        }else if section == 1 || section == 2{
            
            return 4
            
        }else if section == 4{
            
            return 7
            
        }else if section == 5{
            
            return 1
            
        }else{
            
            return 0
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionKeyString = "section\(indexPath.section)"

        if indexPath.row == 0 {
            
            if indexPath.section == 5 {
                
                let cell = CreateProjectTableViewCell.populateCreateProjectTableViewCell(indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
            }else{
                
                let cell = ProjectSectionTitleTableViewCell.populateProjectSectionTitleTableViewCell(projectTitleString: sectionsTitlesArray[indexPath.section], indexPath: indexPath as NSIndexPath, tableView: tableView)
                return cell
                
            }
            
        }else{
            
            let sectionFormOptionsArray = sectionsDictionary[sectionKeyString]
            let formObject = sectionFormOptionsArray?[indexPath.row - 1]
            
            if formObject?.formOptionType == FormOptionType.regularTextField {
                
                let cell = ProjectSharedTextFieldTableViewCell.populateProjectSharedTextFieldTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
            }else if formObject?.formOptionType == FormOptionType.dropDownTextField {
                
                let cell = ProjectDropDownTextFieldTableViewCell.populateProjectDropDownTextFieldTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
            }else if formObject?.formOptionType == FormOptionType.textView {
                
                let cell = ProjectSharedTexViewTableViewCell.populateProjectSharedTexViewTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
            }else if formObject?.formOptionType == FormOptionType.multipleDropDownsTextField {
                
                let cell = ProjectDropSownMultipleTextFieldsTableViewCell.populateProjectDropSownMultipleTextFieldsTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
                
            }else if formObject?.formOptionType == FormOptionType.pollOption {
                
                let cell = ProjectOptionTableViewCell.populateProjectOptionTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
                
            }else{
                
                return UITableViewCell()
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let sectionKeyString = "section\(indexPath.section)"

        if indexPath.row == 0 {
            
            if indexPath.section == 5 {
                
                return 90
                
            }else{
                
                return 48
                
            }
            
        }else{
            
            let sectionFormOptionsArray = sectionsDictionary[sectionKeyString]
            let formObject = sectionFormOptionsArray?[indexPath.row - 1]
            
            if (formObject?.formOptionType == FormOptionType.regularTextField) || (formObject?.formOptionType == FormOptionType.dropDownTextField) || (formObject?.formOptionType == FormOptionType.multipleDropDownsTextField) || (formObject?.formOptionType == FormOptionType.pollOption){
                
                return 70
                
            }else if formObject?.formOptionType == FormOptionType.textView {
                
                return 200
                
            }else{
                
                return 0
                
            }
            
        }
    }
    
}
