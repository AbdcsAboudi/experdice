//
//  AddTeambuildingProjectViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//



import UIKit
import MBProgressHUD
class AddTeambuildingProjectViewController: DTViewController, UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate {
   
    
    
    @IBOutlet var addTeambuildingTableView: UITableView!
    
    var countryName = [String]()
    var countryID = [Int]()
    var city = [String]()
    var cityID = [Int]()
    var sectionsTitlesArray = [String]()
    var section0TextFieldsObjectsArray = [FormOptionObject]()
    var section1TextFieldsObjectsArray = [FormOptionObject]()
    var section2TextFieldsObjectsArray = [FormOptionObject]()
    var section3TextFieldsObjectsArray = [FormOptionObject]()
    var section4TextFieldsObjectsArray = [FormOptionObject]()
    var section5TextFieldsObjectsArray = [FormOptionObject]()
    var section6TextFieldsObjectsArray = [FormOptionObject]()
    var section7TextFieldsObjectsArray = [FormOptionObject]()
    var sectionsDictionary = [String : [FormOptionObject]]()
    var projectTypeID = 0
    var numberOfSelection = 0
    var notEditMode = false
    var countryDomianOption = [DTDomainObject]()
    var cityDomainOption = [DTDomainObject]()
    var languageObjectArray = [LanguagesObject]()
    var paymentMethodsArray = [PaymentMethodsObject]()
    var profssionalSeniority = [ProfssionalSeniorityObject]()
    var businessTypes = [BusinessTypeObject]()
    
    var profssionalSeniorityName = [String]()
    var profssionalSeniorityId = [Int]()
    
    var businessTypesName = [String]()
    var businessTypesId = [Int]()
    
    var paymentMethodObjectName = [String]()
    var paymentMethodObjectId = [Int]()
    
    var languageObjectName = [String]()
    var languageObjectId = [Int]()
    
    
    //MARK: - Life Clyce
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillSectionsArray()
        setupAddProjectTableView()
        
        //AS: API CALLS
        callCountryAPI()
        getLanguageAPI()
        getPaymentMethodsAPI()
        getProfssionalSeniorityAPI()
        getBusinessTypesIndustryAPI()

        title = "Teambuilding"
       
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.needSideBar = false
        super.viewWillAppear(animated)
    }
    
    //MARK: - Selector And Actiion
    
    func createProjectButtonSelector(){
        
        createProjectAPI()
        
    }
    
    func saveProjectButtonSelector(){
        
      //here save projetc api
        
    }
    
    
    // MARK: - Helping Methods
   
    
       func setupLanguageArray(){
        
        section4TextFieldsObjectsArray[0].pickerObjects?.removeAll()
        section4TextFieldsObjectsArray[1].pickerObjects?.removeAll()
        
        for languageObject in languageObjectArray {

            languageObjectName.append(languageObject.name!)
            languageObjectId.append(languageObject.id!)
        }
        
        for languageName in languageObjectName {
            
            var index = 0
            section4TextFieldsObjectsArray[0].pickerObjects?.append(DTDomainObject(id:languageObjectId[index], display: languageName))
             section4TextFieldsObjectsArray[1].pickerObjects?.append(DTDomainObject(id:languageObjectId[index], display: languageName))
            index += 1
        }
        
    }
    
    func setupPaymentMethodsArray(){
        
        section3TextFieldsObjectsArray[0].pickerObjects?.removeAll()
        
        if paymentMethodsArray.isEmpty {
            
        }
        
        for paymentMethod in paymentMethodsArray {
            
            paymentMethodObjectName.append(paymentMethod.name!)
            paymentMethodObjectId.append(paymentMethod.id!)
        }
        
        for  paymentMethodName in paymentMethodObjectName {
          
            section3TextFieldsObjectsArray[0].pickerObjects?.append(DTDomainObject(id:paymentMethodObjectId[0], display: paymentMethodName))
        }
        
    }
    
    func setupBusinessTypeArray(){
        
        section6TextFieldsObjectsArray[2].pickerObjects?.removeAll()
        
        for businessType in businessTypes {
            
            businessTypesName.append(businessType.name!)
            businessTypesId.append(businessType.id!)
        }
        
        for  businessName in businessTypesName {
            
            //var index = 0
            
            section6TextFieldsObjectsArray[2].pickerObjects?.append(DTDomainObject(id:businessTypesId[0], display: businessName))
            
        }
        
    }
    
    func setupProfssionalSeniorityArray(){
        
        section6TextFieldsObjectsArray[0].pickerObjects?.removeAll()
        
        for profssionalSeniority in profssionalSeniority {
            
            profssionalSeniorityName.append(profssionalSeniority.name!)
            profssionalSeniorityId.append(profssionalSeniority.id!)
        }
        
        for  profssionalSeniorityName in profssionalSeniorityName {
            
            var index = 0
            
            section6TextFieldsObjectsArray[0].pickerObjects?.append(DTDomainObject(id:profssionalSeniorityId[index], display: profssionalSeniorityName))
            
            index += 1
        }
        
    }
    
    func setupCountryArray(){
        
        var index = 0
        for countryName in countryName {
            
           countryDomianOption.append(DTDomainObject(id: countryID[index], display: countryName))
            
            index += 1
        }
      
    }
    
    func setupCityArray(){
        
        var index = 0
        for cityName in city {
            
            cityDomainOption.append(DTDomainObject(id: cityID[index] , display: cityName))
            
            index += 1
        }
    }
    
    
    
    func countryTextFildValueChange(_ textField:DTtextField){
        
        city.removeAll()
        cityID.removeAll()
        callGetCityAPI(id:textField.selectdIndex+1)

    }
    
    
    func setupAddProjectTableView() {
        
        addTeambuildingTableView.delegate = self
        addTeambuildingTableView.dataSource = self
        addTeambuildingTableView.estimatedRowHeight = 45
        addTeambuildingTableView.rowHeight = UITableViewAutomaticDimension
        
        addTeambuildingTableView.register(UINib(nibName: "ProjectSectionTitleTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSectionTitleTableViewCell")
        
        addTeambuildingTableView.register(UINib(nibName: "ProjectSharedTextFieldTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSharedTextFieldTableViewCell")
        
        addTeambuildingTableView.register(UINib(nibName: "ProjectSharedTexViewTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSharedTexViewTableViewCell")
        
        addTeambuildingTableView.register(UINib(nibName: "ProjectDropDownTextFieldTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectDropDownTextFieldTableViewCell")
        
        addTeambuildingTableView.register(UINib(nibName: "ProjectDropSownMultipleTextFieldsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectDropSownMultipleTextFieldsTableViewCell")
        
        addTeambuildingTableView.register(UINib(nibName: "ProjectOptionTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectOptionTableViewCell")
        
        addTeambuildingTableView.register(UINib(nibName: "CreateProjectTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "CreateProjectTableViewCell")
        
    }
    
    
//    func setupParameters()->[String:Any]{
//
//
//        var parameters = [String: Any]()
//
//        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
//
//
//        parameters["ClientId"] = user?.profile.businessId
//        parameters["ProjectId"] = 0
//
//        if DTValidationManager.isValidProjectTitle(text: section0TextFieldsObjectsArray[0].data) {
//            parameters["Title"] = section0TextFieldsObjectsArray[0].data
//        }else{
//            print("the project title wrong")
//        }
//
//        parameters["ProjectTypeId"] = projectTypeID
//
//        parameters["Objective"] = section1TextFieldsObjectsArray[0].data
//
//        parameters["GoalAchievements"] = section1TextFieldsObjectsArray[2].data
//        parameters["GoalDescription "] = section1TextFieldsObjectsArray[1].data
//        parameters["GoalNotes"] = section1TextFieldsObjectsArray[3].data
//
//        if DTValidationManager.isVaildDateRange(dateTo: section2TextFieldsObjectsArray[1].data, dateFrom: section2TextFieldsObjectsArray[1].secondData) {
//
//            parameters["ActualBeginDate"] = section2TextFieldsObjectsArray[1].data
//
//            parameters["ActualEndDate"] = section2TextFieldsObjectsArray[1].secondData
//
//        }else{
//
//            let alertMessage = "Please Fill (To) Date Smaller Than The (From) Date"
//
//            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
//
//            self.present(alertView, animated: true, completion: nil)
//
//        }
//
//
//
//        parameters["BudgetRate"] = section3TextFieldsObjectsArray[1].data
//
//        parameters["PerfTrainerGenderId"] = 0 // there is no gender
//
//
//        for pref in section5TextFieldsObjectsArray{
//
//            if pref.data == "Selected" {
//                  parameters["Preference"] = pref.textFieldTitle
//            }
//
//        }
//
//        if let data = section6TextFieldsObjectsArray[1].data {
//
//               parameters["ExpectedNumber"] = Int(data)
//        }
//
//        if let data = section0TextFieldsObjectsArray[1].data {
//
//            parameters["ProjectCategoryId"] = data
//
//        }
//
//        if let data = section4TextFieldsObjectsArray[0].data {
//
//            parameters["SessionLanguageId"] = languageObjectId[(languageObjectName.index(of:data))!]
//
//        }
//
//        if let data = section2TextFieldsObjectsArray[0].data {
//
//            parameters["CountryId"] = countryID[countryName.index(of: data)!]
//
//        }
//        if let data = section2TextFieldsObjectsArray[0].secondData {
//
//            parameters["CityId"] = cityID[city.index(of: data)!]
//
//        }
//
//        if let data = section4TextFieldsObjectsArray[1].data {
//
//            parameters["ContentLanguageId"] = languageObjectId[(languageObjectName.index(of: data))!]
//
//        }
//
//        if let data = section6TextFieldsObjectsArray[2].data {
//
//            parameters["BusinessIndustryId"] = businessTypesId[businessTypesName.index(of:data)!]
//        }
//
//        if let data = section6TextFieldsObjectsArray[0].data {
//
//            parameters["ProSeniorityId"] = profssionalSeniorityId[profssionalSeniorityName.index(of: data)!]
//
//        }
//
//        if let data = section3TextFieldsObjectsArray[0].data {
//
//            parameters["PaymentMethodId"] = paymentMethodObjectId[paymentMethodObjectName.index(of: data)!]
//        }
//        return parameters
//
//    }
    
    func fillSectionsArray() {
        
        sectionsTitlesArray.append(contentsOf: ["Main Details","Project Goals","Location & Time Frame","Budget","Preference","Preference","Target Audience","",""])
        
        section0TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Project Title", textFieldPlaceholder: "Enter Project Title", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:""),
            
            FormOptionObject(textFieldTitle: "Project Category", textFieldPlaceholder: "Choose Project Category", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")])])
        
        section1TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Main Objectives",
                             textFieldPlaceholder: "Write the main objectives",
                             formOptionType: FormOptionType.regularTextField,
                             keyboardType: TextFieldType.string,
                             rowTag: 0,data:"",secondData:""),
            
            FormOptionObject(textFieldTitle: "Description",
                             textFieldPlaceholder: "",
                             formOptionType: FormOptionType.textView,
                             keyboardType: TextFieldType.string,
                             rowTag: 0,
                             data:"",secondData:""),
            
            FormOptionObject(textFieldTitle: "What do you aim to acheive with this team building session ?",
                             textFieldPlaceholder: "",
                             formOptionType: FormOptionType.textView,
                             keyboardType: TextFieldType.string,
                             rowTag: 0,
                             data:"",secondData:""),
            FormOptionObject(textFieldTitle: "Notes",
                             textFieldPlaceholder: "",
                             formOptionType: FormOptionType.textView,
                             keyboardType: TextFieldType.string,
                             rowTag: 0,
                             data:"",secondData:"")])
        
        section2TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Location",
                             textFieldPlaceholder: "",
                             formOptionType: FormOptionType.multipleDropDownsTextField,
                             keyboardType: TextFieldType.domain,
                             rowTag: 2,
                             pickerObjects : [DTDomainObject(id: 0, display: "1"),
                                              DTDomainObject(id: 1, display: "2")],
                             
                             secondPickerObject:[DTDomainObject(id: 0, display: "1"),
                                                 DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Preffered Date Range",
                             textFieldPlaceholder: "dateRange",
                             formOptionType: FormOptionType.multipleDropDownsTextField,
                             keyboardType: TextFieldType.birthDay,
                             rowTag: 0,
                             data:"",
                             secondData:"")])
        
        section3TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Payment Method", textFieldPlaceholder: "Select Payment Method", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            FormOptionObject(textFieldTitle: "Rate / Budget", textFieldPlaceholder: "Set Rate / Budget", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")])])
        
        section4TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Session Language", textFieldPlaceholder: "Choose Session Language", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            FormOptionObject(textFieldTitle: "Content Language", textFieldPlaceholder: "Choose Content Language", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")])])
        
        section5TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Content & Delivery", textFieldPlaceholder: "", formOptionType: FormOptionType.pollOption, keyboardType: TextFieldType.string, rowTag: 0,data:"UnSelected",secondData:""),
            
            FormOptionObject(textFieldTitle: "Venue Booking(INCL. Food)", textFieldPlaceholder: "", formOptionType: FormOptionType.pollOption, keyboardType: TextFieldType.string, rowTag: 0,data:"UnSelected",secondData:""),
            
            FormOptionObject(textFieldTitle: "Other Details", textFieldPlaceholder: "", formOptionType: FormOptionType.pollOption, keyboardType: TextFieldType.string, rowTag: 0,data:"UnSelected",secondData:"")])
        
        section6TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Professional Seniority", textFieldPlaceholder: "Select Professional Seniority", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Expected Number", textFieldPlaceholder: "Set Expected Number", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,
                             pickerObjects : [DTDomainObject(id:1, display: "2"),
                                              DTDomainObject(id:2, display: "5"),
                                               DTDomainObject(id:3, display: "10"),
                                                DTDomainObject(id:4, display: "20"),
                                                 DTDomainObject(id:5, display: "50"),
                                                  DTDomainObject(id:6, display: "+100")]),
            
            
            FormOptionObject(textFieldTitle: "Business Industry", textFieldPlaceholder: "Select Business Industry", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")])])
        
        
        
        
        sectionsDictionary["section0"] = section0TextFieldsObjectsArray
        sectionsDictionary["section1"] = section1TextFieldsObjectsArray
        sectionsDictionary["section2"] = section2TextFieldsObjectsArray
        sectionsDictionary["section3"] = section3TextFieldsObjectsArray
        sectionsDictionary["section4"] = section4TextFieldsObjectsArray
        sectionsDictionary["section5"] = section5TextFieldsObjectsArray
        sectionsDictionary["section6"] = section6TextFieldsObjectsArray
        
    }
    
      //MARK:- TextView Delgate
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let dtTextView = textView as! DTTextView
        
        if let arraySectionObject = sectionsDictionary["section\(dtTextView.sectionIndex)"]{
            
            arraySectionObject[textView.tag].data = textView.text
        }
        
        
    }
    
    //MARK:- TextFiled Delgate
    
     func textFieldDidEndEditing(_ textField: UITextField) {

        let dtTextField = textField as! DTtextField
       
        if let arraySectionObject = sectionsDictionary["section\(dtTextField.sectionIndex)"] {
            
            if arraySectionObject[dtTextField.tag].formOptionType == FormOptionType.multipleDropDownsTextField{
                
                if dtTextField.multipleFirstTextFieldTag != -1{
                    
                    arraySectionObject[dtTextField.tag].data = textField.text
                    
                    if  dtTextField.tag == 0 && dtTextField.sectionIndex == 2 {
                        
                        cityDomainOption.removeAll()
                        city.removeAll()
                        cityID.removeAll()
                        callGetCityAPI(id: dtTextField.selectdIndex + 1)
                    }
                    
                }else{
                    arraySectionObject[dtTextField.tag].secondData = textField.text
                }
            }else{
                arraySectionObject[dtTextField.tag].data = textField.text
            }
            if arraySectionObject[dtTextField.tag].data == ""{
                
                let alertView = EXAlertViewController(title: "Please Fill * \(arraySectionObject[dtTextField.tag].textFieldTitle ?? "All Filed")*", twoButton: false, alertMessage: "", buttonTitle: "Done", zeroHighView: false, centerTextAlertMessage: true, speratorViewEnable: true)
                self.present(alertView, animated: true, completion: nil)
                
                
            }
        }
        
    
     
    }
    
    //MARK:- TableView Delegate && DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionsTitlesArray.count
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section == 5  && indexPath.row >= 1{
          
            if section5TextFieldsObjectsArray[indexPath.row - 1].data == "Selected" && numberOfSelection == 1{
                
                section5TextFieldsObjectsArray[indexPath.row - 1].data = "UnSelected"
                
                numberOfSelection -= 1
                
                addTeambuildingTableView.reloadData()
                
            }else if numberOfSelection == 0{
                
                section5TextFieldsObjectsArray[indexPath.row - 1].data = "Selected"
                
                numberOfSelection += 1
                
                addTeambuildingTableView.reloadData()
            }else if numberOfSelection == 1 && section5TextFieldsObjectsArray[indexPath.row - 1].data == "UnSelected"{
                
                section5TextFieldsObjectsArray[indexPath.row - 1].data = "Selected"
                 var index = 0
                for selected in section5TextFieldsObjectsArray{
                 
                    if index != (indexPath.row - 1) && selected.data == "Selected" {
                        
                        section5TextFieldsObjectsArray[index].data = "UnSelected"
                    }
                    index += 1
                }
                addTeambuildingTableView.reloadData()
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  section == 3 || section == 4 || section == 2 {
            
            return 3
            
        }else if section == 1{
            
            return 5
            
        }else if  section == 5 || section == 6{
            
            return 4
            
        }else if section == 0{
            
            return 2
            
        }else if section == 8{
            
            return 2
            
        }else{
            
            return 0
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionKeyString = "section\(indexPath.section)"
        
        if indexPath.row == 0 {
            
            if indexPath.section == 8 {
                
                let cell = CreateProjectTableViewCell.populateCreateProjectTableViewCell(indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.createProjectButtonContainerButton.addTarget(self, action: #selector(createProjectButtonSelector), for: .touchUpInside)
                
                cell.saveProjectButtonContainerButton.addTarget(self, action: #selector(saveProjectButtonSelector), for: .touchUpInside)
                
                return cell
                
            }else{
                
                let cell = ProjectSectionTitleTableViewCell.populateProjectSectionTitleTableViewCell(projectTitleString: sectionsTitlesArray[indexPath.section], indexPath: indexPath as NSIndexPath, tableView: tableView)
                return cell
                
            }
            
        }else{
            
            let sectionFormOptionsArray = sectionsDictionary[sectionKeyString]
            let formObject = sectionFormOptionsArray?[indexPath.row - 1]
            
            if formObject?.formOptionType == FormOptionType.regularTextField {
                
                let cell = ProjectSharedTextFieldTableViewCell.populateProjectSharedTextFieldTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.textField.delegate = self
                
                return cell
                
            }else if formObject?.formOptionType == FormOptionType.dropDownTextField {
                
                let cell = ProjectDropDownTextFieldTableViewCell.populateProjectDropDownTextFieldTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.textField.delegate = self
                
                return cell
                
            }else if formObject?.formOptionType == FormOptionType.textView {
                
                let cell = ProjectSharedTexViewTableViewCell.populateProjectSharedTexViewTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.textViewData.delegate = self
             

                return cell
                
            }else if formObject?.formOptionType == FormOptionType.multipleDropDownsTextField {
                
                let cell = ProjectDropSownMultipleTextFieldsTableViewCell.populateProjectDropSownMultipleTextFieldsTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.firstTextField.domainOptions = countryDomianOption
                
                //AA : To identify each textField with the custom tags
                //AA ,Ex : multipleFirstTextFieldTag = -1 then the second textField
                cell.firstTextField.multipleFirstTextFieldTag = 0
                cell.firstTextField.multipleSecondTextFieldTag = -1
                
                cell.secondTextField.multipleFirstTextFieldTag = -1
                cell.secondTextField.multipleSecondTextFieldTag = 0

                cell.secondTextField.domainOptions = cityDomainOption
               
                
                cell.secondTextField.delegate = self
                cell.firstTextField.delegate = self
              
               
                
                return cell
                
                
            }else if formObject?.formOptionType == FormOptionType.pollOption {
                
                let cell = ProjectOptionTableViewCell.populateProjectOptionTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
                
            }else{
                
                return UITableViewCell()
                
            }
            
        }
    }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            let sectionKeyString = "section\(indexPath.section)"
            
            if indexPath.row == 0 {
                
                if indexPath.section == 8 {
                    
                    return 90
                    
                }else{
                    
                    return 48
                    
                }
                
            }else{
                
                let sectionFormOptionsArray = sectionsDictionary[sectionKeyString]
                let formObject = sectionFormOptionsArray?[indexPath.row - 1]
                
                if (formObject?.formOptionType == FormOptionType.regularTextField) || (formObject?.formOptionType == FormOptionType.dropDownTextField) || (formObject?.formOptionType == FormOptionType.multipleDropDownsTextField) || (formObject?.formOptionType == FormOptionType.pollOption){
                    
                    return 70
                    
                }else if formObject?.formOptionType == FormOptionType.textView {
                    
                    return 200
                    
                }else{
                    
                    return 0
                    
                }
                
            }
    }
    
    //MARK: - API Calls
    
    func createProjectAPI(){
        
        //let parm = setupParameters()
        
        var parameters = [String: Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parameters["ClientId"] = user?.profile.businessId
        parameters["ProjectId"] = 0
        parameters["ProjectTypeId"] = projectTypeID
        parameters["PerfTrainerGenderId"] = 0 // there is no gender
        
        if let data = section0TextFieldsObjectsArray[1].data {
            parameters["ProjectCategoryId"] = data
        }
        
        if DTValidationManager.isValidProjectTitle(text: section0TextFieldsObjectsArray[0].data) {
            parameters["Title"] = section0TextFieldsObjectsArray[0].data
        }else{
           
            let alertMessage = "Please Fill Valid Project Title"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        
       if DTValidationManager.isValidProjectObjective(text: section1TextFieldsObjectsArray[0].data) {
             parameters["Objective"] = section1TextFieldsObjectsArray[0].data
        }else{
            let alertMessage = "Please Fill Vaild Project Objecitve"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        
        if DTValidationManager.isValidProjectGoalAchievements(text: section1TextFieldsObjectsArray[2].data) {
            parameters["GoalAchievements"] = section1TextFieldsObjectsArray[2].data
            
        }else{
            let alertMessage = "Please Fill Vaild Project Goal Achievements"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        
        if DTValidationManager.isValidProjectGoalDescription(text: section1TextFieldsObjectsArray[1].data) {
             parameters["GoalDescription"] = section1TextFieldsObjectsArray[1].data
            
        }else{
            let alertMessage = "Please Fill Vaild Project Goal Description"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        
        if DTValidationManager.isValidProjectGoalNotes(text: section1TextFieldsObjectsArray[3].data) {
             parameters["GoalNotes"] = section1TextFieldsObjectsArray[3].data
            
        }else{
            let alertMessage = "Please Fill Vaild Project Goal Notes"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        
        if DTValidationManager.isVaildDateRange(dateTo: section2TextFieldsObjectsArray[1].data,  dateFrom: section2TextFieldsObjectsArray[1].secondData) {
            
            parameters["ActualBeginDate"] = section2TextFieldsObjectsArray[1].data
            
            parameters["ActualEndDate"] = section2TextFieldsObjectsArray[1].secondData
            
        }else{
            
            let alertMessage = "Please Fill (To) Date Smaller Than The (From) Date"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
            
            return
            
        }
        
        if DTValidationManager.isValidProjectBudgetRate(text: section3TextFieldsObjectsArray[1].data) {
             parameters["BudgetRate"] = section3TextFieldsObjectsArray[1].data
            
        }else{
            let alertMessage = "Please Fill Vaild Project Budget Rate"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        
        if DTValidationManager.isValidProjectExpectedNumber(text:  section6TextFieldsObjectsArray[1].data) {
            parameters["ExpectedNumber"] =  section6TextFieldsObjectsArray[1].data
            
        }else{
            let alertMessage = "Please Fill Vaild Project Expected Number"
            
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            
            self.present(alertView, animated: true, completion: nil)
            
            return
        }
        
        for pref in section5TextFieldsObjectsArray{
            
            if pref.data == "Selected" {
                parameters["Preference"] = pref.textFieldTitle
            }
            
        }
      
        if let data = section4TextFieldsObjectsArray[0].data {
            
            parameters["SessionLanguageId"] = languageObjectId[(languageObjectName.index(of:data))!]
            
        }
        
        if let data = section2TextFieldsObjectsArray[0].data {
            
            parameters["CountryId"] = countryID[countryName.index(of: data)!]
            
        }
        if let data = section2TextFieldsObjectsArray[0].secondData {
            
            parameters["CityId"] = cityID[city.index(of: data)!]
            
        }
        
        if let data = section4TextFieldsObjectsArray[1].data {
            
            parameters["ContentLanguageId"] = languageObjectId[(languageObjectName.index(of: data))!]
            
        }
        
        if let data = section6TextFieldsObjectsArray[2].data {
            
            parameters["BusinessIndustryId"] = businessTypesId[businessTypesName.index(of:data)!]
        }
        
        if let data = section6TextFieldsObjectsArray[0].data {
            
            parameters["ProSeniorityId"] = profssionalSeniorityId[profssionalSeniorityName.index(of: data)!]
            
        }
        
        if let data = section3TextFieldsObjectsArray[0].data {
            
            parameters["PaymentMethodId"] = paymentMethodObjectId[paymentMethodObjectName.index(of: data)!]
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .createOrEditProject, requestParams:parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
              viewController.dismiss(animated: true, completion: nil)
                
            }
        }
        
        
    }
    
    func callGetCityAPI(id:Int){
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCity, requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let cityArray = DTParser.parseCityObjectArray(from: response)
                    
                    for city in cityArray  {
                        
                        if let cityName = city.name {
                            
                            self?.city.append(cityName)
                            
                        }
                        
                        if let cityID = city.id {
                            
                            self?.cityID.append(cityID)
                        }
                        
                    }
                    self?.setupCityArray()
                    
                   
                    viewController.section2TextFieldsObjectsArray[0].pickerObjects = viewController.cityDomainOption
                    viewController.addTeambuildingTableView.reloadData()
                   
                }
            }
        }
    }
    
    func callCountryAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCountry, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let countryArray = DTParser.parseCountryObjectArray(from: response)
                    
                    for country in countryArray  {
                        
                        if let countryName = country.name {
                            
                            self?.countryName.append(countryName)
                            
                        }
                        
                        if let countryID = country.id {
                            
                            self?.countryID.append(countryID)
                        }
                        
                    }
                    self?.setupCountryArray()
                }
                
            }
        }
        
    }
    
    func getLanguageAPI(){
        
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getLanguages, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            //MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let languageArray = DTParser.parseLanguagesObjectFromResponseArray(from: response)
                    
                    viewController.languageObjectArray = languageArray as! [LanguagesObject]
                    
                }
                viewController.setupLanguageArray()
            }
        }
        
    }
    
    func getPaymentMethodsAPI(){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getPaymentMethods, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let paymentMethodsArray = DTParser.parsePaymentMethodsFromResponseArray(from: response)
                    
                    viewController.paymentMethodsArray = paymentMethodsArray as! [PaymentMethodsObject]
                    
                }
                viewController.setupPaymentMethodsArray()
            }
        }
        
    }
    
    func getBusinessTypesIndustryAPI(){
        
        
        if let path = Bundle.main.path(forResource: "BusinessTypes", ofType: "json"){
            do {
                do{
                    let data = try Data(contentsOf: URL(fileURLWithPath: path))
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [Any]
                    
                    let businessArray = DTParser.parseBusinessTypeOjectArray(from: jsonResult)
                    
                    for business in businessArray  {
                        
                        if let businessName = business.name {
                            
                            self.businessTypesName.append(businessName)
                            
                        }
                        
                        if let businessID = business.id {
                            
                            self.businessTypesId.append(businessID)
                        }
                        
                    }
                    self.setupBusinessTypeArray()
                    
                } catch {}
            } catch {}
        }
        
//        //MBProgressHUD.showAdded(to: self.view, animated: true)
//
//        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .businessTypes, requestParams: nil)
//
//        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
//
//        }) { [weak self](apiResponse,success) -> (Void) in
//
//            guard let viewController = self else{
//                return
//            }
//
//            //MBProgressHUD.hide(for: viewController.view, animated: true)
//
//
//            if success {
//
//                if let response = apiResponse as? [Any] {
//
//                    let businessTypesArray = DTParser.parseBusinessTypeOjectArray(from: response)
//
//                    viewController.businessTypes = businessTypesArray
//
//                }
//                viewController.setupBusinessTypeArray()
//            }
//        }
//
    }
    
    func getProfssionalSeniorityAPI(){
        
         // MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getProfessionalSeniorities, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
           // MBProgressHUD.hide(for: viewController.view, animated: true)

            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let profssionalSeniorityArray = DTParser.parseProfessionalSenioritiesFromResponseArray(from: response)
                    
                    viewController.profssionalSeniority = profssionalSeniorityArray as! [ProfssionalSeniorityObject]
                    
                }
                viewController.setupProfssionalSeniorityArray()
            }
        }
        
    }

    
}
