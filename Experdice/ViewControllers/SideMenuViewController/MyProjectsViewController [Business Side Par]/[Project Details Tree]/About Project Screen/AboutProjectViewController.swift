//
//  AboutProjectViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/12/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class AboutProjectViewController: DTViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var aboutProjectTableView: UITableView!
    
    var passedObject:ProjectObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAboutProjectTableView()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helping Methods
    
    func recevieNotificationObject(){
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(receivedNotification),
                                               name: Notification.Name("SendingProjectObject"),
                                               object: nil)
        
    }
    
    func receivedNotification(notification: Notification){
        
        passedObject = notification.object as? ProjectObject
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("SendingProjectObject"), object: nil)
    }
    
    func setupAboutProjectTableView() {
        
        aboutProjectTableView.delegate = self
        aboutProjectTableView.dataSource = self
        aboutProjectTableView.estimatedRowHeight = 45
        aboutProjectTableView.rowHeight = UITableViewAutomaticDimension
        aboutProjectTableView.separatorStyle = .none
        aboutProjectTableView.register(UINib(nibName: "ProjectDescriptionTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectDescriptionTableViewCell")
        
    }
    
    //MARK:- TableView Delegate && DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ProjectDescriptionTableViewCell.populateProjectDescriptionTableViewCell(indexPath: indexPath as NSIndexPath , tableView: tableView)
        
        return cell
        
    }
    
   
    
}
