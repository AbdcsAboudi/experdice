//
//  ProjectDescriptionTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/12/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectDescriptionTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Cells population Methods
    
    class func populateProjectDescriptionTableViewCell(indexPath:NSIndexPath, tableView:UITableView) -> ProjectDescriptionTableViewCell {
        
        let cell:ProjectDescriptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectDescriptionTableViewCell") as! ProjectDescriptionTableViewCell
                
        return cell
        
    }
    
}
