//
//  ProjectDetailsBidTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import SDWebImage
import HCSStarRatingView

class ProjectDetailsBidTableViewCell: UITableViewCell {

    @IBOutlet weak var expertName:UILabel!
    @IBOutlet weak var expertImage:UIImageView!
    @IBOutlet weak var expertRating:HCSStarRatingView!
    @IBOutlet weak var bidAmount:UILabel!
    @IBOutlet weak var bidDate:UILabel!
    @IBOutlet weak var bidLocation:UILabel!
    @IBOutlet weak var acceptButton:UIButton!
    @IBOutlet weak var rejectButton:UIButton!
    @IBOutlet weak var buttonView:UIView!
    @IBOutlet weak var imageRoundedView:UIView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        imageRoundedView.makeCorneredViewWith(corenerRadius: 10, and: 0)
        expertImage.image = expertImage.image?.circleMaskBlue
       
    }
    
    // MARK: - Cells population Methods
    
    class func populateProjectDetailsBidTableViewCell(bidObject : ProjectBidsObject, indexPath:NSIndexPath, tableView:UITableView) -> ProjectDetailsBidTableViewCell {
        
        let cell:ProjectDetailsBidTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectDetailsBidTableViewCell") as! ProjectDetailsBidTableViewCell
        
        let imageURL = "http://numidiajo.com/Experdice/API/images/\(bidObject.expertInfo?.imageURL ?? "")"

        cell.expertName.text = bidObject.expertInfo?.name
        cell.bidDate.text = bidObject.date
        cell.bidAmount.text = bidObject.bidAmount
        cell.bidLocation.text = "\(bidObject.expertInfo?.country ?? "") , \(bidObject.expertInfo?.city ?? "")"
        
        cell.expertImage?.sd_setImage(with:URL(string:imageURL) , placeholderImage: #imageLiteral(resourceName: "memper_name"), options: .retryFailed, completed: { (image, error, cacheType, url) in
            
            if error != nil {
                
                print(error?.localizedDescription ?? "error found")
                
            } else {
                cell.expertImage?.contentMode = .scaleAspectFill
                cell.expertImage?.clipsToBounds = true
                
            }
        })
        
        return cell
        
    }
    
}
