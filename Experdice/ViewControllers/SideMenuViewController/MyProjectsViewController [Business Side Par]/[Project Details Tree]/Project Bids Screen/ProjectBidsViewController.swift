//
//  ProjectBidsViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/12/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProjectBidsViewController: DTViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var projectBidsTableView: UITableView!
    
    var projectBidsArray = [ProjectBidsObject]()
    var passedObject:ProjectObject?
    let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recevieNotificationObject()
        setupProjectBidsableView()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Notification Reciving Method
    func recevieNotificationObject(){
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(receivedNotification),
                                               name: Notification.Name("SendingProjectObject"),
                                               object: nil)
        
    }
    
    func receivedNotification(notification: Notification){
        
        passedObject = notification.object as? ProjectObject
        
        if let passedBidObjectArray = passedObject?.projectBids {
            
            projectBidsArray = passedBidObjectArray
            projectBidsTableView.reloadData()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("SendingProjectObject"), object: nil)
    }
    
    //MARK: - Selector
    
    func acceptButtonSelector(sender:UIButton){
        
        if let project = passedObject,let projectBidArray = project.projectBids {
            setAcceptOrRegectOnProjectAPI(projectBidId: projectBidArray[sender.tag].bidId!, bidType: .acceptProjectBid)
        }
    }
    
    func rejectButtonSelector(sender:UIButton){
        if let project = passedObject,let projectBidArray = project.projectBids {
            setAcceptOrRegectOnProjectAPI(projectBidId: projectBidArray[sender.tag].bidId!, bidType: .rejectProjectBid)
        }
    }
    
    // MARK: - Helping Methods
    
    func setupProjectBidsableView() {
        
        projectBidsTableView.delegate = self
        projectBidsTableView.dataSource = self
        projectBidsTableView.estimatedRowHeight = 45
        projectBidsTableView.rowHeight = UITableViewAutomaticDimension
        
        projectBidsTableView.register(UINib(nibName: "ProjectDetailsBidTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectDetailsBidTableViewCell")
        
    }
    
    
    //MARK:- TableView Delegate && DataSource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if user?.userType == "1" {
            return 100
        }
        return UITableViewAutomaticDimension
     }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return projectBidsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ProjectDetailsBidTableViewCell.populateProjectDetailsBidTableViewCell(bidObject: projectBidsArray[indexPath.row], indexPath: indexPath as NSIndexPath, tableView: tableView)
        
        //AS: If userType is expert the button view of accpet the bid it will be hidden
        if user?.userType == "1"{
            cell.buttonView.isHidden = true
        }
    
        cell.acceptButton.tag = indexPath.row
        cell.rejectButton.tag = indexPath.row
        cell.acceptButton.addTarget(self, action: #selector(acceptButtonSelector), for: .touchUpInside)
        cell.rejectButton.addTarget(self, action: #selector(rejectButtonSelector), for: .touchUpInside)
        
        return cell
        
    }
    
    func setAcceptOrRegectOnProjectAPI(projectBidId:String?,bidType:ApiRequestID){
        
        var parm = [String:Any]()
        if let id = projectBidId{
            parm["ProjectBidId"] = id
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId:bidType, requestParams: parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any]{
                    
                    
                }
                
                
            }
            
        }
        
        
    }
    
   
}

