//
//  ProjectOverviewViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/12/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectOverviewViewController: DTViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var projectOverviewTableView: UITableView!
    
    var aboutProjectFormObjectsArray = [FormOptionObject]()
    var passedProject:ProjectObject?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        recevieNotificationObject()

        setupProjectOverviewTableView()
        
        //fillFormTextFieldsObjects()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    func recevieNotificationObject(){
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(receivedNotification),
                                               name: Notification.Name("SendingProjectObject"),
                                               object: nil)

    }
    
    //MARK: - Notification And Observer
    func receivedNotification(notification: Notification){
        //Take Action on Notification
        
        passedProject = notification.object as? ProjectObject
        fillFormTextFieldsObjects()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("SendingProjectObject"), object: nil)
    }
    
    // MARK: - Helping Methods
    
    func setupProjectOverviewTableView() {
        
        projectOverviewTableView.delegate = self
        projectOverviewTableView.dataSource = self
        projectOverviewTableView.estimatedRowHeight = 45
        projectOverviewTableView.rowHeight = UITableViewAutomaticDimension
        
        projectOverviewTableView.register(UINib(nibName: "ProjectSharedTextFieldTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSharedTextFieldTableViewCell")
        
    }
    
    func fillFormTextFieldsObjects() {
        

        aboutProjectFormObjectsArray.append(contentsOf: [
            
            FormOptionObject(textFieldTitle: "Location",
                             textFieldPlaceholder: "",
                             formOptionType: FormOptionType.regularTextField,
                             keyboardType: TextFieldType.string,
                             rowTag: 0,
                             data:"\(passedProject?.city?.name ?? "Test") \(passedProject?.country?.name ?? "Test")",
                             secondData:""),
            
            FormOptionObject(textFieldTitle: "Bid Average", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,
                             data:"\(passedProject?.bidAverage ?? 0.0)",secondData:""),
            
            FormOptionObject(textFieldTitle: "Budget", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:passedProject?.budget,secondData:""),
            
            FormOptionObject(textFieldTitle: "Bid Number", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:"\(passedProject?.bidNumber ?? 0)",secondData:""),
            
            FormOptionObject(textFieldTitle: "Industry", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:passedProject?.businessIndustry?.name,secondData:""),
            
            FormOptionObject(textFieldTitle: "Objective", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:passedProject?.objectives,secondData:""),
            
            FormOptionObject(textFieldTitle: "Language", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:passedProject?.contentLanguage?.name,secondData:""),
            
            FormOptionObject(textFieldTitle: "Trainer Pref", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:passedProject?.trainerGender?.genderName,secondData:""),
            
            FormOptionObject(textFieldTitle: "Audience", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:passedProject?.expectedNumber,secondData:""),
            
            FormOptionObject(textFieldTitle: "Date/Length", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:passedProject?.openUntilDate,secondData:"")
            
            ])
        

        projectOverviewTableView.reloadData()
        
    }
    
    //MARK:- TableView Delegate && DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return aboutProjectFormObjectsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let formObject = aboutProjectFormObjectsArray[indexPath.row]
            
            if formObject.formOptionType == FormOptionType.regularTextField {
                
                let cell = ProjectSharedTextFieldTableViewCell.populateProjectSharedTextFieldTableViewCell(textFieldObject: formObject, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                
                return cell
                
            } else {
                
                return UITableViewCell()
                
            }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       return 70
    
    }
    
    

}
