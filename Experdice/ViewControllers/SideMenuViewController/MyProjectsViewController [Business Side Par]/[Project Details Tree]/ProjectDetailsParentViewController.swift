//
//  ProjectDetailsParentViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/12/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProjectDetailsParentViewController: DTViewController {
    
    
    //MARK: - Outlet
    @IBOutlet var overViewButtonView: UIView!
    @IBOutlet var aboutButtonView: UIView!
    @IBOutlet var bidsButtonView: UIView!
    @IBOutlet var overViewButtonLabel: UILabel!
    @IBOutlet var aboutButtonLabel: UILabel!
    @IBOutlet var bidsButtonLabel: UILabel!
    @IBOutlet weak var projectNameLabel:UILabel!
    @IBOutlet weak var clientNameLabel:UILabel!
    @IBOutlet weak var projectlocation:UILabel!
    @IBOutlet weak var projectDateStart:UILabel!
    @IBOutlet weak var projectUntilDateLabel:UILabel!
    @IBOutlet weak var projectCatgoryTypeLabel:UILabel!
    @IBOutlet weak var clientLogoImage:UIImageView!
    @IBOutlet var projectOverviewContainerView: UIView!
    @IBOutlet var aboutProjecContainerView: UIView!
    @IBOutlet var projectBidsContainerView: UIView!
    @IBOutlet var bottomButtonContainerView: UIView!
    @IBOutlet var bottomButtonLabel: UILabel!
    @IBOutlet weak var bottomButtonView:UIView!
    
    
    var canEdit = false
    var isFromMyProject = false
    var passedProject:ProjectObject?
    var projectId:Int?
    
      //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProjectDetailsView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        needEditButton = true
        getProjectDetialsAPI()
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setEditButton()
        super.viewDidAppear(animated)
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Notification And Observer
    func sendProjectObject(){
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendingProjectObject"), object: passedProject)
    }
    
    // MARK: - Actions and Selector
    
    func setEditEnabelSelector(){
        
        // canEdit is false
        projectNameLabel.isEnabled = !canEdit
        clientNameLabel.isEnabled = !canEdit
        projectlocation.isEnabled = !canEdit
        projectDateStart.isEnabled = !canEdit
        projectCatgoryTypeLabel.isEnabled = !canEdit
        
        
        if !canEdit {
            
            //AS : FOR CHANGING THE STATE OF THE NAVIGATION BUTTON'S
            setIconButtonForEditngState()
        }
        
        canEdit = !canEdit
    
    }
    
    func cancelEditSelector(){
        
        setEditButton()
    }
    
    func doneEditSelector(){
        
        setEditButton()
        
    }

    @IBAction func overviewButtonAction(_ sender: Any) {
        
       changeTabButtonColorAndText(buttonContainerView: overViewButtonView, label: overViewButtonLabel, isSelected: true, containerView: projectOverviewContainerView)
        changeTabButtonColorAndText(buttonContainerView: aboutButtonView, label: aboutButtonLabel, isSelected: false, containerView: aboutProjecContainerView)
        changeTabButtonColorAndText(buttonContainerView: bidsButtonView, label: bidsButtonLabel, isSelected: false, containerView: projectBidsContainerView)
        
         
    }

    @IBAction func aboutProjectButtonAction(_ sender: Any) {
        
        changeTabButtonColorAndText(buttonContainerView: overViewButtonView, label: overViewButtonLabel, isSelected: false, containerView: projectOverviewContainerView)
        changeTabButtonColorAndText(buttonContainerView: aboutButtonView, label: aboutButtonLabel, isSelected: true, containerView: aboutProjecContainerView)
        changeTabButtonColorAndText(buttonContainerView: bidsButtonView, label: bidsButtonLabel, isSelected: false, containerView: projectBidsContainerView)
        
    }
    
    @IBAction func projectBidsButtonAction(_ sender: Any) {
        
        changeTabButtonColorAndText(buttonContainerView: overViewButtonView, label: overViewButtonLabel, isSelected: false, containerView: projectOverviewContainerView)
        changeTabButtonColorAndText(buttonContainerView: aboutButtonView, label: aboutButtonLabel, isSelected: false, containerView: aboutProjecContainerView)
        changeTabButtonColorAndText(buttonContainerView: bidsButtonView, label: bidsButtonLabel, isSelected: true, containerView: projectBidsContainerView)
        
    }
    
    @IBAction func bidButtonAction(_ sender: Any) {
     
        //AA : Go to bid on project vc
        let bidOnProjectVc = BidOnProjectViewController(nibName: "BidOnProjectViewController", bundle: nil)
        bidOnProjectVc.projectId = passedProject?.id
        show(bidOnProjectVc, sender: self)
        
    }
 
    // MARK: - Helping
  
    func setEditButton(){
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        if user?.userType != "1" && isFromMyProject{
            let button = UIButton(type: .custom)
            button.setImage(#imageLiteral(resourceName: "edit_icon"), for: .normal)
            button.addTarget(self, action: #selector(setEditEnabelSelector), for: .touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: 25, height: 30)
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView:button)
            navigationItem.leftBarButtonItem = navigationController?.navigationItem.backBarButtonItem
            
            if canEdit {
                
                setEditEnabelSelector()
            }
        }
    }
    
    
    func setIconButtonForEditngState(){
        
        let doneButton = UIButton(type: .custom)
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(#imageLiteral(resourceName: "closeEdit"), for: .normal)
        doneButton.setImage(#imageLiteral(resourceName: "doneEditpng"), for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelEditSelector), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneEditSelector), for: .touchUpInside)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        doneButton.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: doneButton)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cancelButton)
        
    }

    func setupProjectDetailsView() {
        
        changeTabButtonColorAndText(buttonContainerView: overViewButtonView, label: overViewButtonLabel, isSelected: true, containerView: projectOverviewContainerView)
        changeTabButtonColorAndText(buttonContainerView: aboutButtonView, label: aboutButtonLabel, isSelected: false, containerView: aboutProjecContainerView)
        changeTabButtonColorAndText(buttonContainerView: bidsButtonView, label: bidsButtonLabel, isSelected: false, containerView: projectBidsContainerView)
        
        if isFromMyProject {
            
            bottomButtonLabel.text = "Invite More People To Bid"
            bidsButtonLabel.text = "Bidders"
            
        }else if UserObject.getUserType() == .expert {
            
            bottomButtonLabel.text = "Bid On This Project"
            bidsButtonLabel.text = "Bids"

        }else{
            bottomButtonContainerView.isHidden = true
        }
        
    }
    
    func changeTabButtonColorAndText(buttonContainerView : UIView, label : UILabel, isSelected : Bool, containerView : UIView) {
        
        if isSelected {
            
            buttonContainerView.backgroundColor = ColorManager.sharedInstance.getTextFiledTextColor()
            label.textColor = UIColor.white
            containerView.isHidden = false

        }else{
            
            buttonContainerView.backgroundColor = ColorManager.sharedInstance.headerViewButtonDefault()
            label.textColor = .white
            containerView.isHidden = true

        }
        
        
        
    }
    
    //MARK: - API Call

    func getProjectDetialsAPI(){
        
        var parameters = [String:Any]()
        
        parameters["projectID"] = projectId
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getProjectDetails, requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any]{
                    
                    viewController.passedProject = DTParser.parseProjectDetailsObject(response: response)
                    
                    viewController.projectNameLabel.text = viewController.passedProject?.title
                    viewController.projectDateStart.text = viewController.passedProject?.actualEndDate
                    
                    if let county = viewController.passedProject?.country?.name,let city = viewController.passedProject?.city?.name{
                        
                        viewController.projectlocation.text = county + "," + city
                    }
                   
                   
                    viewController.clientNameLabel.text = viewController.passedProject?.businessName
                    viewController.projectCatgoryTypeLabel.text = viewController.passedProject?.projectType
                    viewController.title = viewController.passedProject?.title
                    viewController.sendProjectObject()
                    
                    
                }
                
               
            }
            
        }
     
        
    }
    
}
