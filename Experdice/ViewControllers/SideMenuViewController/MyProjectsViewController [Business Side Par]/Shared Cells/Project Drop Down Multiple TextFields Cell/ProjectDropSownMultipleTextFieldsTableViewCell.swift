//
//  ProjectDropSownMultipleTextFieldsTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectDropSownMultipleTextFieldsTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var firstTextField: DTtextField!
    @IBOutlet var secondTextField: DTtextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Cells population Methods
    
    class func populateProjectDropSownMultipleTextFieldsTableViewCell(textFieldObject : FormOptionObject,indexPath:NSIndexPath, tableView:UITableView) -> ProjectDropSownMultipleTextFieldsTableViewCell {
        
        let cell:ProjectDropSownMultipleTextFieldsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectDropSownMultipleTextFieldsTableViewCell") as! ProjectDropSownMultipleTextFieldsTableViewCell
        
        cell.titleLabel.text = textFieldObject.textFieldTitle
        
        cell.firstTextField.text = textFieldObject.data
        cell.secondTextField.text = textFieldObject.secondData
        
        cell.firstTextField.tag = indexPath.row - 1
        cell.secondTextField.tag = indexPath.row - 1

        cell.firstTextField.sectionIndex = indexPath.section
        cell.secondTextField.sectionIndex = indexPath.section
        
        cell.firstTextField.textFieldType = textFieldObject.keyboardType!
        cell.secondTextField.textFieldType = textFieldObject.keyboardType!
     

        return cell
        
    }

}
