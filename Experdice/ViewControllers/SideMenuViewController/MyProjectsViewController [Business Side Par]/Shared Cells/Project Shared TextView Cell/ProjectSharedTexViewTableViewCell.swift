//
//  ProjectSharedTexViewTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectSharedTexViewTableViewCell: UITableViewCell {

    @IBOutlet var textViewTitleLabel: UILabel!
    @IBOutlet var textViewContainerView: UIView!
    @IBOutlet var textViewData:DTTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        textViewContainerView.makeCorneredViewWith(corenerRadius: 5, and: 1, borderColor: DTHelper.sharedInstance.colorWithHexString("B1B2B5"))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Cells population Methods
    
    class func populateProjectSharedTexViewTableViewCell(textFieldObject : FormOptionObject,indexPath:NSIndexPath, tableView:UITableView) -> ProjectSharedTexViewTableViewCell {
        
        let cell:ProjectSharedTexViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectSharedTexViewTableViewCell") as! ProjectSharedTexViewTableViewCell
        
        cell.textViewTitleLabel.text = textFieldObject.textFieldTitle
        cell.textViewData.tag = indexPath.row - 1
        cell.textViewData.sectionIndex = indexPath.section
        cell.textViewData.text = textFieldObject.data
        
        return cell
        
    }
    
}
