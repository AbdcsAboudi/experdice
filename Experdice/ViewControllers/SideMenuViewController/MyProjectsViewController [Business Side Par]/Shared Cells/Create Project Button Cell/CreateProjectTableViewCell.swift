//
//  CreateProjectTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/11/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class CreateProjectTableViewCell: UITableViewCell {

    @IBOutlet var createProjectButtonContainerView: UIView!
    @IBOutlet var saveProjectButtonContainerView: UIView!
    @IBOutlet var createProjectButtonContainerButton: UIButton!
    @IBOutlet var saveProjectButtonContainerButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        createProjectButtonContainerView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        saveProjectButtonContainerView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Cells population Methods
    
    class func populateCreateProjectTableViewCell(indexPath:NSIndexPath, tableView:UITableView) -> CreateProjectTableViewCell {
        
        let cell:CreateProjectTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CreateProjectTableViewCell") as! CreateProjectTableViewCell
        
        return cell
        
    }
    
}
