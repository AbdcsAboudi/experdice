//
//  ProjectSectionTitleTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectSectionTitleTableViewCell: UITableViewCell {

    @IBOutlet var sectionTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Cells population Methods
    
    class func populateProjectSectionTitleTableViewCell(projectTitleString : String,indexPath:NSIndexPath, tableView:UITableView) -> ProjectSectionTitleTableViewCell {
        
        let cell:ProjectSectionTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectSectionTitleTableViewCell") as! ProjectSectionTitleTableViewCell
        
        cell.sectionTitleLabel.text = projectTitleString
        
        return cell
        
    }
    
}
