//
//  ProjectDropDownTextFieldTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectDropDownTextFieldTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textField: DTtextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Cells population Methods
    
    class func populateProjectDropDownTextFieldTableViewCell(textFieldObject : FormOptionObject,indexPath:NSIndexPath, tableView:UITableView) -> ProjectDropDownTextFieldTableViewCell {
        
        let cell:ProjectDropDownTextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectDropDownTextFieldTableViewCell") as! ProjectDropDownTextFieldTableViewCell
        
        cell.titleLabel.text = textFieldObject.textFieldTitle
        cell.textField.setTextFieldPlaceHolderColor(title: textFieldObject.textFieldPlaceholder! , color: ColorManager.sharedInstance.getTextFiledTextColor())
        
        cell.textField.text = textFieldObject.data
        cell.textField.tag = indexPath.row - 1
        cell.textField.sectionIndex = indexPath.section
        cell.textField.textFieldType = textFieldObject.keyboardType!
        cell.textField.domainOptions = textFieldObject.pickerObjects!
        
        return cell
        
    }
    
}
