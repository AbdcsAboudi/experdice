//
//  ProjectOptionTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectOptionTableViewCell: UITableViewCell {

    @IBOutlet var projectOptionLabel: UILabel!
    @IBOutlet var innerProjectOptionView: UIView!
    @IBOutlet var outerProjectOptionView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
          outerProjectOptionView.makeCorneredViewWith(corenerRadius: 10, and: 1, borderColor: ColorManager.sharedInstance.getBordaerColorForSwitchView())
        
        innerProjectOptionView.makeCorneredViewWith(corenerRadius: 8, and: 0)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
    // MARK: - Cells population Methods
    
    class func populateProjectOptionTableViewCell(textFieldObject : FormOptionObject,indexPath:NSIndexPath, tableView:UITableView) -> ProjectOptionTableViewCell {
        
        let cell:ProjectOptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectOptionTableViewCell") as! ProjectOptionTableViewCell
        
        cell.projectOptionLabel.text = textFieldObject.textFieldTitle
       
        if textFieldObject.data == "Selected"{
            
            cell.innerProjectOptionView.backgroundColor = UIColor.blue
            
        }else{
             cell.innerProjectOptionView.backgroundColor = UIColor.white
        }
       
        return cell
        
    }
    
   
    
}
