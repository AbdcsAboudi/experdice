//
//  ProjectSharedTextFieldTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectSharedTextFieldTableViewCell: UITableViewCell{
    
    @IBOutlet var projectTitleLabel: UILabel!
    @IBOutlet var textField: DTtextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: - Cells population Methods
    
    class func populateProjectSharedTextFieldTableViewCell(textFieldObject : FormOptionObject,indexPath:NSIndexPath, tableView:UITableView) -> ProjectSharedTextFieldTableViewCell {
        
        let cell:ProjectSharedTextFieldTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectSharedTextFieldTableViewCell") as! ProjectSharedTextFieldTableViewCell
        
        cell.projectTitleLabel.text = textFieldObject.textFieldTitle
        
        cell.textField.setTextFieldPlaceHolderColor(title: textFieldObject.textFieldPlaceholder! , color: ColorManager.sharedInstance.getTextFiledTextColor())
        cell.textField.text = textFieldObject.data
        cell.textField.tag = indexPath.row - 1
        cell.textField.sectionIndex = indexPath.section
        cell.textField.textFieldType = textFieldObject.keyboardType!
        
        return cell
        
    }
    
    
    
}
