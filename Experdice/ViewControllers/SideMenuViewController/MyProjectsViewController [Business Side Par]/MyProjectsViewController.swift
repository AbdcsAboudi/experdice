//
//  MyProjectsViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class MyProjectsViewController: DTListViewController {
    
    
    @IBOutlet weak var publshedTableView:UITableView!
    @IBOutlet weak var savedTableView:UITableView!
  
    @IBOutlet weak var savedProjectButton:UIButton!
    @IBOutlet weak var publshedProjectButton:UIButton!
    
    
    var projectArray = [ProjectObject]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        savedTableView.isHidden = true
        title = "My Projects"
        needSideBar = true
        setupTableViewHeader()
        tableView = publshedTableView
        tableViewSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        customizeNavigationBar()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: - TableView Delegate and Data Source
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == (list.count - requestNextPageOffset) {
            getNextPage()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if tableView == publshedTableView{
            
            if !list.isEmpty, let projectObject = list[indexPath.section] as? ProjectObject {
                
                if indexPath.row == 2, projectObject.isOpend == true{
                    
                    tableView.beginUpdates()
                    projectObject.isOpend = false
                    publshedTableView.deleteRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                    publshedTableView.endUpdates()
                    
                    publshedTableView.reloadRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                    
                }else if indexPath.row == 1, projectObject.isOpend == false {
                    
                    publshedTableView.beginUpdates()
                    projectObject.isOpend = true
                    publshedTableView.insertRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                    publshedTableView.endUpdates()
                    
                    publshedTableView.reloadRows(at: [IndexPath(row: 2, section: indexPath.section)], with: .automatic)
                }
            }
            //AA : Go to project details vc
            let projectDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "ProjectDetailsParentViewController") as! ProjectDetailsParentViewController
            
            projectDetailsVc.projectId = (list[indexPath.row] as! ProjectObject).id

            //TODO: - MSUT IMPLEMENT GET PROJECT DETIALS API
            
            self.navigationController?.pushViewController(projectDetailsVc, animated: true)

        } else {
          
            //Saved Table View
            
            //AA : Go to project details vc
            let projectDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "ProjectDetailsParentViewController") as! ProjectDetailsParentViewController
            self.navigationController?.pushViewController(projectDetailsVc, animated: true)
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == publshedTableView {
            
            if !list.isEmpty, let myProjectArray = list[section] as? ProjectObject {
                
                if myProjectArray.isOpend{
                    
                    return 3
                    
                }else {
                    
                    return 2
                }
                
            }
        }else{
            
            return 1
            
        }
        return 0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if !list.isEmpty{
            return list.count
        }
        return 0

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == publshedTableView {
            
            if indexPath.row == 0 {
                
                return 90
                
            }else if indexPath.row == 1{
                if !list.isEmpty,let projectObject = list[indexPath.section] as? ProjectObject {
                    if projectObject.isOpend {
                        return 45
                    }else {
                        return 30
                    }
                }
            }else if indexPath.row == 2{
            
                return 30
                
            }else{
                
                return 0
                
            }
         
        }else{
            
            return 90

        }
        return 0
    }
    
    override func populateTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !list.isEmpty, let projectObject = list[indexPath.section] as? ProjectObject {
            if tableView == publshedTableView {
                
                if indexPath.row == 0 {
                    
                    let cell = HeaderHomeTableViewCell.populateHomeHeaderCell(projectObject: projectObject, tableView: publshedTableView, indexPath: indexPath as NSIndexPath)
                    
                    cell.optionButton.tag = indexPath.section
                    
                    cell.optionButton.addTarget(self, action: #selector(showActionSheetSelector), for: .touchUpInside)
                    
                    return cell
                    
                }else if indexPath.row == 1 {
                    
                    if projectObject.isOpend {
                        
                        let cell = MiddleHomeTableViewCell.populateHomeMiddleCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                        
                        return cell
                        
                    }else{
                        
                        let cell = ButtomHomeTableViewCell.populateHomeButtomCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                        
                        return cell
                        
                    }
                    
                }else if indexPath.row == 2{
                    
                    let cell = ButtomHomeTableViewCell.populateHomeButtomCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                    
                    return cell
                    
                }else{
                    
                    return UITableViewCell()
                    
                }
                
            }else {
                
                let cell = HeaderHomeTableViewCell.populateHomeHeaderCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    //MARK: - Helping Method
    
    func setupParameters()->[String:Any]{
        
        var userId = 0
        var parameters = [String: Any]()
        
        if let user = DTCredentialManager.credentialSharedInstance.user as? UserObject {
            
            userId = user.profile.businessId!
            
            parameters["clientId"] = String(userId)
            parameters["industryID"] = "0"
            parameters["onlyForClient"] = "1"
            parameters["budget"] = "0"
            parameters["cityID"] = "0"
            parameters["dateFrom"] = "0"
            parameters["dateTo"] = "0"
            parameters["statusID"] = "0"
            parameters["pageSize"] = String(limit)
            parameters["pageNo"] = String(currentPage)
        }
        
        return parameters
        
    }
    
    func setupProjectArray(array:[ProjectObject]){
        
        for project in array {
            
            list.append(project)
            
        }
        
    }
    override func tableViewSetup() {
        
        super.tableViewSetup()
        
        setupPubleshdTableView()
        setupSavedTableView()
    }
    
    override func beginRequest() {
         callGetMyProjectAPI()
    }
    
    func setupPubleshdTableView(){
        
        publshedTableView.register(UINib(nibName: "HeaderHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpertHeaderCell")
        
        publshedTableView.register(UINib(nibName: "MiddleHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "MiddleCell")
        
        publshedTableView.register(UINib(nibName: "ButtomHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtomCell")
    
        publshedTableView.separatorStyle = .none
        
        
    }
    
    func setupSavedTableView(){
        
        savedTableView.register(UINib(nibName: "HeaderHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpertHeaderCell")
        
        savedTableView.separatorStyle = .none
        
    }
    

    func setupTableViewHeader(){
      
        savedProjectButton.addTarget(self, action:#selector(showSavedSelector), for: .touchUpInside)
        publshedProjectButton.addTarget(self, action:#selector(showPublshedSelector), for: .touchUpInside)
        
    }
    
    func showPublshedSelector(){
        
        publshedTableView.isHidden = false
        savedTableView.isHidden = true
        
        publshedProjectButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonColorSelected()
        publshedProjectButton.setTitleColor(DTHelper.sharedInstance.colorWithHexString("#F4D45E"), for: .normal)
        
        savedProjectButton.backgroundColor  = ColorManager.sharedInstance.headerViewButtonDefault()
        savedProjectButton.setTitleColor(.white, for: .normal)
        
       tableView = publshedTableView
    }
    
    func showSavedSelector(){
        
        publshedTableView.isHidden = true
        savedTableView.isHidden = false
        
        savedProjectButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonColorSelected()
        savedProjectButton.setTitleColor(DTHelper.sharedInstance.colorWithHexString("#F4D45E"), for: .normal)

        publshedProjectButton.backgroundColor  = ColorManager.sharedInstance.headerViewButtonDefault()
        publshedProjectButton.setTitleColor(.white, for: .normal)

       tableView = savedTableView
    }
    
    func showActionSheetSelector(sender:UIButton){
        
        let alertView = UIAlertController(title: "Choose Please", message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
            alertView.dismiss(animated: true, completion: nil)
        })
        
        let messageAction = UIAlertAction(title: "Message", style: .default, handler: { (alert) in
            
        })
        
//        let addToWatchlistAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
//
//            UIView.transition(with: self.publshedTableView,
//                              duration: 0.30,
//                              options: .curveEaseInOut,
//                              animations: { self.publshedTableView.reloadData() })
//        })
        
        //let reportProjectAction = UIAlertAction(title: "Report Project", style: .default, handler: { (alert) in
            
       // })
        alertView.addAction(cancelAction)
        alertView.addAction(messageAction)
//        alertView.addAction(addToWatchlistAction)
       // alertView.addAction(reportProjectAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    //AS: Fill some project object
    func fillHomeArray(){
       
        
        
        
    }

    //MARK: - Actions

    func addProjectAction() {
        
        //AA : Go to add project vc
        let addProjectVc = AddProjectViewController(nibName: "AddProjectViewController", bundle: nil)
        let addProjectNavVc = UINavigationController(rootViewController: addProjectVc)
        
        self.present(addProjectNavVc, animated: true, completion: nil)
        
    }
    
    //MARK: - Design Methods

    func customizeNavigationBar() {
     
        let addProjectButtonTitle = DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Add")
        
        let rightBarButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 25))
        rightBarButton.setTitleColor(UIColor.white, for: .normal)
        rightBarButton.addTarget(self, action: #selector(self.addProjectAction), for: .touchUpInside)
        rightBarButton.setTitle(addProjectButtonTitle, for: .normal)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBarButton)
        
    }
    
    //MARK: - API Calls
    
    
    
    func callGetMyProjectAPI(){
        
        let parameters = setupParameters()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getProjectList, requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any]{
                    
                    let project = DTParser.parseProjectObjectArray(from: response) as! [ProjectObject]
                    
                    viewController.setupProjectArray(array:project)
                    
                    
                    self?.endRequest(hasMore: true)
                    
                }
                
            }else{
                viewController.endRequestWithFailure()
                
                
            }
        }
    }
    
    
}
