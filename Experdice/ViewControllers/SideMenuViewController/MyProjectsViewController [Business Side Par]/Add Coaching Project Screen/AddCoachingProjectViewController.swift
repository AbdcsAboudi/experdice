//
//  AddCoachingProjectViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class AddCoachingProjectViewController: DTViewController, UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate{
    
    @IBOutlet var addCoachingProjectTableView: UITableView!

    var sectionsTitlesArray = [String]()
    var section0TextFieldsObjectsArray = [FormOptionObject]()
    var section1TextFieldsObjectsArray = [FormOptionObject]()
    var section2TextFieldsObjectsArray = [FormOptionObject]()
    var section3TextFieldsObjectsArray = [FormOptionObject]()
    var section4TextFieldsObjectsArray = [FormOptionObject]()
    var sectionsDictionary = [String : [FormOptionObject]]()
    var languageObjectName = [String]()
    var paymentMethodObjectName = [String]()
    var genderTypeName = [String]()
    var genderTypeId = [Int]()
    var paymentMethodObjectId = [Int]()
    var languageObjectId = [Int]()
    var countryName = [String]()
    var countryID = [Int]()
    var city = [String]()
    var cityID = [Int]()
    var degreesName = [String]()
    var degreesId = [Int]()
    var businessTypesName = [String]()
    var businessTypesId = [Int]()
    var genderTypeObjectArray = [GenderObject]()
    var languageObjectArray = [LanguagesObject]()
    var degreesObjectArray = [EducationDegreeObject]()
    var paymentMethodsArray = [PaymentMethodsObject]()
    var businessTypes = [BusinessTypeObject]()
    var countryDomianOption = [DTDomainObject]()
    var cityDomainOption = [DTDomainObject]()
    var genderDomainOption = [DTDomainObject]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
       
        fillSectionsArray()
        setupAddProjectTableView()
        
        fillGenderTypeArray()
        callCountryAPI()
        getDegressAPI()
        getLanguageAPI()
        getBusinessTypesIndustryAPI()
        getPaymentMethodsAPI()
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.needSideBar = false
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Helping Methods
    
    func fillGenderTypeArray(){
       
        genderTypeObjectArray.append(GenderObject(id: 1, genderName: "Male"))
        genderTypeObjectArray.append(GenderObject(id: 2, genderName: "Female"))
        setupGenderArray()

    }
    
    func setupGenderArray(){
        
        section4TextFieldsObjectsArray[4].pickerObjects?.removeAll()
        
        for genderObject in genderTypeObjectArray {
            
            genderTypeName.append(genderObject.genderName!)
            genderTypeId.append(genderObject.id!)
        }
        
        for genderName in genderTypeName {
            
            var index = 0
            section4TextFieldsObjectsArray[4].pickerObjects?.append(DTDomainObject(id:genderTypeId[index], display: genderName))
            index += 1
        }
        
    }
    
    func setupCityArray(){
        
        var index = 0
        for cityName in city {
            
            cityDomainOption.append(DTDomainObject(id: cityID[index] , display: cityName))
            
            index += 1
        }
    }
    
    func setupCountryArray(){
        
        var index = 0
        for countryName in countryName {
            
            countryDomianOption.append(DTDomainObject(id: countryID[index], display: countryName))
            
            index += 1
        }
        
    }
    
    
    func countryTextFildValueChange(_ textField:DTtextField){
        
        city.removeAll()
        cityID.removeAll()
        callGetCityAPI(id:textField.selectdIndex+1)
        
    }
    
    func setupLanguageArray(){
        
        section4TextFieldsObjectsArray[3].pickerObjects?.removeAll()
        
        for languageObject in languageObjectArray {
            
            languageObjectName.append(languageObject.name!)
            languageObjectId.append(languageObject.id!)
        }
        
        for languageName in languageObjectName {
            
            var index = 0
            section4TextFieldsObjectsArray[3].pickerObjects?.append(DTDomainObject(id:languageObjectId[index], display: languageName))
            index += 1
        }
        
    }
    
    func setupPaymentMethodsArray(){
        
        section3TextFieldsObjectsArray[0].pickerObjects?.removeAll()
        
        for paymentMethod in paymentMethodsArray {
            
            paymentMethodObjectName.append(paymentMethod.name!)
            paymentMethodObjectId.append(paymentMethod.id!)
        }
        
        for  paymentMethodName in paymentMethodObjectName {
            
            //var index = 0
            
            section3TextFieldsObjectsArray[0].pickerObjects?.append(DTDomainObject(id:paymentMethodObjectId[0], display: paymentMethodName))
            
        }
        
    }
    
    func setupBusinessTypeArray(){
        
        section4TextFieldsObjectsArray[0].pickerObjects?.removeAll()
        
        for businessType in businessTypes {
            
            businessTypesName.append(businessType.name!)
            businessTypesId.append(businessType.id!)
        }
        
        for  businessName in businessTypesName {
            
           // var index = 0
            
            section4TextFieldsObjectsArray[0].pickerObjects?.append(DTDomainObject(id:businessTypesId[0], display: businessName))
            
        }
        
    }
    
    func setupDegressArray(){
        
        section4TextFieldsObjectsArray[2].pickerObjects?.removeAll()
        
        for degrees in degreesObjectArray {
            
            degreesName.append(degrees.degreeName!)
            degreesId.append(degrees.id!)
        }
        
        for  degreesName in degreesName {
            
           // var index = 0
            
            section4TextFieldsObjectsArray[2].pickerObjects?.append(DTDomainObject(id:degreesId[0], display: degreesName))
            
        }
        
    }
    
    func setupParameters()->[String:Any]{
        
        
        var parameters = [String: Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        
        parameters["ClientId"] = user?.profile.businessId
        parameters["ProjectId"] = 0
        parameters["Title"] = section0TextFieldsObjectsArray[0].data
        
        parameters["ProjectTypeId"] = 2
       
        if let data = section0TextFieldsObjectsArray[1].data {
            
            parameters["ProjectCategoryId"] = data
            
        }
        
        
        
        parameters["Objective"] = section1TextFieldsObjectsArray[0].data
        
        parameters["GoalDescription "] = section1TextFieldsObjectsArray[1].data
        parameters["GoalAchievements"] = section1TextFieldsObjectsArray[2].data
        parameters["GoalNotes"] = section1TextFieldsObjectsArray[3].data
        
        if let data = section2TextFieldsObjectsArray[0].data {
            
            parameters["CountryId"] = countryID[countryName.index(of: data)!]
            
        }
        if let data = section2TextFieldsObjectsArray[0].secondData {
            
            parameters["CityId"] = cityID[city.index(of: data)!]
            
        }
        parameters["ActualBeginDate"] = section2TextFieldsObjectsArray[1].data
        parameters["SessionFrequancyRate"] = section2TextFieldsObjectsArray[2].data
        
        if let data = section3TextFieldsObjectsArray[0].data {
            
            parameters["PaymentMethodId"] = paymentMethodObjectId[paymentMethodObjectName.index(of: data)!]
        }
        
        parameters["BudgetRate"] = section3TextFieldsObjectsArray[1].data
        
        if let data = section4TextFieldsObjectsArray[0].data {
            
            parameters["BusinessIndustryId"] = businessTypesId[businessTypesName.index(of:data)!]
        }
        
        parameters["ExperienceYears"] = section4TextFieldsObjectsArray[1].data

      
        
        if let data = section4TextFieldsObjectsArray[4].data {
            
            parameters["PerfTrainerGenderId"] = genderTypeId[genderTypeName.index(of:data)!]
        }
        
        
        if let data = section4TextFieldsObjectsArray[3].data {
            
            parameters["ContentLanguageId"] = languageObjectId[(languageObjectName.index(of: data))!]
            
        }
        
     
        
        return parameters
        
    }
    
    func setupAddProjectTableView() {
        
        addCoachingProjectTableView.delegate = self
        addCoachingProjectTableView.dataSource = self
        addCoachingProjectTableView.estimatedRowHeight = 45
        addCoachingProjectTableView.rowHeight = UITableViewAutomaticDimension
        
        addCoachingProjectTableView.register(UINib(nibName: "ProjectSectionTitleTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSectionTitleTableViewCell")
        addCoachingProjectTableView.register(UINib(nibName: "ProjectSharedTextFieldTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSharedTextFieldTableViewCell")
        addCoachingProjectTableView.register(UINib(nibName: "ProjectSharedTexViewTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectSharedTexViewTableViewCell")
        addCoachingProjectTableView.register(UINib(nibName: "ProjectDropDownTextFieldTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectDropDownTextFieldTableViewCell")
        addCoachingProjectTableView.register(UINib(nibName: "ProjectDropSownMultipleTextFieldsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectDropSownMultipleTextFieldsTableViewCell")
        addCoachingProjectTableView.register(UINib(nibName: "ProjectOptionTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectOptionTableViewCell")
        addCoachingProjectTableView.register(UINib(nibName: "CreateProjectTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "CreateProjectTableViewCell")
        
    }
    
    func fillSectionsArray() {
        
        sectionsTitlesArray.append(contentsOf: ["Main Details","Project Goals","Location & Time Frame","Budget","Candidate",""])
        
        section0TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Project Title", textFieldPlaceholder: "Enter Project Title", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:"")])
        
        section1TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Main Objectives", textFieldPlaceholder: "Write the main objectives", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:""),
            
            FormOptionObject(textFieldTitle: "Description", textFieldPlaceholder: "", formOptionType: FormOptionType.textView, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:""),
            
            FormOptionObject(textFieldTitle: "Pin Point the Problem", textFieldPlaceholder: "", formOptionType: FormOptionType.textView, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:""),
            
            FormOptionObject(textFieldTitle: "Notes", textFieldPlaceholder: "", formOptionType: FormOptionType.textView, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:"")])
        
        section2TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Location", textFieldPlaceholder: "", formOptionType: FormOptionType.multipleDropDownsTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            FormOptionObject(textFieldTitle: "Start Date", textFieldPlaceholder: "", formOptionType: FormOptionType.regularTextField, keyboardType: TextFieldType.birthDay, rowTag: 0,data:"",secondData:"")])
        
        section3TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Payment Method", textFieldPlaceholder: "Select Payment Method", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            FormOptionObject(textFieldTitle: "Rate / Budget", textFieldPlaceholder: "Set Rate / Budget", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")])])
        
        section4TextFieldsObjectsArray.append(contentsOf: [
            FormOptionObject(textFieldTitle: "Field Of Experience", textFieldPlaceholder: "Select Field Of Experience", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Years Of Experience", textFieldPlaceholder: "Select Years Of Experience", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Education", textFieldPlaceholder: "Choose Education", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Language", textFieldPlaceholder: "Choose Language", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Preferred Coach Gender", textFieldPlaceholder: "Select Coach Gender", formOptionType: FormOptionType.dropDownTextField, keyboardType: TextFieldType.domain, rowTag: 0,pickerObjects : [DTDomainObject(id: 0, display: "1"),DTDomainObject(id: 0, display: "2")]),
            
            FormOptionObject(textFieldTitle: "Please describe your ideal Candidate for this project", textFieldPlaceholder: "", formOptionType: FormOptionType.textView, keyboardType: TextFieldType.string, rowTag: 0,data:"",secondData:"")])
        
        sectionsDictionary["section0"] = section0TextFieldsObjectsArray
        sectionsDictionary["section1"] = section1TextFieldsObjectsArray
        sectionsDictionary["section2"] = section2TextFieldsObjectsArray
        sectionsDictionary["section3"] = section3TextFieldsObjectsArray
        sectionsDictionary["section4"] = section4TextFieldsObjectsArray
 
    }
    
    //MARK:- TextView Delgate
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let dtTextView = textView as! DTTextView
        
        if let arraySectionObject = sectionsDictionary["section\(dtTextView.sectionIndex)"]{
            
            arraySectionObject[textView.tag].data = textView.text
        }
        
        
    }
    
    //MARK:- TextFiled Delgate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let dtTextField = textField as! DTtextField
        
        if let arraySectionObject = sectionsDictionary["section\(dtTextField.sectionIndex)"] {
            
            if arraySectionObject[dtTextField.tag].formOptionType == FormOptionType.multipleDropDownsTextField{
                
                if dtTextField.multipleFirstTextFieldTag != -1{
                    
                    arraySectionObject[dtTextField.tag].data = textField.text
                    
                    if  dtTextField.tag == 0 && dtTextField.sectionIndex == 2 {
                        
                        cityDomainOption.removeAll()
                        city.removeAll()
                        cityID.removeAll()
                        callGetCityAPI(id: dtTextField.selectdIndex + 1)
                    }
                    
                }else{
                    arraySectionObject[dtTextField.tag].secondData = textField.text
                }
            }else{
                arraySectionObject[dtTextField.tag].data = textField.text
            }
            if arraySectionObject[dtTextField.tag].data == ""{
                
                let alertView = EXAlertViewController(title: "Please Fill * \(arraySectionObject[dtTextField.tag].textFieldTitle ?? "All Filed")*", twoButton: false, alertMessage: "", buttonTitle: "Done", zeroHighView: false, centerTextAlertMessage: true, speratorViewEnable: true)
                self.present(alertView, animated: true, completion: nil)
                
                
            }
        }
       
    }
    
    //MARK: - Selector And Actiion
    
    func createProjectButtonSelector(){
        
        createProjectAPI()
        
    }
    
    func saveProjectButtonSelector(){
        
        //here save projetc api
        
    }
    
    //MARK:- TableView Delegate && DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionsTitlesArray.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  section == 3 || section == 2{
            
            return 3
            
        }else if section == 0 {
         
            return 2
            
        }else if section == 1{
            
            return 5
            
        }else if section == 4{
            
            return 7
            
        }else if section == 5{
            
            return 1
            
        }else{
            
            return 0
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionKeyString = "section\(indexPath.section)"

        if indexPath.row == 0 {
            
            if indexPath.section == 5 {
                
                let cell = CreateProjectTableViewCell.populateCreateProjectTableViewCell(indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.createProjectButtonContainerButton.addTarget(self, action: #selector(createProjectButtonSelector), for: .touchUpInside)
                
                cell.saveProjectButtonContainerButton.addTarget(self, action: #selector(saveProjectButtonSelector), for: .touchUpInside)
                
                return cell
                
            }else{
                
                let cell = ProjectSectionTitleTableViewCell.populateProjectSectionTitleTableViewCell(projectTitleString: sectionsTitlesArray[indexPath.section], indexPath: indexPath as NSIndexPath, tableView: tableView)
                return cell
                
            }
            
        }else{
            
            let sectionFormOptionsArray = sectionsDictionary[sectionKeyString]
            let formObject = sectionFormOptionsArray?[indexPath.row - 1]
            
            if formObject?.formOptionType == FormOptionType.regularTextField {
                
                let cell = ProjectSharedTextFieldTableViewCell.populateProjectSharedTextFieldTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.textField.delegate = self

                
                return cell
                
            }else if formObject?.formOptionType == FormOptionType.dropDownTextField {
                
                let cell = ProjectDropDownTextFieldTableViewCell.populateProjectDropDownTextFieldTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.textField.delegate = self
              
                return cell
                
            }else if formObject?.formOptionType == FormOptionType.textView {
                
                let cell = ProjectSharedTexViewTableViewCell.populateProjectSharedTexViewTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                cell.textViewData.delegate = self
                
                return cell
                
            }else if formObject?.formOptionType == FormOptionType.multipleDropDownsTextField {
                
                let cell = ProjectDropSownMultipleTextFieldsTableViewCell.populateProjectDropSownMultipleTextFieldsTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                
                cell.firstTextField.domainOptions = countryDomianOption
                
                //AA : To identify each textField with the custom tags
                //AA ,Ex : multipleFirstTextFieldTag = -1 then the second textField
                cell.firstTextField.multipleFirstTextFieldTag = 0
                cell.firstTextField.multipleSecondTextFieldTag = -1
                
                cell.secondTextField.multipleFirstTextFieldTag = -1
                cell.secondTextField.multipleSecondTextFieldTag = 0
                
                cell.secondTextField.domainOptions = cityDomainOption
                
                
                cell.secondTextField.delegate = self
                cell.firstTextField.delegate = self
                
                
                return cell
                
                
            }else if formObject?.formOptionType == FormOptionType.pollOption {
                
                let cell = ProjectOptionTableViewCell.populateProjectOptionTableViewCell(textFieldObject: formObject!, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
                
            }else{
                
                return UITableViewCell()
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let sectionKeyString = "section\(indexPath.section)"

        if indexPath.row == 0 {
            
            if indexPath.section == 5 {
                
                return 90
                
            }else{
                
                return 48
                
            }
            
        }else{
            
            let sectionFormOptionsArray = sectionsDictionary[sectionKeyString]
            let formObject = sectionFormOptionsArray?[indexPath.row - 1]
            
            if (formObject?.formOptionType == FormOptionType.regularTextField) || (formObject?.formOptionType == FormOptionType.dropDownTextField) || (formObject?.formOptionType == FormOptionType.multipleDropDownsTextField) || (formObject?.formOptionType == FormOptionType.pollOption){
                
                return 70
                
            }else if formObject?.formOptionType == FormOptionType.textView {
                
                return 200
                
            }else{
                
                return 0
                
            }
            
        }
    }
    
    //MARK: - API Call
    
    func createProjectAPI(){
        
        let parm = setupParameters()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .createOrEditProject, requestParams:parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                viewController.dismiss(animated: true, completion: nil)
                
            }
        }
        
    }
    
    func getDegressAPI(){
        
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getDegrees, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            //MBProgressHUD.hide(for: viewController.view, animated: true)
            
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let degrees = DTParser.parseDegreesOjectArray(from: response)
                    
                    viewController.degreesObjectArray = degrees //as! [EducationDegreeObject]
                    
                }
                viewController.setupDegressArray()
            }
        }
        
    }
    
    func getBusinessTypesIndustryAPI(){
        
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .businessTypes, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            //MBProgressHUD.hide(for: viewController.view, animated: true)
            
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let businessTypesArray = DTParser.parseBusinessTypeOjectArray(from: response)
                    
                    viewController.businessTypes = businessTypesArray //as! [BusinessTypeObject]
                    
                }
                viewController.setupBusinessTypeArray()
            }
        }
        
    }
    
    func callGetCityAPI(id:Int){
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCity, requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let cityArray = DTParser.parseCityObjectArray(from: response)
                    
                    for city in cityArray  {
                        
                        if let cityName = city.name {
                            
                            self?.city.append(cityName)
                            
                        }
                        
                        if let cityID = city.id {
                            
                            self?.cityID.append(cityID)
                        }
                        
                    }
                    self?.setupCityArray()
                    
                    
                    viewController.section2TextFieldsObjectsArray[0].pickerObjects = viewController.cityDomainOption
                    viewController.addCoachingProjectTableView.reloadData()
                    
                }
            }
        }
    }
    
    func callCountryAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCountry, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let countryArray = DTParser.parseCountryObjectArray(from: response)
                    
                    for country in countryArray  {
                        
                        if let countryName = country.name {
                            
                            self?.countryName.append(countryName)
                            
                        }
                        
                        if let countryID = country.id {
                            
                            self?.countryID.append(countryID)
                        }
                        
                    }
                    self?.setupCountryArray()
                }
                
            }
        }
        
    }
    
    
    func getLanguageAPI(){
        
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getLanguages, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            //MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let languageArray = DTParser.parseLanguagesObjectFromResponseArray(from: response)
                    
                    viewController.languageObjectArray = languageArray as! [LanguagesObject]
                    
                }
                viewController.setupLanguageArray()
            }
        }
        
    }
    
    func getPaymentMethodsAPI(){
        
        //MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getPaymentMethods, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            //MBProgressHUD.hide(for: viewController.view, animated: true)
            
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let paymentMethodsArray = DTParser.parsePaymentMethodsFromResponseArray(from: response)
                    
                    viewController.paymentMethodsArray = paymentMethodsArray as! [PaymentMethodsObject]
                    
                }
                viewController.setupPaymentMethodsArray()
            }
        }
        
    }
    
    
    
}
