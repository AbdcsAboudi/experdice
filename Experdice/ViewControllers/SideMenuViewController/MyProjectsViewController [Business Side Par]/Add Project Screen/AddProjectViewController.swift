//
//  AddProjectViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//


import UIKit

class AddProjectViewController: DTViewController, UITableViewDelegate, UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {
   
   
    @IBOutlet var addProjectTableView: UITableView!
   
    @IBOutlet var nextButtonContainerView: UIView!
    
    var projectTypePicker: UIPickerView! = UIPickerView()
    var projectTypeTextFied = UITextField()
    var projectTypesArray = [ProjectTypeObject]()
    var selectedProjectTypeObject = ProjectTypeObject(projectTypeId: -1, projectTypeName: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //AA : To fill project types array
        fillProjectTypesArray()
        
        projectTypePicker!.delegate = self
        projectTypePicker!.dataSource = self
        projectTypePicker!.backgroundColor = UIColor.lightGray
        
        //AA : To cornarize container view
        nextButtonContainerView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        setupAddProjectTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.needSideBar = false
        super.viewWillAppear(animated)
        
        customizeNavigationBar()
        
    }
    
    //MARK: - Actions
    
    func cancelAction() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func projectTypeTextFiledButtonAction() {
        
        projectTypeTextFied.becomeFirstResponder()
        
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        //AA : To go to the desired selected type of projects
        var addProjectVc = DTViewController()
        
        if selectedProjectTypeObject.projectTypeId != -1 {
            
            if selectedProjectTypeObject.projectTypeId == 1 {
                
            addProjectVc = AddTeambuildingProjectViewController(nibName: "AddTeambuildingProjectViewController",bundle: nil)
                
                
                
               
            }else if selectedProjectTypeObject.projectTypeId == 2 {
                
                addProjectVc = AddCoachingProjectViewController(nibName: "AddCoachingProjectViewController", bundle: nil)
                
            }else if selectedProjectTypeObject.projectTypeId == 3 {
                
                addProjectVc = AddConsultationProjectViewController(nibName: "AddConsultationProjectViewController", bundle: nil)
                
            }else if selectedProjectTypeObject.projectTypeId == 4 {
                
                addProjectVc = AddTrainingProjectViewController(nibName: "AddTrainingProjectViewController", bundle: nil)
                
            }else{
                
                addProjectVc = AddSpeakersProjectViewController(nibName: "AddSpeakersProjectViewController", bundle: nil)
                
            }
            
            self.navigationController?.pushViewController(addProjectVc, animated: true)
        }else{
            
            let alertView = EXAlertViewController(title: "Must Select Project Type", twoButton: false, alertMessage: "", buttonTitle: "Done", zeroHighView: false, centerTextAlertMessage: true, speratorViewEnable: true)
            self.present(alertView, animated: true, completion: nil)
         
        }
        
    }
    
    // MARK: - Helping Methods
  
    func setupAddProjectTableView() {
        
        addProjectTableView.delegate = self
        addProjectTableView.dataSource = self
        addProjectTableView.estimatedRowHeight = 45
        addProjectTableView.rowHeight = UITableViewAutomaticDimension
        
        addProjectTableView.register(UINib(nibName: "ProjectTypeTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ProjectTypeTableViewCell")
        
    }
    
    func fillProjectTypesArray(){
        
        projectTypesArray.append(contentsOf: [
            ProjectTypeObject(projectTypeId: 1, projectTypeName: "Teambuilding"),
            ProjectTypeObject(projectTypeId: 2, projectTypeName: "Coaching"),
            ProjectTypeObject(projectTypeId: 3, projectTypeName: "Consultation"),
            ProjectTypeObject(projectTypeId: 4, projectTypeName: "Training"),
            ProjectTypeObject(projectTypeId: 5, projectTypeName: "Speakers")])
        
    }
    
    //MARK:- TableView Delegate && DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ProjectTypeTableViewCell.populateProjectTypeTableViewCell(indexPath: indexPath as NSIndexPath, tableView: tableView)
       
        cell.projectTypeTextFieldButton.addTarget(self, action: #selector(self.projectTypeTextFiledButtonAction), for: .touchUpInside)
        cell.projectTypeTextField.inputView = projectTypePicker
        projectTypeTextFied = cell.projectTypeTextField
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return projectTypesArray.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        return projectTypesArray[row].projectTypeName
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        //AA : To get reference of the selected project type
        selectedProjectTypeObject = projectTypesArray[row]
        
        projectTypeTextFied.text = projectTypesArray[row].projectTypeName
        
    }
    
    //MARK: - Design Methods
    
    func customizeNavigationBar() {
        
        let button = UIBarButtonItem(title: "Cancel",
                                     style: .plain,
                                     target: self,
                                     action: #selector(AddProjectViewController.goBack))
        self.navigationItem.leftBarButtonItem = button
        
    }
    
    func goBack(){
        
       self.dismiss(animated: true, completion: nil)
    
    }
    
}
