//
//  ProjectTypeTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectTypeTableViewCell: UITableViewCell {

    @IBOutlet var projectTypeTextField: UITextField!
    @IBOutlet var projectTypeTextFieldButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    // MARK: - Cells population Methods
    
    class func populateProjectTypeTableViewCell(indexPath:NSIndexPath, tableView:UITableView) -> ProjectTypeTableViewCell {
        
        let cell:ProjectTypeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProjectTypeTableViewCell") as! ProjectTypeTableViewCell
        
        return cell
        
    }

}
