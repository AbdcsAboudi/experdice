//
//  BidOnProjectViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class BidOnProjectViewController: DTViewController,ContactDetailsDeleagte {
  
    @IBOutlet weak var submitButtonView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var durationToDateTextField: UITextField!
    @IBOutlet weak var durationFromDateTextField: UITextField!
    @IBOutlet weak var bidAmountTextField: UITextField!
    
    var projectId:Int?
    var fromDatePikcker = UIDatePicker()
    var toDatePikcker = UIDatePicker()
    
    override func viewDidLoad() {
        super.needSideBar = false
        super.viewDidLoad()
        setupView()
        title = "Bid On Project"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
   
    
    // MARK: - Helping Methods
    
    func fromDatePickerValueChange(sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd"
        durationFromDateTextField.text = dateFormatter.string(from: sender.date)
    }
    
    func toDatePickreValueChange(sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd"
        durationToDateTextField.text = dateFormatter.string(from: sender.date)
    }
    
    func setupView(){
        submitButtonView.makeCorneredViewWith(corenerRadius: 10, and: 1)
        noteTextView.layer.cornerRadius = 10
        submitButton.addTarget(self, action: #selector(bidOnProjectAPI), for: .touchUpInside)
        fromDatePikcker.addTarget(self, action: #selector(fromDatePickerValueChange), for: .valueChanged)
        toDatePikcker.addTarget(self, action: #selector(toDatePickreValueChange), for: .valueChanged)
        durationToDateTextField.inputView = toDatePikcker
        durationFromDateTextField.inputView = fromDatePikcker
        toDatePikcker.datePickerMode = .date
        fromDatePikcker.datePickerMode = .date
        bidAmountTextField.setTextFieldPlaceHolderColor(title: "Enrer Bid Amount", color:UIColor(hex: "#006B88"))
        durationFromDateTextField.setTextFieldPlaceHolderColor(title: "From", color:UIColor(hex: "#006B88"))
        durationToDateTextField.setTextFieldPlaceHolderColor(title: "To", color:UIColor(hex: "#006B88"))
        
    }
    
    func showAlertView(){
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        let alertMassage = "Please Complete Your Profile To Bid Projects."
        
        let alertView = EXAlertViewController(title: "Hello! \(user?.profile.profileFirstName ?? "")", twoButton: false, alertMessage: alertMassage, buttonTitle: "Complete Your Profile", zeroHighView: false, centerTextAlertMessage: false,speratorViewEnable:true)
        
        self.present(alertView, animated: true, completion: nil)
        alertView.singalOkeyButtonClicked = {
            
            if #available(iOS 10.0, *) {
                Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false, block: { (timer) in
                    let contactDetails = ContactDetailsViewController(nibName: "ContactDetailsViewController", bundle: nil)
                    let navVC = UINavigationController(rootViewController: contactDetails)
                    contactDetails.contactDetailsDeleagte = self
                    contactDetails.isFromBidScreen = true
                    self.present(navVC, animated: true, completion: nil)
                })
            } else {
                // Fallback on earlier versions
            }
        }
    }
  
    func customizeNavigationBar() {
        let rightBarButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        rightBarButton.setTitleColor(UIColor.white, for: .normal)
        rightBarButton.setImage(#imageLiteral(resourceName: "closeEdit"), for: .normal)
        rightBarButton.addTarget(self, action: #selector(closeSelector), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBarButton)
    }
    
    @objc func closeSelector(){
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - API Call
    
    func bidOnProjectAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        var parms = [String:Any]()
        
        //        ExpertId*
        //        ProjectId*
        //        DateFrom
        //        DateTo
        //        Note
        //        Amount
        
        parms["ExpertId"] = user?.profile.expertId
        parms["ProjectId"] = projectId
        parms["DateFrom"] = durationFromDateTextField.text
        parms["DateTo"] = durationToDateTextField.text
        parms["Note"] = noteTextView.text
        parms["Amount"] = bidAmountTextField.text
        
        if let cityName = user?.cityId,let countryName = user?.countryId {
            if cityName == 0 || countryName == 0 {
                MBProgressHUD.hide(for: view, animated: true)
                showAlertView()
                return
            }
        }
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .bidOnProject, requestParams: parms)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                viewController.navigationController?.popViewController(animated: true)
            }else{
                let alert = APIClient.sharedInstance.makeAlertViewForResponseDictionary(response: apiResponse)
                viewController.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func isCompletedHisProfile(isChange: Bool) {
        if isChange {
            bidOnProjectAPI()
        }else{
            showAlertView()
        }
    }

    
    
}
