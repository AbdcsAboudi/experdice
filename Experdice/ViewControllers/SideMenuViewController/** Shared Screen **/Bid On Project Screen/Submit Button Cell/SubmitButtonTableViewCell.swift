//
//  SubmitButtonTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class SubmitButtonTableViewCell: UITableViewCell {

    @IBOutlet var submitButtonContainerView: UIView!

    @IBOutlet weak var submitButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        submitButtonContainerView.makeCorneredViewWith(corenerRadius: 20, and: 0)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: - Cells population Methods
    
    class func populateSubmitButtonTableViewCell(indexPath:NSIndexPath, tableView:UITableView) -> SubmitButtonTableViewCell {
        
        let cell:SubmitButtonTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SubmitButtonTableViewCell") as! SubmitButtonTableViewCell
        
        return cell
        
    }
    
    
    
    
    

}
