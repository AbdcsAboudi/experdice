//
//  FindExpertsViewController.swift
//  Experdice
//
//  Created by Yousef ALselawe on 9/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class FindExpertsViewController: DTViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var findExpertsTableView: UITableView!

     var findExpertsArray = [ExeprtObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupFindExpertTableView()
    
        title = "Find Experts"
    }
     
    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = false
        needSettingButton = false
        
        
        super.viewWillAppear(true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupFindExpertTableView(){
        
        findExpertsArray.append(ExeprtObject(userImage: #imageLiteral(resourceName: "myProfileImage"), userName: "Yousf Selawi", addressLabel: "Amman, Jordan", jopTitleLabel: "Ios Developer"))
        findExpertsArray.append(ExeprtObject(userImage: #imageLiteral(resourceName: "myProfileImage"), userName: "Ahmad Sallam", addressLabel: "Amman, Jordan", jopTitleLabel: "Ios Developer"))
        findExpertsArray.append(ExeprtObject(userImage: #imageLiteral(resourceName: "myProfileImage"), userName: "Yousf Selawi", addressLabel: "Amman, Jordan", jopTitleLabel: "Ios Developer"))
        findExpertsArray.append(ExeprtObject(userImage: #imageLiteral(resourceName: "myProfileImage"), userName: "Ahmad Sallam", addressLabel: "Amman, Jordan", jopTitleLabel: "Ios Developer"))
        findExpertsArray.append(ExeprtObject(userImage: #imageLiteral(resourceName: "myProfileImage"), userName: "Yousf Selawi", addressLabel: "Amman, Jordan", jopTitleLabel: "Ios Developer"))
        findExpertsArray.append(ExeprtObject(userImage: #imageLiteral(resourceName: "myProfileImage"), userName: "Ahmad Sallam", addressLabel: "Amman, Jordan", jopTitleLabel: "Ios Developer"))
        findExpertsArray.append(ExeprtObject(userImage: #imageLiteral(resourceName: "myProfileImage"), userName: "Yousf Selawi", addressLabel: "Amman, Jordan", jopTitleLabel: "Ios Developer"))
        findExpertsArray.append(ExeprtObject(userImage: #imageLiteral(resourceName: "myProfileImage"), userName: "Ahmad Sallam", addressLabel: "Amman, Jordan", jopTitleLabel: "Ios Developer"))
        
        setupFindExpert()
        
    }
    
    
    //MARK: - Helping Methods

    func setupFindExpert(){
    
        findExpertsTableView.delegate = self
        findExpertsTableView.dataSource = self
        
        findExpertsTableView.register(UINib(nibName: "FindExpertsTableViewCell", bundle: nil), forCellReuseIdentifier: "FindExpertCell")
    
    }
    
    //MARK: TableView Delegate && DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return findExpertsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let expertObject = findExpertsArray[indexPath.row]
        
       let cell = FindExpertsTableViewCell.populateInviteExpertCell(exeprtObject: expertObject, tableView: tableView, indexPath: (indexPath as NSIndexPath) as IndexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    
   public class ExeprtObject{
        
        var userImage:UIImage?
        var userName:String?
        var addressLabel:String?
    var jopTitleLabel:String?
    
    init(userImage:UIImage!,userName:String!,addressLabel:String!,jopTitleLabel:String!){
        
            self.userImage = userImage
            self.userName = userName
            self.addressLabel = addressLabel
            self.jopTitleLabel = jopTitleLabel
            
        }
    }

}
