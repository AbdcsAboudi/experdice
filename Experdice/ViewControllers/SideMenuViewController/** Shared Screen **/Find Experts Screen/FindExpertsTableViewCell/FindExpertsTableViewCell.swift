//
//  FindExpertsTableViewCell.swift
//  Experdice
//
//  Created by Yousef ALselawe on 9/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class FindExpertsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var inviteButtonView:UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var jopTitleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Helping Methods
    
    class func populateInviteExpertCell(exeprtObject:FindExpertsViewController.ExeprtObject, tableView : UITableView , indexPath: IndexPath) -> FindExpertsTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FindExpertCell", for: indexPath as IndexPath) as! FindExpertsTableViewCell
        
    
        cell.inviteButtonView.makeCorneredViewWith(corenerRadius: cell.inviteButtonView.frame.height/2, and: 0)
        cell.userImage.image = exeprtObject.userImage?.circleMaskBlue
        cell.userNameLabel.text = exeprtObject.userName
        cell.addressLabel.text = exeprtObject.addressLabel
        cell.jopTitleLabel.text = exeprtObject.jopTitleLabel
        tableView.separatorStyle = .none
        cell.selectionStyle = .none

        return cell

    }
    
    
    
    
    
    
}
