//
//  SettingsCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 9/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

    @IBOutlet weak var switchControl:UISwitch!
    @IBOutlet weak var labelText:UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        switchControl.transform = CGAffineTransform(scaleX: 0.7, y: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    class func populateSettingsCell(settingsObject:SettingObject,tableView:UITableView,indexPath:NSIndexPath) -> SettingsCell{
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsCell
        
        cell.labelText.text = settingsObject.title
        
        if !settingsObject.needSwitch! {
            
           cell.switchControl.isHidden = true
        }
        
        return cell
        
        
        
        
    }
    
}
