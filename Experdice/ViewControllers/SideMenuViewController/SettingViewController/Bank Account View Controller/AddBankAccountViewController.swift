//
//  BankAccountViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 9/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class AddBankAccountViewController: DTViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tableView:UITableView!
    
    var firstSectionObjectArray = [FormOptionObject]()
    var secondSectionObjectArray = [FormOptionObject]()
    
    var sectionTitleArray = [String]()
   
    var firstTextFiledTitleArray = [String]()
    var firstTextFiledPlaceholderArray = [String]()
    
    
    var secondTextFiledTitleArray = [String]()
    var secondTextFiledPlaceholderArray = [String]()
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSections()
        
        setupTableView()
        
        title = "Add a Bank Account"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = false
        needEditButton = true
        super.viewWillAppear(animated)
    }
    
    //MARK: - TableView Delegate & DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return firstSectionObjectArray.count + 1
            
        }else{
            
            return secondSectionObjectArray.count + 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                
                let sectionTitle = sectionTitleArray[indexPath.section]
                
                let cell = ProjectSectionTitleTableViewCell.populateProjectSectionTitleTableViewCell(projectTitleString: sectionTitle, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
            } else{
                let formObject = firstSectionObjectArray[indexPath.row - 1]

                let cell = ProjectSharedTextFieldTableViewCell.populateProjectSharedTextFieldTableViewCell(textFieldObject: formObject, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
            }
            
        } else {
            
            if indexPath.row == 0{
                
                let sectionTitle = sectionTitleArray[indexPath.section]

                let cell = ProjectSectionTitleTableViewCell.populateProjectSectionTitleTableViewCell(projectTitleString: sectionTitle, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
                
            } else {
                
                let formObject = secondSectionObjectArray[indexPath.row - 1]
                
                let cell = ProjectSharedTextFieldTableViewCell.populateProjectSharedTextFieldTableViewCell(textFieldObject: formObject, indexPath: indexPath as NSIndexPath, tableView: tableView)
                
                return cell
            }
            
        }
    }
    
    func setupTableView(){
        
        
        tableView.register(UINib(nibName: "ProjectSharedTextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "ProjectSharedTextFieldTableViewCell")
        
        tableView.register(UINib(nibName: "ProjectSectionTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "ProjectSectionTitleTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorStyle = .none
        
    }
    
    func setupSections(){
        
        
        sectionTitleArray.append("Account Details")
        sectionTitleArray.append("Bank Details")

        //Section 1
        firstTextFiledTitleArray.append("Bank Name")
        firstTextFiledTitleArray.append("Swift Code")
        firstTextFiledTitleArray.append("St Address")
        firstTextFiledTitleArray.append("Country")
        firstTextFiledTitleArray.append("State/Province")
        firstTextFiledTitleArray.append("City")
        firstTextFiledTitleArray.append("ZIP/Postal Code")
        firstTextFiledTitleArray.append("Phone")
        
    
        
        //Section 1 Place Holder
        firstTextFiledPlaceholderArray.append("Enter Your Bank Name")
        firstTextFiledPlaceholderArray.append("Enter Swift Code")
        firstTextFiledPlaceholderArray.append("Enter St Address")
        firstTextFiledPlaceholderArray.append("Enter Country Name")
        firstTextFiledPlaceholderArray.append("Enter State/Province Name")
        firstTextFiledPlaceholderArray.append("Enter City Name")
        firstTextFiledPlaceholderArray.append("Enter ZIP/Postal Code")
        firstTextFiledPlaceholderArray.append("Enter Bank Phone Number")
        
        //Section 2
        secondTextFiledTitleArray.append("Bank Account Number")
        secondTextFiledTitleArray.append("IBAN Number")
        secondTextFiledTitleArray.append("Account Holder Name")
        
        //Section 2 Place Holder
        secondTextFiledPlaceholderArray.append("Enter Bank Account Number")
        secondTextFiledPlaceholderArray.append("Enter IBAN Number")
        secondTextFiledPlaceholderArray.append("Enter Account Holder Name")
        
        
        for index in 0..<firstTextFiledTitleArray.count {
            
            let formObject = FormOptionObject(textFieldTitle: firstTextFiledTitleArray[index],
                                              textFieldPlaceholder: firstTextFiledPlaceholderArray[index],
                                              formOptionType: .regularTextField,
                                              keyboardType: .string,
                                              rowTag: index,
                                              data:"",secondData:"")
            
            firstSectionObjectArray.append(formObject)
        }
        
        for index in 0..<secondTextFiledTitleArray.count {
            
            let formObject = FormOptionObject(textFieldTitle: secondTextFiledTitleArray[index],
                                              textFieldPlaceholder: secondTextFiledPlaceholderArray[index],
                                              formOptionType: .regularTextField,
                                              keyboardType: .string,
                                              rowTag: index,
                                              data:"",secondData:"")
            
            secondSectionObjectArray.append(formObject)
        }
   
    }

    
    
    

}
