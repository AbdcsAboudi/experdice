//
//  BankAccountListViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 9/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class BankAccountListViewController: DTViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var roundedButtonView:UIView!
    @IBOutlet weak var addAccountButton:UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = false
        super.viewWillAppear(animated)
    }
    
     //MARK: - TableView Delegate & DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        return UITableViewCell()
    }
    
    func setupTableView(){
        
        tableView.separatorStyle = .none
        
        tableView.delegate = self
        tableView.dataSource = self
        
        addAccountButton.addTarget(self, action: #selector(addAccountSelector), for: .touchUpInside)
    }
    
    
    func addAccountSelector(){
        
        let bank = AddBankAccountViewController(nibName: "AddBankAccountViewController", bundle: nil)
        let navVC = UINavigationController(rootViewController: bank)
        self.navigationController?.present(navVC, animated: true, completion: nil)
        
    }
    

    

}
