//
//  SettingsViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class SettingsViewController: DTViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var settingsTableView:UITableView!
    
    var settingArray = [SettingObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       needSideBar = true
       title = "Settings"
        
       setupSettingsTableView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - TableView Delegate And DataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return settingArray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let settingObject = settingArray[indexPath.row]
        
        let cell = SettingsCell.populateSettingsCell(settingsObject:settingObject,tableView:tableView,indexPath:indexPath as NSIndexPath)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)

        
        if settingArray[indexPath.row].needSwitch == false {
            
            let bankAccountVC = BankAccountListViewController(nibName: "BankAccountListViewController", bundle: nil)
            self.navigationController?.pushViewController(bankAccountVC, animated: true)
            
        }
       
        
        
    }
    
    
    //MARK: - Helping Methods
    
    func setupSettingsTableView(){
        
        settingArray.append(SettingObject(title: "Notification", needSwitch: true))
        
        if UserObject.getUserType() == .business {
            
            settingArray.append(SettingObject(title: "Bank Account", needSwitch: false))
        }
        
        settingsTableView.register(UINib(nibName: "SettingsCell", bundle: nil), forCellReuseIdentifier: "SettingsCell")
        
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        
        settingsTableView.separatorStyle = .none
        
        
    }
    
  
    
}




