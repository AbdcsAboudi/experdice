//
//  WatchlistViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class WatchlistViewController: DTViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView:UITableView!
    
    var projectArray = [ProjectObject]()
    var isSuccess = false
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        needSideBar = true
        
        callWatchlistAPI()
        setupWatchlistTableView()
   
       
        title = "Watchlist"
        
        //AS: for hidding the side menu
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if projectArray[section].isOpend{
            
            return 3
            
        }else{
            
            return 2
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 2 && projectArray.count != 0{
            
            projectArray[indexPath.section].isOpend = !projectArray[indexPath.section].isOpend
            
        }else if indexPath.row == 1 {
            
            projectArray[indexPath.section].isOpend = !projectArray[indexPath.section].isOpend
            
        }
        UIView.transition(with: tableView,
                          duration: 0.30,
                          options: .curveEaseInOut,
                          animations: { self.tableView.reloadData() })
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return projectArray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if projectArray[indexPath.section].isOpend {
            
            if indexPath.row == 0 {
                
                return 90
                
            }else if indexPath.row == 1 {
                
                return 45
                
            }
            
            return 30
        }
        else {
            
            if indexPath.row == 0 {
                
                return 90
                
            }
        }
        
        return 30
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let projectObject = projectArray[indexPath.section]
        
         if indexPath.row == 0 {
                
                let cell = HeaderWatchlistTableViewCell.populateWatchlistHeaderCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                cell.optionButton.tag = indexPath.section
                
                cell.optionButton.addTarget(self, action: #selector(removeProjectSelector), for: .touchUpInside)
               
                return cell
                
            }else if indexPath.row == 1 && tableView.numberOfRows(inSection: indexPath.section) == 3 {
                
                let cell = MiddleWatchlistTableViewCell.populateWatchlistMiddleCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                return cell
                
            }else if indexPath.row == 1 && tableView.numberOfRows(inSection: indexPath.section) == 2 {
                
                let cell = ButtomWatchlistTableViewCell.populateWatchlistButtomCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                return cell
            }else{
                
                let cell = ButtomWatchlistTableViewCell.populateWatchlistButtomCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                return cell
                
            }
        
    }
    
    //MARK: - Helping Methods
    
    func setupWatchlistTableView(){
        
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "HeaderWatchlistTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView.register(UINib(nibName: "MiddleWatchlistTableViewCell", bundle: nil), forCellReuseIdentifier: "MiddleCell")
        tableView.register(UINib(nibName: "ButtomWatchlistTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtomCell")
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        
    }
  
    
    //MARK: - Selector & Action
    func removeProjectSelector(sender:UIButton){
        
       // projectArray[sender.tag].isInWatchlist = !projectArray[sender.tag].isInWatchlist!
    
        callDeleteWatchlistAPI(indexOfProject:sender.tag,idOfProject:projectArray[sender.tag].id)
        
        
    }
    
    //MARK: -  API CALL
    func callWatchlistAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getWatchlist, requestParams:user?.profile.expertId)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    viewController.projectArray = DTParser.parseProjectObjectArray(from: response) as! [ProjectObject]
                    
                    viewController.tableView.reloadData()
                }
                
            }
        }

    }
    
    func callDeleteWatchlistAPI(indexOfProject:Int?,idOfProject:Int?){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        var parm = [String:Any]()
        
        parm["expertId"] = user?.profile.expertId
        parm["projectId"] = idOfProject
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId:.deleteWatchlist,requestParams:parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                viewController.projectArray.remove(at: indexOfProject!)
                viewController.tableView.reloadData()
            }
            
        }
        
    }
    
    
    
    
    
    
    
}
