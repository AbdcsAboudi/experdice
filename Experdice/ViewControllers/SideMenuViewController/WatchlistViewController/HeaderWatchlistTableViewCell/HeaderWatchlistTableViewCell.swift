//
//  HeaderHomeTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class HeaderWatchlistTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var projectName:UILabel!
    @IBOutlet weak var projectType:UILabel!
    @IBOutlet weak var dateOfProject:UILabel!
    @IBOutlet weak var projectImage:UIImageView!
    @IBOutlet weak var projectCompanyName:UILabel!
    @IBOutlet weak var optionButton:UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    //MARK :- Papulate Table View
    
    class func populateWatchlistHeaderCell(projectObject : ProjectObject, tableView : UITableView, indexPath: NSIndexPath) -> HeaderWatchlistTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath as IndexPath) as? HeaderWatchlistTableViewCell
        
        cell?.projectName.text = projectObject.title
        cell?.projectType.text = projectObject.projectType
        cell?.projectCompanyName.text = projectObject.businessName
        cell?.dateOfProject.text = projectObject.addingDate
       
        cell?.projectImage?.sd_setImage(with: URL(string : "https://www.google.jo/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiqkdOoqLPWAhWBPhQKHf8RBqgQjRwIBw&url=http%3A%2F%2Fwww.iconsdb.com%2Fgray-icons%2Fcontacts-icon.html&psig=AFQjCNGC_qZNyw82tVmFvpd57cGTMQNiLQ&ust=1505981458950638"), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
            
            if error != nil {
                
                print(error?.localizedDescription ?? "error found")
                
            } else {
                
                cell?.projectImage?.image = image?.circleMaskBlue
                cell?.projectImage?.contentMode = .scaleAspectFill
                cell?.projectImage?.clipsToBounds = true
                
            }
        })
        
        
        cell?.selectionStyle = .none
        
        return cell!
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
