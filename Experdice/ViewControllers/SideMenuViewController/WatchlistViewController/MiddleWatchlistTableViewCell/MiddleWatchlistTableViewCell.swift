//
//  MiddleHomeTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class MiddleWatchlistTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var projectBidAverage:UILabel!
    @IBOutlet weak var projectBudget:UILabel!
    @IBOutlet weak var projectBidNumber:UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK :- Papulate Table View
    
    class func populateWatchlistMiddleCell(projectObject : ProjectObject, tableView : UITableView, indexPath: NSIndexPath) -> MiddleWatchlistTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MiddleCell", for: indexPath as IndexPath) as? MiddleWatchlistTableViewCell
        
        cell?.projectBidAverage.text = "13000 JD"
        cell?.projectBudget.text = "500 JD"
        cell?.projectBidNumber.text = "600"
        
        
        cell?.selectionStyle = .none
        
        return cell!
        
    }
    
}

