//
//  ButtomHomeTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ButtomWatchlistTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var arrowImage:UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    //MARK :- Papulate Table View
    
    class func populateWatchlistButtomCell(projectObject : ProjectObject, tableView : UITableView, indexPath: NSIndexPath) -> ButtomWatchlistTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ButtomCell", for: indexPath as IndexPath) as? ButtomWatchlistTableViewCell
        
        if projectObject.isOpend {
            cell?.arrowImage.transform = CGAffineTransform(scaleX: 1, y: -1)
        }else{
            cell?.arrowImage.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        cell?.selectionStyle = .none
        
        return cell!
        
    }
    
}
