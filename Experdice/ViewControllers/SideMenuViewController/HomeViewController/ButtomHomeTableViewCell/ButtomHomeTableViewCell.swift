//
//  ButtomHomeTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ButtomHomeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var arrowImage:UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    //MARK :- Papulate Table View
    
    class func populateHomeButtomCell(projectObject : ProjectObject, tableView : UITableView, indexPath: NSIndexPath) -> ButtomHomeTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ButtomCell", for: indexPath as IndexPath) as? ButtomHomeTableViewCell
        
        if projectObject.isOpend {
            
            cell?.arrowImage.transform = CGAffineTransform(scaleX: 1, y: -1)
           
            
        }else{
            
            cell?.arrowImage.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        cell?.selectionStyle = .none
        
        return cell!
        
    }
    
}
