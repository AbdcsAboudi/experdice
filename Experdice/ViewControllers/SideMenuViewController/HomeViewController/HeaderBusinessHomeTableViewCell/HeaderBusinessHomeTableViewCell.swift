//
//  HeaderBusinessHomeTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/24/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

protocol HeaderBusinessHomeTableViewCellDelegate: NSObjectProtocol {
    
    func findExpertViewController(index: Int)
    
}

class HeaderBusinessHomeTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    
    
    @IBOutlet weak var projectName:UILabel!
    @IBOutlet weak var projectType:UILabel!
    @IBOutlet weak var dateOfProject:UILabel!
    @IBOutlet weak var projectImage:UIImageView!
    @IBOutlet weak var projectCompanyName:UILabel!
    @IBOutlet weak var optionButton:UIButton!
    @IBOutlet weak var watchListView:UIView!
    @IBOutlet weak var collectionView:UICollectionView!
    
    var delegate:HeaderBusinessHomeTableViewCellDelegate?
    var inviteExpertArray:[UserObject]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
         setupCollectionView()
    }
    
    //MARK :- Papulate Table View
    
    class func populateHomeBusinessHeaderCell(projectObject : ProjectObject, tableView : UITableView, indexPath: NSIndexPath) -> HeaderBusinessHomeTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath as IndexPath) as? HeaderBusinessHomeTableViewCell
        
        cell?.projectName.text = projectObject.title
        cell?.projectType.text = projectObject.projectType
        cell?.projectCompanyName.text = projectObject.businessName
        cell?.dateOfProject.text = projectObject.addingDate
        
        
        cell?.projectImage?.sd_setImage(with: URL(string : projectObject.imageURL!), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
            
            if error != nil {
                
                print(error?.localizedDescription ?? "error found")
                
            } else {
                
                cell?.projectImage?.image = image?.circleMaskBlue
                cell?.projectImage?.contentMode = .scaleAspectFill
                cell?.projectImage?.clipsToBounds = true
                
            }
        })
        
        cell?.selectionStyle = .none
        
        return cell!
        
    }

    
    //MARK: -  UICollectionView Delegate and Datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row < 4 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InviteExpertCell", for: indexPath as IndexPath) as? InviteExpertCell
            
            //let expertUserObject = inviteExpertArray![indexPath.row]
            
            //TODO : MUST HERE ADD EXPERT USERNAME COME FROM API
            cell?.expertName.text = "Ahmad Sallam"
            cell?.expertImage.image = #imageLiteral(resourceName: "test123").circleMaskBlue
            cell?.expertSpecialist.text = "iOS Develeoper"
            
            return cell!
        }else{
          
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InviteMoreExpertCell", for: indexPath as IndexPath) as? InviteMoreExpertCell
            
            cell?.inviteMoreButton.addTarget(self, action: #selector(findExpertSelectore), for: .touchUpInside)
            
            return cell!
        }
        
    }
  
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //let pageWidth = self.collectionView.frame.size.width as CGFloat
    }

    
    //MARK: - Helping Method
    func setupCollectionView(){
        
        let flowLayout = UICollectionViewFlowLayout()
        
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 0
        
        
        let width = (UIScreen.main.bounds.width - 62) / 3
        let height: CGFloat = 152.0
        
        flowLayout.itemSize = CGSize(width: width, height: height)
        
        collectionView.setCollectionViewLayout(flowLayout, animated: false)
        collectionView.bounces = true
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        
        collectionView.isScrollEnabled = true
         self.collectionView.backgroundColor = UIColor.clear
        
        collectionView.register(UINib(nibName: "InviteExpertCell",bundle:nil), forCellWithReuseIdentifier: "InviteExpertCell")
        
         collectionView.register(UINib(nibName: "InviteMoreExpertCell",bundle:nil), forCellWithReuseIdentifier: "InviteMoreExpertCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
    
    }
    
    //MARK: - Selector & Action
    
    func findExpertSelectore(){
        
        delegate?.findExpertViewController(index: 1)
            
    }
  
   
    
    
}
