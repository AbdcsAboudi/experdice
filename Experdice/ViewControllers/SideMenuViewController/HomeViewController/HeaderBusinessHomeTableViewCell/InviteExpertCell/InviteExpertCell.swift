//
//  InviteExpertCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/28/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class InviteExpertCell: UICollectionViewCell {

    @IBOutlet weak var expertName:UILabel!
    @IBOutlet weak var expertImage:UIImageView!
    @IBOutlet weak var expertSpecialist:UILabel!
    @IBOutlet weak var inviteButton:UIButton!
    @IBOutlet weak var roundedButtonView:UIView!
    @IBOutlet weak var rounderSuperView:UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        roundedButtonView.makeCorneredViewWith(corenerRadius: 10, and: 0)
        expertImage.image = expertImage.image?.circleMaskBlue
        rounderSuperView.makeCorneredViewWith(corenerRadius: 3, and: 0)
    }
    
    
    
    

}
