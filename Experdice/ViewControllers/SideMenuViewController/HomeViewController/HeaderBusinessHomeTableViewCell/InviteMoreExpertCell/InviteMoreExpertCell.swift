//
//  InviteMoreExpertCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 9/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class InviteMoreExpertCell: UICollectionViewCell {

    @IBOutlet weak var inviteMoreButton:UIButton!
    @IBOutlet weak var inviteMoreView:UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        inviteMoreView.makeCorneredViewWith(corenerRadius: 10, and: 0)
        
    }

}
