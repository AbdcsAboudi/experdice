//
//  HomeViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class HomeViewController: DTListViewController, HeaderBusinessHomeTableViewCellDelegate{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        needSideBar = true
        title = "Home"
        tableViewSetup()
        
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - UITableView Methods
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == (list.count - requestNextPageOffset) {
            getNextPage()
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return list.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !list.isEmpty, let myProjectArray = list[section] as? ProjectObject{
            
            if myProjectArray.isOpend{
                
                return 3
                
            }else{
                
                return 2
                
            }
        }
        return 0
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if !list.isEmpty, let projectObject = list[indexPath.section] as? ProjectObject {
            
            if indexPath.row == 0 {
                
                let projectDetailsVc = self.storyboard?.instantiateViewController(withIdentifier: "ProjectDetailsParentViewController") as! ProjectDetailsParentViewController
                
                let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
                projectDetailsVc.projectId = projectObject.id
                if projectObject.businessID == user?.profile.businessId {
                    
                    projectDetailsVc.isFromMyProject = true
                }
                self.navigationController?.pushViewController(projectDetailsVc, animated: true)
                
            }
            
            if indexPath.row == 2, projectObject.isOpend == true{
                
                tableView.beginUpdates()
                projectObject.isOpend = false
                tableView.deleteRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                tableView.endUpdates()
                
                tableView.reloadRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                
            }else if indexPath.row == 1, projectObject.isOpend == false {
                
                tableView.beginUpdates()
                projectObject.isOpend = true
                tableView.insertRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                tableView.endUpdates()
                
                tableView.reloadRows(at: [IndexPath(row: 2, section: indexPath.section)], with: .automatic)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            if UserObject.getUserType() == .business {
                return 265
            }
            
            return 90
        }
        
        if indexPath.row == 1 {
            
            if !list.isEmpty, let myProjectArray = list[indexPath.section] as? ProjectObject{
                
                if myProjectArray.isOpend {
                    
                    // for row 2 opened
                    return 45
                }
                
                // for row 2 opened
                return 30
            }
            
            // for last row collapse button
            return 30
        }
        
        return 30
    }
    
    override func populateTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !list.isEmpty, let projectObject = list[indexPath.section] as? ProjectObject {
            
            if indexPath.row == 0 {
                
                if UserObject.getUserType() == .expert {
                    let expertCell = HeaderHomeTableViewCell.populateHomeHeaderCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                    expertCell.optionButton.tag = indexPath.section
                    expertCell.optionButton.addTarget(self, action: #selector(showActionSheetSelector), for: .touchUpInside)
                    
                    return expertCell
                }else{
                    let businessCell = HeaderBusinessHomeTableViewCell.populateHomeBusinessHeaderCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                    
                    businessCell.delegate = self
                    businessCell.optionButton.tag = indexPath.section
                    businessCell.optionButton.addTarget(self, action: #selector(showActionSheetSelector), for: .touchUpInside)
                    
                    return businessCell
                }
            }else if indexPath.row == 1 && tableView.numberOfRows(inSection: indexPath.section) == 3 {
                
                let cell = MiddleHomeTableViewCell.populateHomeMiddleCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                return cell
                
            }else if indexPath.row == 1 && tableView.numberOfRows(inSection: indexPath.section) == 2 {
                
                let cell = ButtomHomeTableViewCell.populateHomeButtomCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                return cell
            }else{
                let cell = ButtomHomeTableViewCell.populateHomeButtomCell(projectObject: projectObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
    
    //MARK: - Helping Methods
    
    func setupProjectArray(array:[ProjectObject]){
        
        for project in array {
            
            list.append(project)
            
        }
        
    }
    
    override func tableViewSetup() {
        super.tableViewSetup()
        
        tableView?.separatorStyle = .none
        tableView?.register(UINib(nibName: "HeaderHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpertHeaderCell")
        tableView?.register(UINib(nibName: "HeaderBusinessHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
        tableView?.register(UINib(nibName: "MiddleHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "MiddleCell")
        tableView?.register(UINib(nibName: "ButtomHomeTableViewCell", bundle: nil), forCellReuseIdentifier: "ButtomCell")
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        
    }
    
    override func beginRequest() {
        callGetProjectAPI()
    }
    
    
    func setupParameters()->[String:Any]{

        var expertID = 0
        var parameters = [String: Any]()
        
        if let user = DTCredentialManager.credentialSharedInstance.user as? UserObject {
            
            if user.userType != "2" {
                expertID = user.profile.expertId!
                parameters["ExpertID"] = expertID
            }else{
                parameters["ExpertID"] = 0
            }
            
            
            //{ClientID}/{IndustryID}/{Budget}/{CityID}/{DateFrom}/{DateTo}/{StatusID}/{OnlyForClient}/{ExpertID}/{PageSize}/{PageNo}
            
            parameters["clientId"] = "0"
            parameters["industryID"] = "0"
            parameters["onlyForClient"] = "0"
            parameters["budget"] = "0"
            parameters["cityID"] = "0"
            parameters["dateFrom"] = "0"
            parameters["dateTo"] = "0"
            parameters["statusID"] = "0"
            parameters["pageSize"] = String(limit)
            parameters["pageNo"] = String(currentPage)
            //            Client id = 1 , 0 all projects
            //
            //            client id  - 1 all my projects
            //
        }
        
        return parameters
        
    }
    
    //MARK: - Selector & Action 
    
    func showActionSheetSelector(sender:UIButton){
        
        let alertView = UIAlertController(title: "Choose Please", message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
            alertView.dismiss(animated: true, completion: nil)
        })
        
        let messageAction = UIAlertAction(title: "Message", style: .default, handler: { (alert) in
            
        })
        
        let addToWatchlistAction = UIAlertAction(title: "Add To Watchlist", style: .default, handler: { (alert) in
            
            if !self.list.isEmpty ,  let project = self.list[sender.tag] as? ProjectObject  {
                
                let projectId = project.id
                
                self.addProjectToWatchListAPI(projectId: projectId!,senderTag: sender.tag)
                
            }
            
            
            UIView.transition(with: self.tableView!,
                              duration: 0.30,
                              options: .curveEaseInOut,
                              animations: { self.tableView?.reloadData() })
        })
        
        let reportProjectAction = UIAlertAction(title: "Report Project", style: .default, handler: { (alert) in
            
        })
        
        alertView.addAction(cancelAction)
        alertView.addAction(messageAction)
        alertView.addAction(addToWatchlistAction)
        alertView.addAction(reportProjectAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    
    //MARK: - API Call
    
    func callGetProjectAPI(){
        
        let parameters = setupParameters()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getProjectList, requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any]{
                    
                    let project = DTParser.parseProjectObjectArray(from: response) as! [ProjectObject]
                    
                    viewController.setupProjectArray(array:project)
                    
                    self?.endRequest(hasMore: true)
                    
                }
                
            }else{
                viewController.endRequestWithFailure()
                
            }
            
        }
    }
    
    
    func addProjectToWatchListAPI(projectId:Int,senderTag:Int){
        
        var parameters = [String:Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parameters["ExpertId"] = user?.profile.expertId
        
        parameters["ProjectId"] = projectId
        
        if !list.isEmpty ,let project = self.list[senderTag] as? ProjectObject{
            
            if project.isWatchList! {
                
                let alertMassage = "The Project Is in WatchList"
                
                let alertView = EXAlertViewController(title: "Hello", twoButton: false, alertMessage: alertMassage, buttonTitle: "Ok", zeroHighView: false, centerTextAlertMessage: true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                
                return
            }
            
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId:.addWatchList,requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if !viewController.list.isEmpty , let projectArray = viewController.list as? [ProjectObject]{
                    
                    for project in projectArray {
                        
                        if project.id == projectId {
                            
                            if !project.isWatchList!{
                                project.isWatchList = !project.isWatchList!
                            }
                            
                            DispatchQueue.main.async {
                                
                                viewController.tableView?.reloadSections(IndexSet(integer:senderTag), with: UITableViewRowAnimation.none)
                            }
                        }
                    }
                    
                }
                
                let alertMassage = "Success!"
                
                let alertView = EXAlertViewController(title: "Hello", twoButton: false, alertMessage: alertMassage, buttonTitle: "Ok", zeroHighView: false, centerTextAlertMessage: true,speratorViewEnable:true)
                
                self?.present(alertView, animated: true, completion: nil)
                
                
            }else{
                
                let alertMassage = "Adding WatchList"
                
                let alertView = EXAlertViewController(title: "Hello", twoButton: false, alertMessage: alertMassage, buttonTitle: "Ok", zeroHighView: false, centerTextAlertMessage: false,speratorViewEnable:true)
                
                self?.present(alertView, animated: true, completion: nil)
                
            }
        }
    }
    
    // MARK: - Header delegate
    
    func findExpertViewController(index : Int) {
        
        let vc = FindExpertsViewController(nibName: "FindExpertsViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
