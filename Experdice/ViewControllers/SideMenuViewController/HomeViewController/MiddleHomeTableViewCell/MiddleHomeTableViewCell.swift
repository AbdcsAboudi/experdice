//
//  MiddleHomeTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class MiddleHomeTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var projectBidAverage:UILabel!
    @IBOutlet weak var projectBudget:UILabel!
    @IBOutlet weak var projectBidNumber:UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK :- Papulate Table View
    
    class func populateHomeMiddleCell(projectObject : ProjectObject, tableView : UITableView, indexPath: NSIndexPath) -> MiddleHomeTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MiddleCell", for: indexPath as IndexPath) as? MiddleHomeTableViewCell
        
        cell?.projectBidAverage.text = "\(projectObject.bidAverage ?? 0)"
        cell?.projectBudget.text = projectObject.budget
        cell?.projectBidNumber.text = "\(projectObject.bidNumber ?? 0)"
        cell?.selectionStyle = .none
        
        return cell!
        
    }
    
}
