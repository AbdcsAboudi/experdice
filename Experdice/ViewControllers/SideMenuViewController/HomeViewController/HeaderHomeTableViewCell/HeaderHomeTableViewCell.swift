//
//  HeaderHomeTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class HeaderHomeTableViewCell: UITableViewCell {
   


    @IBOutlet weak var projectName:UILabel!
    @IBOutlet weak var projectType:UILabel!
    @IBOutlet weak var dateOfProject:UILabel!
    @IBOutlet weak var projectImage:UIImageView!
    @IBOutlet weak var projectCompanyName:UILabel!
    @IBOutlet weak var optionButton:UIButton!
    @IBOutlet weak var watchListView:UIView!
    
   
    
    override func awakeFromNib() {
        super.awakeFromNib()

       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    //MARK :- Papulate Table View
    
    class func populateHomeHeaderCell(projectObject : ProjectObject, tableView : UITableView, indexPath: NSIndexPath) -> HeaderHomeTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpertHeaderCell", for: indexPath as IndexPath) as? HeaderHomeTableViewCell
        
        cell?.projectName.text = projectObject.title
        cell?.projectType.text = projectObject.projectType
        cell?.projectCompanyName.text = projectObject.businessName
        cell?.dateOfProject.text = projectObject.addingDate
        cell?.watchListView.isHidden = !projectObject.isWatchList!
        
        cell?.projectImage?.sd_setImage(with: URL(string : projectObject.imageURL!), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
            
            if error != nil {
                
                print(error?.localizedDescription ?? "error found")
                
            } else {
                
                cell?.projectImage?.image = image?.circleMaskBlue
                cell?.projectImage?.contentMode = .scaleAspectFill
                cell?.projectImage?.clipsToBounds = true
                
            }
        })
        
       
       
        cell?.selectionStyle = .none
        
        return cell!
        
    }
    
    


   

    
    
    
    
    
    
    
    
    
}
