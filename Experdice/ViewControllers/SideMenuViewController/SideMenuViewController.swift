//
//  SideMenuViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/1/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class SideMenuViewController: DTViewController,UITableViewDelegate,UITableViewDataSource {
    
   
    @IBOutlet weak var tableView:UITableView!
    
    var sideHeaderMenuArray:[SideMenuObject]!
    
    var sideMenuArray: [SideMenuObject]!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSideMenuView()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        setupSideMenuView()
        tableView.reloadData()
    }
    
    //MARK: - UITableView Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 1
            
        }else{
            
            return sideMenuArray.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section > 0 && indexPath.row != 9 {
            
            self.performSegue(withIdentifier: sideMenuArray[indexPath.row].segueName!, sender: self)
          
        }else if indexPath.row == 9 {
                
            doLogout()
            
        }else{
            self.performSegue(withIdentifier: sideHeaderMenuArray[indexPath.row].segueName!, sender: self)
        }

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            return 120
            
        }else{
            
            return 60
            
        }
     
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var sideMenuObject = sideMenuArray[indexPath.row]
        
        if indexPath.section == 0 {
            
            sideMenuObject = sideHeaderMenuArray[indexPath.row]
            
            let cell = HeaderSideMenuTableViewCell.populateHeaderSideMenuCell(sideMenuObject: sideMenuObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
        
            return cell
            
        }
       
            let cell = SideMenuTableViewCell.populateSideMenuCell(sideMenuObject: sideMenuObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
        
            return cell
     
    }
    
    //MARK: - Helping Methods
    
    
    func getImage(url:String)->UIImage{
      
       let imageView = UIImageView()
        let imagePath = "http://numidiajo.com/Experdice/API/images/\(url)"
        
        imageView.sd_setImage(with: URL(string : imagePath), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
            
            if error != nil {
                
                print(error?.localizedDescription ?? "error found")
                
            } else {
                
               // imageView.image = image?.circleMaskBlue
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                
            }
        })
      
        return imageView.image!
        
    }
    
    func setupSideMenuView(){
        
        var imagePath = ""
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        if let userImagePath = user?.imageURL {
            imagePath = userImagePath
        }
        if user?.userType == "2" {
            
            sideMenuArray  = [
                SideMenuObject(id: 0, title: "Home", segueName: "HomeViewController", badgeLabel: false),
                SideMenuObject(id: 1, title: "My Project", segueName: "MyProjectViewController", badgeLabel: false),
                SideMenuObject(id: 2, title: "Notifications", segueName: "NotificationsViewController", badgeLabel: true),
                SideMenuObject(id: 3, title: "Messages", segueName: "MessagesViewController", badgeLabel: true),
                SideMenuObject(id: 4, title: "Watchlist", segueName: "WatchlistViewController", badgeLabel: false),
                SideMenuObject(id: 5, title: "Terms and condtions", segueName: "TermsAndConditionsViewController", badgeLabel: false),
                SideMenuObject(id: 6, title: "FAQ", segueName: "FAQViewController", badgeLabel: false),
                SideMenuObject(id: 7, title: "Contact Us", segueName: "ContactUsViewController", badgeLabel: false),
                SideMenuObject(id: 8, title: "Settings", segueName: "SettingsViewController", badgeLabel: false),
                SideMenuObject(id: 9, title: "Logout", segueName: "", badgeLabel: false),
            ]
            
            sideHeaderMenuArray = [
                SideMenuObject(id: 0,
                               profileImage:getImage(url: imagePath),
                               segueName: "profileBusinessSegue",
                               userName: user?.profile.businessName,
                               specializationName: user?.profile.specialties)
            ]
            
        }else{
            
            //AS : if not userType is not business it will exute this code
            sideMenuArray  = [
                SideMenuObject(id: 0, title: "Home", segueName: "HomeViewController", badgeLabel: false),
                SideMenuObject(id: 1, title: "My Bids", segueName: "MyBidsViewController", badgeLabel: false),
                SideMenuObject(id: 2, title: "Notifications", segueName: "NotificationsViewController", badgeLabel: true),
                SideMenuObject(id: 3, title: "Messages", segueName: "MessagesViewController", badgeLabel: true),
                SideMenuObject(id: 4, title: "Watchlist", segueName: "WatchlistViewController", badgeLabel: false),
                SideMenuObject(id: 5, title: "Terms and condtions", segueName: "TermsAndConditionsViewController", badgeLabel: false),
                SideMenuObject(id: 6, title: "FAQ", segueName: "FAQViewController", badgeLabel: false),
                SideMenuObject(id: 7, title: "Contact Us", segueName: "ContactUsViewController", badgeLabel: false),
                SideMenuObject(id: 8, title: "Settings", segueName: "SettingsViewController", badgeLabel: false),
                SideMenuObject(id: 9, title: "Logout", segueName: "", badgeLabel: false),
            ]
            
            sideHeaderMenuArray = [
                SideMenuObject(id: 0,
                               profileImage: getImage(url: imagePath),
                               segueName: "profileSegue",
                               userName: ("\(user?.profile.profileFirstName ?? "") \(user?.profile.profileSecondName ?? "")"),
                               specializationName: user?.headLine)
            ]
        }
      
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView?.register(UINib(nibName: "SideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableViewCell")
        
        tableView?.register(UINib(nibName: "HeaderSideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderSideMenuTableViewCell")
        
        tableView.separatorStyle = .none
        
        navigationController?.navigationBar.isHidden = true
        
    }
    
    //AS : Logout
    func doLogout(){
        
        let alertView = EXAlertViewController(title: "Logout", twoButton: true, alertMessage:"Logout from Experdice" ,buttonTitle: "Logout",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
        
        self.present(alertView, animated: true, completion: nil)
        
        alertView.confirmClicked = {
            DTCredentialManager.resetUserCredentials()
            self.performSegue(withIdentifier: "unwindSegueToLogin", sender: self)
        }
        
    }
  
    
    
  

    
}
