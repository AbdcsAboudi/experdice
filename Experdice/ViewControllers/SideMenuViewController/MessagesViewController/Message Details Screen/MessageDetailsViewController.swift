//
//  MessageDetailsViewController.swift
//  Experdice
//
//  Created by A.Aboudi on 9/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import SDWebImage

public enum Setting: String {
    
    case removeBubbleTails = "Remove message bubble tails"
    case removeSenderDisplayName = "Remove sender Display Name"
    case removeAvatar = "Remove Avatars"
    
}

class MessageDetailsViewController: JSQMessagesViewController {
    
    var messages = [ChatMessageObject]()
    let defaults = UserDefaults.standard
    var incomingBubble: JSQMessagesBubbleImage!
    var outgoingBubble: JSQMessagesBubbleImage!
    
    fileprivate var displayName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //AA : To fix loading to bottom crash
        if #available(iOS 10.0, *) {
            self.collectionView?.isPrefetchingEnabled = false
        } else {
            //Fallback on earlier versions
        }
     
        //AA : To setup chat collectionView
        setupChatCollectionView()
        
        //AA : To setup chat cell custom view
        setupChatCellView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Helping Methods
    
    func setupChatCollectionView() {
        
        //AA : To remove accessory button
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        self.senderDisplayName = "Abd"
        self.senderId = "1"
        
        // This is a beta feature that mostly works but to make things more stable it is diabled.
        collectionView?.collectionViewLayout.springinessEnabled = true
        
        automaticallyScrollsToMostRecentMessage = true
        
        self.collectionView?.reloadData()
        self.collectionView?.layoutIfNeeded()
        
    }
    
    func setupChatCellView() {
        
        /**
         *  Override point:
         *
         *  Example of how to cusomize the bubble appearence for incoming and outgoing messages.
         *  Based on the Settings of the user display two differnent type of bubbles.
         *
         */
        
        if defaults.bool(forKey: Setting.removeBubbleTails.rawValue) {
            
            // Make taillessBubbles
            incomingBubble = JSQMessagesBubbleImage(messageBubble: UIImage.jsq_bubbleCompactTailless(), highlightedImage: UIImage.jsq_bubbleCompactTailless())
            
            outgoingBubble = JSQMessagesBubbleImage(messageBubble: UIImage.jsq_bubbleCompactTailless(), highlightedImage: UIImage.jsq_bubbleCompactTailless())
            
        }
        else {
            
            // Bubbles with tails
            incomingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: ColorManager.sharedInstance.getTextFiledTextColor())
            outgoingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.lightGray)
            
        }
        
    }
    
    func setupShowAvatarInCellOrNot() {
        
        /**
         *  Example on showing or removing Avatars based on user settings.
         */
        
        //AA : To show avatar or not based on from group or not
        
        collectionView?.collectionViewLayout.incomingAvatarViewSize = .zero
        collectionView?.collectionViewLayout.outgoingAvatarViewSize = .zero
        
    }
    
    //MARK: Actions
    
    func backButtonTapped() {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func receiveMessagePressed(_ sender: UIBarButtonItem) {
        /**
         *  DEMO ONLY
         *
         *  The following is simply to simulate received messages for the demo.
         *  Do not actually do this.
         */
        
        /**
         *  Show the typing indicator to be shown
         */
        self.showTypingIndicator = !self.showTypingIndicator
        
        /**
         *  Scroll to actually view the indicator
         */
        self.scrollToBottom(animated: true)
        
        /**
         *  Copy last sent message, this will be the new "received" message
         */
        var copyMessage = self.messages.last?.copy()
        
        if (copyMessage == nil) {
            copyMessage = JSQMessage(senderId: "1", displayName: "Abd Aboudi", text: "First received!")
        }
        
        let newMessage = JSQMessage(senderId: "1 ", displayName: "Abd Aboudi", text: (copyMessage! as AnyObject).text)
        /**
         *  Upon receiving a message, you should:
         *
         *  1. Play sound (optional)
         *  2. Add new JSQMessageData object to your data source
         *  3. Call `finishReceivingMessage`
         */
        
        let newChatMessage = ChatMessageObject()
        newChatMessage.messageObject = newMessage
        
        self.messages.append(newChatMessage)
        self.finishReceivingMessage(animated: true)
        
    }
    
    // MARK: JSQMessagesViewController method overrides
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
        /**
         *  Sending a message. Your implementation of this method should do *at least* the following:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishSendingMessage`
         */
        
        let message = JSQMessage(senderId: "1", senderDisplayName: senderDisplayName, date: date, text: text)
        let newChatMessage = ChatMessageObject()
        newChatMessage.isThisMessageMine = true
        newChatMessage.messageObject = message
        
        self.messages.append(newChatMessage)
        self.finishSendingMessage(animated: true)
        
        //AA : To request send message API
        //            sendMessageApi(messageObject: message!)
        
        
    }
    
    //MARK: JSQMessages CollectionView DataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return messages.count
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        
        return messages[indexPath.item].messageObject!
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource {
        
        //AA : To show my message on the right side and the others messages on the left side
        if !messages[indexPath.item].isThisMessageMine {
            
            return outgoingBubble
            
        }else{
            
            return incomingBubble
            
        }
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        
        return nil
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        if (indexPath.item % 3 == 0) {
            
            let message = self.messages[indexPath.item]
            
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.messageObject?.date)
            
        }
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        let message = messages[indexPath.item]
        
        // Displaying names above messages
        //Mark: Removing Sender Display Name
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         */
        if defaults.bool(forKey: Setting.removeSenderDisplayName.rawValue) {
            return nil
        }
        
        if message.messageObject?.senderId == self.senderId {
            return nil
        }
        
        return NSAttributedString(string: (message.messageObject?.senderDisplayName)!)
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every 3rd message
         */
        if indexPath.item % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        return 0.0
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         */
        if defaults.bool(forKey: Setting.removeSenderDisplayName.rawValue) {
            return 0.0
        }
        
        /**
         *  iOS7-style sender name labels
         */
        let currentMessage = self.messages[indexPath.item]
        
        if currentMessage.messageObject?.senderId == self.senderId {
            return 0.0
        }
        
        if indexPath.item - 1 > 0 {
            let previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.messageObject?.senderId == currentMessage.messageObject?.senderId {
                return 0.0
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        
        print("Load More Button Tapped")
        
    }
    
}
