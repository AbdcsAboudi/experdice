//
//  MessageTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 9/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var lastMessageLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var avatarImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Cells population Methods
    
    class func PopulateMessageTableViewCell(messageObject : MessageObject, indexPath:NSIndexPath, tableView:UITableView) -> MessageTableViewCell {
        
        let cell:MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell") as! MessageTableViewCell
        
//        cell.userNameLabel.text = messageObject.user.name
//        cell.lastMessageLabel.text = messageObject.lastMsgText
//        cell.dateLabel.text = messageObject.dateTime.timeAgo()
//        
//        cell.avatarImageView?.sd_setImage(with: URL(string : messageObject.user.imagePath), placeholderImage: UIImage(named: "user_image_placeholder"), options: .retryFailed, completed: { (image, error, cacheType, url) in
//            
//            if error != nil {
//                
//                print(error?.localizedDescription ?? "error found")
//                
//            } else {
//                
//                cell.avatarImageView?.image = image?.circleMask
//                cell.avatarImageView?.contentMode = .scaleAspectFill
//                cell.avatarImageView?.clipsToBounds = true
//                
//                
//            }
//        })
        
        return cell
        
    }

}
