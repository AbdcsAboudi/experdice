//
//  MessagesViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class MessagesViewController: DTViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var messagesTableView:UITableView!
    
    var messagesArray = [MessageObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillMessagesArray()
        
        setupMessagesTableView()
        
        //AS: for hidding the side menu
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return messagesArray.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = MessageTableViewCell.PopulateMessageTableViewCell(messageObject: messagesArray[indexPath.row], indexPath: indexPath as NSIndexPath, tableView: tableView)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //AA : Go to messge details vc
        let messageDetailsVc = MessageDetailsViewController(nibName: "MessageDetailsViewController", bundle: nil)
        self.navigationController?.pushViewController(messageDetailsVc, animated: true)
        
    }
    
    //MARK: - Helping Methods
    
    func setupMessagesTableView(){
        
        title = "Messages"
        
        //AA : To remove the extra space above the tableView
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.messagesTableView.delegate = self
        self.messagesTableView.dataSource = self
        
        self.messagesTableView.estimatedRowHeight = 45
        self.messagesTableView.rowHeight = UITableViewAutomaticDimension
        
        self.messagesTableView.register(UINib(nibName: "MessageTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "MessageTableViewCell")
        
    }
    
    func fillMessagesArray(){
        
        messagesArray.append(contentsOf: [MessageObject(), MessageObject(), MessageObject(), MessageObject()])
        
    }
    
}
