//
//  BidTableViewCell.swift
//  Experdice
//
//  Created by A.Aboudi on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class BidTableViewCell: UITableViewCell {
    
    @IBOutlet var bidStatusImageView: UIImageView!
    @IBOutlet var projectNameLabel: UILabel!
    @IBOutlet var projectStatusLabel: UILabel!
    @IBOutlet var bidLocationLabel: UILabel!
    @IBOutlet var bidDateLabel: UILabel!
    @IBOutlet var bidAmountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK :- Papulate Table View
    
    class func populateBidTableViewCell(bidObject : ProjectBidsObject, tableView : UITableView, indexPath: NSIndexPath) -> BidTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BidTableViewCell", for: indexPath as IndexPath) as? BidTableViewCell
        
        cell?.projectNameLabel.text = bidObject.projectName
        cell?.bidStatusImageView.image = getTheProperBidStatusImage(value:bidObject.bidStatus!)
        cell?.projectStatusLabel.text = getBidStatusString(value:bidObject.bidStatus!)
        cell?.projectStatusLabel.textColor = getTheProperBidStatusColor(value:bidObject.bidStatus!)
        cell?.bidLocationLabel.text = "Test Location"
        cell?.bidAmountLabel.text = bidObject.bidAmount
        
        cell?.selectionStyle = .none
        
        return cell!
        
        
    }
    
    class func getBidStatusString(value:String) -> String {
        
        switch value {
        case "1":return "Pending"
        case "2":return "Accepted"
        case "3":return "Rejected"
        default:
            return "Pending"
        }
        
    }
    
    class func getTheProperBidStatusImage(value:String) -> UIImage {
        
        switch value {
            
        case "1":  return #imageLiteral(resourceName: "pending_icon")
            
        case "2":  return #imageLiteral(resourceName: "accepted_icon")
            
        case "3": return #imageLiteral(resourceName: "rejected_icon")
            
        default:
              return #imageLiteral(resourceName: "pending_icon")
        }
        
        
        
    }
    
    class func getTheProperBidStatusColor(value:String) -> UIColor {
        
        switch value {
            
        case "1":      return DTHelper.sharedInstance.colorWithHexString("EF592B")
            
        case "2":  return DTHelper.sharedInstance.colorWithHexString("00A240")
            
        case "3":   return DTHelper.sharedInstance.colorWithHexString("EC1C26")
            
        default:
             return DTHelper.sharedInstance.colorWithHexString("EF592B")
        }
        
        
        
    }
    
    
    
}
