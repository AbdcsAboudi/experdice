//
//  MyBidsViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class MyBidsViewController: DTListViewController {
    

    var bidsObjectsArray = [ProjectBidsObject]()
    
    @IBOutlet weak var tableView2:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView = tableView2
        
        tableViewSetup()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Helping Methods
    
  
    func setupParameters()->[String:Any]{
        
        var parm = [String:Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parm["expertId"] = user?.profile.expertId
        parm["statusId"] = 0
        
        return parm
    }

    func setupProjectArray(array:[ProjectBidsObject]){
        
        for project in array {
            
            list.append(project)
            
        }
        
    }
    override func beginRequest() {
        
        callGetProjectBidsList()
    }
    
    override func tableViewSetup() {
        super.tableViewSetup()
      
        tableView?.register(UINib(nibName: "BidTableViewCell", bundle: nil), forCellReuseIdentifier: "BidTableViewCell")
        tableView?.separatorStyle = .none
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        
    }
    
    
    //MARK: - UITableView Methods
            
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return list.count
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if !list.isEmpty {
            
            return 1
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 130
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     //    if !list.isEmpty, let projectObject = list[indexPath.section] as? ProjectObject {
        if !list.isEmpty,let bidObject = list[indexPath.row] as? ProjectBidsObject {
            
            let cell = BidTableViewCell.populateBidTableViewCell(bidObject: bidObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
            
            return cell
            
        }
        return UITableViewCell()
    }
    
    
    //MARK: - API Call
    
    func callGetProjectBidsList(){
        
        let parameters = setupParameters()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getProjectBids, requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any]{
                    
                    let project = DTParser.parseExpertBiderListFromResponseArray(from: response) as! [ProjectBidsObject]
                    
                    viewController.setupProjectArray(array:project)
                    
                    self?.endRequest(hasMore: false)
                    
                }
                
            }else{
                viewController.endRequestWithFailure()
                
            }
            
        }
    }
    
    
    
}

