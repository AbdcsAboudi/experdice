//
//  NotificationsTableViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/7/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var notificationMessage:UILabel!
    @IBOutlet weak var notificationDate:UILabel!
    @IBOutlet weak var deleteNotiffication:UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    //MARK :- Papulate Table View
    
    class func populateNotificationsCell(notificationsObject: NotificationObject, tableView : UITableView, indexPath: NSIndexPath) -> NotificationsTableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsCell", for: indexPath as IndexPath) as? NotificationsTableViewCell
       
        
        cell?.notificationDate.text = notificationsObject.notificationDate
        
        
        
        cell?.selectionStyle = .none
        
        return cell!
        
    }
    
   
   
    
}
