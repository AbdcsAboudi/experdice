//
//  NotificationsViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class NotificationsViewController: DTViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView:UITableView!
    
    
    var notificationArray = [NotificationObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        needSideBar = true
        
        setupNotificationsTableView()
        
        fillNotificationArray()
        
        setDeleteButton()
        
        title = "Notifications"
        
        //AS: for hidding the side menu
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    //MARK: - UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return notificationArray.count
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
       
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let notificationObject = notificationArray[indexPath.row]
        
        let cell = NotificationsTableViewCell.populateNotificationsCell(notificationsObject: notificationObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
        
        
        cell.deleteNotiffication.tag = indexPath.row
        cell.deleteNotiffication.addTarget(self, action: #selector(deleteNotificationSelector), for: .touchUpInside)
        cell.notificationMessage.attributedText = setAttributeText(notificationsObject: notificationObject)

        return cell
        
    }
    
    
    //MARK: - Helping Methods
    
    func setDeleteButton(){
        
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "closeEdit"), for: .normal)
        button.addTarget(self, action: #selector(deleteAllNotificationSelector), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView:button)
        navigationItem.leftBarButtonItem = navigationController?.navigationItem.backBarButtonItem
        
    }
    
    
    
    func setupNotificationsTableView(){
        
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "NotificationsTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationsCell")
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        
    }
    
    func deleteAllNotificationSelector(){
        
        notificationArray.removeAll()
        
        tableView.reloadData()
        
    }
    
    func deleteNotificationSelector(_ seneder:UIButton){
        
        
        notificationArray.remove(at: seneder.tag)
        
        tableView.reloadData()
        
        
    }
    
    //AS: Fill some project object
    func fillNotificationArray(){
        
        notificationArray.append(NotificationObject(notificationType: " Approved your Bid", notificationDate: "March 11,2017", notificationStatus: true, notificationMemberName: "Trust & Go", notificationProjectName: "Test Project Name"))
        
        
        notificationArray.append(NotificationObject(notificationType: " Got New Bid", notificationDate: "June 11,2017", notificationStatus: true,notificationMemberName: "Project Name", notificationProjectName: ""))
        
        notificationArray.append(NotificationObject(notificationType: " Rejected your Bid on", notificationDate: "June 11,2017", notificationStatus: true,notificationMemberName: "Project Test", notificationProjectName: ""))
        
        notificationArray.append(NotificationObject(notificationType: " Bid on", notificationDate: "June 11,2017", notificationStatus: true,notificationMemberName: "Project Platinum", notificationProjectName: ""))
        
        
    }
    
    //AS: Text Attributes
    
    func setAttributeText(notificationsObject:NotificationObject)->NSAttributedString{
        
        let projectName = notificationsObject.notificationProjectName
        let projectMemberName = notificationsObject.notificationMemberName
        let notificationType = notificationsObject.notificationType
        
        
        //TODO: MUST IMPLEMENT THES USING API
        
        //AS: Concatenation three values from notification object
        let tempString = projectMemberName! + " " + notificationType! + " " + projectName!
        
        
        let textWithAttribute = NSMutableAttributedString(string:tempString)
        
        //AS: For adding color attribute to the member name in notification cell label
        textWithAttribute.addAttribute(NSForegroundColorAttributeName, value: ColorManager.sharedInstance.getAttributeTextColor(), range: textWithAttribute.mutableString.range(of:projectMemberName!))
        
        //AS: For adding color attribute to the project name in notification cell label
        textWithAttribute.addAttribute(NSForegroundColorAttributeName, value: ColorManager.sharedInstance.getAttributeTextColor(), range: textWithAttribute.mutableString.range(of:projectName!))
        
        //AS: For adding link attribute to the member name in notification cell label
        
       // textWithAttribute.addAttribute(NSLinkAttributeName,value: "LinkToTheMemperName",range: textWithAttribute.mutableString.range(of:projectMemberName!))
        
       
        
        //AS: For adding link attribute to the project name in notification cell label
        
        // textWithAttribute.addAttribute(NSLinkAttributeName,value: "LinkToTheProjectName",range: textWithAttribute.mutableString.range(of:projectName!))
        
        return textWithAttribute
        
        
    }
    
    
    

    
    
    
    
    
    
    
    
    

   

}
