//
//  TutorialCollectionViewCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/20/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class TutorialImageCell: UICollectionViewCell {
    
    
    
    @IBOutlet weak var tutorialImageView: UIImageView!
    @IBOutlet weak var headerTextLabel: UILabel!
    @IBOutlet weak var subtitleTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

//    class func populateTutorialCell(tutorialObject : TutorialViewController.TutorialObject, collectionView : UICollectionView, indexPath: NSIndexPath) -> TutorialImageCell {
//        
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TutorialImageCell", for: indexPath as IndexPath) as? TutorialImageCell
//        
//        cell?.tutorialImageView.image = tutorialObject.image
//        cell?.headerTextLabel.text = tutorialObject.text
//        cell?.subtitleTextLabel.text = tutorialObject.subTitle
//        
//        return cell!
//
//        
//    }
}
