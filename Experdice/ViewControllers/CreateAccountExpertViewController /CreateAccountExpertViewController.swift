//
//  CreateAccountExpertViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/30/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class CreateAccountExpertViewController: DTViewController,UIScrollViewDelegate {
    
    
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var joinNowButton:UIButton!
    @IBOutlet weak var connectWithButton:UIButton!
    @IBOutlet weak var signUpBusinessLabel:UILabel!
    @IBOutlet weak var termsAndCondtion:UIButton!
    @IBOutlet weak var joinNowView:UIView!
    @IBOutlet weak var connectWithView:UIView!
    @IBOutlet weak var firstNameTextField:DTtextField!
    @IBOutlet weak var lastNameTextField:DTtextField!
    @IBOutlet weak var emailAddressTextField:DTtextField!
    @IBOutlet weak var passwordTextField:DTtextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        //AS : To setup buttons corner style
        setup()
        setupSignUpLabelTarget()
        
        //AS : Set title and back buuton for navigationBar
        title = "Create Account"
       
       
        needSideBar = false

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: - Helping Methods
    
    func setup(){
        
        //Style
        joinNowView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        connectWithView.makeCorneredViewWith(corenerRadius: 20, and: 0)
        
        joinNowButton.addTarget(self, action: #selector(joinNowSelector), for: .touchUpInside)
        
        
        firstNameTextField.textFieldType = .firstName
        lastNameTextField.textFieldType = .firstName
        emailAddressTextField.textFieldType = .email
        passwordTextField.textFieldType = .password


    }
    
    
    func setupSignUpLabelTarget(){
        
        //AS: Setup target for label
        let signUpBusinessTap = UITapGestureRecognizer(target: self, action: #selector(signUpBusinessSelector))
        signUpBusinessLabel.isUserInteractionEnabled = true
        signUpBusinessLabel.addGestureRecognizer(signUpBusinessTap)
        
    }
    
    func checkTextFiledValidation()->Bool{
        
        if let firstNameTextField = firstNameTextField.text {
            
            if firstNameTextField.isEmpty {
                
                let alertMessage = "Please Fill First Name"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                
                return false
            }else{
                
                if !DTValidationManager.isValidName(text: firstNameTextField) {
                    let alertMessage = "Please Enter Valid First Name"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
            }
            
        }
        
        if let lastNameTextField = lastNameTextField.text {
            
            if lastNameTextField.isEmpty {
                
                let alertMessage = "Please Fill Last Name"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                
                return false
            }else{
                
                if !DTValidationManager.isValidName(text: lastNameTextField) {
                    let alertMessage = "Please Enter Valid Last Name"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
            }
            
        }
        
        if let emailTextField = emailAddressTextField.text {
            
            if emailTextField.isEmpty {
                let alertMessage = "Please Fill E-Mail"
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                return false
                
            }else{
                if !DTValidationManager.isValidEmail(text: emailTextField) {
                    let alertMessage = "Please Enter Valid E-Mail"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
                
            }
            
        }
        
        if let passwordTextField = passwordTextField.text {
            
            if passwordTextField.isEmpty {
                
                let alertMessage = "Please Fill Password"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                self.present(alertView, animated: true, completion: nil)
                
                return false
            }else{
                
                if !DTValidationManager.isValidPassword(text: passwordTextField) {
                    let alertMessage = "Please Enter Valid Password"
                    
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    
                    self.present(alertView, animated: true, completion: nil)
                    
                    return false
                }
            }
            
        }
        
      
        
        return true
    }
    
    //MARK: - Actions Methods
    
    func signUpBusinessSelector(){
        
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        self.performSegue(withIdentifier: "signUpBusinessSegueNew", sender: self)
        
    }
    
    func joinNowSelector(){
        
        callEmailExistAPI()
        
    }
    
    //MARK: - API Calls
    
    func callEmailExistAPI(){
        
        if !checkTextFiledValidation(){return}
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .emailExist , requestParams: ["email": emailAddressTextField.text])
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
        
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                let alertMessage = "Email Already Exist"
                
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                
                viewController.present(alertView, animated: true, completion: nil)
                
            }else{
                
                self?.signUpAPI()
                
            }
        }
        
    }
    
    
    func alertView(alertMessag:String){
        
        
        let alertMessage = alertMessag
   
        
        let alertView = EXAlertViewController(title: "Warning", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
        
         self.present(alertView, animated: true, completion: nil)
        
    }
   
    func signUpAPI(){
        
    
        var parameters = [String: Any]()
        
        if let emailTextField = firstNameTextField.text {
            
            parameters["FirstName"] = emailTextField
            
        }
        
        if let lastName = lastNameTextField.text {
            
            parameters["LastName"] = lastName
    
        }
        
        if let emailAddress = emailAddressTextField.text {
            
            parameters["EmailAddress"] = emailAddress
            
        }
        
        if let password = passwordTextField.text {
            
            parameters["Password"] = password
        }
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .signUp, requestParams: parameters)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                viewController.navigationController?.popToRootViewController(animated: true)
                
            }else{
                // TODO: handle error messages
            }
            
        }
    }

}
