//
//  OverViewTableViewCell.swift
//  Experdice
//
//  Created by Yousef ALselawe on 7/30/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class AboutCell: UITableViewCell {

    
    @IBOutlet weak var aboutImageView: UIImageView!
    @IBOutlet weak var aboutTitleLabel:UILabel!
    @IBOutlet weak var aboutFirstSubTitleLabel:UILabel!
    @IBOutlet weak var aboutSecondSubTitleLabel:UILabel!
    @IBOutlet weak var aboutThirdSubTitleLable:UILabel!
    @IBOutlet weak var aboutHeaderTitle:UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func populateAboutCell(aboutObject : ProfileObject, tableView : UITableView, indexPath: NSIndexPath) -> AboutCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutCell") as! AboutCell
        
        var profileDetialsArray = ["Experince","Education","Certification"]
        let profileDetials = profileDetialsArray[(indexPath.section-1)]

        
        if indexPath.section == 1 {
            
            let experienceObject = aboutObject.experienceObject?[indexPath.row]

            cell.aboutHeaderTitle.text = profileDetials
            cell.aboutTitleLabel.text = experienceObject?.experienceTitle
            cell.aboutFirstSubTitleLabel.text = experienceObject?.experienceCompanyName
            cell.aboutSecondSubTitleLabel.text = experienceObject?.experienceDateFrom
            cell.aboutThirdSubTitleLable.text = experienceObject?.experienceCountry
            
            cell.aboutImageView?.sd_setImage(with: URL(string : ""), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
                
                if error != nil {
                    
                    print(error?.localizedDescription ?? "error found")
                    
                } else {
                    
                    cell.aboutImageView?.image = image?.circleMaskBlue
                    cell.aboutImageView?.contentMode = .scaleAspectFill
                    cell.aboutImageView?.clipsToBounds = true
                    
                }
            })

           

        }else if indexPath.section == 2 {
            
            let  educationObject = aboutObject.educationObject?[indexPath.row]
            let countryName = educationObject?.educationCountry
            let cityName = educationObject?.educationCity
            cell.aboutHeaderTitle.text = profileDetials
            
            cell.aboutTitleLabel.text = educationObject?.educationSchoolName ?? "Test"
            cell.aboutFirstSubTitleLabel.text = educationObject?.educationDegree ?? "Test"
            cell.aboutSecondSubTitleLabel.text = educationObject?.educationCompletionOn ?? "Test"
            
            
            cell.aboutThirdSubTitleLable.text = countryName! ?? "" + " ," + cityName! ?? ""
           
            cell.aboutImageView?.sd_setImage(with: URL(string : ""), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
                
                if error != nil {
                    
                    print(error?.localizedDescription ?? "error found")
                    
                } else {
                    
                    cell.aboutImageView?.image = image?.circleMaskBlue
                    cell.aboutImageView?.contentMode = .scaleAspectFill
                    cell.aboutImageView?.clipsToBounds = true
                    
                }
            })
            

         
        }else {
           
            let certificationObject = aboutObject.certificationObject?[indexPath.row]
            
            cell.aboutHeaderTitle.text = profileDetials
            cell.aboutTitleLabel.text = certificationObject?.certificationName
            cell.aboutFirstSubTitleLabel.text = certificationObject?.certificationAuthority
            cell.aboutSecondSubTitleLabel.text = certificationObject?.certificationDateTo
            
            cell.aboutImageView?.sd_setImage(with: URL(string : ""), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
                
                if error != nil {
                    
                    print(error?.localizedDescription ?? "error found")
                    
                } else {
                    
                    cell.aboutImageView?.image = image?.circleMaskBlue
                    cell.aboutImageView?.contentMode = .scaleAspectFill
                    cell.aboutImageView?.clipsToBounds = true
                    
                }
            })
            
    
        }
        
        cell.selectionStyle = .none
        
        return cell
    
    }
    
 
    
    
    
    
}
