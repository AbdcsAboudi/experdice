//
//  BadgesTableViewCell.swift
//  Experdice
//
//  Created by Yousef ALselawe on 7/31/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class BadgeCell: UITableViewCell {

    @IBOutlet weak var badgesHeaderTitileLabel:UILabel!
    @IBOutlet weak var badgesImageView:UIImageView!
    @IBOutlet weak var badgesTitleLabel:UILabel!
    @IBOutlet weak var badgesMessageLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func populateBadgesCell(badgesObject : BadgeObject, tableView : UITableView, indexPath: NSIndexPath) -> BadgeCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"BadgeCell") as! BadgeCell
        
        cell.selectionStyle = .none
        
        cell.badgesHeaderTitileLabel.text = badgesObject.badgeName
        cell.badgesMessageLabel.text = badgesObject.badgeDescription
        cell.badgesTitleLabel.text = ""
        
        cell.badgesImageView?.sd_setImage(with: URL(string : ""), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
            
            if error != nil {
                
                print(error?.localizedDescription ?? "error found")
                
            } else {
                
                cell.badgesImageView?.image = image?.circleMaskBlue
                cell.badgesImageView?.contentMode = .scaleAspectFill
                cell.badgesImageView?.clipsToBounds = true
                
            }
        })
        
        
        
        return cell
        
    }
    
}
