//
//  ReviewTableViewCell.swift
//  Experdice
//
//  Created by Yousef ALselawe on 7/31/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ReviewRatingAverageCell: UITableViewCell {
    
    
    @IBOutlet weak var ratingLabelView:UIView!
    @IBOutlet weak var ratingLable:UILabel!
    
    @IBOutlet weak var roleView1:HCSStarRatingView!
    @IBOutlet weak var roleView2:HCSStarRatingView!
    @IBOutlet weak var roleView3:HCSStarRatingView!
    @IBOutlet weak var roleView4:HCSStarRatingView!
    @IBOutlet weak var roleView5:HCSStarRatingView!
    
    @IBOutlet weak var roleLable1:UILabel!
    @IBOutlet weak var roleLable2:UILabel!
    @IBOutlet weak var roleLable3:UILabel!
    @IBOutlet weak var roleLable4:UILabel!
    @IBOutlet weak var roleLable5:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
       ratingLabelView.makeCorneredViewWith(corenerRadius: 27 , and: 0)
       selectionStyle = .none
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
