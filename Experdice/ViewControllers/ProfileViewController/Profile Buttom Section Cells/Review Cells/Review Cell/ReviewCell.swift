//
//  ReviewCell.swift
//  Experdice
//
//  Created by Yousef ALselawe on 8/1/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ReviewCell: UITableViewCell {
    
    @IBOutlet weak var reviewImageView: UIImageView!
    @IBOutlet weak var reviewMessageLabel:UILabel!
    @IBOutlet weak var reviewTitleLabel:UILabel!
    @IBOutlet weak var ratingStartsView:HCSStarRatingView!
    @IBOutlet weak var businessNameLabel:UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    class func populateReviewCell(reviewObject : ReviewObject, tableView : UITableView, indexPath: NSIndexPath) -> ReviewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"ReviewCell") as! ReviewCell
        
        cell.selectionStyle = .none
        
        let number = NumberFormatter().number(from: reviewObject.ratingLevel!)
       
        
        cell.businessNameLabel.text = reviewObject.businessName
        cell.ratingStartsView.value = CGFloat(number!)
        
        cell.reviewMessageLabel.text = reviewObject.reviewDescription
        
        cell.reviewImageView?.sd_setImage(with: URL(string : ""), placeholderImage: #imageLiteral(resourceName: "myProfileImage"), options: .retryFailed, completed: { (image, error, cacheType, url) in
            
            if error != nil {
                
                print(error?.localizedDescription ?? "error found")
                
            } else {
                
                cell.reviewImageView?.image = image?.circleMaskBlue
                cell.reviewImageView?.contentMode = .scaleAspectFill
                cell.reviewImageView?.clipsToBounds = true
                
            }
        })
        
        return cell
        
    }
    
}
