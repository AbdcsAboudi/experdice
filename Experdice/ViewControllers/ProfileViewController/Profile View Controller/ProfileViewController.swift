//
//  AboutViewController.swift
//  Experdice
//
//   Created by Ahmad Sallam  on 8/1/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProfileViewController: DTViewController , UITableViewDelegate , UITableViewDataSource {
    
    
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var badgesTableView: UITableView!
    @IBOutlet weak var reviewTableVIew: UITableView!
    
    //MARK: - Variables
    var iscollapsed = false
    var profileObject:ProfileObject?
    var userObject:UserObject?
    var reviewArray = [ReviewObject]()
    var badgesArray = [BadgeObject]()
    var savedIndex:IndexPath?
    var educationDegree = [EducationDegreeObject]()
    var experinceObjectArray = [ExperienceObject]()
    var educationObjectArray = [EducationObject]()
    var certificationObjectArray = [CertificationObject]()
    var ratingRoleArray = [RatingRoleObject]()
    var averageRatingObjectArray = [AverageRatingObject]()
    var profileDetailsArray = [ProfileDetailsObject]()
    var city = String()
    var cityID = Int()
    var countryName = String()
    var countryID = Int()
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "My Profile"
        setupProfileTableView()
        setupReviewTableVIew()
        setupBadgesTableView()
        setupProfileDetailsArray()
        
        //AS: for hidding the side menu
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = true
        needSettingButton = true
        userObject = DTCredentialManager.credentialSharedInstance.user as? UserObject
        getExpertUserAPI()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableView Delegate & DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == profileTableView {
            
            return (profileDetailsArray.count + 1)
            
        }else{
            
            return 2
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //AA :
        /*
         Section 0 : Shared header between all tableViews (about, reviews, badges)
         
         Section 1 : a. In about tableView every row is an experience object.
         b. In badges tableView every row is a badge object.
         c. In reviews tableView row 1 is a rating average object and the rest rows are review objects.
         
         Section 2 : a. In about tableView every row is an education object.
         
         Section 3 : a. In about tableView every row is a certification object.
         */
        
        if section != 0 {
            
            if tableView == profileTableView {
                
                if section == 1{
                    
                    return experinceObjectArray.count
                    
                }else if section == 2 {
                    
                    return educationObjectArray.count
                    
                }else{
                    
                    return certificationObjectArray.count
                    
                }
                
            }else if tableView == badgesTableView{
                
                return badgesArray.count
                
            }else{
                
                return reviewArray.count+1
            }
            
        }else{
            
            return profileObject == nil ? 0 : 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //AA :
        /*
         Section 0 : Shared header between all tableViews (about, reviews, badges)
         
         Section 1 : a. In about tableView every row is an experience object.
         b. In badges tableView every row is a badge object.
         c. In reviews tableView row 1 is a rating average object and the rest rows are review objects.
         
         Section 2 : a. In about tableView every row is an education object.
         
         Section 3 : a. In about tableView every row is a certification object.
         
         */
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
            
                let cell = ProfileTopCell.populateProfileCell(userObject: userObject!, tableView: tableView, indexPath: indexPath as NSIndexPath)
                cell.profileLocationName.text = "\(userObject?.countryName ?? "") ,\(userObject?.cityName ?? "")"
                
                return cell
                
            }else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileButtomCell") as! ProfileButtomCell
                
                cell.seeMoreButton.addTarget(self, action: #selector(self.toggleButton(sender:)), for: .touchUpInside)
                cell.messageLabel.text = userObject?.overviewText
                if iscollapsed {
                    
                    cell.seeMoreButton.setTitle("See Less", for: .normal)
                    cell.dropImage.image = #imageLiteral(resourceName: "drop_down")
                    cell.dropImage.transform = CGAffineTransform(scaleX: 1, y: -1)
                    
                    cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1)
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.layer.transform = CATransform3DMakeScale(1.05, 1.05, 1)
                    },completion: { finished in
                        UIView.animate(withDuration: 0.1, animations: {
                            cell.layer.transform = CATransform3DMakeScale(1, 1, 1)
                        })
                    })
                }else {
                    
                    cell.seeMoreButton.setTitle("See More", for: .normal)
                    cell.dropImage.image = #imageLiteral(resourceName: "drop_down")
                    cell.dropImage.transform = CGAffineTransform(scaleX: -1, y: 1)

                }
                
                return cell
                
            }
            
            
        } else {
            
            if tableView == profileTableView {
                
                let cell = AboutCell.populateAboutCell(aboutObject:profileObject! , tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                return cell
                
            }else if tableView == badgesTableView {
                
                let badgeObject = badgesArray[indexPath.row]
                
                let cell = BadgeCell.populateBadgesCell(badgesObject: badgeObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                return cell
                
            }else {
                
                if indexPath.row == 0 {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier:"ReviewRatingAverageCell" ) as! ReviewRatingAverageCell
                    
                    return cell
                    
                }else {
                    
                    let index = indexPath.row - 1
                    let reviewObject = reviewArray[index]
                    
                    let cell = ReviewCell.populateReviewCell(reviewObject:reviewObject, tableView: reviewTableVIew, indexPath: indexPath as NSIndexPath)
                    
                    return cell
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                
                return 180
                
            }else {
                
                if iscollapsed {
                    
                    return UITableViewAutomaticDimension
                    
                }else {
                    
                    return 70
                }
            }
            
        }else {
            
            if tableView == reviewTableVIew {
                
                if indexPath.row == 0 {
                    
                    return 130
                    
                } else {
                    
                    return UITableViewAutomaticDimension
                }
                
            }else{
                
                return 135
                
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            
            if let headerSectionView = Bundle.main.loadNibNamed("ProfileHeaderSectionButtonView", owner: self, options: nil)?.first as? ProfileHeaderSectionButtonView {
                
                if tableView == profileTableView {
                    
                    headerSectionView.reviewsButton.addTarget(self, action: #selector(showReviewSelector), for: .touchUpInside)
                    
                    headerSectionView.badgesButton.addTarget(self, action: #selector(showBadgesTableView), for: .touchUpInside)
                    
                    headerSectionView.setSelected(buttonIndex: 1)
                    
                    
                } else if tableView == badgesTableView {
                    
                    
                    headerSectionView.aboutButton.addTarget(self, action: #selector(showMyProfileTableView), for: .touchUpInside)
                    
                    headerSectionView.reviewsButton.addTarget(self, action: #selector(showReviewSelector), for: .touchUpInside)
                    
                    headerSectionView.setSelected(buttonIndex: 2)
                    
                } else {
                    
                    
                    headerSectionView.aboutButton.addTarget(self, action: #selector(showMyProfileTableView), for: .touchUpInside)
                    
                    headerSectionView.badgesButton.addTarget(self, action: #selector(showBadgesTableView), for: .touchUpInside)
                    
                    headerSectionView.setSelected(buttonIndex: 3)
                    
                }
                
                return headerSectionView
            }
        }
        
        return nil
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 && profileObject != nil {
            
            return 45
            
        } else {
            
            return 0
            
        }
        
    }
    
    
    //MARK: - Helping Methods
    
    
    func setupProfileDetailsArray(){
        
        profileDetailsArray.append(ProfileDetailsObject(title:"Education" , id: 0))
        profileDetailsArray.append(ProfileDetailsObject(title:"Certification" , id: 1))
        profileDetailsArray.append(ProfileDetailsObject(title:"Experience" , id: 2))
        
    }
    
    func setupProfileTableView(){
        
        profileTableView.register(UINib(nibName: "ProfileTopCell", bundle: nil), forCellReuseIdentifier: "ProfileTopCell")
        
        profileTableView.register(UINib(nibName: "ProfileButtomCell", bundle : nil), forCellReuseIdentifier: "ProfileButtomCell")
        
        profileTableView.register(UINib(nibName: "ProfileHeaderSectionButtonView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfileHeaderSectionButtonView")
        
        profileTableView.register(UINib(nibName: "AboutCell", bundle: nil), forCellReuseIdentifier: "AboutCell")
        
        profileTableView.delegate = self
        profileTableView.dataSource = self
        
        profileTableView.separatorStyle = .none
        
        
    }
    
    func setupBadgesTableView () {
        
        badgesTableView.register(UINib(nibName: "ProfileTopCell", bundle: nil), forCellReuseIdentifier: "ProfileTopCell")
        badgesTableView.register(UINib(nibName: "ProfileButtomCell", bundle : nil), forCellReuseIdentifier: "ProfileButtomCell")
        
        badgesTableView.register(UINib(nibName: "ProfileHeaderSectionButtonView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfileHeaderSectionButtonView")
        
        
        badgesTableView.register(UINib(nibName: "BadgeCell", bundle: nil), forCellReuseIdentifier: "BadgeCell")
        
        badgesTableView.delegate = self
        badgesTableView.dataSource = self
        
        badgesTableView.separatorStyle = .none
        
    }
    
    func setupReviewTableVIew() {
        
        
        reviewTableVIew.register(UINib(nibName: "ProfileTopCell", bundle: nil), forCellReuseIdentifier: "ProfileTopCell")
        reviewTableVIew.register(UINib(nibName: "ProfileButtomCell", bundle : nil), forCellReuseIdentifier: "ProfileButtomCell")
        
        reviewTableVIew.register(UINib(nibName: "ProfileHeaderSectionButtonView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfileHeaderSectionButtonView")
        
        
        reviewTableVIew.register(UINib(nibName:"ReviewCell",bundle:nil), forCellReuseIdentifier: "ReviewCell")
        reviewTableVIew.register(UINib(nibName: "ReviewRatingAverageCell",bundle:nil), forCellReuseIdentifier: "ReviewRatingAverageCell")
        
        reviewTableVIew.delegate = self
        reviewTableVIew.dataSource = self
        
        reviewTableVIew.separatorStyle = .none
        
    }
    
    func fillArraysWithObject(){
        if let profile = userObject?.profile {
            profileObject = profile
            reviewArray = profile.reviews!
            badgesArray = profile.badges!
            
            educationObjectArray = profile.educationObject!
            certificationObjectArray = profile.certificationObject!
            experinceObjectArray = profile.experienceObject!
            
            reviewTableVIew.reloadData()
            profileTableView.reloadData()
            badgesTableView.reloadData()
        }
    }
    
    //MARK: - API Call
    
    func callCountryAPI(countryId:Int){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCountry, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let countryArray = DTParser.parseCountryObjectArray(from: response)
                    
                    for country in countryArray {
                        if country.id == countryId {
                            viewController.countryName = country.name!
                        }
                    }
                    if let countryId = viewController.userObject?.countryId {
                        viewController.callGetCityAPI(id:countryId)
                    }
                }
            }else{
            }
        }
        
    }
    
    func callGetCityAPI(id:Int){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCity, requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            MBProgressHUD.hide(for: viewController.view, animated: true)
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let cityArray = DTParser.parseCityObjectArray(from: response)
                    if let cityId = viewController.userObject?.cityId{
                        for city in cityArray  {
                            if city.id == cityId  {
                                viewController.city = city.name!
                            }
                            
                        }
                    }
                }
            }
        }
   
    }
    
    func getExpertUserAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getExpert, requestParams: user?.userID)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            MBProgressHUD.hide(for: viewController.view, animated: true)
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let user = DTParser.parseExpertUserObjectFromResponseArray(response: response)
                    viewController.userObject?.profile.experienceObject = user.profile.experienceObject
                    viewController.userObject?.profile.educationObject = user.profile.educationObject
                    viewController.userObject?.profile.certificationObject = user.profile.certificationObject
                    viewController.userObject?.profile.reviews = user.profile.reviews
                    viewController.userObject?.profile.badges = user.profile.badges
                    viewController.userObject?.countryName = user.countryName
                    viewController.userObject?.cityName = user.cityName
                    viewController.userObject?.countryId = user.countryId
                    viewController.userObject?.cityId = user.cityId
                    viewController.userObject?.stateId = user.stateId
                    DTCredentialManager.credentialSharedInstance.user = viewController.userObject
                
                    if let countryId = viewController.userObject?.countryId {
                        //viewController.callCountryAPI(countryId: countryId)
                    }

                }
                viewController.fillArraysWithObject()
            }
        }
        
    }
    
    
    // MARK: - Actions Or Selectors
    
    func toggleButton (sender:UIButton) {
        
        iscollapsed = !iscollapsed
        
        badgesTableView.reloadData()
        profileTableView.reloadData()
        reviewTableVIew.reloadData()
        
    }
    
    func showMyProfileTableView(){
        
        profileTableView.isHidden = false
        badgesTableView.isHidden = true
        reviewTableVIew.isHidden = true
        
    }
    
    func showBadgesTableView(){
        
        badgesTableView.isHidden = false
        profileTableView.isHidden = true
        reviewTableVIew.isHidden = true
        
    }
    
    func showReviewSelector(){
        
        reviewTableVIew.isHidden = false
        profileTableView.isHidden = true
        badgesTableView.isHidden = true
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "profileSettingsSegue" {
            
            let vc = segue.destination as? ProfileSettingsViewController
            
            //vc?.user = userObject
        }
        
    }
    
    
    
    
}
