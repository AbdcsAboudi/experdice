//
//  ProfileBusinessViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 9/12/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProfileBusinessViewController: DTViewController , UITableViewDelegate , UITableViewDataSource {
    
    
    
    @IBOutlet weak var profileTableView:UITableView!
    @IBOutlet weak var reviewTableView:UITableView!
    
    //MARK: - Variables
    var iscollapsed = false
    var profileObject:ProfileObject?
    var userObject:UserObject?
    var reviewArray = [ReviewObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "My Profile"
        
        getBusinessUserAPI()
        
        setupProfileTableView()
        
        setupReviewTableVIew()
        
        //AS: for hidding the side menu
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = true
        needSettingButton = true
        
        super.viewWillAppear(animated)
        
    }
    
    
    //MARK: - UITableView Delegate & DataSource Methods
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 && profileObject != nil {
            
            return 45
            
        } else {
            
            return 0
            
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
       
        if section == 1 {
            
            if let headerSectionView = Bundle.main.loadNibNamed("ProfileBusinessHeaderButton", owner: self, options: nil)?.first as? ProfileBusinessHeaderSectionButton {
                
                if tableView == profileTableView {
                    
                    headerSectionView.profileReviewsTableViewButton.addTarget(self, action: #selector(showReviewSelector), for: .touchUpInside)
                    
                    headerSectionView.setSelected(buttonIndex: 1)
                    
                    
                    
                }  else {
                    
                    headerSectionView.aboutProfileTableViewButton.addTarget(self, action: #selector(showProfileSelector), for: .touchUpInside)
                    
                    
                    headerSectionView.setSelected(buttonIndex: 2)
                    
                }
                
                return headerSectionView
            }
        }
        
        return nil
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                
                return 180
                
            }else {
                
                if iscollapsed {
                    
                    return UITableViewAutomaticDimension
                    
                }else {
                    
                    return 70
                }
            }
            
        }
        else if tableView == reviewTableView {
            
            if indexPath.row == 0 {
                
                return 130
                
            } else {
                
                return UITableViewAutomaticDimension
            }
            
        }
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            
            return 2
            
        }
        if tableView == profileTableView {
            
            return 3
        }else{
            
            return reviewArray.count + 1
        }
    
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if userObject != nil {
            
            if indexPath.section == 0  {
                
                if indexPath.row == 0 {
                    
                    let cell = ProfileTopCell.populateProfileCell(userObject: userObject!, tableView: tableView, indexPath: indexPath as NSIndexPath)
                    
                    return cell
                    
                }else {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileButtomCell") as! ProfileButtomCell
                    
                    cell.seeMoreButton.addTarget(self, action: #selector(self.toggleButton(sender:)), for: .touchUpInside)
                    
                    if iscollapsed {
                        
                        cell.seeMoreButton.setTitle("See Less", for: .normal)
                        cell.dropImage.image = #imageLiteral(resourceName: "drop_down")
                        cell.dropImage.transform = CGAffineTransform(scaleX: 1, y: -1)
                        
                    }else {
                        
                        cell.seeMoreButton.setTitle("See More", for: .normal)
                        cell.dropImage.image = #imageLiteral(resourceName: "drop_down")
                        cell.dropImage.transform = CGAffineTransform(scaleX: -1, y: 1)
                        
                    }
                    
                    return cell
                }
                
            }else{
                
                if tableView == profileTableView{
                    let cell = ProfileBusinessCell.populateBusinessProfileCell(userObject: userObject!, tableView: tableView, indexPath: indexPath as NSIndexPath)
                    
                    return cell
                }
                else {
                    
                    if indexPath.row == 0 {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier:"ReviewRatingAverageCell" ) as! ReviewRatingAverageCell
                        
                        return cell
                        
                    }else{
                        
                        let index = indexPath.row - 1
                        let reviewObject = reviewArray[index]
                        let cell = ReviewCell.populateReviewCell(reviewObject:reviewObject, tableView: reviewTableView, indexPath: indexPath as NSIndexPath)
                        
                        return cell
                        
                    }
                    
                }
                
            }
        }else{
             return UITableViewCell()
        }
    }
    
    //MARK: - Helping Method
    
    func fillDumyData(){
        
        reviewArray.append(ReviewObject(id: 0,
                                        reviewTitle: "Review Test",
                                        reviewDescription: "Review Description",
                                        ratingId: 1,
                                        ratingLevel: "1",
                                        businessId: 1,
                                        businessName: "Platinum"))
        
        reviewArray.append(ReviewObject(id: 0,
                                        reviewTitle: "Review Test",
                                        reviewDescription: "Review Description",
                                        ratingId: 1,
                                        ratingLevel: "1",
                                        businessId: 1,
                                        businessName: "Platinum"))
        
        
        reviewArray.append(ReviewObject(id: 0,
                                        reviewTitle: "Review Test",
                                        reviewDescription: "Review Description",
                                        ratingId: 1,
                                        ratingLevel: "1",
                                        businessId: 1,
                                        businessName: "Platinum"))
    }
    
    func setupProfileTableView(){
        
        profileTableView.register(UINib(nibName: "ProfileTopCell", bundle: nil), forCellReuseIdentifier: "ProfileTopCell")
        
        profileTableView.register(UINib(nibName: "ProfileButtomCell", bundle : nil), forCellReuseIdentifier: "ProfileButtomCell")
        
        
        profileTableView.register(UINib(nibName: "ProfileBusinessCell", bundle : nil), forCellReuseIdentifier: "ProfileBusinessCell")
        
        profileTableView.register(UINib(nibName: "ProfileBusinessHeaderSectionButton", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfileBusinessHeaderSectionButton")
        
        
        profileTableView.delegate = self
        profileTableView.dataSource = self
        
        profileTableView.separatorStyle = .none
        
    }
    
    
    func setupReviewTableVIew() {
        
        
        reviewTableView.register(UINib(nibName: "ProfileTopCell", bundle: nil), forCellReuseIdentifier: "ProfileTopCell")
        reviewTableView.register(UINib(nibName: "ProfileButtomCell", bundle : nil), forCellReuseIdentifier: "ProfileButtomCell")
        
        reviewTableView.register(UINib(nibName: "ProfileBusinessCell", bundle : nil), forCellReuseIdentifier: "ProfileBusinessCell")
        
        reviewTableView.register(UINib(nibName:"ReviewCell",bundle:nil), forCellReuseIdentifier: "ReviewCell")
        reviewTableView.register(UINib(nibName: "ReviewRatingAverageCell",bundle:nil), forCellReuseIdentifier: "ReviewRatingAverageCell")
        
        reviewTableView.register(UINib(nibName: "ProfileBusinessHeaderSectionButton", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfileBusinessHeaderSectionButton")
        
        reviewTableView.delegate = self
        reviewTableView.dataSource = self
        
        reviewTableView.separatorStyle = .none
        
    }
    
    
    func toggleButton (sender:UIButton) {
        
        iscollapsed = !iscollapsed
        
        profileTableView.reloadData()
        reviewTableView.reloadData()
        
    }
   
    
    //MARK: - Selector & Action
    func showReviewSelector(){
        
        reviewTableView.isHidden = false
        profileTableView.isHidden = true
        
    }
    func showProfileSelector(){
        
        profileTableView.isHidden = false
        reviewTableView.isHidden = true
        
    }
    
    //MARK: - API Call
    
    func getBusinessUserAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getClient, requestParams: user?.userID)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                   viewController.userObject = DTParser.parseBusinessUserObjectFromResponseArray(response: response)
                    viewController.reviewArray = DTParser.parseReviewObjectArray(response: response)
                    viewController.profileObject = viewController.userObject?.profile
                     viewController.fillDumyData()
                    viewController.reviewTableView.reloadData()
                    viewController.profileTableView.reloadData()
                   
                    
                }
                
            }
        }
    }
    
    
    
    
    
    
}
