//
//  ProfileHeaderSectionButton.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 9/12/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProfileBusinessHeaderSectionButton: UITableViewHeaderFooterView {

    @IBOutlet weak var aboutProfileTableViewButton: UIButton!
    @IBOutlet weak var profileReviewsTableViewButton: UIButton!
    
    var colorSelect:UIColor?
    var tintColorr:UIColor?
    var defaultColor:UIColor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //AS: Configure the view for the selected state
        setSelected(buttonIndex: 2)
        
    }
    
    
    func selectAboutButton (){
        
        aboutProfileTableViewButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonColorSelected()
        aboutProfileTableViewButton.setTitleColor(DTHelper.sharedInstance.colorWithHexString("#F4D45E"), for: .normal)
        
        aboutProfileTableViewButton.isSelected = true
        
        profileReviewsTableViewButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonDefault()
        //profileReviewsTableViewButton.setTitleColor(.white, for: .normal)
    }
    
    
    func selectReviewsButton (){
        
        profileReviewsTableViewButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonColorSelected()
        profileReviewsTableViewButton.setTitleColor(DTHelper.sharedInstance.colorWithHexString("#F4D45E"), for: .normal)
        
        profileReviewsTableViewButton.isSelected = true
        
        aboutProfileTableViewButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonDefault()
        //aboutProfileTableViewButton.setTitleColor(.white, for: .normal)
    }
    
    // MARK : Action
    
    func setSelected(buttonIndex: Int) {
        
        resetSelectedButton()
        
        switch buttonIndex {
        case 1:
            selectAboutButton()
            break
            
        case 2:
            selectReviewsButton ()
            break
            
        default:
            print("x")
        }
        
    }
    
    func resetSelectedButton() {
        
        aboutProfileTableViewButton.isSelected = false
        profileReviewsTableViewButton.isSelected = false
        
        aboutProfileTableViewButton.isEnabled = true
        profileReviewsTableViewButton.isEnabled = true
        
    }
    
    

}
