//
//  ProfileBusinessCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 9/12/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProfileBusinessCell: UITableViewCell {

    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var message:UILabel!
    
   
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    
    
    
    class func populateBusinessProfileCell(userObject : UserObject, tableView : UITableView, indexPath: NSIndexPath) -> ProfileBusinessCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"ProfileBusinessCell") as! ProfileBusinessCell
        var titleForRow = ["Founded","Type","Specialties"]
        
        cell.selectionStyle = .none

        cell.title.text = titleForRow[indexPath.row]
        
        if indexPath.row == 0 {
            
            cell.message.text = userObject.profile.founded
            
        }else if indexPath.row == 1{
            
            cell.message.text = userObject.profile.typeOfBusiness
        }
        else{
            
            cell.message.text = userObject.profile.specialties
         }
        
        return cell
        
    }
    
}
