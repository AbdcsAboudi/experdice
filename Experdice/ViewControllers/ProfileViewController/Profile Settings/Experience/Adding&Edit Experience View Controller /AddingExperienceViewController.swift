//
//  Adding&EditExperienceViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/21/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol AddingExperienceViewControllerDelegate {
    func reloadTableView()
}

class AddingExperienceViewController: DTViewController {
 
    @IBOutlet weak var titleTextFiled:UITextField!
    @IBOutlet weak var companyNameTextFiled:UITextField!
    @IBOutlet weak var countryTextFiled:DTtextField!
    @IBOutlet weak var cityTextFiled:DTtextField!
    @IBOutlet weak var fromTimePeriodTextFiled:UITextField!
    @IBOutlet weak var toTimePeriodTextFiled:UITextField!
    @IBOutlet weak var switchWorkHere:UISwitch!
    @IBOutlet weak var doneButton:UIButton!
    @IBOutlet weak var roundedDoneButtonView:UIView!
    @IBOutlet weak var activityForCity:UIActivityIndicatorView!
    @IBOutlet weak var toDateLabel: UILabel!
    
    var city = [String]()
    var cityID = [Int]()
    var countryName = [String]()
    var countryID = [Int]()
    
    var fromDatePicker = UIDatePicker()
    var toDatePicker = UIDatePicker()
    var didSelectCountry = false
    var experinceViewControllerDelegate: AddingExperienceViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Add Experience"
        activityForCity.isHidden = true
        switchWorkHere.isOn = false
        setupTextFiledAndButton()
        callCountryAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        super.viewWillAppear(true)
    }
    
    //MARK: - Selector and Action 
    
    @objc func doneButtonSelector(){
        
        callAddExperienceAPI()
    }
    
    func fromDatePickerValueChange(sender:UIDatePicker){
        
        //use date formatter in dthelper
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd"
        fromTimePeriodTextFiled.text = dateFormatter.string(from: sender.date)
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    //MARK: - Nawaf was here
    func toDatePickreValueChange(sender:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd"
        toTimePeriodTextFiled.text = dateFormatter.string(from: sender.date)
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    //MARK: - TextFiled Delegate
    
    func countryTextFildEndEditing(_ textField:DTtextField){
        
        city.removeAll()
        cityID.removeAll()
        cityTextFiled.domainOptions.removeAll()
        cityTextFiled.text = ""
        cityTextFiled.isEnabled = false
        activityForCity.isHidden = false
        activityForCity.startAnimating()
        callGetCityAPI(id:textField.selectdIndex+1)
        cityTextFiled.isEnabled = didSelectCountry
        
    }
    
    //MARK: -  Switch Delegate
    
    func switchWorkHereEndEditing(_ switchWorkHere2:UISwitch){
        
        if switchWorkHere2.isOn {
            toDateLabel.isEnabled = false
            toTimePeriodTextFiled.isEnabled = false
        }else{
            toDateLabel.isEnabled = true
            toTimePeriodTextFiled.isEnabled = true
        }
    }
    
    
    //MARK: - API Call
    
    func callAddExperienceAPI(){
        
        var parm = [String:Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parm["ExpertId"] = user?.profile.expertId
        parm["ExperienceId"] = 0
        parm["Title"] = titleTextFiled.text
        parm["Company"] = companyNameTextFiled.text
        parm["DateFrom"] = fromTimePeriodTextFiled.text
        parm["DateTo"] = toTimePeriodTextFiled.text
        if switchWorkHere.isOn {
            parm["CurrentlyWorking"] = "1"
        }else{
             parm["CurrentlyWorking"] = "0"
        }
        
        parm["CountryId"] = countryID[countryName.index(of: countryTextFiled.text!)!]
        parm["CityId"] = cityID[city.index(of: cityTextFiled.text!)!]
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)

        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .editOrAddExperience, requestParams: parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
             viewController.fireDelegate()
            }
            
        }
        
    }
    
    func callCountryAPI(){
       
        MBProgressHUD.showAdded(to: self.view, animated: true)
      
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCountry, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)

            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let countryArray = DTParser.parseCountryObjectArray(from: response)
                    
                    for country in countryArray  {
                        
                        if let countryName = country.name {
                            
                            self?.countryName.append(countryName)
                            
                        }
                        
                        if let countryID = country.id {
                            
                            self?.countryID.append(countryID)
                        }
                        
                    }
                    self?.setupCountryArray()
                    //                    if viewController.user!.countryId != 0{
                    //
                    //                        viewController.countryTextFiled.text = viewController.countryName[viewController.user!.countryId!-1]
                    //                    }
                }
                  viewController.didSelectCountry = !viewController.didSelectCountry
                MBProgressHUD.hide(for: viewController.view, animated: true)
                
            }
        }
        
    }
    
    func callGetCityAPI(id:Int){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCity, requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let cityArray = DTParser.parseCityObjectArray(from: response)
                    
                    for city in cityArray  {
                        
                        if let cityName = city.name {
                            
                            viewController.city.append(cityName)
                            
                        }
                        
                        if let cityID = city.id {
                            
                            viewController.cityID.append(cityID)
                        }
                        
                    }
                    viewController.setupCityArray()
                    
                  
                }
                viewController.didSelectCountry = true
               viewController.activityForCity.stopAnimating()
              viewController.activityForCity.isHidden = true
            }
        }
    }
    
    

    //MARK: - Experince ViewController Deleg
    
    func fireDelegate() {
        
        experinceViewControllerDelegate?.reloadTableView()
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Helping Method
    
    func setupCountryArray(){
        
        var index = 0
        for countryName in countryName {
            
            countryTextFiled.domainOptions.append(DTDomainObject(id: countryID[index], display: countryName))
            
            index += 1
        }
        
    }
    
    func setupCityArray(){
        
        var index = 0
        for cityName in city {
            
            cityTextFiled.domainOptions.append(DTDomainObject(id: cityID[index] , display: cityName))
            
            index += 1
        }
    }
    
    func setupTextFiledAndButton(){
        
        titleTextFiled.setTextFieldTextColor()
        companyNameTextFiled.setTextFieldTextColor()
        countryTextFiled.setTextFieldTextColor()
        cityTextFiled.setTextFieldTextColor()
        fromTimePeriodTextFiled.setTextFieldTextColor()
        toTimePeriodTextFiled.setTextFieldTextColor()
        cityTextFiled.isEnabled = didSelectCountry
        roundedDoneButtonView.makeCorneredViewWith(corenerRadius: 15, and: 0)
        doneButton.addTarget(self, action: #selector(doneButtonSelector), for: .touchUpInside)
        fromTimePeriodTextFiled.inputView = fromDatePicker
        toTimePeriodTextFiled.inputView = toDatePicker
        
        fromDatePicker.datePickerMode = .date
        toDatePicker.datePickerMode = .date
        
        fromDatePicker.addTarget(self, action: #selector(fromDatePickerValueChange), for: .valueChanged)
        toDatePicker.addTarget(self, action: #selector(toDatePickreValueChange), for: .valueChanged)
        
        countryTextFiled.textFieldType = .domain
        cityTextFiled.textFieldType = .domain
        
        countryTextFiled.addTarget(self, action: #selector(countryTextFildEndEditing), for: .editingDidEnd)
       
        switchWorkHere.addTarget(self, action: #selector(switchWorkHereEndEditing),for:.valueChanged)

        
    }
    
    
    
    

}
