//
//  EditExperienceViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/21/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol EditingExperienceViewControllerDelegate {
    func reloadAfterEditTableView()
}

class EditExperienceViewController: DTViewController {
    
    @IBOutlet weak var titleTextFiled:UITextField!
    @IBOutlet weak var companyNameTextFiled:UITextField!
    @IBOutlet weak var countryTextFiled:DTtextField!
    @IBOutlet weak var cityTextFiled:DTtextField!
    @IBOutlet weak var fromTimePeriodTextFiled:UITextField!
    @IBOutlet weak var toTimePeriodTextFiled:UITextField!
    @IBOutlet weak var switchWorkHere:UISwitch!
    @IBOutlet weak var activityForCity:UIActivityIndicatorView!
    @IBOutlet weak var toDateLabel: UILabel!
    
    var didSelectCountry = false
    var fromDatePicker = UIDatePicker()
    var toDatePicker = UIDatePicker()
    var city = [String]()
    var cityID = [Int]()
    var countryName = [String]()
    var countryID = [Int]()
    var experienceObject:ExperienceObject?
    var experinceViewControllerDelegate:EditingExperienceViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityForCity.isHidden = true
        setupTextFiledAndButton()
        fillDataObject()
        callCountryAPI()
        title = "Edit Experience"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        super.viewWillAppear(true)
    }
    
    //MARK: - Selector And Action
    
    func closeExperinceSelector(){
        navigationController?.popViewController(animated: true)
    }
    
    func doneEditSelector(){
        callEditExperienceAPI()
    }
    
    func fromDatePickerValueChange(sender:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd"
        fromTimePeriodTextFiled.text = dateFormatter.string(from: sender.date)
    }
    
    func toDatePickreValueChange(sender:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd"
        toTimePeriodTextFiled.text = dateFormatter.string(from: sender.date)
     
    }
    
    //MARK: - TextFiled Delegate
    
    func countryTextFildEndEditing(_ textField:DTtextField){
        
        city.removeAll()
        cityID.removeAll()
        cityTextFiled.domainOptions.removeAll()
        cityTextFiled.text = ""
        cityTextFiled.isEnabled = false
        activityForCity.isHidden = false
        activityForCity.startAnimating()
        callGetCityAPI(id:textField.selectdIndex+1)
        cityTextFiled.isEnabled = didSelectCountry
        
    }
    
    //MARK: -  Switch Delegate
    
    func switchWorkHereEndEditing(_ switchWorkHere2:UISwitch){
        
        let tempDate = toTimePeriodTextFiled.text
        
        if switchWorkHere2.isOn {
            toTimePeriodTextFiled.text?.removeAll()
            toDateLabel.isEnabled = false
            toTimePeriodTextFiled.isEnabled = false
        }else{
            toDateLabel.isEnabled = true
            toTimePeriodTextFiled.isEnabled = true
            toTimePeriodTextFiled.text = tempDate
        }
    }
    
    //MARK: - Helping Method
    
    func setupCountryArray(){
        
        var index = 0
        for countryName in countryName {
            
            countryTextFiled.domainOptions.append(DTDomainObject(id: countryID[index], display: countryName))
            
            index += 1
        }
    }
    func setupCityArray(){
        
        var index = 0
        for cityName in city {
            
            cityTextFiled.domainOptions.append(DTDomainObject(id: cityID[index] , display: cityName))
            
            index += 1
        }
    }
    
   
    func fillDataObject(){
        
        titleTextFiled.text = experienceObject?.experienceTitle
        companyNameTextFiled.text = experienceObject?.experienceCompanyName
        countryTextFiled.text = experienceObject?.experienceCountry
        if let object = experienceObject {
            if !object.experienceCity!.isEmpty {
                cityTextFiled.isEnabled = true
                activityForCity.isHidden = true
                callGetCityAPI(id: object.countryId!)
            }
            if object.isWorkHere == "1"{
                switchWorkHere.setOn(true, animated: false)
                toDateLabel.isEnabled = false
                toTimePeriodTextFiled.isEnabled = false
                toTimePeriodTextFiled.text = ""
            }else{
                switchWorkHere.setOn(false, animated: false)
                toDateLabel.isEnabled = true
                toTimePeriodTextFiled.isEnabled = true
                toTimePeriodTextFiled.text = experienceObject?.experienceDateTo
            }
        }
        
        cityTextFiled.text = experienceObject?.experienceCity
        fromTimePeriodTextFiled.text = experienceObject?.experienceDateFrom
        
        
    }
  
    
    func setupTextFiledAndButton(){
       
        titleTextFiled.setTextFieldTextColor()
        companyNameTextFiled.setTextFieldTextColor()
        countryTextFiled.setTextFieldTextColor()
        cityTextFiled.setTextFieldTextColor()
        fromTimePeriodTextFiled.setTextFieldTextColor()
        toTimePeriodTextFiled.setTextFieldTextColor()
        cityTextFiled.isEnabled = didSelectCountry
        
        fromTimePeriodTextFiled.inputView = fromDatePicker
        toTimePeriodTextFiled.inputView = toDatePicker
        
        fromDatePicker.datePickerMode = .date
        toDatePicker.datePickerMode = .date
        
        fromDatePicker.addTarget(self, action: #selector(fromDatePickerValueChange), for: .valueChanged)
        toDatePicker.addTarget(self, action: #selector(toDatePickreValueChange), for: .valueChanged)
        switchWorkHere.addTarget(self, action: #selector(switchWorkHereEndEditing),for:.valueChanged)
        countryTextFiled.textFieldType = .domain
        cityTextFiled.textFieldType = .domain
        countryTextFiled.addTarget(self, action: #selector(countryTextFildEndEditing), for: .editingDidEnd)
        
    }
    
  
    
    func setEditButton(){
        
        let cancelButton = UIButton(type: .custom)
        let doneButton = UIButton(type: .custom)
        
        doneButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        doneButton.setImage(#imageLiteral(resourceName: "doneEditpng") ,for: .normal)
        cancelButton.setImage(#imageLiteral(resourceName: "closeEdit"), for: .normal)
        
        cancelButton.addTarget(self, action: #selector(closeExperinceSelector), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneEditSelector), for: .touchUpInside)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cancelButton)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: doneButton)
        
    }
    
    //MARK: - API Call
    
    func callEditExperienceAPI(){
        
        var parm = [String:Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parm["ExpertId"] = user?.profile.expertId
        parm["ExperienceId"] = experienceObject?.id
        parm["Title"] = titleTextFiled.text
        parm["Company"] = companyNameTextFiled.text
        parm["DateFrom"] = fromTimePeriodTextFiled.text
        
        if switchWorkHere.isOn {
            parm["CurrentlyWorking"] = "1"
            parm["DateTo"] = ""
        }else{
            parm["CurrentlyWorking"] = "2"
            parm["DateTo"] = toTimePeriodTextFiled.text
        }
        
        parm["CountryId"] = countryID[countryName.index(of: countryTextFiled.text!)!]
        parm["CityId"] = cityID[city.index(of: cityTextFiled.text!)!]
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .editOrAddExperience, requestParams: parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {                
                viewController.fireDelegate()
            }
            
        }
        
    }
    
    func callCountryAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCountry, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let countryArray = DTParser.parseCountryObjectArray(from: response)
                    
                    for country in countryArray  {
                        
                        if let countryName = country.name {
                            
                            self?.countryName.append(countryName)
                            
                        }
                        
                        if let countryID = country.id {
                            self?.countryID.append(countryID)
                        }
                        
                    }
                    self?.setupCountryArray()
                }
                if !viewController.didSelectCountry{
                    viewController.didSelectCountry = !viewController.didSelectCountry
                }else{
                    viewController.didSelectCountry = true

                }
                viewController.setEditButton()
                MBProgressHUD.hide(for: viewController.view, animated: true)
                
            }
        }
        
    }
    
    func callGetCityAPI(id:Int){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCity, requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let cityArray = DTParser.parseCityObjectArray(from: response)
                    
                    for city in cityArray  {
                        
                        if let cityName = city.name {
                            
                            viewController.city.append(cityName)
                            
                        }
                        
                        if let cityID = city.id {
                            
                            viewController.cityID.append(cityID)
                        }
                        
                    }
                    viewController.setupCityArray()
                    
                }
                viewController.didSelectCountry = true
                viewController.activityForCity.stopAnimating()
                viewController.activityForCity.isHidden = true
            }
        }
    }
    
    
    //MARK: - Experince ViewController Deleg
    
    func fireDelegate() {
        experinceViewControllerDelegate?.reloadAfterEditTableView()
        navigationController?.popViewController(animated: true)
    }
    
   
    


}
