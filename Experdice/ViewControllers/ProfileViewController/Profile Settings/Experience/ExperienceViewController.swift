//
//  ExperienceViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/20/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD


class ExperienceViewController: DTListViewController,AddingExperienceViewControllerDelegate,EditingExperienceViewControllerDelegate{
   
    @IBOutlet weak var experinceTableView:UITableView!
    
   var experinceObjectArray = [ExperienceObject]()
    
    override func viewDidLoad() {
        tableView = experinceTableView
        super.viewDidLoad()
        tableViewSetup()
        title = "Experience"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        super.viewWillAppear(true)
    }
    
    //MARK: - Table View Delegate And Data Source
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete && !list.isEmpty {
            deleteExperienceAPI(experienceObjetc:list[indexPath.row] as! ExperienceObject)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if !list.isEmpty,let experinceObject = list[indexPath.row] as? ExperienceObject {
            
            let desnationViewControler = EditExperienceViewController(nibName: "EditExperienceViewController", bundle: nil)
            desnationViewControler.experinceViewControllerDelegate = self
            desnationViewControler.experienceObject = experinceObject
            show(desnationViewControler, sender: self)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 110
    }
    
    override func populateTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !list.isEmpty,let experinceObject = list[indexPath.row] as? ExperienceObject {
            
            let cell = ExperienceCell.populateExperienceCell(experienceObject:experinceObject,tableView: tableView, indexPath: indexPath as NSIndexPath)
            
            return cell
        }
        return UITableViewCell()
    }
    
    //MARK: - Selector & Action
    
    func addExperinceSelector(){
        
        let vc = AddingExperienceViewController(nibName: "AddingExperienceViewController", bundle: nil)
        vc.experinceViewControllerDelegate = self
        self.show(vc, sender: self)
    }
    
    
   
     //MARK: - Calling API
    
    func getExperienceListAPI(){
        
        var parms = [String:Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parms["ExpertId"] = user?.profile.expertId
        parms["ExperienceID"] = 0
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getExperience, requestParams: parms)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                viewController.list = DTParser.parseExperiencesListArray(response: apiResponse)
                viewController.addExperinceButton()
                viewController.endRequest(hasMore: false)
                MBProgressHUD.hide(for: viewController.view, animated: true)

            }else{
                viewController.endRequestWithFailure()
            }
        }
    }
    
    func deleteExperienceAPI(experienceObjetc:ExperienceObject){
        
        var parm = [String:Any]()
        
        parm["ExperienceID"] = experienceObjetc.id
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .deleteExperience, requestParams: parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) {[weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
           
            if success {
                
                DispatchQueue.main.async {
                    viewController.experinceObjectArray = viewController.list as! [ExperienceObject]  
                    viewController.experinceObjectArray.remove(at: viewController.experinceObjectArray.index(of: experienceObjetc)!)
                    viewController.getExperienceListAPI()
                   
                }
                
            }
        }
        
    }
    
     //MARK: - Helping Method
    override func beginRequest() {
        getExperienceListAPI()
    }
    
    override func tableViewSetup() {
        super.tableViewSetup()
        
        tableView?.register(UINib(nibName: "ExperienceCell", bundle: nil), forCellReuseIdentifier: "ExperienceCell")
        tableView?.separatorStyle = .none
    }
    
    
    func addExperinceButton(){
        
        let addButton = UIButton(type: .custom)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        addButton.setImage(#imageLiteral(resourceName: "edit_icon"), for: .normal)
        addButton.addTarget(self, action: #selector(addExperinceSelector), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addButton)
    }
    
    
    //MARK: - Delegate
    
    func reloadTableView() {
        getExperienceListAPI()
        tableView?.reloadData()
    }
    
    func reloadAfterEditTableView(){
        getExperienceListAPI()
        tableView?.reloadData()
    }
    
    
    
    
    
    
    
}
