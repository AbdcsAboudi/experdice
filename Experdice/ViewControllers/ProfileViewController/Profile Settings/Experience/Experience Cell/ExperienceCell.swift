//
//  ExperienceCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/20/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ExperienceCell: UITableViewCell {
    
    
    
    @IBOutlet weak var supTitleLabel:UILabel!
    @IBOutlet weak var firstTitle:UILabel!
    @IBOutlet weak var fromDateLabel:UILabel!
    @IBOutlet weak var toDateLabel:UILabel!
    @IBOutlet weak var thirdTitle:UILabel!
    @IBOutlet weak var roundImageView:UIImageView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
    class func populateExperienceCell(experienceObject : ExperienceObject, tableView : UITableView, indexPath: NSIndexPath) -> ExperienceCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"ExperienceCell") as! ExperienceCell
        
        cell.selectionStyle = .none
        
        cell.supTitleLabel.text = experienceObject.experienceTitle
        cell.firstTitle.text = experienceObject.experienceCompanyName
        cell.fromDateLabel.text =  "From \(experienceObject.experienceDateFrom ?? "")"
        if experienceObject.isWorkHere == "1"{
           cell.toDateLabel.text = "To Present"
        }else{
            cell.toDateLabel.text = "To \(experienceObject.experienceDateTo ?? "")"
        }
        
        cell.thirdTitle.text = "\(experienceObject.experienceCity ?? "") , \(experienceObject.experienceCountry ?? "")"
        cell.roundImageView.image = nil
        
        
        
        
        return cell
        
    }
    
    override func layoutSubviews() {
        
        for subview in self.subviews {
            if String(describing: type(of: subview.self)) == "UITableViewCellDeleteConfirmationView" {
                let deleteButton = subview
                if deleteButton.viewWithTag(100) == nil {
                    let vv = UIView(frame: CGRect(x: 0, y: -10, width: 120, height: 10))
                    vv.backgroundColor = UIColor(red: 229/255, green: 233/255, blue: 234/255, alpha: 1)
                    vv.tag = 100
                    deleteButton.addSubview(vv)
                }
            }
        }
        
    }
    
}
