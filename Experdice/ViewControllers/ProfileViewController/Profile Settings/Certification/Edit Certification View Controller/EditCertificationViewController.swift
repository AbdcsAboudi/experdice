//
//  EditCertificationViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/23/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class EditCertificationViewController: DTViewController {
    
    
    
    var certificationObject:CertificationObject?

    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Edit Certificaiton"
        
        setEditButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = false
        
        super.viewWillAppear(true)
        
    }
    
    
    //MARK: - Selector and Action
    
    func editSelector(){
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func doneEditSelector(){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Helping Method
    func setEditButton(){
        
        let cancelButton = UIButton(type: .custom)
        let doneButton = UIButton(type: .custom)
        
        doneButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        doneButton.setImage(#imageLiteral(resourceName: "done_edit"), for: .normal)
        cancelButton.setImage(#imageLiteral(resourceName: "cancel_edit"), for: .normal)
        
        cancelButton.addTarget(self, action: #selector(editSelector), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneEditSelector), for: .touchUpInside)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cancelButton)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: doneButton)
        
    }

    

}
