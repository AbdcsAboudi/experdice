//
//  AddingCertificationViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/23/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class AddingCertificationViewController: DTViewController {

    
    
    @IBOutlet weak var yearEarned:UITextField!
    @IBOutlet weak var certificationName:UITextField!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var uploadButton:UIButton!
    @IBOutlet weak var saveButton:UIButton!
    @IBOutlet weak var saveButtonView:UIView!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

     title = "Add Certification"
        
        saveButtonView.makeCorneredViewWith(corenerRadius: 15, and: 0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = false
        
        
        super.viewWillAppear(true)
        
    }
    

    
}
