//
//  CertificationViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/23/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class CertificationViewController: DTViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView:UITableView!
    
    
    var certificationObjectArray = [CertificationObject]()

    override func viewDidLoad() {
        super.viewDidLoad()

        fillArraysWithObject()
        setupCertificationTableView()
        
        title = "Certificaiton"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = false
       // addCertificationButton()
        
        super.viewWillAppear(true)
        
    }
    
    
    //MARK: - Table View Delegate And Data Source
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let certificaitonObject = certificationObjectArray[indexPath.row]
        
        let desnationViewControler = EditCertificationViewController(nibName: "EditCertificationViewController", bundle: nil)
        
        desnationViewControler.certificationObject = certificaitonObject
        
        let navigationController = UINavigationController(rootViewController: desnationViewControler)
        
        
        self.present(navigationController, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 109
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !certificationObjectArray.isEmpty{
            let cell = CertificationCell.populateCertificationCell(certificationObject: certificationObjectArray[indexPath.row], tableView: tableView, indexPath: indexPath as NSIndexPath)
            
            return cell
        }
        return UITableViewCell()
        
    }
    
    //MARK: - Selector & Action
    
    func addCertificationSelector(){
        
        self.navigationController?.pushViewController(AddingCertificationViewController(nibName: "AddingCertificationViewController", bundle: nil), animated: true)
        
    }
    
    
    //MARK: - Helping Method
    
    
    func fillArraysWithObject(){
        
       
    }
    
    
    func setupCertificationTableView(){
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "CertificationCell", bundle: nil), forCellReuseIdentifier: "CertificationCell")
        
        tableView.separatorStyle = .none
        
        
    }
    
    
    func addCertificationButton(){
        
        let addButton = UIButton(type: .custom)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        addButton.setImage(#imageLiteral(resourceName: "edit_icon"), for: .normal)
        addButton.addTarget(self, action: #selector(addCertificationSelector), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addButton)
        
        
    }

    
    
    

    
    

}
