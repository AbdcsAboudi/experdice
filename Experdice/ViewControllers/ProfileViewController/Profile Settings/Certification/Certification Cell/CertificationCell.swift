//
//  CertificationCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/23/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class CertificationCell: UITableViewCell {
    
    @IBOutlet weak var supTitleLabel:UILabel!
    @IBOutlet weak var firstTitle:UILabel!
    @IBOutlet weak var secondTitle:UILabel!
    @IBOutlet weak var roundImageView:UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
    class func populateCertificationCell(certificationObject : CertificationObject, tableView : UITableView, indexPath: NSIndexPath) -> CertificationCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"CertificationCell") as! CertificationCell
        
        cell.selectionStyle = .none
        
        cell.supTitleLabel.text = certificationObject.certificationName
        cell.firstTitle.text = certificationObject.certificationAuthority
        cell.secondTitle.text = certificationObject.certificationDateFrom
        cell.roundImageView.image = nil
        
        
        
        
        return cell
        
    }
    
    
    
    
}
