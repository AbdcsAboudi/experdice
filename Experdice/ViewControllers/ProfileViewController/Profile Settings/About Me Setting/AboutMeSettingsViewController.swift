//
//  AboutMeSettingsViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/16/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class AboutMeSettingsViewController: DTViewController {

    
    @IBOutlet weak var textView:UITextView!
    @IBOutlet weak var saveButton:UIButton!
    
    let oldUser = DTCredentialManager.credentialSharedInstance.user as? UserObject
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        title = "About Me"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = false
        
        super.viewWillAppear(true)
    }
    
    //MARK: - Helping Method
    
    func setupView(){
        textView.scrollsToTop = true
        textView.isEditable = true
        
        saveButton.addTarget(self, action: #selector(saveButtonSelector), for: .touchUpInside)

        textView.text = oldUser?.overviewText
    }
    
    //MARK: - Selecotr And Action
    
    func saveButtonSelector(){
        editAboutMeAPI()
    }
    
    //MARK: - API Call
    
    func editAboutMeAPI(){
        var parms = [String:Any]()

        parms["UserId"] = oldUser?.userID
        parms["AboutMe"] = textView.text
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .editAboutMe, requestParams: parms)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            MBProgressHUD.hide(for: viewController.view, animated: true)
            if success {
                
                viewController.oldUser?.overviewText = viewController.textView.text
                DTCredentialManager.credentialSharedInstance.user = viewController.oldUser
                viewController.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    
    
    
    
    
    
    

    

}
