//
//  ProfileSettingsHeaderCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/16/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileSettingsHeaderCell: UITableViewCell {
    
    
    @IBOutlet weak var profileImageRoundView:UIView!
    @IBOutlet weak var profileImageView:UIImageView!
    @IBOutlet weak var changeButtonImage:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    class func populateProfileSettingsHeaderCell(userObject : UserObject, tableView : UITableView, indexPath: NSIndexPath) -> ProfileSettingsHeaderCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"ProfileSettingsHeaderCell") as! ProfileSettingsHeaderCell
        
        cell.selectionStyle = .none
        
        if let imagePath = userObject.imageURL {
            let image = UIImageView(image: #imageLiteral(resourceName: "memper_name"))
            image.contentMode = .center
            cell.profileImageView.sd_setImage(with: URL(string : "http://numidiajo.com/Experdice/API/images/\(imagePath)" ), placeholderImage: image.image, options: .retryFailed, completed: { (image, error, cacheType, url) in
                
                if error != nil {
                    
                    print(error?.localizedDescription ?? "error found")
                    
                } else {
                   
                    cell.profileImageView?.contentMode = .scaleAspectFill
                    cell.profileImageView?.clipsToBounds = true
                   
                }
                
            })
            
        }
       
        return cell
        
    }
   
}
