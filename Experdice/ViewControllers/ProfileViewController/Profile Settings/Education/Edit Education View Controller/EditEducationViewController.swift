//
//  EditEducationViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/22/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol EditingEducationViewControllerDelegate {
    func reloadAfterEditTableView()
}

class EditEducationViewController: DTViewController {
   
    @IBOutlet weak var schoolTextFiled:UITextField!
    @IBOutlet weak var degreeTextFiled:DTtextField!
    @IBOutlet weak var filedOfStudyTextFiled:UITextField!
    @IBOutlet weak var completionOnTextFiled:DTtextField!
    @IBOutlet weak var countryTextFiled:DTtextField!
    @IBOutlet weak var cityTextFiled:DTtextField!
    @IBOutlet weak var cityLoading:UIActivityIndicatorView!
    
    //MARK: - Variables
    var datePicker = UIPickerView()
    var degreesObjectArray = [EducationDegreeObject]()
    var gradeObjectArray = [EducationGradeObject]()
    var countryObjectArray = [CountryObject]()
    var cityObjectArray = [CityObject]()
    var nameOfYear = [String]()
    var degreesName = [String]()
    var degreesId = [Int]()
    
    var degreeName = [String]()
    var degreeId = [Int]()
    var countryName = [String]()
    var countryID = [Int]()
    var city = [String]()
    var cityID = [Int]()
    var educationViewControllerDelegate:EditingEducationViewControllerDelegate?
    var educationObject:EducationObject?
    var didSelectCountry = false
    var educationArray = [EducationObject]()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
         fillYearArray()
        getDegressAPI()
        setupTextFiledAndButton()
        title = "Edit Education"
        cityLoading.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        super.viewWillAppear(true)
    }
    
    //MARK: - Selector And Action
    
    func editEducationSelector(){
        navigationController?.popViewController(animated: true)
    }
    
    func doneEditSelector(){
        callEditEducationAPI()
    }
    
    
    //MARK: - Helping Method
    
    func fillYearArray(){
        
        let year = Date()
        let calender = Calendar.current
        let curruntYear:Int = (calender.dateComponents([.year], from: year).year)!
        var index2 = 0
        for index in 1970...curruntYear{
           nameOfYear.append("\(index)")
            completionOnTextFiled.domainOptions.append(DTDomainObject(id:index2 , display: nameOfYear[index2]))
            index2 += 1
        }
        
        
    }
    
    func countryTextFildEndEditing(_ textField:DTtextField){
        city.removeAll()
        cityID.removeAll()
        cityTextFiled.text = ""
        cityLoading.isHidden = false
        cityLoading.startAnimating()
        cityTextFiled.domainOptions.removeAll()
        callGetCityAPI(id:textField.selectdIndex+1)
        cityTextFiled.isEnabled = didSelectCountry

    }
    func dateSelectedEndEdting(_ textField:DTtextField){
        textField.text = nameOfYear[textField.selectdIndex]
    }
    
    func setupCityArray(){
        
        var index1 = 0
        var index2 = 0
        for cityName in city {
            cityObjectArray.append(CityObject(id: cityID[index1], name: cityName))
            index1 += 1
        }
        for cityName in city {
            cityTextFiled.domainOptions.append(DTDomainObject(id: cityID[index2] , display: cityName))
            index2 += 1
        }
    }
    
    func setupCountryArray(){
        
        var index1 = 0
        var index2 = 0
        for countryName in countryName {
            countryObjectArray.append(CountryObject(id: countryID[index1], code: "", name: countryName))
            index1 += 1
        }
        for countryName in countryName {
            countryTextFiled.domainOptions.append(DTDomainObject(id: countryID[index2], display: countryName))
            index2 += 1
        }
    }
  
    func setupDegressArray(){
        var index1 = 0

        for degrees in degreesObjectArray {
            
            degreesName.append(degrees.degreeName!)
            degreesId.append(degrees.id!)
        }
        
        for degreeName in degreesName {
            degreeTextFiled.domainOptions.append(DTDomainObject(id: degreesId[index1], display: degreeName))
            index1 += 1
        }
        
    }
    
    func fillDataObject(){
        
        schoolTextFiled.text = educationObject?.educationSchoolName
        filedOfStudyTextFiled.text = educationObject?.educationFieldOfStudy
        completionOnTextFiled.text = educationObject?.educationCompletionOn
        cityTextFiled.text = educationObject?.educationCity
        countryTextFiled.text = educationObject?.educationCountry
        
        if let degreeId = educationObject?.educationDegreeId,!degreesObjectArray.isEmpty {
            degreeTextFiled.text = degreesObjectArray[Int(degreeId)!].degreeName
        }
        
        if let countryId = educationObject?.countryId {
            cityLoading.isHidden = false
            cityLoading.startAnimating()
            callGetCityAPI(id: countryId)
        }
        
    }
    
    func setupTextFiledAndButton(){
        
        schoolTextFiled.setTextFieldTextColor()
        degreeTextFiled.setTextFieldTextColor()
        filedOfStudyTextFiled.setTextFieldTextColor()
        countryTextFiled.setTextFieldTextColor()
        completionOnTextFiled.setTextFieldTextColor()
        cityTextFiled.setTextFieldTextColor()
        cityTextFiled.isEnabled = didSelectCountry
        degreeTextFiled.textFieldType = .domain
        countryTextFiled.textFieldType = .domain
        cityTextFiled.textFieldType = .domain
        completionOnTextFiled.textFieldType = .domain
        completionOnTextFiled.addTarget(self, action: #selector(dateSelectedEndEdting), for: .editingDidEnd)
        countryTextFiled.addTarget(self, action: #selector(countryTextFildEndEditing), for: .editingDidEnd)
    }
    
    func setEditButton(){
        
        let cancelButton = UIButton(type: .custom)
        let doneButton = UIButton(type: .custom)
        
        doneButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        doneButton.setImage(#imageLiteral(resourceName: "doneEditpng"), for: .normal)
        cancelButton.setImage(#imageLiteral(resourceName: "closeEdit"), for: .normal)
        
        cancelButton.addTarget(self, action: #selector(editEducationSelector), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneEditSelector), for: .touchUpInside)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cancelButton)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: doneButton)
        
    }
    
    //MARK: - API Call
    
    func getDegressAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getDegrees, requestParams: nil)
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let degrees = DTParser.parseDegreesOjectArray(from: response)
                    
                    viewController.degreesObjectArray = degrees
                    
                }
                viewController.setupDegressArray()
                viewController.callCountryAPI()
            }
        }
        
    }
    
    func callCountryAPI(){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCountry, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let countryArray = DTParser.parseCountryObjectArray(from: response)
                    
                    for country in countryArray  {
                        
                        if let countryName = country.name {
                            
                            self?.countryName.append(countryName)
                            
                        }
                        
                        if let countryID = country.id {
                            
                            self?.countryID.append(countryID)
                        }
                        
                    }
                    self?.setupCountryArray()
                    
                }
                if !viewController.didSelectCountry{
                    viewController.didSelectCountry = !viewController.didSelectCountry
                }else{
                    viewController.didSelectCountry = true
                    
                }
                viewController.fillDataObject()
                viewController.setEditButton()
                MBProgressHUD.hide(for: viewController.view, animated: true)
            }
        }
        
    }
    
    func callGetCityAPI(id:Int){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCity, requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let cityArray = DTParser.parseCityObjectArray(from: response)
                    
                    for city in cityArray  {
                        
                        if let cityName = city.name {
                            
                            self?.city.append(cityName)
                            
                        }
                        
                        if let cityID = city.id {
                            
                            self?.cityID.append(cityID)
                        }
                        
                    }
                    self?.setupCityArray()
                    
                }
                viewController.didSelectCountry = true
                viewController.cityLoading.stopAnimating()
                viewController.cityLoading.isHidden = true
                
            }
        }
    }
    
    func callEditEducationAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)

        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        var parm = [String:Any]()
        
        parm["EducationId"] = educationObject?.id
        parm["ExpertId"] = user?.profile.expertId
        parm["School"] = schoolTextFiled.text
        parm["Year"] = completionOnTextFiled.text
        parm["CountryId"] = countryID[countryName.index(of: countryTextFiled.text!)!]
        parm["CityId"] = cityID[city.index(of: cityTextFiled.text!)!]
        parm["FieldOfStudy"] = filedOfStudyTextFiled.text
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .editEducation, requestParams: parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            MBProgressHUD.hide(for: viewController.view, animated: true)

            if success {
                viewController.fireDelegate()
            }
            
        }
        
    }
  
    
    //MARK: - Experince ViewController Deleg
    
    func fireDelegate() {
        educationViewControllerDelegate?.reloadAfterEditTableView()
        navigationController?.popViewController(animated: true)
    }

    
}
