//
//  EducationCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/22/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
//AS : change the name of the cell from educationCell to UserEducationCell was  get nill value when its educationCell
class UserEducationCell: UITableViewCell {
    
    @IBOutlet weak var supTitleLabel:UILabel!
    @IBOutlet weak var firstTitle:UILabel!
    @IBOutlet weak var secondTitle:UILabel!
    @IBOutlet weak var thirdTitle:UILabel!
    @IBOutlet weak var roundImageView:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    
    class func populateEducationCell(educationObject : EducationObject, tableView : UITableView, indexPath: NSIndexPath) -> UserEducationCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"UserEducationCell") as! UserEducationCell
        
        cell.selectionStyle = .none
        
        cell.supTitleLabel.text = educationObject.educationSchoolName
        cell.firstTitle.text = educationObject.educationFieldOfStudy
        cell.secondTitle.text = educationObject.educationCompletionOn
        cell.thirdTitle.text = "\(educationObject.educationCity ?? "") , \(educationObject.educationCountry ?? "")"
        
        cell.roundImageView.image = nil
        
        return cell
        
    }
    
    override func layoutSubviews() {
        
        for subview in self.subviews {
            if String(describing: type(of: subview.self)) == "UITableViewCellDeleteConfirmationView" {
                let deleteButton = subview
                if deleteButton.viewWithTag(100) == nil {
                    let vv = UIView(frame: CGRect(x: -10, y: 0, width: 100, height: 10))
                    vv.backgroundColor = UIColor(red: 229/255, green: 233/255, blue: 234/255, alpha: 1)
                    vv.tag = 100
                    deleteButton.addSubview(vv)
                }
            }
        }
        
    }
    
    
   
    
    
}
