//
//  AddingEducationViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/22/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol AddingEducationViewControllerDelegate {
    func reloadTableView()
}

class AddingEducationViewController: DTViewController {
    
    @IBOutlet weak var schoolTextFiled:UITextField!
    @IBOutlet weak var degreeTextFiled:DTtextField!
    @IBOutlet weak var filedOfStudyTextFiled:UITextField!
    @IBOutlet weak var completionOnTextFiled:UITextField!
    @IBOutlet weak var countryTextFiled:DTtextField!
    @IBOutlet weak var cityTextFiled:DTtextField!
    @IBOutlet weak var roundedButtonView:UIView!
    @IBOutlet weak var activityForCity:UIActivityIndicatorView!
    @IBOutlet weak var doneButton:UIButton!

    var datePicker = UIDatePicker()
    var didSelectCountry = false
    var degreePickerView = UIPickerView()

    var countryPickerView = UIPickerView()
    var cityPickerView = UIPickerView()
    var degreesObjectArray = [EducationDegreeObject]()
   
    var countryName = [String]()
    var countryID = [Int]()
    
    var degreesName = [String]()
    var degreesId = [Int]()
    
    var cityName = [String]()
    var cityID = [Int]()
    
    var educationViewControllerDelegate:AddingEducationViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTextFiledAndButton()
        title = "Add Education"
        activityForCity.isHidden = true
        getDegressAPI()
        callCountryAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        super.viewWillAppear(true)
   }
    
    //MARK: - Selector and Action
    
    func datePickerValueChange(sender:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd"
        completionOnTextFiled.text = dateFormatter.string(from: sender.date)
        
        
    }
    
    func doneButtonSelector(){
        callAddEducationAPI()
    }
    
    //MARK: - Helping Method
    
    func setupCountryArray(){
        
        var index = 0
        for countryName in countryName {
            
            countryTextFiled.domainOptions.append(DTDomainObject(id: countryID[index], display: countryName))
            
            index += 1
        }
        
    }
    
    func setupCityArray(){
        
        var index = 0
        for city in cityName {
            
            cityTextFiled.domainOptions.append(DTDomainObject(id: cityID[index] , display: city))
            
            index += 1
        }
    }
    
    func setupDegressArray(){
        
        for degrees in degreesObjectArray {
            degreesName.append(degrees.degreeName!)
            degreesId.append(degrees.id!)
        }
        
        var index = 0
        for degrees in degreesName {
            
            degreeTextFiled.domainOptions.append(DTDomainObject(id: degreesId[index] , display: degrees))
            
            index += 1
        }
    }
    
    
    func setupTextFiledAndButton(){
        
        schoolTextFiled.setTextFieldTextColor()
        degreeTextFiled.setTextFieldTextColor()
        filedOfStudyTextFiled.setTextFieldTextColor()
        completionOnTextFiled.setTextFieldTextColor()
        countryTextFiled.setTextFieldTextColor()
        cityTextFiled.setTextFieldTextColor()
        
        degreeTextFiled.textFieldType = .domain
        countryTextFiled.textFieldType = .domain
        cityTextFiled.textFieldType = .domain
        
        datePicker.datePickerMode = .date
        completionOnTextFiled.inputView = datePicker
        
        datePicker.addTarget(self, action: #selector(datePickerValueChange), for: .valueChanged)
        countryTextFiled.addTarget(self, action: #selector(countryTextFildEndEditing), for: .editingDidEnd)
        doneButton.addTarget(self, action: #selector(doneButtonSelector), for: .touchUpInside)
        roundedButtonView.makeCorneredViewWith(corenerRadius: 15, and: 0)
        
    }
    
    
    
    
    //MARK: - API Call
    
    func callAddEducationAPI(){
        
        var parm = [String:Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parm["ExpertId"] = user?.profile.expertId
        parm["EducationId"] = 0
        parm["School"] = schoolTextFiled.text
        parm["Year"] = completionOnTextFiled.text
        parm["DegreeId"] = degreeTextFiled.text
        parm["FieldOfStudy"] = filedOfStudyTextFiled.text
        parm["CountryId"] = countryID[countryName.index(of: countryTextFiled.text!)!]
        parm["CityId"] = cityID[cityName.index(of: cityTextFiled.text!)!]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .editEducation, requestParams: parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                viewController.fireDelegate()
            }
            
        }
        
    }
    
    func callCountryAPI(){
  
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCountry, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
       
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let countryArray = DTParser.parseCountryObjectArray(from: response)
                    
                    for country in countryArray  {
                        
                        if let countryName = country.name {
                            
                            self?.countryName.append(countryName)
                            
                        }
                        
                        if let countryID = country.id {
                            
                            self?.countryID.append(countryID)
                        }
                        
                    }
                    self?.setupCountryArray()
                    //                    if viewController.user!.countryId != 0{
                    //
                    //                        viewController.countryTextFiled.text = viewController.countryName[viewController.user!.countryId!-1]
                    //                    }
                }
                viewController.didSelectCountry = !viewController.didSelectCountry
                MBProgressHUD.hide(for: viewController.view, animated: true)
                
            }
        }
        
    }
    
    func callGetCityAPI(id:Int){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCity, requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let cityArray = DTParser.parseCityObjectArray(from: response)
                    
                    for city in cityArray  {
                        
                        if let cityName = city.name {
                            
                            viewController.cityName.append(cityName)
                            
                        }
                        
                        if let cityID = city.id {
                            
                            viewController.cityID.append(cityID)
                        }
                        
                    }
                    viewController.setupCityArray()
                    
                    
                }
                viewController.didSelectCountry = true
                viewController.activityForCity.stopAnimating()
                viewController.activityForCity.isHidden = true
            }
        }
    }
    
    func getDegressAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getDegrees, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let degrees = DTParser.parseDegreesOjectArray(from: response)
                    
                    viewController.degreesObjectArray = degrees
                    
                }
                viewController.setupDegressArray()
            }
        }
        
    }
    
    
    //MARK: - TextFiled Delegate
    
    func countryTextFildEndEditing(_ textField:DTtextField){
        
        cityName.removeAll()
        cityID.removeAll()
        cityTextFiled.domainOptions.removeAll()
        cityTextFiled.text = ""
        cityTextFiled.isEnabled = false
        activityForCity.isHidden = false
        activityForCity.startAnimating()
        callGetCityAPI(id:textField.selectdIndex+1)
        cityTextFiled.isEnabled = didSelectCountry
        
    }
    
    //MARK: - Experince ViewController Deleg
    
    func fireDelegate() {
        educationViewControllerDelegate?.reloadTableView()
        navigationController?.popViewController(animated: true)
    }
    
    

}
