//
//  EducationViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/22/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class EducationViewController: DTListViewController,AddingEducationViewControllerDelegate,EditingEducationViewControllerDelegate {
    
    @IBOutlet weak var educationTableView:UITableView!
    
    var educaitonObjectArray = [EducationObject]()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        tableView = educationTableView
        super.viewDidLoad()
        tableViewSetup()
        title = "Eduction"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        super.viewWillAppear(true)
    }
    
    
    //MARK: - Table View Delegate And Data Source
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let educationObject = list[indexPath.row] as? EducationObject {
            let desnationViewControler = EditEducationViewController(nibName: "EditEducationViewController", bundle: nil)
            desnationViewControler.educationObject = educationObject
            desnationViewControler.educationViewControllerDelegate = self
            show(desnationViewControler, sender: self)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 110
    }
    
    override func populateTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !list.isEmpty,let educationObject = list[indexPath.row] as? EducationObject {
            let cell = UserEducationCell.populateEducationCell(educationObject: educationObject, tableView: tableView, indexPath: indexPath as NSIndexPath)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete && !list.isEmpty  {
           deleteEducationAPI(educationObject:list[indexPath.row] as! EducationObject)
        }
    }
    
    
    //MARK: - Selector & Action
    
    func addEducaitonSelector(){
       
        let vc = AddingEducationViewController(nibName: "AddingEducationViewController", bundle: nil)
        vc.educationViewControllerDelegate = self
        self.show(vc, sender: self)
    }
    
    
    //MARK: - Helping Method
    
    override func beginRequest() {
        getEducationListAPI()
    }
    
    override func tableViewSetup() {
        super.tableViewSetup()
        tableView?.register(UINib(nibName: "UserEducationCell", bundle: nil), forCellReuseIdentifier: "UserEducationCell")
        tableView?.separatorStyle = .none
    }
  
    func addEducationButton() {
        
        let addButton = UIButton(type: .custom)
        addButton.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
        addButton.setImage(#imageLiteral(resourceName: "edit_icon"), for: .normal)
        addButton.addTarget(self, action: #selector(addEducaitonSelector), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addButton)
        
    }
    
    //MARK: - API Call
    
    func getEducationListAPI(){
        
        var parms = [String:Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parms["ExpertId"] = user?.profile.expertId
        parms["EducationID"] = 0
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getEducation, requestParams: parms)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                viewController.list = DTParser.parseEducationListArray(response: apiResponse)
                viewController.addEducationButton()
                viewController.endRequest(hasMore: false)
                MBProgressHUD.hide(for: viewController.view, animated: true)
                
            }else{
                viewController.endRequestWithFailure()
            }
        }
    }
    
    func deleteEducationAPI(educationObject:EducationObject){
      
        var parm = [String:Any]()
        
        parm["EducationID"] = educationObject.id
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .deleteEducation, requestParams: parm)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) {[weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            MBProgressHUD.hide(for: viewController.view, animated: true)
            
            if success {
                
                DispatchQueue.main.async {
                    viewController.educaitonObjectArray = viewController.list as! [EducationObject]
                    viewController.educaitonObjectArray.remove(at: viewController.educaitonObjectArray.index(of: educationObject)!)
                    viewController.getEducationListAPI()
                }
                
            }
        }

    }
    
    //MARK: - Delegate
    
    func reloadTableView() {
        getEducationListAPI()
        tableView?.reloadData()
    }
    
    func reloadAfterEditTableView() {
        getEducationListAPI()
        tableView?.reloadData()
    }
    

    
    


    

}
