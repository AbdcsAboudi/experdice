//
//  BasicInformationViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/17/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class BasicInformationViewController: DTViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var firstNameLable:UITextField!
    @IBOutlet weak var lastNameLable:UITextField!
    @IBOutlet weak var headlineLable:UITextField!
    @IBOutlet weak var genderLable:UITextField!
    @IBOutlet weak var birthdateLable:UITextField!
    @IBOutlet weak var changePasswordButton:UIButton!
    
    var canEdit = false
    var genderArray = [GenderObject]()
    var pickerView = UIPickerView()
    var datePikcer = UIDatePicker()
    var genders = [String:Int]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFiled()
        fillGenderArray()
        title = "Basic Information"
        getUserDataFromCredential()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        super.viewWillAppear(true)
    }
    
    //MARK: - Picker View Delegate And Data Source
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return genderArray[row].genderName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        genderLable.text = genderArray[row].genderName
        
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        
        return 30
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    //MARK: - Helping Methods
    
    func fillGenderArray(){
        
        genderArray.append(GenderObject(id: 1, genderName: "Male"))
        genderArray.append(GenderObject(id: 2, genderName: "Female"))
        genders[genderArray[0].genderName!] = genderArray[0].id
        genders[genderArray[1].genderName!] = genderArray[1].id
        
        
    }
    
    func getUserDataFromCredential(){
        if let user = DTCredentialManager.credentialSharedInstance.user as? UserObject {
            firstNameLable.text =  user.profile.profileFirstName
            lastNameLable.text = user.profile.profileSecondName
            headlineLable.text = user.headLine
            if user.genderId != 0 {
                genderLable.text = genderArray[(user.genderId ?? 1)].genderName
            }
            birthdateLable.text = user.dateOfBirth
        }
        setEditButton()
    }
    
    func back() {
        navigationController?.popViewController(animated: true)
    }
    
    func setEditButton(){
        navigationItem.leftBarButtonItem = navigationController?.navigationItem.backBarButtonItem
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "edit_icon"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(setEditEnabelSelector), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView:button)
        if canEdit {
            setEditEnabelSelector()
        }
    }
    
    func setIconButtonForEditngState(){
        
        let doneButton = UIButton(type: .custom)
        let cancelButton = UIButton(type: .custom)
        
        cancelButton.setImage(#imageLiteral(resourceName: "closeEdit"), for: .normal)
        doneButton.setImage(#imageLiteral(resourceName: "doneEditpng"), for: .normal)
        cancelButton.imageView?.contentMode = .scaleAspectFit
        doneButton.imageView?.contentMode = .scaleAspectFit
        
        cancelButton.addTarget(self, action: #selector(cancelEditSelector), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneEditSelector), for: .touchUpInside)
        
        cancelButton.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        doneButton.frame = CGRect(x: 0, y: 0, width: 35, height: 30)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: doneButton)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cancelButton)
    }
    
    func setupTextFiled(){
        
        firstNameLable.setTextFieldTextColor()
        lastNameLable.setTextFieldTextColor()
        headlineLable.setTextFieldTextColor()
        genderLable.setTextFieldTextColor()
        birthdateLable.setTextFieldTextColor()
        
        firstNameLable.isEnabled = canEdit
        lastNameLable.isEnabled = canEdit
        headlineLable.isEnabled = canEdit
        genderLable.isEnabled = canEdit
        birthdateLable.isEnabled = canEdit
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        birthdateLable.inputView = datePikcer
        genderLable.inputView = pickerView
        datePikcer.datePickerMode = .date
        datePikcer.addTarget(self, action: #selector(datePickerValueChange), for: .valueChanged)
        changePasswordButton.addTarget(self, action: #selector(changePasswordSelector), for: .touchUpInside)
        
    }
    
    //MARK: - Action & Selector
    
    func changePasswordSelector(){
         let desnationViewControler = ChangePasswordViewController(nibName: "ChangePasswordViewController", bundle: nil)
         show(desnationViewControler, sender: self)
    }
    
    func setEditEnabelSelector(){
        
        // canEdit is false
        firstNameLable.isEnabled = !canEdit
        lastNameLable.isEnabled = !canEdit
        headlineLable.isEnabled = !canEdit
        genderLable.isEnabled = !canEdit
        birthdateLable.isEnabled = !canEdit
        
        if !canEdit {
            
            //AS : FOR CHANGING THE STATE OF THE NAVIGATION BUTTON'S
            setIconButtonForEditngState()
        }
        
        canEdit = !canEdit
    }
    
    func cancelEditSelector(){
        setEditButton()
    }
    
    func doneEditSelector(){
        cellEditBasicInfoAPI()
    }
    
    func datePickerValueChange(sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "YYYY-MM-dd"
        birthdateLable.text = dateFormatter.string(from: sender.date)
        
    }
    
    //MARK: - API Call

    func cellEditBasicInfoAPI(){
        
        var parms = [String:Any]()
        let oldUser = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parms["ExpertId"] = oldUser?.profile.expertId
        
        if let genderName = genderLable.text{
            parms["GenderId"] = genders[genderName]
        }
        
        
        
        if DTValidationManager.isValidName(text: firstNameLable.text) {
            parms["FirstName"] = firstNameLable.text
            
        }else{
            let alertMessage = "Please Fill Vaild First Name"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        if DTValidationManager.isValidName(text: lastNameLable.text) {
            parms["LastName"] = lastNameLable.text
            
        }else{
            let alertMessage = "Please Fill Vaild Last Name"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        if DTValidationManager.isValidHeadLine(text: headlineLable.text) {
            parms["Headline"] = headlineLable.text
        }else{
            let alertMessage = "Please Fill Vaild HeadeLine"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
       // if  {
            parms["DateOfBirth"] = birthdateLable.text
//        }else{
//            let alertMessage = "Please Fill Vaild Birthdate"
//            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
//            self.present(alertView, animated: true, completion: nil)
//            return
//        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .editBasicInfo, requestParams: parms)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                if let userResponcse = apiResponse as? [String:Any]{
                  let oldUser = DTCredentialManager.credentialSharedInstance.user as? UserObject
                    
                    if let newExpertInfo = userResponcse["Expert"] as? [String:Any]{
                        oldUser?.profile.profileFirstName = newExpertInfo["FIRST_NAME"] as? String
                        oldUser?.profile.profileSecondName = newExpertInfo["LAST_NAME"] as? String
                        oldUser?.profile.expertOverallRating = 0
                        oldUser?.profile.expertStatusId = 1
                        oldUser?.profile.earilyRegistered = "1"
                        
                    }
                    if let newUserInfo = userResponcse["User"] as? [String:Any]{
                        oldUser?.dateOfBirth = newUserInfo["DATE_OF_BIRTH"] as? String
                        oldUser?.genderId = Int(newUserInfo["GENDER_ID"] as? String ?? "1")
                        oldUser?.headLine = newUserInfo["HEADLINE"] as? String
                    }
                    DTCredentialManager.credentialSharedInstance.user = oldUser
               
                    viewController.setEditButton()
                    MBProgressHUD.hide(for: viewController.view, animated: true)
                }
            }
            
        }
    }
  
    
    
}
