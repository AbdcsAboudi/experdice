//
//  ChangePasswordViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 2/4/18.
//  Copyright © 2018 DreamTech. All rights reserved.
//

import UIKit
import MBProgressHUD

class ChangePasswordViewController: DTViewController {
    
    @IBOutlet weak var passwordTextFieldConfirmation: DTtextField!
    @IBOutlet weak var passwordTextField: DTtextField!
    @IBOutlet weak var saveButton:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        needSideBar = false
        super.viewWillAppear(animated)
    }
    
    
    //MARK: - Helping Method
    
    func setupView(){
        title = "Change Password"
        saveButton.addTarget(self, action: #selector(changePasswordSelector), for: .touchUpInside)
    }
  
    //MARK: - Selector And Actios
    
    func changePasswordSelector(){
        changePasswordAPI()
    }
    
    //MARK: - API Call
    
    func changePasswordAPI(){
        var parms = [String:Any]()
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        if let passwordConfairm = passwordTextFieldConfirmation.text{
            if passwordConfairm.isEmpty{
                let alertMessage = "Please Fill Confirm Password"
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                self.present(alertView, animated: true, completion: nil)
                return
            }
            if let passowrd = passwordTextField.text,let passwordConfairm = passwordTextFieldConfirmation.text{
                if passwordConfairm != passowrd{
                    let alertMessage = "Password And Password Confirmation Does Not Match"
                    let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                    self.present(alertView, animated: true, completion: nil)
                    return
                }
            }
            
            if DTValidationManager.isValidPassword(text: passwordTextField.text) {
                parms["Password"] = passwordTextField.text
            }else{
                let alertMessage = "Please Fill Vaild Password"
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                self.present(alertView, animated: true, completion: nil)
                return
            }
            
        }
        
        parms["UserId"] = user?.userID
        parms["Password"] = passwordTextField.text
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .changePassword, requestParams: parms)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            MBProgressHUD.hide(for: viewController.view, animated: true)
            if success {
                
                viewController.navigationController?.popViewController(animated: true)
            }
            
        }
        
    }

    

}
