//
//  ProfileSettingsViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/16/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class ProfileSettingsViewController: DTViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var profileSettingsTableView:UITableView!
    
    
    var profileSettingArray = [ProfileSettingsObject]()
    var user = DTCredentialManager.credentialSharedInstance.user as? UserObject
    var imagePicker = UIImagePickerController()
    var imageView = UIImageView()
    

    //MARK: - Life Cylce
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSettingsTableView()
        fillSettingsArray()
        setupImagePicker()
        title = "Profile Settings"
        
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        needSideBar = false
        super.viewWillAppear(true)
        
    }
    
   
    
    //MARK: - TableView Delegate And Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return profileSettingArray.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            
            return 150
        }else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        tableView.deselectRow(at: indexPath, animated: false)

        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
         let settingObject = profileSettingArray[indexPath.row]
        
        switch settingObject.settingType {
        case .none:
            print("test")
            
        case .aboutMe:
            
            let aboutMeViewController = AboutMeSettingsViewController(nibName: "AboutMeSettingsViewController", bundle: nil)
            self.navigationController?.pushViewController(aboutMeViewController, animated: true)
            
        case .basicInfo:
            let basicInfoViewController = BasicInformationViewController(nibName: "BasicInformationViewController", bundle: nil)
            self.navigationController?.pushViewController(basicInfoViewController, animated: true)
            
        case .contactDetials:
            let contactDetialsViewController = ContactDetailsViewController(nibName: "ContactDetailsViewController", bundle: nil)
            contactDetialsViewController.user = user
            self.navigationController?.pushViewController(contactDetialsViewController, animated: true)
       
            //Business account
            
        case .founded:
            let foundedViewController = FoundedViewController(nibName: "FoundedViewController", bundle: nil)
            self.navigationController?.pushViewController(foundedViewController, animated: true)
            
        case .type:
                print("test")
        case .specialties:
             print("test")
           
            //Business account
            
        case .expiernce:
            let experienceViewController = ExperienceViewController(nibName: "ExperienceViewController", bundle: nil)
            if let expertUserExperience = user?.profile.experienceObject {
              experienceViewController.experinceObjectArray = expertUserExperience
            }
            self.navigationController?.pushViewController(experienceViewController, animated: true)
            
        case .education:
            let educationViewController = EducationViewController(nibName: "EducationViewController", bundle: nil)
            if let expertUserEducation = user?.profile.educationObject {
                educationViewController.educaitonObjectArray = expertUserEducation
            }
            self.navigationController?.pushViewController(educationViewController, animated: true)
            
        case .certification:
            let certificationViewController = CertificationViewController(nibName: "CertificationViewController", bundle: nil)
            if let expertUserCertification = user?.profile.certificationObject {
                certificationViewController.certificationObjectArray = expertUserCertification
            }
            self.navigationController?.pushViewController(certificationViewController, animated: true)
            
       
        default:
            print("defult")
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if indexPath.row == 0 {
            
            if let user = DTCredentialManager.credentialSharedInstance.user as? UserObject {
                
                let cell = ProfileSettingsHeaderCell.populateProfileSettingsHeaderCell(userObject: user, tableView: tableView, indexPath: indexPath as NSIndexPath)
                
                cell.changeButtonImage.addTarget(self, action: #selector(openImagePicker), for: .touchUpInside)
                
                return cell
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSettingsCell", for: indexPath) as! ProfileSettingsCell
   
        cell.settingLable.text = profileSettingArray[indexPath.row].title
        
        return cell
        
    }
    
    //MARK: - image Picker Deleagte
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageView.contentMode = .scaleAspectFit
            imageView.image = imagePicked
            
        }
        changeProfileImageAPI()
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Helpeing Methods
    
    
    func setupImagePicker(){
    
        imagePicker.delegate = self
      
    }
    
    func openImagePicker(){
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    func fillSettingsArray(){
        
        profileSettingArray.append(ProfileSettingsObject(
            title: "",
            settingType: .none,
            profileImage: ""))
        
        
        
        if UserObject.getUserType() == .business {
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "Founded",
                settingType: .founded,
                profileImage: nil))
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "Type",
                settingType: .type,
                profileImage: nil))
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "Specialties",
                settingType: .specialties,
                profileImage: nil))
        }else{
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "BIO",
                settingType: .aboutMe,
                profileImage: nil))
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "Basic Inforamtion",
                settingType: .basicInfo,
                profileImage: nil))
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "Contact Details",
                settingType: .contactDetials,
                profileImage: nil))
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "Experience",
                settingType: .expiernce,
                profileImage: nil))
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "Education",
                settingType: .education,
                profileImage: nil))
            
            profileSettingArray.append(ProfileSettingsObject(
                title: "Certification",
                settingType: .certification,
                profileImage: nil))
        }
    
 
    }
    
    func setupSettingsTableView(){
        
        profileSettingsTableView.dataSource = self
        profileSettingsTableView.delegate = self
        
        profileSettingsTableView.register(UINib(nibName: "ProfileSettingsHeaderCell", bundle: nil), forCellReuseIdentifier: "ProfileSettingsHeaderCell")
        
          profileSettingsTableView.register(UINib(nibName: "ProfileSettingsCell", bundle: nil), forCellReuseIdentifier: "ProfileSettingsCell")
        
        profileSettingsTableView.separatorStyle = .none
        
    }

    //MARK: - API Call
    
    func changeProfileImageAPI(){
        
        var params = [String: Any] ()
        let oldUser = DTCredentialManager.credentialSharedInstance.user as! UserObject
        
        if let selectedImage = imageView.image {
            let data = UIImageJPEGRepresentation(selectedImage, 0.7)
            params["file"] = data
        }
        
        params["UserId"] = oldUser.userID
        
        MBProgressHUD.showAdded(to: view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .editImage, requestParams: params)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            MBProgressHUD.hide(for: viewController.view, animated: true)
            if success {
                
                if let response = apiResponse as? [String:Any] {
                    if let newUserResponse = response["User"] as? [String:Any] {
                        let newImagePath = newUserResponse["IMAGE_URL"] as? String
                        let newUser = DTCredentialManager.credentialSharedInstance.user as! UserObject
                        newUser.imageURL = newImagePath
                        DTCredentialManager.credentialSharedInstance.user = newUser
                        
                        DispatchQueue.main.async {
                            viewController.profileSettingsTableView.reloadData()
                        }
                    }
                    
                }
            }
        }
        
    }
    
    

    

}
