//
//  ContactDetailsViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/20/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//
import UIKit
import MBProgressHUD

protocol ContactDetailsDeleagte:NSObjectProtocol {
    func isCompletedHisProfile(isChange:Bool)
}
class ContactDetailsViewController: DTViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var addressTextFiled:UITextField!
    @IBOutlet weak var countryTextFiled:DTtextField!
    @IBOutlet weak var stateTextFiled:DTtextField!
    @IBOutlet weak var cityTextFiled:DTtextField!
    @IBOutlet weak var zipCodeTextFiled:UITextField!
    @IBOutlet weak var emailTextFiled:UITextField!
    @IBOutlet weak var mobileTextFiled:UITextField!
    @IBOutlet weak var cityLoading: UIActivityIndicatorView!
    @IBOutlet weak var stateLoading: UIActivityIndicatorView!
    
    //MARK: - Varible
    var canEdit = false
    var city = [String]()
    var cityID = [Int]()
    var countryName = [String]()
    var countryID = [Int]()
    var stateArray = [String]()
    var stateID = [Int]()
    var user:UserObject?
    var notEditMode = false
    var didSelectCountry = false
    var didSelectCity = false
    var parms = [String:Any]()
    weak var contactDetailsDeleagte:ContactDetailsDeleagte?
    var newUser:UserObject?
    var isFromBidScreen = false
    var responseCountryId:String?
    var responseCityId:String?
    var responseStateId:String?
    
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityLoading.isHidden = true
        stateLoading.isHidden = true
        
        setupTextFiled()
        callGetContactDetailAPI()
        callCountryAPI()
        title = "Contact Details"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        needSideBar = false
        super.viewWillAppear(true)
        
    }
    
    //MARK: - Helping Methods
    
    func dismissView(){
        dismiss(animated: true, completion: nil)
    }
    
    func setEditButton(){
        
        if !isFromBidScreen {
            let button = UIButton(type: .custom)
            button.setImage(#imageLiteral(resourceName: "edit_icon"), for: .normal)
            button.addTarget(self, action: #selector(setEditEnabelSelector), for: .touchUpInside)
            button.imageView?.contentMode = .scaleAspectFit
            button.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView:button)
            navigationItem.leftBarButtonItem = navigationController?.navigationItem.backBarButtonItem
            if canEdit {
                setEditEnabelSelector()
            }
        }else{
            let button = UIButton(type: .custom)
            let closeButton = UIButton(type: .custom)
            closeButton.setImage(#imageLiteral(resourceName: "closeEdit"), for: .normal)
            button.setImage(#imageLiteral(resourceName: "edit_icon"), for: .normal)
            button.addTarget(self, action: #selector(setEditEnabelSelector), for: .touchUpInside)
            closeButton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
            button.imageView?.contentMode = .scaleAspectFit
            closeButton.imageView?.contentMode = .scaleAspectFit
            button.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
            closeButton.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView:button)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView:closeButton)
            
            if canEdit {
                setEditEnabelSelector()
            }
        }
    }
    
    
    func setIconButtonForEditngState(){
        
        let doneButton = UIButton(type: .custom)
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(#imageLiteral(resourceName: "closeEdit"), for: .normal)
        doneButton.setImage(#imageLiteral(resourceName: "doneEditpng"), for: .normal)
        cancelButton.imageView?.contentMode = .scaleAspectFit
        doneButton.imageView?.contentMode = .scaleAspectFit
        cancelButton.addTarget(self, action: #selector(cancelEditSelector), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneEditSelector), for: .touchUpInside)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        doneButton.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: doneButton)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cancelButton)
        
    }
    
    func setupTextFiled(){
        
        addressTextFiled.setTextFieldTextColor()
        countryTextFiled.setTextFieldTextColor()
        countryTextFiled.textFieldType = .domain
        stateTextFiled.setTextFieldTextColor()
        stateTextFiled.textFieldType = .domain
        cityTextFiled.setTextFieldTextColor()
        cityTextFiled.textFieldType = .domain
        zipCodeTextFiled.setTextFieldTextColor()
        emailTextFiled.setTextFieldTextColor()
        mobileTextFiled.setTextFieldTextColor()
        
        addressTextFiled.isEnabled = canEdit
        countryTextFiled.isEnabled = canEdit
        cityTextFiled.isEnabled = canEdit
        stateTextFiled.isEnabled = canEdit
        zipCodeTextFiled.isEnabled = canEdit
        emailTextFiled.isEnabled = canEdit
        mobileTextFiled.isEnabled = canEdit
        
        countryTextFiled.addTarget(self, action: #selector(countryTextFildEndEditing), for: .editingDidEnd)
        cityTextFiled.addTarget(self, action: #selector(stateTextFieldVslueChange), for: .editingDidEnd)

    }
    
    func setupCountryArray(){
        
        var index = 0
        for countryName in countryName {
            
            countryTextFiled.domainOptions.append(DTDomainObject(id: countryID[index], display: countryName))
            
            index += 1
        }
        
    }
    
    func setupCityArray(){
        
        var index = 0
        for cityName in city {
            
            cityTextFiled.domainOptions.append(DTDomainObject(id: cityID[index] , display: cityName))
            
            index += 1
        }
    }
    
    func setupStateArray(){
        
        var index = 0
        for stateName in stateArray {
            
            stateTextFiled.domainOptions.append(DTDomainObject(id: stateID[index], display: stateName))
            
            index += 1
        }
        
    }

    func countryTextFildEndEditing(_ textField:DTtextField){
        city.removeAll()
        cityID.removeAll()
        cityTextFiled.domainOptions.removeAll()
        cityTextFiled.text = ""
        stateArray.removeAll()
        stateID.removeAll()
        stateTextFiled.domainOptions.removeAll()
        
        if !(stateTextFiled.text?.isEmpty)! {
            cityTextFiled.text = ""
            stateTextFiled.text = ""
        }
        cityLoading.isHidden = false
        cityLoading.startAnimating()
        callGetCityAPI(id:textField.selectdIndex+1)
        cityTextFiled.isEnabled = didSelectCountry

    }
    
    func stateTextFieldVslueChange(_ textField:DTtextField){
        stateArray.removeAll()
        stateID.removeAll()
        stateTextFiled.domainOptions.removeAll()
        stateTextFiled.text = ""
        
        stateLoading.isHidden = false
        stateLoading.startAnimating()
        
        callGetStaetAPI(id:cityID[textField.selectdIndex])
        stateTextFiled.isEnabled = true
        
    }
    
    //MARK: - Action & Selector
    
    func setEditEnabelSelector(){
        
        // canEdit is false
        addressTextFiled.isEnabled = !canEdit
        countryTextFiled.isEnabled = !canEdit
        zipCodeTextFiled.isEnabled = !canEdit
        emailTextFiled.isEnabled = !canEdit
        mobileTextFiled.isEnabled = !canEdit
        
        
        if let user = DTCredentialManager.credentialSharedInstance.user as? UserObject {
            
            if  user.stateId != 0 {
                stateTextFiled.isEnabled = true
            }
            if user.cityId != 0 {
                cityTextFiled.isEnabled = true
            }
        }
        
        if !canEdit {
            
            //AS : FOR CHANGING THE STATE OF THE NAVIGATION BUTTON'S
            setIconButtonForEditngState()
        }
        
        canEdit = !canEdit
        
        notEditMode = !notEditMode
        
        didSelectCountry = !didSelectCountry
        didSelectCity = !didSelectCity
        
        if !notEditMode {
            
            cityTextFiled.isEnabled = false
            stateTextFiled.isEnabled = false
            
        }
    }
    
    func cancelEditSelector(){
        setEditButton()
    }
    
    func doneEditSelector(){
        cellEditContactDetialsAPI()
    }
    
    //MARK: - API Call
    
    func callGetContactDetailAPI(){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .contactDetails, requestParams: user?.userID)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                
                if let response = apiResponse as? [Any] {
                    
                    if let responseArray = response.first as? [String:Any],let userResponse = responseArray["User"] as? [String:Any]{
                        
                        viewController.emailTextFiled.text = userResponse["EMAIL"] as? String
                        viewController.addressTextFiled.text = userResponse["STREET"] as? String
                        viewController.mobileTextFiled.text = userResponse["PHONE_NUMBER"] as? String
                        viewController.zipCodeTextFiled.text = userResponse["ZIP_CODE"] as? String
                        
                        if let country = userResponse["Country"] as? [String:Any]{
                            viewController.countryTextFiled.text = country["NAME"] as? String
                            viewController.responseCountryId = country["ID"] as? String
                        }
                        if let city = userResponse["City"] as? [String:Any]{
                            viewController.cityTextFiled.text = city["NAME"] as? String
                             viewController.responseCityId = city["ID"] as? String
                        }
                        if let state = userResponse["State"] as? [String:Any]{
                            viewController.stateTextFiled.text = state["NAME"] as? String
                             viewController.responseStateId = state["ID"] as? String
                        }
                
                    }
                    if let countryId = viewController.responseCountryId {
                        viewController.callGetCityAPI(id: Int(countryId)!)
                    }
                    viewController.setEditButton()
                }
                
            }
            
        }
    }
    
    func cellEditContactDetialsAPI(){
        
        let oldUser = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        parms["UserId"] = oldUser?.userID
        parms["EmailAddress"] = emailTextFiled.text
        parms["Street"] = addressTextFiled.text
        parms["ZipCode"] = zipCodeTextFiled.text
        parms["PhoneNumber"] = mobileTextFiled.text
        //parms["CountryId"] = countryID[countryName.index(of: countryTextFiled.text!)!]
        
        if DTValidationManager.isValidEmail(text: emailTextFiled.text) {
            parms["EmailAddress"] = emailTextFiled.text
            
        }else{
            let alertMessage = "Please Fill Vaild E-Mail"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        if DTValidationManager.isValidStreetAddress(text: addressTextFiled.text) {
            parms["Street"] = addressTextFiled.text
            
        }else{
            let alertMessage = "Please Fill Vaild Street Name"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        if DTValidationManager.isValidZipCode(text: zipCodeTextFiled.text) {
            parms["ZipCode"] = zipCodeTextFiled.text
            
        }else{
            let alertMessage = "Please Fill Vaild ZipCode"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        if DTValidationManager.isValidPhoneNumber(text: zipCodeTextFiled.text) {
            parms["PhoneNumber"] = mobileTextFiled.text
            
        }else{
            let alertMessage = "Please Fill Vaild Phone Number"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        if let cityName = cityTextFiled.text {
            if DTValidationManager.isValidCityName(text: cityName) {
                parms["CityId"] = cityID[city.index(of: cityName) ?? 0]
            }else{
                let alertMessage = "Please Fill Vaild City Name"
                let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
                self.present(alertView, animated: true, completion: nil)
                return
            }
        }
        
        if DTValidationManager.isValidCountryName(text: countryTextFiled.text!) {
            parms["CountryId"] = countryID[countryName.index(of: countryTextFiled.text ?? "") ?? 0]
        }else{
            let alertMessage = "Please Fill Vaild Country Name"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        if DTValidationManager.isValidStateName(text: stateTextFiled.text!) {
            parms["StateId"] = stateID[stateArray.index(of: stateTextFiled.text!)!]
        }else{
            let alertMessage = "Please Fill Vaild State Name"
            let alertView = EXAlertViewController(title: "Wrong", twoButton: false, alertMessage:alertMessage ,buttonTitle: "OK",zeroHighView:false,centerTextAlertMessage:true,speratorViewEnable:true)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .editContactDetails, requestParams: parms)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                if let userResponcse = apiResponse as? [String:Any]{
                    viewController.newUser = DTParser.parseUserObjectFromResponse(response: userResponcse)
                    oldUser?.email = viewController.newUser?.email
                    oldUser?.phoneNumber = viewController.newUser?.phoneNumber
                    oldUser?.countryId = viewController.newUser?.countryId
                    oldUser?.countryName = viewController.newUser?.countryName
                    oldUser?.cityId = viewController.newUser?.cityId
                    oldUser?.cityName = viewController.newUser?.cityName
                    oldUser?.stateId = viewController.newUser?.stateId
                    oldUser?.zipCode = viewController.newUser?.zipCode
                    DTCredentialManager.credentialSharedInstance.user = oldUser
                    
                    if viewController.isFromBidScreen {
                        viewController.fireDelegate()
                    }
                    viewController.setEditButton()
                    MBProgressHUD.hide(for: viewController.view, animated: true)
                }
            }
            
        }
    }
    
    
    func callCountryAPI(){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCountry, requestParams: nil)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let countryArray = DTParser.parseCountryObjectArray(from: response)
                    
                    for country in countryArray  {
                        
                        if let countryName = country.name {
                            
                            self?.countryName.append(countryName)
                            
                        }
                        
                        if let countryID = country.id {
                            
                            self?.countryID.append(countryID)
                        }
                        
                    }
                    self?.setupCountryArray()
                }
                MBProgressHUD.hide(for: viewController.view, animated: true)
                
            }
        }
        
    }
    
    func callGetCityAPI(id:Int){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getCity, requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let cityArray = DTParser.parseCityObjectArray(from: response)
                    
                    for city in cityArray  {
                        
                        if let cityName = city.name {
                            
                            self?.city.append(cityName)
                            
                        }
                        
                        if let cityID = city.id {
                            
                            self?.cityID.append(cityID)
                        }
                        
                    }
                    self?.setupCityArray()
                }
                viewController.callGetStaetAPI(id: viewController.user!.cityId!)
                
                viewController.cityLoading.stopAnimating()
                viewController.cityLoading.isHidden = true
                
            }
        }
    }
    
    
    func callGetStaetAPI(id:Int){
        
        let requst = APIRequestBuilder.createRequestObjectForRequestId(requestId: .getStates , requestParams: id)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: requst, withCachedCompletionBlock: { (data, success) -> (Void) in
            
            
        }) { [weak self](apiResponse,success) -> (Void) in
            
            guard let viewController = self else{
                return
            }
            
            
            
            if success {
                
                if let response = apiResponse as? [Any] {
                    
                    let stateArray = DTParser.parseStateObjectArray(from: response)
                    
                    for state in stateArray  {
                        
                        if let stateName = state.name {
                            
                            self?.stateArray.append(stateName)
                            
                        }
                        
                        if let stateID = state.id {
                            
                            self?.stateID.append(stateID)
                        }
                        
                    }
                    self?.setupStateArray()
                }
                viewController.stateLoading.stopAnimating()
                viewController.stateLoading.isHidden = true
                
            }
        }
        
    }
    
    //MARK: - Experince ViewController Deleg
    func fireDelegate() {
        if newUser?.countryId == 0 || newUser?.cityId == 0 {
            contactDetailsDeleagte?.isCompletedHisProfile(isChange:false)
            isFromBidScreen = false
            dismiss(animated: true, completion: nil)
        }else{
            contactDetailsDeleagte?.isCompletedHisProfile(isChange:true)
            isFromBidScreen = false
            dismiss(animated: true, completion: nil)
            
        }
    }
    
}
