//
//  ProfileHeaderSectionButtonView.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProfileHeaderSectionButtonView: UITableViewHeaderFooterView {

    
    @IBOutlet weak var aboutButton:UIButton!
    @IBOutlet weak var badgesButton:UIButton!
    @IBOutlet weak var reviewsButton:UIButton!
    
   
    var colorSelect:UIColor?
    var tintColorr:UIColor?
    var defaultColor:UIColor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //AS: Configure the view for the selected state
        setSelected(buttonIndex: 1)
        
    }
    
 
    func selectAboutButton (){
        
        aboutButton.isSelected = true
        aboutButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonColorSelected()
        aboutButton.setTitleColor(DTHelper.sharedInstance.colorWithHexString("#F4D45E"), for: .normal)

        
        badgesButton.isSelected = false
        badgesButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonDefault()
        badgesButton.setTitleColor(.white, for: .normal)
        
        reviewsButton.isSelected = false
        reviewsButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonDefault()
        reviewsButton.setTitleColor(.white, for: .normal)
    }
  
    func selectBadgeseButton (){
        
        aboutButton.isSelected = false
        aboutButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonDefault()
        aboutButton.setTitleColor(.white, for: .normal)

        
        badgesButton.isSelected = true
        badgesButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonColorSelected()
        badgesButton.setTitleColor(DTHelper.sharedInstance.colorWithHexString("#F4D45E"), for: .normal)

        reviewsButton.isSelected = false
        reviewsButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonDefault()
        reviewsButton.setTitleColor(.white, for: .normal)
    }
    
    func selectReviewsButton (){
        
        aboutButton.isSelected = false
        aboutButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonDefault()
        aboutButton.setTitleColor(.white, for: .normal)

        
        badgesButton.isSelected = false
        badgesButton.backgroundColor =  ColorManager.sharedInstance.headerViewButtonDefault()
        badgesButton.setTitleColor(.white, for: .normal)

        
        reviewsButton.isSelected = true
        reviewsButton.backgroundColor = ColorManager.sharedInstance.headerViewButtonColorSelected()
        reviewsButton.setTitleColor(DTHelper.sharedInstance.colorWithHexString("#F4D45E"), for: .normal)
    }
    
    // MARK : Action
    
    func setSelected(buttonIndex: Int) {
        
        resetSelectedButton()
        
        switch buttonIndex {
        case 1:
            selectAboutButton()
            break
            
        case 2:
            
            selectBadgeseButton()
            break
            
        case 3 :
            
            selectReviewsButton ()
            break
            
        default:
            print("x")
        }
        
    }
    
    func resetSelectedButton() {
        
        aboutButton.isSelected = false
        badgesButton.isSelected = false
        reviewsButton.isSelected = false
        
        aboutButton.isEnabled = true
        badgesButton.isEnabled = true
        reviewsButton.isEnabled = true
        
    }


  

}
