//
//  ProfileButtomCell.swift
//  Experdice
//
//   Created by Ahmad Sallam  on 8/1/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProfileButtomCell: UITableViewCell {

    @IBOutlet weak var seeMoreButton: UIButton!
    @IBOutlet weak var dropImage: UIImageView!
    @IBOutlet weak var messageLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
