//
//  ProfileTopCell.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/1/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileTopCell: UITableViewCell {
    
    
    @IBOutlet weak var profilImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileJopTitle: UILabel!
    @IBOutlet weak var profileLocationName: UILabel!
    @IBOutlet weak var imageActivityIndacto: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageActivityIndacto.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    class func populateProfileCell(userObject : UserObject, tableView : UITableView, indexPath: NSIndexPath) -> ProfileTopCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"ProfileTopCell") as! ProfileTopCell
        
        cell.selectionStyle = .none
        let imageURL = "http://numidiajo.com/Experdice/API/images/\(userObject.imageURL ?? "")"
        
        cell.profilImage?.sd_setImage(with:URL(string:imageURL) , placeholderImage: #imageLiteral(resourceName: "memper_name"), options: .retryFailed, completed: { (image, error, cacheType, url) in
       //     ProfileTopCell.sharedInstance.activeLoading()

            if error != nil {
                
                print(error?.localizedDescription ?? "error found")
                
            } else {
                cell.profilImage?.contentMode = .scaleAspectFill
                cell.profilImage?.clipsToBounds = true
                
            }
        })
       // ProfileTopCell.sharedInstance.desactiveLoading()
        if UserObject.getUserType() == .expert {
          
            cell.profileName.text = userObject.profile.profileFirstName! + " " + userObject.profile.profileSecondName!
            cell.profileJopTitle.text =  userObject.headLine ?? "iOS Developer"
            
        }else{
            cell.profileName.text = userObject.profile.businessName
            cell.profileJopTitle.text = userObject.profile.businessIndustry
            cell.profileLocationName.text =  "\(userObject.cityName ?? ""),\(userObject.countryName ?? "")"

        }
        
       
        return cell
        
    }
    
  
    
}
