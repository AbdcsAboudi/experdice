//
//  GradesObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 1/22/18.
//  Copyright © 2018 DreamTech. All rights reserved.
//

import Foundation


class EducationGradeObject:NSObject {
    
    var id:Int?,gradeName:String?
    
    init(id:Int!,gradeName:String!) {
        self.gradeName = gradeName
        self.id = id
     
    }
    
    override init() {
        
    }
    
    
}
