//
//  AppDelegate.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/20/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import Firebase
import SDWebImage
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

     let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        Fabric.with([Crashlytics.self])
        
        FIRApp.configure()
        FIRConfiguration.sharedInstance().setLoggerLevel(.min)
         application.registerForRemoteNotifications()
        
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            // For iOS 10 data message (sent via FCM)
//            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        IQKeyboardManager.sharedManager().enable = true
        
        //AS : For change the bar item button color
        UINavigationBar.appearance().tintColor = ColorManager.sharedInstance.getNavigationBarItemColor()
       
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        
        
        

        return true
    }
    
    // MARK: - Actions and selectors 🚬
    
    func tokenRefreshNotification(_ notification: Notification) {
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            
            // AR: Token has been refreshed replace user token with new one
            if DTCredentialManager.isLoggedIn(), let user = DTCredentialManager.credentialSharedInstance.user as? UserObject {
                
                user.updateDeviceTokenAPI(deviceToken: refreshedToken, onComplete: { (success) -> (Void) in
                    
                    if success {
                        print(" -----> Token updated <------")
                    }
                })
            }
            
            return
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        //connectToFcm()
    }
    
    // START connect_to_fcm
//    func connectToFcm() {
//        FIRMessaging.messaging().connect { (error) in
//            if error != nil {
//                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
//            } else {
//
//                //AA : To get the notification token
//                let token = FIRInstanceID.instanceID().token()
//                print("Token ------------------------> ", token ?? "Error With token")
//                print("Connected to FCM.")
//            }
//        }
//    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("Failed to register for remote with \(error.localizedDescription)")
    }
    

    //AS: this is for LinkedIn SDK
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
      
        if LISDKCallbackHandler.shouldHandle(url){
            LISDKCallbackHandler.application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!  , annotation:nil )
        }
    
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
  
    
    



}

// MARK: - User Notifications center delegates

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        self.pushNotificationReceivedWhileActiveWithInfo(pushNotificationDict: userInfo as NSDictionary)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // AY: Complete the notification scenario if user is logged in
        
        guard DTCredentialManager.isLoggedIn() else{
            
            return
        }
        
        let userInfo = response.notification.request.content.userInfo
        
        // Print message ID.
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        self.pushNotificationReceivedWhileInBackgroundWithInfo(pushNotificationDict: userInfo as NSDictionary )
    }
    
    //MARK: - Push Notification Handling
    func pushNotificationReceivedWhileActiveWithInfo(pushNotificationDict : NSDictionary) {
        
        print("[AppDelegate] Notification Received while app running in foreground: \(pushNotificationDict)")
        
        DTPushNotificationHandler.sharedInstance.inAppNotificationReceived(with: pushNotificationDict)
        
    }
    
    func pushNotificationReceivedWhileInBackgroundWithInfo(pushNotificationDict : NSDictionary) {
        
        print("[AppDelegate] Notification Received while app in background: \(pushNotificationDict)")
        
        DTPushNotificationHandler.sharedInstance.opendAppFromPushNotification(with: pushNotificationDict)
        
    }
    
    
}

