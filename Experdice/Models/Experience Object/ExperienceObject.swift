//
//  BadgeObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit



class ExperienceObject:NSObject , NSCoding {
    
    var id:Int?,
    experienceImage:String?,
    experienceTitle:String?,
    experienceCompanyName:String?,
    experienceCountry:String?,
    experienceCity:String?,
    experienceDateTo:String?,
    isWorkHere:String?,
    experienceDateFrom:String?,
    cityId:Int?,
    countryId:Int?
    
    init(id:Int!,
         experienceImage:String! ,
         experienceTitle:String! ,
         experienceCompanyName:String! ,
         experienceCountry:String! ,
         experienceCity:String! ,
         experienceDateFrom:String! ,
         experienceDateTo:String!,
         isWorkHere:String!,
         cityId:Int!,
         countryId:Int!){
       
        self.experienceImage = experienceImage
        self.experienceTitle = experienceTitle
        self.experienceCompanyName = experienceCompanyName
        self.experienceCountry = experienceCountry
        self.experienceCity = experienceCity
        self.experienceDateFrom = experienceDateFrom
        self.experienceDateTo = experienceDateTo
        self.isWorkHere = isWorkHere
        self.id = id
        self.cityId = cityId
        self.countryId = countryId
        
        
    }
    
    override init() {
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let id = id {
            
            aCoder.encode(id, forKey: "id")
        }
        
        if let experienceImage = experienceImage {
            
            aCoder.encode(experienceImage, forKey: "experienceImage")
        }
        
        if let experienceTitle = experienceTitle {
            
            aCoder.encode(experienceTitle, forKey: "experienceTitle")
        }
        
        if let experienceCompanyName = experienceCompanyName {
            
            aCoder.encode(experienceCompanyName, forKey: "experienceCompanyName")
        }
        
        
        if let experienceCountry = experienceCountry {
            
            aCoder.encode(experienceCountry, forKey: "experienceCountry")
        }
        
        if let experienceCity = experienceCity {
            
            aCoder.encode(experienceCity, forKey: "experienceCity")
        }
        
        if let experienceDateFrom = experienceDateFrom {
            
            aCoder.encode(experienceDateFrom, forKey: "experienceDateFrom")
        }
        
        if let experienceDateTo = experienceDateTo {
            
            aCoder.encode(experienceDateTo, forKey: "experienceDateTo")
        }
        
        
        if let isWorkHere = isWorkHere {
            
            aCoder.encode(isWorkHere, forKey: "isWorkHere")
        }
        
        if let cityId = cityId {
            
            aCoder.encode(cityId, forKey: "cityId")
        }
        
        if let countryId = countryId {
            
            aCoder.encode(countryId, forKey: "countryId")
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.id = aDecoder.decodeInteger(forKey: "id")
        self.cityId = aDecoder.decodeInteger(forKey: "cityId")
        self.countryId = aDecoder.decodeInteger(forKey:"countryId")
        
        if let experienceImage = aDecoder.decodeObject(forKey: "experienceImage") as? String{
            
            self.experienceImage = experienceImage
        }
        
        if let experienceTitle = aDecoder.decodeObject(forKey: "experienceTitle") as? String{
            
            self.experienceTitle = experienceTitle
        }
        
        if let experienceCompanyName = aDecoder.decodeObject(forKey: "experienceCompanyName") as? String{
            
            self.experienceCompanyName = experienceCompanyName
        }
        
        if let experienceCountry = aDecoder.decodeObject(forKey: "experienceCountry") as? String{
            
            self.experienceCountry = experienceCountry
        }
        
        if let experienceCity = aDecoder.decodeObject(forKey: "experienceCity") as? String{
            
            self.experienceCity = experienceCity
        }
        
        if let experienceDateFrom = aDecoder.decodeObject(forKey: "experienceDateFrom") as? String{
            
            self.experienceDateFrom = experienceDateFrom
        }
        if let experienceDateTo = aDecoder.decodeObject(forKey: "experienceDateTo") as? String{
            
            self.experienceDateTo = experienceDateTo
        }
        
        if let isWorkHere = aDecoder.decodeObject(forKey: "isWorkHere") as? String {
            
            self.isWorkHere = isWorkHere
        }
        
        
        
    }
    
    
    
}
