//
//  TextField Object.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

public enum FormOptionType {
    
    case none
    case regularTextField
    case dropDownTextField
    case multipleDropDownsTextField
    case textView
    case date
    case pollOption

}

public enum KeyboardType {
    
    case number
    case text
    case birthDate
    case picker
    case date

}

enum TextFieldType {
    
    case email
    case phoneNamber
    case firstName
    case birthDay
    case domain
    case string
    case password

}

class FormOptionObject: NSObject {
    
    var textFieldTitle:String?,
    data:String?,
    secondData:String?,
    textFieldPlaceholder:String?,
    formOptionType:FormOptionType?,
    keyboardType:TextFieldType?,
    rowTag:Int?,
    pickerObjects:[DTDomainObject]?,
    secondPickerObject:[DTDomainObject]?

    init(textFieldTitle:String?,
         textFieldPlaceholder:String?,
         formOptionType :FormOptionType,
         keyboardType :TextFieldType,
         rowTag:Int,
         data:String!,
         secondData:String!) {
        
        self.textFieldTitle = textFieldTitle
        self.textFieldPlaceholder = textFieldPlaceholder
        self.formOptionType = formOptionType
        self.keyboardType = keyboardType
        self.rowTag = rowTag
        self.data = data
        self.secondData = secondData

    }
    
    init(textFieldTitle:String?,textFieldPlaceholder:String?,formOptionType :FormOptionType,keyboardType :TextFieldType,rowTag:Int,pickerObjects:[DTDomainObject]) {
        
        self.textFieldTitle = textFieldTitle
        self.textFieldPlaceholder = textFieldPlaceholder
        self.formOptionType = formOptionType
        self.keyboardType = keyboardType
        self.rowTag = rowTag
        self.pickerObjects = pickerObjects

    }
    
    init(textFieldTitle:String?,
         textFieldPlaceholder:String?,
         formOptionType :FormOptionType,
         keyboardType :TextFieldType,
         rowTag:Int,
         pickerObjects:[DTDomainObject],
         secondPickerObject:[DTDomainObject]) {
        
        self.textFieldTitle = textFieldTitle
        self.textFieldPlaceholder = textFieldPlaceholder
        self.formOptionType = formOptionType
        self.keyboardType = keyboardType
        self.rowTag = rowTag
        self.pickerObjects = pickerObjects
        self.secondPickerObject = secondPickerObject
        
    }


}
