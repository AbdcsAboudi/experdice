//
//  UserObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/24/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation

class UserObject: NSObject,NSCoding {
    
    
    
    var userID:Int?,
    email:String?,
    password:String?,
    phoneNumber:String?,
    countryId:Int?,
    countryName:String?,
    cityId:Int?,
    cityName:String?,
    stateId:Int?,
    stateName:String?,
    streetName:String?,
    zipCode:String?,
    userType:String?,
    overviewText:String?,
    overviewVideoLink:String?,
    imageURL:String?,
    bussinessIndustryID:String?,
    invitedExpertName:String?,
    genderId:Int?,
    dateOfBirth:String?,
    headLine:String?,
    profile: ProfileObject!,
    deviceTokenId:Int?
    
    
    
    override init() {
        
    }
    
    init(userID:Int!,
         profile: ProfileObject,
         email:String!,
         password:String!,
         phoneNumber:String!,
         countryId:Int!,
         countryName:String!,
         cityId:Int!,
         cityName:String!,
         stateId:Int!,
         stateName:String!,
         streetName:String!,
         zipCode:String!,
         userType:String!,
         overviewText:String!,
         overviewVideoLink:String!,
         bussinessIndustryID:String!,
         invitedExpertName:String!,
         genderId:Int!,
         dateOfBirth:String!,
         headLine:String!,
         imageURL:String!) {
        
        self.password = password
        self.userID = userID
        self.profile = profile
        self.email = email
        self.phoneNumber = phoneNumber
        self.countryId = countryId
        self.countryName = countryName
        self.cityId = cityId
        self.cityName = cityName
        self.stateId = stateId
        self.stateName = stateName
        self.zipCode = zipCode
        self.userType = userType
        self.overviewText = overviewText
        self.overviewVideoLink = overviewVideoLink
        self.imageURL = imageURL
        self.bussinessIndustryID = bussinessIndustryID
        self.invitedExpertName = invitedExpertName
        self.streetName = streetName
        self.genderId = genderId
        self.dateOfBirth = dateOfBirth
        self.headLine = headLine
    }
   
    func encode(with aCoder: NSCoder) {
        
        if let genderId = genderId {
            
            aCoder.encode(genderId, forKey: "genderId")
        }
        
        if let dateOfBirth = dateOfBirth {
            
            aCoder.encode(dateOfBirth, forKey: "dateOfBirth")
        }
        
        if let headLine = headLine {
            
            aCoder.encode(headLine, forKey: "headLine")
        }
        
        if let userID = userID {
            
            aCoder.encode(userID, forKey: "userID")
        }
        
        
        if let invitedExpertName = invitedExpertName {
            
            aCoder.encode(invitedExpertName, forKey: "invitedExpertName")
        }
        
        
        
        if let profile = profile {
            
            aCoder.encode(profile, forKey: "profile")
        }
        
        if let email = email {
            
            aCoder.encode(email, forKey: "email")
        }
        
        if let bussinessIndustryID = bussinessIndustryID {
            
            aCoder.encode(bussinessIndustryID, forKey: "bussinessIndustryID")
        }
        
        if let password = password {
            
            aCoder.encode(password, forKey: "password")
        }
        
        if let phoneNumber = phoneNumber {
            
            aCoder.encode(phoneNumber, forKey: "phoneNumber")
        }
        
        if let countryId = countryId {
            
            aCoder.encode(countryId, forKey: "countryId")
        }
        
        if let countryName = countryName {
            
            aCoder.encode(countryName, forKey: "countryName")
        }
        
        if let cityId = cityId {
            
            aCoder.encode(cityId, forKey: "cityId")
        }
        
        if let cityName = cityName {
            
            aCoder.encode(cityName, forKey: "cityName")
        }
        
        if let stateId = stateId {
            
            aCoder.encode(stateId, forKey: "stateId")
        }
        
        if let stateName = stateName {
            
            aCoder.encode(stateName, forKey: "stateName")
        }
        
        if let streetName = streetName {
            
            aCoder.encode(streetName, forKey: "streetName")
        }
        
        
        if let zipCode = zipCode {
            
            aCoder.encode(zipCode, forKey: "zipCode")
        }
        
        if let userType = userType {
            
            aCoder.encode(userType, forKey: "userType")
        }
        
        if let overviewText = overviewText {
            
            aCoder.encode(overviewText, forKey: "overviewText")
        }
        
        if let overviewVideoLink = overviewVideoLink {
            
            aCoder.encode(overviewVideoLink, forKey: "overviewVideoLink")
        }
        
        if let imageURL = imageURL {
            
            aCoder.encode(imageURL, forKey: "imageURL")
        }
      
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
       
        if let profile = aDecoder.decodeObject(forKey: "profile") as? ProfileObject {
            
            self.profile = profile
        }
        
        if let headLine = aDecoder.decodeObject(forKey: "headLine") as? String {
            
            self.headLine = headLine
        }
       
        if let invitedExpertName = aDecoder.decodeObject(forKey: "invitedExpertName") as? String {
            
           self.invitedExpertName = invitedExpertName
        }
        
        if let bussinessIndustryID = aDecoder.decodeObject(forKey: "bussinessIndustryID") as? String {
            
            self.bussinessIndustryID = bussinessIndustryID
        }
        
        if let email = aDecoder.decodeObject(forKey: "email") as? String {
            
            self.email = email
        }
        
        if let password = aDecoder.decodeObject(forKey: "password") as? String {
            
            self.password = password
        }
        
        if let phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String {
            
            self.phoneNumber = phoneNumber
        }
        
        if let countryName = aDecoder.decodeObject(forKey: "countryName") as? String {
            
            self.countryName = countryName
        }
        
        if let cityName = aDecoder.decodeObject(forKey: "cityName") as? String {
            
            self.cityName = cityName
        }
        
        if let stateName = aDecoder.decodeObject(forKey: "stateName") as? String {
            
            self.stateName = stateName
        }
        
        if let streetName = aDecoder.decodeObject(forKey: "streetName") as? String {
            
            self.streetName = streetName
        }
        
        if let zipCode = aDecoder.decodeObject(forKey: "zipCode") as? String {
            
            self.zipCode = zipCode
        }
        
        if let userType = aDecoder.decodeObject(forKey: "userType") as? String {
            
            self.userType = userType
        }
        
        if let overviewText = aDecoder.decodeObject(forKey: "overviewText") as? String {
            
            self.overviewText = overviewText
        }
        
        if let overviewVideoLink = aDecoder.decodeObject(forKey: "overviewVideoLink") as? String {
            
            self.overviewVideoLink = overviewVideoLink
        }
        
        if let imageURL = aDecoder.decodeObject(forKey: "imageURL") as? String {
            
            self.imageURL = imageURL
        }
        
        if let dateOfBirth = aDecoder.decodeObject(forKey: "dateOfBirth") as? String {
            
            self.dateOfBirth = dateOfBirth
        }
        
       
        self.genderId = aDecoder.decodeInteger(forKey: "genderId")
        self.countryId = aDecoder.decodeInteger(forKey: "countryId")
        self.cityId = aDecoder.decodeInteger(forKey: "cityId")
        self.stateId = aDecoder.decodeInteger(forKey: "stateId")
        self.userID = aDecoder.decodeInteger(forKey: "userID")
        
        
    }
    
     static func getUserType() -> ProfileObject.UserType{
        
        let user = DTCredentialManager.credentialSharedInstance.user as? UserObject
        
        return user!.profile.userType
   }
    func updateDeviceTokenAPI(deviceToken: String?,  onComplete: @escaping (_ success: Bool) -> (Void)){
        
        guard let deviceToken = deviceToken, let tokenId = deviceTokenId else {
            return
        }
        
        var params = [String: Any]()
        
        params["deviceToken"] = deviceToken
        params["tokenID"] = tokenId
        
        let request = APIRequestBuilder.createRequestObjectForRequestId(requestId: .updateDeviceToken, requestParams:params)
        
        DTDataSource.sharedInstance.executeAPIRequest(requestObject: request, withCachedCompletionBlock: { (data,success ) -> (Void) in
            
        }) { (apiResponse, success) -> (Void) in
            
            onComplete(success)
        }
    }
    
   
    
    
    
}
