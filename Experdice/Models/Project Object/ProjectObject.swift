//
//  Project Object.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/6/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit


class ProjectObject: NSObject{
    
    
    var id:Int?,
    title:String?,
    projectType:String?,
    businessID:Int?,
    businessName:String?,
    imageURL:String?,
    addingDate:String?,
    openUntilDate:String?,
    actualBeginDate:String?,
    actualEndDate:String?,
    budget:String?,
    bidNumber:Int?,
    bidAverage:Double?, //To here Project List
    isWatchList:Bool?,
    invitesExpert:[UserObject]?,
    
    objectives:String? ,
    goalsAchievement:String? ,
    goalsDescription:String? ,
    goalsNotes:String? ,
    seccionDuration:String? ,
    expectedNumber:String?,
    projectStatusId:String?,
    preference:String?,
    paymentMethodID:String?,
    sessionFrequencyRate:String?,
    budgetRate:String?,
    experinceYear:String?,
    candidateDescription:String?,
    
    
    
    proSeniority:ProfssionalSeniorityObject?,
    client:String?,
    user:[UserObject]?,
    projectBids:[ProjectBidsObject]?,
    projectCategory:ProjectCategoryObject?,
    paymentMethod:PaymentMethodsObject?,
    sessionLanguage:LanguagesObject? ,
    contentLanguage:LanguagesObject? ,
    trainerGender:GenderObject? ,
    businessIndustry:BusinessTypeObject? ,
    country:CountryObject?,
    city:CityObject?,
    isOpend = false
    

    override init() {
        
    }
    
    
    init(id:Int!,
         title:String!,
         projectType:String!,
         businessID:Int!,
         businessName:String!,
         imageURL:String!,
         addingDate:String!,
         openUntilDate:String!,
         actualBeginDate:String!,
         actualEndDate:String!,
         budget:String!,
         bidNumber:Int!,
         bidAverage:Double!,
         
         invitesExpert:[UserObject]!,
         
         objectives:String!,
         goalsAchievement:String!,
         goalsDescription:String!,
         goalsNotes:String! ,
         seccionDuration:String! ,
         expectedNumber:String!,
         projectStatusId:String!,
         preference:String!,
         paymentMethodID:String!,
         sessionFrequencyRate:String!,
         budgetRate:String!,
         experinceYear:String!,
         candidateDescription:String!,
         
         proSeniority:ProfssionalSeniorityObject!,
         client:String!,
         user:[UserObject]!,
         projectBids:[ProjectBidsObject]!,
         projectCategory:ProjectCategoryObject!,
         paymentMethod:PaymentMethodsObject!,
         sessionLanguage:LanguagesObject! ,
         contentLanguage:LanguagesObject! ,
         trainerGender:GenderObject! ,
         businessIndustry:BusinessTypeObject! ,
         country:CountryObject!,
         city:CityObject!,
         isOpend:Bool,
         isWatchList:Bool!) {
        
        
        self.invitesExpert = invitesExpert
        self.id = id
        self.title = title
        self.projectType = projectType
        self.businessID = businessID
        self.businessName = businessName
        self.imageURL = imageURL
        self.addingDate = addingDate
        self.openUntilDate = openUntilDate
        self.actualBeginDate = actualBeginDate
        self.actualEndDate = actualEndDate
        self.budget = budget
        self.bidNumber = bidNumber
        self.bidAverage = bidAverage
        self.objectives = objectives
        self.goalsAchievement = goalsAchievement
        self.goalsDescription = goalsDescription
        self.goalsNotes = goalsNotes
        self.seccionDuration = seccionDuration
        self.expectedNumber = expectedNumber
        self.projectStatusId = projectStatusId
        self.preference = preference
        self.paymentMethodID = paymentMethodID
        self.sessionFrequencyRate = sessionFrequencyRate
        self.budgetRate = budgetRate
        self.experinceYear = experinceYear
        self.candidateDescription = candidateDescription
        self.proSeniority = proSeniority
        self.client = client
        self.user = user
        self.projectBids = projectBids
        self.projectCategory = projectCategory
        self.paymentMethod = paymentMethod
        self.sessionLanguage = sessionLanguage
        self.contentLanguage = contentLanguage
        self.trainerGender = trainerGender
        self.businessIndustry = businessIndustry
        self.country = country
        self.city = city
        self.isWatchList = isWatchList

        
    }
    
    
    
    
    
    
    
    
    
}


