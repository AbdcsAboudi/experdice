//
//  LocationObject.swift
//  Experdice
//
//  Created by A.Aboudi on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class LocationObject: NSObject {

    var location = ""
    var longitude = ""
    var latitude = ""

    init(location:String ,longitude:String, latitude:String) {
        
        self.location = location
        self.longitude = longitude
        self.latitude = latitude
        
    }
    
}
