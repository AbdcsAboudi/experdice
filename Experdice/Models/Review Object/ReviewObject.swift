//
//  ReviewObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit


class ReviewObject:NSObject , NSCoding {
    
    //AS: Shared Var
    var id:Int?,
    reviewTitle:String?,
    reviewDescription:String?,
    ratingId:Int?,
    ratingLevel:String?,
    
    //AS: Client Var
    businessId:Int? ,
    businessName:String?,
    
    
    //AS: Expert Var
    expertName:String?,
    expertId:Int?
    
    
    override init() {
        
        
    }
   
    //AS: Business init
    init(id:Int!,
         reviewTitle:String!,
         reviewDescription:String!,
         ratingId:Int!,
         ratingLevel:String!,
         businessId:Int!,
         businessName:String!) {
        
        self.id = id
        self.reviewTitle = reviewTitle
        self.reviewDescription = reviewDescription
        self.ratingId = ratingId
        self.ratingLevel = ratingLevel
        self.businessId = businessId
        self.businessName = businessName
        
    }
    
    //AS: Expert init
    init(id:Int?,
         reviewTitle:String?,
         reviewDescription:String?,
         ratingId:Int?,
         ratingLevel:String?,
         expertName:String?,
         expertId:Int?) {
        
        self.id = id
        self.reviewTitle = reviewTitle
        self.reviewDescription = reviewDescription
        self.ratingId = ratingId
        self.ratingLevel = ratingLevel
        self.expertName = expertName
        self.expertId = expertId
        
        
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let reviewTitle = reviewTitle {
            
            aCoder.encode(reviewTitle, forKey: "reviewTitle")
        }
        if let reviewDescription = reviewDescription {
            
            aCoder.encode(reviewDescription, forKey: "reviewDescription")
        }
  
        if let ratingId = ratingId {
            
            aCoder.encode(ratingId, forKey: "ratingId")
        }
        
        if let ratingLevel = ratingLevel {
            
            aCoder.encode(ratingLevel, forKey: "ratingLevel")
        }
        if let userID = id {
            
            aCoder.encode(userID, forKey: "userId")
        }
        
        if let expertName = expertName {
            
            aCoder.encode(expertName, forKey: "expertName")
        }
        
        if let expertId = expertId {
            
            aCoder.encode(expertId, forKey: "expertId")
        }
        
        if let businessId = businessId {
            
            aCoder.encode(businessId, forKey: "businessId")
        }
        
        if let businessName = businessName {
            
            aCoder.encode(businessName, forKey: "businessName")
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        if let reviewTitle = aDecoder.decodeObject(forKey: "reviewTitle") as? String {
            
            self.reviewTitle = reviewTitle
        }
        
        if let reviewDescription = aDecoder.decodeObject(forKey: "reviewDescription") as? String {
            
            self.reviewDescription = reviewDescription
        }
       
        if let expertName = aDecoder.decodeObject(forKey: "expertName") as? String {
            
            self.expertName = expertName
        }
        
        if let businessName = aDecoder.decodeObject(forKey: "businessName") as? String{
            
            self.businessName = businessName
        }
        
        if let ratingLevel = aDecoder.decodeObject(forKey: "ratingLevel") as? String {
            
            self.ratingLevel = ratingLevel
        }
        
        self.id = aDecoder.decodeInteger(forKey: "userId")
        self.businessId = aDecoder.decodeInteger(forKey: "businessId")
        self.expertId = aDecoder.decodeInteger(forKey: "expertId")
        self.ratingId = aDecoder.decodeInteger(forKey: "ratingId")
        
    }

    
    
    
    
    
    
}
