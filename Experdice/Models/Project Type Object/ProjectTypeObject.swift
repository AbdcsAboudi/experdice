//
//  ProjectTypeObject.swift
//  Experdice
//
//  Created by A.Aboudi on 9/10/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class ProjectTypeObject: NSObject {

    var  projectTypeId:Int?,
    projectTypeName:String?
    
    init(projectTypeId:Int?,projectTypeName:String?) {
        
        self.projectTypeId = projectTypeId
        self.projectTypeName = projectTypeName
        
    }
}
