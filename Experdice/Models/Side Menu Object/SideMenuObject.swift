//
//  SideMenuObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/2/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit



class SideMenuObject {
    
    var id = 0 , title:String? , profileImage:UIImage? , segueName:String? , userName:String? , specializationName:String?,
    badgeLabel:Bool?
    
    //AS : Side Menu
    init(id:Int!,title:String!,segueName:String!,badgeLabel:Bool!) {
        
        self.id = id
        self.title = title
        self.segueName = segueName
        self.badgeLabel = badgeLabel
     
    }
    
    //AS: Header Side Menu
    init(id:Int!,profileImage:UIImage!,segueName:String!,userName:String? , specializationName:String?) {
        
        self.id = id
        self.profileImage = profileImage
        self.segueName = segueName
        self.userName = userName
        self.specializationName = specializationName
        
        
        
    }
    
    
    
    
    
    
    
    
}
