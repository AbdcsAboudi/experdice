//
//  NotificationObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/7/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit


class NotificationObject {
    
    
    var  notificationType:String? , notificationDate:String? , notificationStatus:Bool? ,
    
    notificationMemberName:String? , notificationProjectName:String?
    
    init(notificationType:String! , notificationDate:String! , notificationStatus:Bool!,notificationMemberName:String! , notificationProjectName:String!) {
        
       
        self.notificationType = notificationType
        self.notificationDate = notificationDate
        self.notificationStatus = notificationStatus
        self.notificationMemberName = notificationMemberName
        self.notificationProjectName = notificationProjectName
        
        
    }
    
    
    
    
    
}
