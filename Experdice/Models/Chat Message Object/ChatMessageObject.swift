//
//  ChatMessageObject.swift
//  Experdice
//
//  Created by A.Aboudi on 9/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import JSQMessagesViewController

class ChatMessageObject: NSObject {

    var messageObject = JSQMessage(senderId: "", displayName: "", text: "")
    var userObject = UserObject()
    var isThisMessageMine = false
    
}
