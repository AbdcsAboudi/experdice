//
//  FAQObject.swift
//  Experdice
//
//  Created by A.Aboudi on 9/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class FAQObject: NSObject {

    var question = ""
    var answer = ""
    var isCollapsed = false
    
    public init(question : String , answer : String) {
        
        self.question = question
        self.answer = answer
    }

}
