//
//  ProfileObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit

class ProfileObject: NSObject , NSCoding {
    
    enum UserType:Int {
        
        case null = 0
        case expert = 1
        case business = 2
    }
    
    override init() {
        
    }
    
    var reviews:[ReviewObject]?
    var badges:[BadgeObject]?
    
    //AS: Expert Profile
    var expertId:Int?,
    profileFirstName:String?,
    profileSecondName:String?,
    profileJopTitle:String?,
    expertCountry:String!,
    expertCity:String!,
    expertOverallRating:Int?,
    expertStatusId:Int?,
    linkedinURL:String?,
    nationalityId:Int?,
    nationalityName:String?,
    earilyRegistered:String?,
    
    //AS: Business Profile
    businessId:Int?,
    businessName:String?,
    businessIndustry:String?,
    businessHeadquarters:String?,
    aboutUs:String?,
    founded:String?,
    typeOfBusiness:String?,
    specialties:String?

    
    var experienceObject:[ExperienceObject]?
    var educationObject:[EducationObject]?
    var certificationObject:[CertificationObject]?
    
    var userType:UserType = .expert
    
    
    //MARK: - Expert init
    init(withUserExpert
         expertId:Int!,
         profileFirstName:String!,
         profileSecondName:String!,
         profileJopTitle:String!,
         expertCountry:String!,
         expertCity:String!,
         expertOverallRating:Int!,
         expertStatusId:Int!,
         linkedinURL:String!,
         nationalityId:Int!,
         nationalityName:String!,
         earilyRegistered:String!,
         experienceObject:[ExperienceObject]!,
         educationObject:[EducationObject]!,
         certificationObject:[CertificationObject]!,
         reviews:[ReviewObject]!,
         badges:[BadgeObject]!) {
        
        self.userType = .expert
        
        self.expertId = expertId
        self.profileFirstName = profileFirstName
        self.profileSecondName = profileSecondName
        self.expertCity = expertCity
        self.expertCountry = expertCountry
        self.profileJopTitle = profileJopTitle
        self.expertOverallRating = expertOverallRating
        self.expertStatusId = expertStatusId
        self.linkedinURL = linkedinURL
        self.nationalityId = nationalityId
        self.nationalityName = nationalityName
        self.earilyRegistered = earilyRegistered
        self.experienceObject = experienceObject
        self.educationObject = educationObject
        self.certificationObject = certificationObject
        self.reviews = reviews
        self.badges = badges
        
    }
    
    //MARK: - Business init
    init(withUserBusiness
        businessId:Int?,
        businessName:String!,
        businessIndustry:String!,
        businessHeadquarters:String! ,
        aboutUs:String!,
        founded:String!,
        typeOfBusiness:String!,
        specialties:String!) {
        
        self.userType = .business
        
        self.businessId = businessId
        self.businessName = businessName
        self.businessIndustry = businessIndustry
        self.businessHeadquarters = businessHeadquarters
        self.aboutUs = aboutUs
        self.founded = founded
        self.specialties = specialties
        self.typeOfBusiness = typeOfBusiness
        
      
        
    }
    

    //MARK: - Encode Method
    func encode(with aCoder: NSCoder) {
        
        if let profileFirstName = profileFirstName {
            
            aCoder.encode(profileFirstName, forKey: "profileFirstName")
        }
        
        if let profileSecondName = profileSecondName {
            
            aCoder.encode(profileSecondName, forKey: "profileSecondName")
        }
        
        if let profileJobTitle = profileJopTitle {
            
            aCoder.encode(profileJobTitle, forKey: "profileJobTitle")
        }
        
        if let expertCity = expertCity {
           
            aCoder.encode(expertCity, forKey: "expertCity")
        }
        
        if let expertCountry = expertCountry {
            
            aCoder.encode(expertCountry, forKey: "expertCountry")
        }
        
        if let experienceObject = experienceObject {
            
            aCoder.encode(experienceObject, forKey: "experienceObject")
        }
        
        if let educationObject = educationObject {
            
            aCoder.encode(educationObject, forKey: "educationObject")
        }
        if let certificationObject = certificationObject {
            
            aCoder.encode(certificationObject, forKey: "certificationObject")
        }
        
        if let expertStatusId = expertStatusId {
            
            aCoder.encode(expertStatusId, forKey: "expertStatusId")
        }
        
        if let nationalityId = nationalityId {
            
            aCoder.encode(nationalityId, forKey: "nationalityId")
        }
        
        if let reviews = reviews {
            
            aCoder.encode(reviews, forKey: "reviews")
        }
        
        if let businessId = businessId {
            
            aCoder.encode(businessId, forKey: "businessId")
            
        }
        
        if let expertId = expertId {
            
            aCoder.encode(expertId, forKey: "expertId")
            
        }
        
        if let businessName = businessName {
            
            aCoder.encode(businessName, forKey: "businessName")
        }
        
        
        if let businessIndustry = businessIndustry {
            
            aCoder.encode(businessIndustry, forKey: "businessIndustry")
        }
        
        if let businessHeadquarters = businessHeadquarters {
            
            aCoder.encode(businessHeadquarters, forKey: "businessHeadquarters")
        }
        
        if let aboutUs = aboutUs {
            
            aCoder.encode(aboutUs, forKey: "aboutUs")
        }
        
        if let founded = founded{
            
            aCoder.encode(founded, forKey: "founded")
        }
        
        if let specialties = specialties{
            
            aCoder.encode(specialties, forKey: "specialties")
        }
        
        if let typeOfBusiness = typeOfBusiness{
            
            aCoder.encode(typeOfBusiness, forKey: "typeOfBusiness")
        }
       
         aCoder.encode(userType.rawValue, forKey:"userType")
      
    }
    
    //MARK: - Decode Method
    required init?(coder aDecoder: NSCoder) {
        
       
        if let founded =  aDecoder.decodeObject(forKey: "founded") as? String {
            
            self.founded = founded
            
        }
        
        if let specialties =  aDecoder.decodeObject(forKey: "specialties") as? String {
            
            self.specialties = specialties
            
        }
        
        if let typeOfBusiness =  aDecoder.decodeObject(forKey: "typeOfBusiness") as? String {
            
            self.typeOfBusiness = typeOfBusiness
            
        }
        
        if let profileFirstName =  aDecoder.decodeObject(forKey: "profileFirstName") as? String {
            
            self.profileFirstName = profileFirstName
            
        }
        
        if let profileSecondName = aDecoder.decodeObject(forKey: "profileSecondName") as? String {
            
            self.profileSecondName = profileSecondName
        }
        
        if let profileJobTitle = aDecoder.decodeObject(forKey: "profileJobTitle") as? String {
            
            self.profileJopTitle = profileJobTitle
            
        }
        
        if let expertCity = aDecoder.decodeObject(forKey: "expertCity") as? String  {
            
            self.expertCity = expertCity
            
        }
        if let expertCountry = aDecoder.decodeObject(forKey: "expertCountry") as? String  {
            
            self.expertCountry = expertCountry
            
        }
        
        if let experienceObject =  aDecoder.decodeObject(forKey: "experienceObject") as? [ExperienceObject] {
            
            self.experienceObject = experienceObject

        }
        
        if let educationObject = aDecoder.decodeObject(forKey: "educationObject") as? [EducationObject] {
            
           self.educationObject = educationObject

        }
        
        if let certificationObject = aDecoder.decodeObject(forKey: "certificationObject") as? [CertificationObject] {
            
            self.certificationObject = certificationObject
        }
       
        if let businessName = aDecoder.decodeObject(forKey: "businessName") as? String {
            
            self.businessName = businessName
        }
        
        if let businessHeadquarters = aDecoder.decodeObject(forKey: "businessHeadquarters") as? String  {
            
            self.businessHeadquarters = businessHeadquarters
            
        }
        
        if let businessIndustry =  aDecoder.decodeObject(forKey: "businessIndustry") as? String {
            
            self.businessIndustry = businessIndustry
        }
      
        if let reviews = aDecoder.decodeObject(forKey: "reviews") as? [ReviewObject] {
            
            self.reviews = reviews
        }
        
        self.businessId = aDecoder.decodeInteger(forKey: "businessId")
        self.expertId = aDecoder.decodeInteger(forKey: "expertId")
        self.nationalityId = aDecoder.decodeInteger(forKey: "nationalityId")
        self.expertStatusId = aDecoder.decodeInteger(forKey: "expertStatusId")
        
        let userType = aDecoder.decodeInteger(forKey: "userType")
        self.userType = UserType(rawValue: userType) ?? .expert
        
        
        
        
       
        
    }
    
    
    
    
}


