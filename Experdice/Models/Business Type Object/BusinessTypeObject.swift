//
//  BusinessObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/31/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit



class BusinessTypeObject:NSObject {
    
    var id:Int?,name:String?
    
    override init() {}
    
    init(id:Int!,name:String!) {
        self.id = id
        self.name = name
    }
    
    
}
