//
//  PaymentMethodsObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 10/11/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation


class PaymentMethodsObject:NSObject{
    
    
    var id:Int?,name:String?,descriptionText: String?
    
    
    override init() {}
    
    init(id:Int!,name:String!,descriptionText:String!){
        
        self.id = id
        self.name = name
        self.descriptionText = descriptionText
    }
    
}
