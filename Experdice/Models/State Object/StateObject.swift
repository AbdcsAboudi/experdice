//
//  StateObject.swift
//  Experdice
//
//  Created by Yousef ALselawe on 9/26/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit


class StateObject:NSObject{
    
    
    var id:Int?
    var name:String?
    var cityId:Int?
    
    override init() {}
    
    init(id:Int!,name:String!,cityId:Int!){
        
        self.id = id
        self.name = name
        self.cityId = cityId
        
    }

}
