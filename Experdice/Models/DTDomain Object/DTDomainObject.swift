//
//  DTDomainObject.swift
//  Experdice
//
//  Created by A.Aboudi on 9/11/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

class DTDomainObject: NSObject {

    var id:Int?
    var display:String?
    
    init(id:Int! , display:String!) {
        
        self.id = id
        self.display = display
        
    }

}
