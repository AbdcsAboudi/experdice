//
//  ExpertInfoObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 11/19/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation



class ExpertInfo {
    
    var name:String?,imageURL:String?,country:String?,city:String?,expertRating:String?
    
    init(name:String!,imageURL:String!,country:String!,city:String!,expertRating:String!) {
        
        self.name = name
        self.imageURL = imageURL
        self.country = country
        self.city = city
        self.expertRating = expertRating
        
    }
    
    
}
