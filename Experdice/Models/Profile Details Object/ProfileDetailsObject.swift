//
//  ProfileDetailsObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/14/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation





class ProfileDetailsObject:NSObject,NSCoding {
    
    
    var title:String? , id:Int?
    
    init(title:String! , id:Int!) {
        
        self.title = title
        self.id = id
        
    }
    
    
    func encode(with aCoder: NSCoder) {
        
        if let title = title {
            
            aCoder.encode(title, forKey: "title")
        }
        
        if let id = id {
            
            aCoder.encode(id, forKey: "id")
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        if let title = aDecoder.decodeObject(forKey: "title") as? String {
            
            self.title = title
        }
        
        let id = aDecoder.decodeInteger(forKey: "id")
        self.id = id
        
    }
    
    
    
}
