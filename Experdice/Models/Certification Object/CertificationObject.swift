//
//  BadgeObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit



class CertificationObject:NSObject , NSCoding {
    
    var id:Int?,
    certificationName:String? ,
    certificationAuthority:String?,
    certificationLicenseNo:String?,
    certificationDateFrom:String?,
    certificationDateTo:String?,
    certificationImage:String? ,
    certificationURL:String? ,
    expireFlag:String?,
    approvedFlag:String?
    
    
    init(id:Int!,certificationName:String!,certificationAuthority:String!,certificationImage:String!, certificationLicenseNo:String!  , certificationDateFrom:String! , certificationDateTo:String!,certificationURL:String!,expireFlag:String!,approvedFlag:String!){
        
        self.id = id
        self.certificationName = certificationName
        self.certificationAuthority = certificationAuthority
        self.certificationImage = certificationImage
        self.certificationLicenseNo = certificationLicenseNo
        self.certificationURL = certificationURL
        self.certificationDateFrom = certificationDateFrom
        self.certificationDateTo = certificationDateTo
        self.approvedFlag = approvedFlag
        self.expireFlag = expireFlag
        
        
    }
    
    override init() {
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let id = id {
            
            aCoder.encode(id, forKey: "id")
        }
        
        if let certificationName = certificationName {
            
            aCoder.encode(certificationName, forKey: "certificationName")
        }
        
        if let certificationAuthority = certificationAuthority {
            
            aCoder.encode(certificationAuthority, forKey: "certificationAuthority")
        }
        
        if let certificationImage = certificationImage {
            
            aCoder.encode(certificationImage, forKey: "certificationImage")
        }
        
        if let certificationLicenseNo = certificationLicenseNo {
            
            aCoder.encode(certificationLicenseNo, forKey: "certificationLicenseNo")
        }
        
        if let certificationURL = certificationURL {
            
            aCoder.encode(certificationURL, forKey: "certificationURL")
        }
        
        if let certificationDateFrom = certificationDateFrom {
            
            aCoder.encode(certificationDateFrom, forKey: "certificationDateFrom")
        }
        
        if let certificationDateTo = certificationDateTo {
            
            aCoder.encode(certificationDateTo, forKey: "certificationDateTo")
        }
        
        if let approvedFlag = approvedFlag {
            
            aCoder.encode(approvedFlag, forKey: "approvedFlag")
        }
        
        if let expireFlag = expireFlag {
            
            aCoder.encode(expireFlag, forKey: "expireFlag")
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
    
        if let certificationName = aDecoder.decodeObject(forKey: "certificationName") as? String{
            
            self.certificationName = certificationName
        }
        
        if let certificationAuthority = aDecoder.decodeObject(forKey: "certificationAuthority") as? String {
            
            self.certificationAuthority = certificationAuthority
        }
        
        if let certificationURL = aDecoder.decodeObject(forKey: "certificationURL") as? String {
            
            self.certificationURL = certificationURL
        }
        
        if let certificationImage = aDecoder.decodeObject(forKey: "certificationImage") as? String {
            
            self.certificationImage = certificationImage
        }
        
        if let certificationDateFrom = aDecoder.decodeObject(forKey: "certificationDateFrom") as? String {
            
            self.certificationDateFrom = certificationDateFrom
        }
        
        
        if let certificationDateTo = aDecoder.decodeObject(forKey: "certificationDateTo") as? String {
            
            self.certificationDateTo = certificationDateTo
        }
        
        
        if let certificationLicenseNo = aDecoder.decodeObject(forKey: "certificationLicenseNo") as? String {
            
            self.certificationLicenseNo = certificationLicenseNo
        }
        
        if let expireFlag = aDecoder.decodeObject(forKey: "expireFlag") as? String {
            
            self.expireFlag = expireFlag
        }
        
        if let approvedFlag = aDecoder.decodeObject(forKey: "approvedFlag") as? String {
            
            self.approvedFlag = approvedFlag
        }
        
        self.id = aDecoder.decodeInteger(forKey: "id")
        
    }
    
    
    
    
}
