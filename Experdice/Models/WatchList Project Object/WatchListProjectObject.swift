//
//  WatchListProjectObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 10/18/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation


class WatchListProjectObject:NSObject {
    
    var expertId:Int?,projectId:Int?,id:Int?
    
    override init() {
        
        
    }
    
    init(id:Int!,expertId:Int!,projectId:Int!) {
        
        self.expertId = expertId
        self.projectId = projectId
        self.id = id
        
    }
    
}
