//
//  ProjectBidsObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 11/19/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation


class ProjectBidsObject: NSObject {
    
  
    var bidId:String?,projectName:String?,expertInfo:ExpertInfo?,bidAmount:String?,bidStatus:String?,date:String?

    override init() {
        
    }
    
    init(bidId:String!,projectName:String!,expertInfo:ExpertInfo!,bidAmount:String!,bidStatus:String!,date:String!) {
        
        self.bidId = bidId
        self.projectName = projectName
        self.expertInfo = expertInfo
        self.bidAmount = bidAmount
        self.bidStatus = bidStatus
        self.date = date
    }
    
    
    
    
    
    
    
}
