//
//  Profile Settings Object.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/16/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit


class ProfileSettingsObject {
    
    enum SettingType {
        
        case aboutMe
        case basicInfo
        case contactDetials
        case expiernce
        case education
        case certification
        case bio
        case basicInformation
        case contactDetails
        case founded
        case type
        case specialties
        case none
    }
    
    
    var title:String? , settingType:SettingType = SettingType.none , profileImage:String?
    
    init(title:String!, settingType:SettingType , profileImage:String!) {
        
        self.title = title
        self.profileImage = profileImage
        self.settingType = settingType
        
        
    }
    
    
    
}
