//
//  BadgeObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit



class EducationObject:NSObject , NSCoding {
    
    var  id:Int? ,
    educationImage:String?,
    educationSchoolName:String? ,
    educationCountry:String? ,
    educationCity:String? ,
    educationDegreeId:String? ,
    educationFieldOfStudy:String? ,
    educationDegree:String? ,
    educationCompletionOn:String?,
    gradeId:Int?,
    cityId:Int?,
    countryId:Int?
   
    
    override init() {
        
    }
    
    
    init(id:Int!,
         educationImage:String!,
         educationSchoolName:String! ,
         educationCountry:String!,
         educationCity:String! ,
         educationDegreeId:String! ,
         educationFieldOfStudy:String! ,
         educationDegree:String! ,
         educationCompletionOn:String!,
         gradeId:Int!,
         cityId:Int!,
         countryId:Int!){
        
        self.id = id
        self.educationImage = educationImage
        self.educationSchoolName = educationSchoolName
        self.educationCountry = educationCountry
        self.educationCity = educationCity
        self.educationDegreeId = educationDegreeId
        self.educationFieldOfStudy = educationFieldOfStudy
        self.educationDegree = educationDegree
        self.educationCompletionOn = educationCompletionOn
        self.gradeId = gradeId
        self.cityId = cityId
        self.countryId = countryId
        
        
        
        
    }
    
    
    func encode(with aCoder: NSCoder) {
        
        if let id = id {
            
            aCoder.encode(id, forKey: "id")
        }
        
        if let educationImage = educationImage {
            
            aCoder.encode(educationImage, forKey: "educationImage")
        }
        
        if let educationSchoolName = educationSchoolName {
            
            aCoder.encode(educationSchoolName, forKey: "educationSchoolName")
        }
        
        if let educationCountry = educationCountry {
            
            aCoder.encode(educationCountry, forKey: "educationCountry")
        }
        
        
        if let educationCity = educationCity {
            
            aCoder.encode(educationCity, forKey: "educationCity")
        }
        
        if let educationDegreeId = educationDegreeId {
            
            aCoder.encode(educationDegreeId, forKey: "educationDegreeId")
        }
        
        if let educationFieldOfStudy = educationFieldOfStudy {
            
            aCoder.encode(educationFieldOfStudy, forKey: "educationFieldOfStudy")
        }
        
        if let educationDegree = educationDegree {
            
            aCoder.encode(educationDegree, forKey: "educationDegree")
        }
        
        
        if let educationCompletionOn = educationCompletionOn {
            
            aCoder.encode(educationCompletionOn, forKey: "educationCompletionOn")
        }
        
        if let cityId = cityId {
            
            aCoder.encode(cityId, forKey: "cityId")
        }
        
        if let countryId = countryId {
            
            aCoder.encode(countryId, forKey: "countryId")
        }
        
        if let gradeId = gradeId {
            
            aCoder.encode(gradeId, forKey: "gradeId")
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.id = aDecoder.decodeInteger(forKey: "id")
        self.cityId = aDecoder.decodeInteger(forKey: "cityId")
        self.countryId = aDecoder.decodeInteger(forKey:"countryId")
        self.gradeId = aDecoder.decodeInteger(forKey: "gradeId")
        //self.educationDegreeId = aDecoder.decodeInteger(forKey: "educationDegreeId")

        
        if let educationImage = aDecoder.decodeObject(forKey: "educationImage") as? String{
            
            self.educationImage = educationImage
        }
        
        if let educationSchoolName = aDecoder.decodeObject(forKey: "educationSchoolName") as? String{
            
            self.educationSchoolName = educationSchoolName
        }
        
        if let educationFieldOfStudy = aDecoder.decodeObject(forKey: "educationFieldOfStudy") as? String{
            
            self.educationFieldOfStudy = educationFieldOfStudy
        }
        
        if let educationDegree = aDecoder.decodeObject(forKey: "educationDegree") as? String{
            
            self.educationDegree = educationDegree
        }
        
        if let educationCity = aDecoder.decodeObject(forKey: "educationCity") as? String{
            
            self.educationCity = educationCity
        }
        
        if let educationCountry = aDecoder.decodeObject(forKey: "educationCountry") as? String{
            
            self.educationCountry = educationCountry
        }
        if let educationCompletionOn = aDecoder.decodeObject(forKey: "educationCompletionOn") as? String{
            
            self.educationCompletionOn = educationCompletionOn
        }
        
        if let educationDegreeId = aDecoder.decodeObject(forKey: "educationDegreeId") as? String{

            self.educationDegreeId = educationDegreeId
        }
        
        
        
        
    }
    
    
    
}
