//
//  BadgeObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit



class AverageRatingObject:NSObject,NSCoding {
    
    
    var id:Int?,
    averageRole:[RatingRoleObject]?,
    averageTotal:String?
    
    

    
    init(id:Int!,
         averageRole:[RatingRoleObject]!,
         averageTotal:String!){
    
        self.id = id
        self.averageRole = averageRole
        self.averageTotal = averageTotal
        
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let averageRole = averageRole {
            
            aCoder.encode(averageRole, forKey: "averageRole")
        }
        
        if let averageTotal = averageTotal {
            
            aCoder.encode(averageTotal, forKey: "averageTotal")
        }
        
        if let userID = id {
            
            aCoder.encode(userID, forKey: "userId")
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        
        if let averageRole = aDecoder.decodeObject(forKey: "averageRole") as? [RatingRoleObject]{
            
            self.averageRole = averageRole
        }
        
        if let averageTotal = aDecoder.decodeObject(forKey: "averageTotal") as? String{
            
            self.averageTotal = averageTotal
        }
        
        self.id = aDecoder.decodeInteger(forKey: "userId")
   
    }
    
}
