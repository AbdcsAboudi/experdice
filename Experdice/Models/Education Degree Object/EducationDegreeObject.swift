//
//  File.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation



class EducationDegreeObject:NSObject {
    
    var id:Int?,degreeName:String?,descriptionText:String?
    
    
    init(id:Int!,degreeName:String!,descriptionText:String!) {
        self.degreeName = degreeName
        self.id = id
        self.descriptionText = descriptionText
    }
    
    override init() {
        
    }
    
    
}
