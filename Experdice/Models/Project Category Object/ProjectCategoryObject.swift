//
//  ProjectCategoryObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 10/16/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation




class ProjectCategoryObject:NSObject {
   
    var id:Int?,name:String?,projectTypeId:Int?
    
    override init() {}
    
    init(id:Int!,name:String!,projectTypeId:Int!){
        
        self.id = id
        self.name = name
        self.projectTypeId = projectTypeId
    }
}

