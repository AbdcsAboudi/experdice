//
//  RatingRoleObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit


class RatingRoleObject:NSObject,NSCoding {
    
    var roleTitle:String? , roleValue:Int?
    
    
    init(roleTitle:String! , roleValue:Int!){
        
        self.roleTitle = roleTitle
        self.roleValue = roleValue
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let roleTitle = roleTitle {
            
            aCoder.encode(roleTitle, forKey: "roleTitle")
        }
        if let roleValue = roleValue {
            
            aCoder.encode(roleValue, forKey: "roleValue")
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        if let roleTitle =  aDecoder.decodeObject(forKey: "roleTitle") as? String {
            
            self.roleTitle = roleTitle
        }
        
        
        let roleValue =  aDecoder.decodeInteger(forKey: "roleValue")
        self.roleValue = roleValue
        
        
        
        
    }
    
    
    
}
