//
//  GenderObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 10/22/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation

class GenderObject:NSObject {
    
    var id:Int?,genderName:String?
    
    override init() {
        
        
    }
    
    init(id:Int!,genderName:String!) {
        
        self.id = id
        self.genderName = genderName
        
    }
}
