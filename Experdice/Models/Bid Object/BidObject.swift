//
//  BidObject.swift
//  Experdice
//
//  Created by A.Aboudi on 8/3/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit

 class BidObject: NSObject {

    public enum BidStatus : Int {
        
        case accepted
        case pending
        case rejected
        
    }

    var projectName = ""
    var bidStatus = BidStatus.accepted
    var bidLocation = LocationObject(location: "", longitude: "", latitude: "")
    var bidDate = Date()
    var bidAmount = 0.0
    var bidCurrency = "JOD"

    init(projectName:String ,bidStatus:BidStatus, bidLocation:LocationObject, bidDate:Date, bidAmount:Double, bidCurrency:String) {
        
        self.projectName = projectName
        self.bidStatus = bidStatus
        self.bidLocation = bidLocation
        self.bidDate = bidDate
        self.bidAmount = bidAmount
        self.bidCurrency = bidCurrency

    }
    
    override init() {
        
    }

}
