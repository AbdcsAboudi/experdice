//
//  BadgeObject.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/13/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit



class BadgeObject:NSObject , NSCoding {
    
    var experdiceBadgeId:Int? , badgeName:String? , badgeDescription:String?
    
    
    init(experdiceBadgeId:Int!,badgeName:String!,badgeDescription:String!){
        
        self.experdiceBadgeId = experdiceBadgeId
        self.badgeName = badgeName
        self.badgeDescription = badgeDescription
        
        
        
    }
    
    override init() {
        
        
    }
    
    
    func encode(with aCoder: NSCoder) {
    
        if let experdiceBadgeId = experdiceBadgeId {
            
            aCoder.encode(experdiceBadgeId, forKey: "experdiceBadgeId")
        }
        
        if let badgeName = badgeName {
            
            aCoder.encode(badgeName, forKey: "badgeName")
        }
        
        if let badgeDescription = badgeDescription {
            
            aCoder.encode(badgeDescription, forKey: "badgeDescription")
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        if let badgeName = aDecoder.decodeObject(forKey: "badgeName") as? String {
            
            self.badgeName = badgeName
        }
        
        if let badgeDescription = aDecoder.decodeObject(forKey: "badgeDescription") as? String {
            
            self.badgeDescription = badgeDescription
        }
        
        self.experdiceBadgeId = aDecoder.decodeInteger(forKey: "experdiceBadgeId")
    }
    
    
    
    
}
