//
//  FAQSectionHeaderView.swift
//  7sWorld
//
//  Created by A.Aboudi on 5/2/17.
//  Copyright © 2017 DreamTechs. All rights reserved.
//

import UIKit

class FAQSectionHeaderView: UITableViewHeaderFooterView {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var cellExpandButton: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
