//
//  LaunchViewController.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 1/16/18.
//  Copyright © 2018 DreamTech. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if DTCredentialManager.isLoggedIn() {
            performSegue(withIdentifier: "signInSuccess", sender: self)
        }else{
            performSegue(withIdentifier: "TutorialViewController", sender: self)
        }
    }
    

  @IBAction func unwindToLoginViewController(segue:UIStoryboardSegue) { }

}
