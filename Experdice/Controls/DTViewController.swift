//
//  DTViewController.swift
//  Enatni
//
//  Created by Saad Basha on 10/10/16.
//  Copyright © 2016 Saad Basha. All rights reserved.
//

import UIKit
import SWRevealViewController

class DTViewController: UIViewController {
    
    //AS: DTViewController is the front of SWRevealViewController
    
    var needSideBar = true
    var needSettingButton = false
    var needEditButton = false
    
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
         // useLocalaizedBackButton()
        //alignInputViewsForLanguage(view: self.view)
 
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(true)
        
        setSideMenuButton()
        setupStyleForNavigationBar()
        setProfileSettingsButton()
  
    }
    
    //MARK:- Helping Methods
    
    func setSideMenuButton(){
        
        if needSideBar {
            let button = UIButton(type: .custom)
            button.setImage(#imageLiteral(resourceName: "menuButton"), for: .normal)
            button.addTarget(self, action: #selector(showSideMenuSelector), for: .touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
            let barButton = UIBarButtonItem(customView: button)
            navigationItem.leftBarButtonItem = barButton
        }else{
           
        }
      
    }
    
    func cancelSelctor(){
        dismiss(animated: true, completion: nil)
    }
    
    func setProfileSettingsButton(){
        
        if needSettingButton {
            
            let button = UIButton(type: .custom)
            button.setImage(#imageLiteral(resourceName: "setting_icon"), for: .normal)
            button.imageView?.contentMode = .scaleAspectFit
            button.addTarget(self, action: #selector(showProfileSettingsSelector), for: .touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
            let barButton = UIBarButtonItem(customView: button)
            navigationItem.rightBarButtonItem = barButton
           
        }
   
    }
    
   
   
    //AS : set style for all navigationBar in app
    func setupStyleForNavigationBar(){
        
        //AS : To set the navigationBar background color ... Blue Color
        self.navigationController?.navigationBar.barTintColor = ColorManager.sharedInstance.getNavigationColor()
        
        
        //AS : To set the back button nil title
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        
        //AS : To set the navigationBar title text color
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
    }
    
    // AS: Setup side menu
    func showSideMenuSelector(){
        revealViewController()?.revealToggle(animated: true)
    }
    
    func showProfileSettingsSelector(){

        if UserObject.getUserType() == .business {
            
          self.performSegue(withIdentifier: "profileBusinessSettingsSegue", sender: self)
            
        }else{
            
            self.performSegue(withIdentifier: "profileSettingsSegue", sender: self)

        }
        
    }
   
    
    
    
    
}


    
