//  DTtextField.swift
//  customizeTextField
//
//  Created by Yousef ALselawe on 7/20/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit


class DTtextField : UITextField , UIPickerViewDelegate , UIPickerViewDataSource {
    
    var color:UIColor?
    var placeHolderText = ""
    var selectdIndex = 0
    var domainOptions = [DTDomainObject]()
    var pickerView = UIPickerView()
    var sectionIndex = 0
    var multipleFirstTextFieldTag = -1
    var multipleSecondTextFieldTag = -1
    
    var textFieldType = TextFieldType.email {
        
        didSet{
            
            switch textFieldType {
                
            case .email:
                keyboardType = UIKeyboardType.emailAddress
                break
                
            case .password:
                keyboardType = UIKeyboardType.emailAddress
                break
                
            case .phoneNamber :
                keyboardType = UIKeyboardType.numberPad
                break
                
            case .firstName :
                keyboardType = UIKeyboardType.default
                break
                
            case .birthDay :
                setupDatePicker()
                break
                
            case .domain:
                setupCustomDomains()
                break
                
            case .string:
                keyboardType = UIKeyboardType.default
                break
                
            }
        }
        
    }
    
    override func awakeFromNib() {
        
        self.addTarget(self, action: #selector(textFieldDidChangeSelector), for: .editingChanged)
        pickerView.reloadAllComponents()
        
    }
    
    // MARK: - Helping Methods
    
    func setupDatePicker() {
        
        //YA : To SetUp DatePicke
        let datepicker = UIDatePicker()
        
        datepicker.datePickerMode = UIDatePickerMode.date
        datepicker.addTarget(self,action: #selector(self.datepickerValueChange(sender:)), for: UIControlEvents.valueChanged)
        
        self.inputView = datepicker
        
    }
    
    func datepickerValueChange(sender : UIDatePicker){
        
        // YA : Datepicker Value Change
        let formatter = DateFormatter()
        
        formatter.dateStyle = DateFormatter.Style.medium
        formatter.timeStyle = DateFormatter.Style.none
        formatter.dateFormat = "yyyy-MM-dd"
        self.text = formatter.string(from: sender.date)
    }
    
    func setupCustomDomains() {
        
        //MI: setup the wheel picker with passed domains
        
        
        //MI: to discuss if best practice to make this textfield the delegate or the VC owner. Another option is to keep a reference of the seleted domain in this class. (to be discussed)
        pickerView.dataSource = self
        pickerView.delegate = self
        
        self.inputView = pickerView
    }
    
    func addErrorIndicator() {
        
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "error_icon"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 10)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 10), y: CGFloat(5), width: CGFloat(15), height: CGFloat(15))
        self.rightView = button
        self.rightViewMode = .always
        
    }
    
    func removeErrorIndicator() {
        
        self.rightView = UIView()
        
    }
    
    //MARK: - Action Methods
    
    func UIColorFromRGB()-> UIColor {
        
        // YA: Change Background Color
        color = UIColor.gray
        return color!
        
    }
    
    func textFieldDidChangeSelector() {
        
        //YA : TextField Did Change Selector
        switch textFieldType {
            
        case .email:
            
            if DTValidationManager.isValidEmail(text: text){
                
                removeErrorIndicator()
                
            }else{
                
                addErrorIndicator()
                
            }
            
            break
            
        case .firstName:
            
            if DTValidationManager.isValidName(text: text){
                
                removeErrorIndicator()
            }else{
                addErrorIndicator()
            }
            break
            
        case .phoneNamber:
            if DTValidationManager.isValidNumber(text: text){
                removeErrorIndicator()
            }else{
                addErrorIndicator()
            }
            break
            
        case .string:
            
            if DTValidationManager.isValidName(text: text){
                removeErrorIndicator()
            }else{
                addErrorIndicator()
            }
            break
            
        case .password:
            if DTValidationManager.isValidPassword(text: text){
                removeErrorIndicator()
            }else{
                addErrorIndicator()
            }
            break
            
        default :
            print("dcd")
            break
        }
    }
    
    //MARK: UIPickerView Delegate && DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return domainOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return domainOptions[row].display
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if !domainOptions.isEmpty{
            self.text = domainOptions[row].display
            self.selectdIndex = row
        }
    }
    
    
    
    
    
    
    
    
}



