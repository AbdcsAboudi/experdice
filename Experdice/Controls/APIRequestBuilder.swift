//
//  APIRequestBuilder.swift
//
//
//  Created by Saad Basha on 1/16/17.
//  Copyright © 2017 DreamTechs. All rights reserved.
//

import UIKit

struct ACURL {
    
    
    static let getDegreesURL                = "Lockups/GetDegrees"
    static let getCountriesURL              = "Lockups/GetCountries"
    static let getExpertURL                 = "Users/Experts/GetExpert"
    static let getClientURL                 = "Users/Clients/GetClient"
    static let getWatchlistURL              = "Users/Experts/GetWatchList"
    static let getCityURL                   = "Lockups/GetCities"
    static let getCountryURL                = "Lockups/GetCountries"
    static let getStatesURL                 = "Lockups/GetStates"
    static let signInURL                    = "Users/SignIn"
    static let emailExist                   = "Users/GetEmailExist"
    static let signUpURL                    = "Users/Experts/SignUp"
    static let getBusinessTypes             = "Lockups/GetBusinessTypes"
    static let signUpBusiness               = "Users/Clients/SignUp"
    static let contactDetails               = "Users/GetContactDetails"
    static let getProjectList               = "Projects/GetProjects"
    static let getLanguagesURL              = "Lockups/GetLanguages"
    static let getPaymentMethodsURL         = "Lockups/GetPaymentMethods"
    static let getProfessionalSenioritieURL = "Lockups/GetProfessionalSeniorities"
    static let createProjectURL             = "Projects/EditProject"
    static let addWatchListURL              = "Users/Experts/AddWatchList"
    static let editBankAccInfoURL           = "Users/Experts/EditBankAccInfo"
    static let deleteWatchListURL           = "Users/Experts/DeleteWatchList"
    static let editContactDetailsURL        = "Users/EditContactDetails"
    static let editBasicInfoURL             = "Users/Experts/EditBasicInfo"
    static let getProjectBidsURL            = "Users/Experts/GetProjectBids"
    static let getProjectDetailsURL         = "Projects/GetProject"
    static let deleteEducationURL           = "Users/Experts/DeleteEducation"
    static let editEducationURL             = "Users/Experts/EditEducation"
    static let getEducationURL              = "Users/Experts/GetEducation"
    static let editImageURL                 = "Users/EditImage"
    static let bidOnProjectURL              = "Projects/AddProjectBid"
    static let editExperienceURL            = "Users/Experts/EditExperience"
    static let getExperienceURL             = "Users/Experts/GetExperience"
    static let deleteExperienceURL          = "Users/Experts/DeleteExperience"
    static let getGradesURL                 = "Lockups/GetGrades"
    static let changePasswordURL            = "Users/ChangePassword"
    static let editAboutMeURL               = "Users/EditAboutMe"
    static let updateDeviceTokenURL         = "updateDeviceToken"
    static let deleteDeviceTokenURL         = "deleteDeviceToken"
    static let acceptProjectBidURL          = "Projects/AcceptProjectBid"
    static let rejectProjectBidURL          = "Projects/RejectProjectBid"
    static let contactUs                    = "Users/ContactUs"
   
    
}

public enum ApiRequestType : Int {
    
    case get
    case post
    case multipartPost
    case multiPart
    case put
    case delete
    case image
    
    
}

public enum ApiRequestID : Int {
    
    case getCountries
    case getExpert
    case getClient
    case getWatchlist
    case getCity
    case getCountry
    case getProjectBids
    case getStates
    case signIn
    case emailExist
    case signUp
    case businessTypes
    case signUpBuisiness
    case contactDetails
    case getProjectList
    case getLanguages
    case getPaymentMethods
    case getProfessionalSeniorities
    case createOrEditProject
    case addWatchList
    case editBankAccInfo
    case getDegrees
    case deleteWatchlist
    case editContactDetails
    case editBasicInfo
    case deleteEducation
    case deleteExperience
    case getProjectDetails
    case editEducation
    case editImage
    case bidOnProject
    case editOrAddExperience
    case getExperience
    case getEducation
    case getGrades
    case changePassword
    case editAboutMe
    case updateDeviceToken
    case deleteDeviceToken
    case acceptProjectBid
    case rejectProjectBid
    case contactUs
    
}

class APIRequestBuilder {
    
    
    static func createRequestObjectForRequestId(requestId:ApiRequestID,requestParams:Any?)->APIRequestObject{
    
        switch requestId {
            
        case .contactUs:
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.acceptProjectBidURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .acceptProjectBid:
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.acceptProjectBidURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .rejectProjectBid:
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.rejectProjectBidURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .deleteDeviceToken:
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.deleteDeviceTokenURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .updateDeviceToken:
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.updateDeviceTokenURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .editAboutMe:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.editAboutMeURL)", entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .changePassword:
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.changePasswordURL)", entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .editBasicInfo:
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.editBasicInfoURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .getGrades:
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.getGradesURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .getEducation:
            
            let parms = requestParams as! [String:Any]
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.getEducationURL)/\(parms["ExpertId"] as? Int ?? 0)/\(parms["EducationID"] as? Int ?? 0)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .deleteExperience:
            
            var experienceId = 0
            
            if let parm = requestParams as? [String:Any]{
                
                experienceId = parm["ExperienceID"] as! Int
            }
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .delete, requestURL: "\(ACURL.deleteExperienceURL)/\(experienceId)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .getExperience:
            
            let parms = requestParams as! [String:Any]
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.getExperienceURL)/\(parms["ExpertId"] as? Int ?? 0)/\(parms["ExperienceID"] as? Int ?? 0)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .editOrAddExperience:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.editExperienceURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .bidOnProject:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.bidOnProjectURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .editImage:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.editImageURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .editEducation:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: "\(ACURL.editEducationURL)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .deleteEducation:
            
            var educationId = 0
            
            if let parm = requestParams as? [String:Any]{
                
                educationId = parm["EducationID"] as! Int
            }
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .delete, requestURL: "\(ACURL.deleteEducationURL)/\(educationId)", entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .getProjectDetails:
            
            var projectID = 0
          
            if let parm = requestParams as? [String:Any] {
                
                projectID = parm["projectID"] as! Int
            }
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ("\(ACURL.getProjectDetailsURL)/\(projectID)"), entityTableName: EntityTableNames.none)
            
            return object
            
        case .getProjectBids:
            
            var expertId = 0
            var statusId = 0
            if let parm = requestParams as? [String:Any] {
            
                expertId = parm["expertId"] as! Int
                statusId = parm["statusId"] as! Int
                
            }
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ("\(ACURL.getProjectBidsURL)/\(expertId)/\(statusId)"), entityTableName: EntityTableNames.none)
            
            return object
            
        case .editBasicInfo:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: ACURL.editBasicInfoURL, entityTableName: EntityTableNames.none)
            
            return object
            
        case .editContactDetails:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: ACURL.editContactDetailsURL, entityTableName: EntityTableNames.none)
            
            return object
            
        case .getCountries:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ACURL.getCountriesURL,entityTableName:EntityTableNames.none)
            
            return object
            
        case .deleteWatchlist:
            
            let parm = requestParams as! [String:Any]
            
            let expertId = parm["expertId"] as? Int ?? 0
            let projectId = parm["projectId"] as? Int ?? 0
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .delete, requestURL:("\(ACURL.deleteWatchListURL)/\(expertId)/\(projectId)"),entityTableName:EntityTableNames.none)
            
            return object
            
        case .getDegrees:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ACURL.getDegreesURL,entityTableName:EntityTableNames.none)
            
            return object
            
        case .getExpert:
            
            let userId = requestParams as? Int ?? -1
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.getExpertURL)/\(userId)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .getClient:
            
            let userId = requestParams as? Int ?? -1
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.getClientURL)/\(userId)", entityTableName: EntityTableNames.none)

            return object
            
            
        case .getWatchlist:
            
            let expertId = requestParams as? Int ?? -1
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.getWatchlistURL)/\(expertId)", entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .getCity:
            
            let countryID = requestParams as? Int ?? -1

            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL:"\(ACURL.getCityURL)/\(countryID)", entityTableName: EntityTableNames.none)
            
            return object
            
    
        case .getCountry:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ACURL.getCountryURL, entityTableName: EntityTableNames.none)
            
            return object
            
        case .getProfessionalSeniorities:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ACURL.getProfessionalSenioritieURL, entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .getStates:
            
            let cityID = requestParams as? Int ?? -1
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.getStatesURL)/\(cityID)", entityTableName: EntityTableNames.none)
            
                return object
            
        case .signIn:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: ACURL.signInURL, entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .editBankAccInfo:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: ACURL.editBankAccInfoURL, entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .emailExist:
            
            let params = requestParams as? [String: String]
            let email = params?["email"] ?? ""
            
            
            let object = APIRequestObject(requestId: requestId, requestParameters: nil, requestType: .get, requestURL: "\(ACURL.emailExist)/\(email)", entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .addWatchList:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: ACURL.addWatchListURL, entityTableName: EntityTableNames.none)
            
            return object
            
      
        case .signUp:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: ACURL.signUpURL, entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .signUpBuisiness:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .multipartPost, requestURL: ACURL.signUpBusiness, entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .businessTypes:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ACURL.getBusinessTypes , entityTableName: EntityTableNames.none)
            
            return object
            
        case .getPaymentMethods:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ACURL.getPaymentMethodsURL , entityTableName: EntityTableNames.none)
            
            return object
            
            
        case .contactDetails:
            
            let userId = requestParams as? Int ?? 0
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.contactDetails)/\(userId)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .getProjectList:
            
            var clientId = "0",
            industryID = "0" ,
            budget = "0",
            cityID = "0",
            dateFrom =  "0",
            dateTo = "0",
            statusID = "0",
            onlyForClient =  "0" ,
            pageSize = "0",
            pageNo = "0",
            expertId = 0
            
            if let parms = requestParams as? [String:Any]{
             
                clientId = parms["clientId"] as? String ?? ""
                industryID = parms["industryID"] as? String ?? ""
                budget = parms["budget"] as? String ?? ""
                cityID = parms["cityID"] as? String ?? ""
                dateFrom = parms["dateFrom"] as? String ?? ""
                dateTo = parms["dateTo"] as? String ?? ""
                statusID = parms["statusID"] as? String ?? ""
                onlyForClient = parms["onlyForClient"] as? String ?? ""
                pageSize = parms["pageSize"] as? String ?? "0"
                pageNo = parms["pageNo"] as? String ?? "0"
                expertId = parms["ExpertID"] as? Int ?? 0
               
            }
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: "\(ACURL.getProjectList)/\(clientId)/\(industryID)/\(budget)/\(cityID)/\(dateFrom)/\(dateTo)/\(statusID)/\(onlyForClient)/\(expertId)/\(pageSize)/\(pageNo)", entityTableName: EntityTableNames.none)
            
            return object
            
        case .getLanguages:
            
            let object = APIRequestObject(requestId: requestId, requestParameters: requestParams, requestType: .get, requestURL: ACURL.getLanguagesURL, entityTableName: EntityTableNames.none)
            
            return object
            
        case .createOrEditProject:
          
            //if let parms = requestParams as? [String:Any]{
                
//                var clientID = 0,
//                projectId = 0 ,
//                title = "0",
//                projectTypeId = 0,
//                objective =  "0",
//                openUntilDate = "0",
//                goalAchievements = "0",
//                goalDescription = "0",
//                goalNote = "0",
//                actualBeginDate =  "0" ,
//                actualEndDate = "0",
//                budget = "0",
//                budgetRate = 0,
//                sessionDuration = "0",
//                sessionLanguageId = 0,
//                countryId = 0,
//                cityId = 0,
//                preference = "0",
//                contentLanguageId = 0,
//                perfTrainerGenderId = 0,
//                expectedNumber = 0,
//                businessIndustryId = 0,
//                proSeniorityId = 0,
//                paymentMethodId = 0,
//                projectCategoryId = 0,
//                sessionFrequancyRate = "0",
//                experienceYears = "0",
//                candidateDescription = "0"
//
//                clientID = parms["clientId"] as! Int
//                projectId = parms["projectId"] as? Int ?? 0
//                title = parms["title"] as? String ?? "0"
//                projectTypeId = parms["projectTypeId"] as? Int ?? 0
//                objective = parms["objective"] as? String ?? "0"
//                openUntilDate = parms["openUntilDate"] as? String ?? "0"
//                goalAchievements = parms["GOAL_ACHIEVEMENTS"] as? String ?? "0"
//                goalDescription = parms["goalDescription"] as? String ?? "0"
//                goalNote = parms["goalNote"] as? String ?? "0"
//                actualBeginDate = parms["actualBeginDate"] as? String ?? "0"
//                actualEndDate = parms["actualEndDate"] as? String ?? "0"
//                budget = parms["budget"] as? String ?? "0"
//                budgetRate = parms["budgetRate"] as? Int ?? 0
//                sessionDuration = parms["sessionDuration"] as? String ?? "0"
//                sessionLanguageId = parms["sessionLanguageId"] as? Int ?? 0
//                countryId = parms["countryId"]  as? Int ?? 0
//                cityId = parms["cityId"]  as? Int ?? 0
//                preference = parms["preference"] as? String ?? "0"
//                contentLanguageId = parms["contentLanguageId"]  as? Int ?? 0
//                perfTrainerGenderId = parms["perfTrainerGenderId"] as? Int ?? 0
//                expectedNumber = parms["expectedNumber"] as? Int ?? 0
//                businessIndustryId = parms["businessIndustryId"] as? Int ?? 0
//                proSeniorityId = parms["proSeniorityId"]  as? Int ?? 0
//                paymentMethodId = parms["paymentMethodId"]  as? Int ?? 0
//                projectCategoryId = parms["projectCategoryId"]  as? Int ?? 0
//                sessionFrequancyRate = parms["sessionFrequancyRate"] as? String ?? "0"
//                experienceYears = parms["experienceYears"] as? String ?? "0"
//                candidateDescription = parms["candidateDescription"] as? String ?? "0"
//
//
//
//
//            }
//
////            let pth = "\(ACURL.createProjectURL)/\(clientID)/\(projectId)/\(title)/\(projectTypeId)/\(objective)/\(openUntilDate)/\(goalAchievements)/\(goalDescription)/\(goalNote)/\(actualBeginDate)/\(actualEndDate)/\(budget)/\(sessionDuration)/\(sessionLanguageId)/\(contentLanguageId)/\(perfTrainerGenderId)/\(expectedNumber)/\(businessIndustryId)/\(projectId)/\(countryId)/\(cityId)/\(preference)/\(proSeniorityId)/\(paymentMethodId)/\(projectCategoryId)/\(sessionFrequancyRate)/\(budgetRate)/\(experienceYears)/\(candidateDescription)".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
            
            let object = APIRequestObject(requestId: requestId,
                                          requestParameters: requestParams,
                                          requestType: .multipartPost,
                                          requestURL: ACURL.createProjectURL,entityTableName: EntityTableNames.none)
            
            return object
            
            
          
        }

    }

}
