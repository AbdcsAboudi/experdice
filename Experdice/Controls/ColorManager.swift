//
//  ColorManager.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/25/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit

public class ColorManager {
    
    public static let sharedInstance = ColorManager()

    func getNavigationColor()->UIColor{
        
        let navigationBarColor = DTHelper.sharedInstance.colorWithHexString("#006B88")
       
        return navigationBarColor
    }
    
    func getBordaerColorForSwitchView()->UIColor{
        
        let color = DTHelper.sharedInstance.colorWithHexString("#989898")
        
        return color
        
    }
    
    func getRounderBoardearColorForSelectionView()->UIColor{
        
        let boardearColor = DTHelper.sharedInstance.colorWithHexString("#5C5C5C")
        
        return boardearColor
        
    }
    func getNavigationBarItemColor()->UIColor{
        
        let navigationBarItemColor = DTHelper.sharedInstance.colorWithHexString("#F4D45E")
        
        return navigationBarItemColor
    }
    
    func getColorImageBorder()->UIColor{
        let color = DTHelper.sharedInstance.colorWithHexString("#006B88")
       
        return color
    }
    
    func getAttributeTextColor()->UIColor{
        
        let color = DTHelper.sharedInstance.colorWithHexString("#162E92")
        
        return color
        
    }
    
    func headerViewButtonColorSelected()->UIColor{
        
       return  DTHelper.sharedInstance.colorWithHexString("#006B88")
        
    }
    func headerViewTintColor()->UIColor{
        
        return UIColor.blue
    }
    func headerViewButtonDefault()->UIColor{
        
        return  DTHelper.sharedInstance.colorWithHexString("#132C34")

    }
    
    func getTextFiledTextColor()->UIColor{
        
        let color = DTHelper.sharedInstance.colorWithHexString("#006B88")
        
        return color
        
    }
  
    
    
}
