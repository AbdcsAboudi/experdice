//
//  DTLocationManager.swift
//
//
//  Created by Saad Basha on 12/7/16.
//  Copyright © 2016 Saad Basha. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import AddressBook

@objc protocol DTLocationManagerDelegate : NSObjectProtocol {
    
    @objc func didGetUserLocation(_ userLocation:CLLocation)
    @objc optional func didFailToGetUserLocation(withError error:NSError)
    @objc optional func didUpdateToLocation(_ newLocation:CLLocation,_ oldLocation:CLLocation)
    
}

/// Used to manager user location and authorization proccess
class DTLocationManager : NSObject,CLLocationManagerDelegate {
    
    static var sharedInstance = DTLocationManager()
    internal weak var delegate : DTLocationManagerDelegate?
    
    var locationManager :  CLLocationManager?
    var canGetLocation : Bool = false
    var locationServiceEnabled : Bool = false
    var requestingLocation : Bool = false
    var mostRecentLocation : CLLocation?
    
    
    //MARK: - life cycle
    
    /// Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
    private override init(){
        super.init()
               
        self.initiate()
        
        if self.didGetUserLocation(){
            
            self.mostRecentLocation = self.getSavedUserLocation()
            
            
        }else {
            
            self.mostRecentLocation = CLLocation(latitude: 24.7117, longitude: 46.7242)
        }
        
    }
    
    
    /// Setup Location settings for ones, its the gate to ask for permissions
    func initiate(){
        
        ///- initiate the location manager
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyKilometer
        //locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.distanceFilter = kCLDistanceFilterNone
        locationManager?.delegate = self
        
        _ = self.checkAndRequestAuthorizationAndSettings()
        
    }
    
    
    /// A step used to distroy the delegate and or the singleton
    func removeLocationDelegate(){
        
        locationManager?.delegate = self
        // not sure that this will destroy the old signleton
        DTLocationManager.sharedInstance = DTLocationManager()
        
    }

    
    //MARK: - CLLocation Authorizations
    
    func checkAndRequestAuthorizationAndSettings()->Bool{
        
        ///- get current authorization status, if yes, check if location is enabled, if not determined will request it
        self.canGetLocation = self.checkLocationAuthorization()
        
        if self.canGetLocation {
            
            self.locationServiceEnabled = self.checkLocationEnabled()
            return self.locationServiceEnabled;
        }
        
        return false
        
    }
    
    func checkLocationAuthorization()->Bool{
        
        var locationAuthorized = false;
        let currentAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch (currentAuthorizationStatus) {
            
        case CLAuthorizationStatus.notDetermined:
            
            locationManager?.requestWhenInUseAuthorization()
            locationAuthorized = false
            break;
            
        case CLAuthorizationStatus.restricted:
            
            locationAuthorized = false
            break;
            
            
        case CLAuthorizationStatus.denied:
            
            locationAuthorized = false
            break;
            
            
        case CLAuthorizationStatus.authorizedAlways:
            
            locationAuthorized = true
            break;
            
            
        case CLAuthorizationStatus.authorizedWhenInUse:
            
            locationAuthorized = true
            break;
            
        }
        
        return locationAuthorized;
        
    }
    
    func checkLocationEnabled()->Bool{
        
        let currentLocationServiceStatus = CLLocationManager.locationServicesEnabled()
        
        if (!currentLocationServiceStatus) {
            
            ///- show message to turn on location services
            let locationAlert = UIAlertController(title: "Warning", message: "Please turn on location services in settings", preferredStyle: UIAlertControllerStyle.alert)
            locationAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            
            self.topMostViewController().present(locationAlert, animated: true, completion: nil)
            
        }
        
        return currentLocationServiceStatus;
        
    }
    
    
    func locationManager(_ manager:CLLocationManager,didChangeAuthorization status:CLAuthorizationStatus)->Void{
        
        ///- update our current authorization status
        self.canGetLocation = self.checkLocationAuthorization()
        
        if self.canGetLocation {
            self.getUserLocation()
        }
        
    }
    
    
    //MARK: - CLLocationManager Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        ///- get last location in array
        let userCurrentLocation = locations[locations.count-1]
        
        //MI: handle it as needed
        self.mostRecentLocation = userCurrentLocation
        print("\(userCurrentLocation.coordinate.latitude) : \(userCurrentLocation.coordinate.longitude)")
        self.saveUserLocation(userLocation: userCurrentLocation)
        
        //MI: then call delegate if available
        if self.delegate != nil && self.delegate!.responds(to: #selector(DTLocationManagerDelegate.didGetUserLocation(_:))){
            self.delegate!.didGetUserLocation(userCurrentLocation)
        }
        
        //SB: Since now didUpdateToLocation is deprecated we can do the folllowing
        if self.delegate != nil && self.delegate!.responds(to: #selector(DTLocationManagerDelegate.didUpdateToLocation(_:_:))){
            self.delegate!.didUpdateToLocation!(locations.first!, locations.last!)
        }
        
        
        //MI: update flag
        self.requestingLocation = false;
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("[LM] \(error.localizedDescription)")
        
        if self.delegate != nil && self.delegate!.responds(to: #selector(DTLocationManagerDelegate.didFailToGetUserLocation(withError:))){
            self.delegate!.didFailToGetUserLocation!(withError: error as NSError)
        }
        
        self.requestingLocation = false
        
    }
    
    
    //MARK: - User Location
    
    func getUserLocation()->Void{
        
        if self.checkAndRequestAuthorizationAndSettings() && !self.requestingLocation {
            
            if #available(iOS 9.0, *) {
                locationManager?.requestLocation()
            } else {
                // Fallback on earlier versions
            }
            self.requestingLocation = true
            
        }
        
    }
    
    
    ///- for later use
    
    func startUpdating()->Void{
        
        locationManager?.startUpdatingLocation()
        
    }
    
    func stopUpdating()->Void{
        
        locationManager?.stopUpdatingLocation()
        
    }
    
    
    //MARK: - Helping Methods
    
    func didGetUserLocation()->Bool{
        
        let didGetUserLocation = UserDefaults.standard.bool(forKey: "didGetUserLocation")
        
        return didGetUserLocation;
        
    }
    
    func getSavedUserLocation()->CLLocation{
        
        if (self.didGetUserLocation()) {
            
            let latitude = UserDefaults.standard.object(forKey: "savedUserLocationLatitude") as? Double
            let longitude = UserDefaults.standard.object(forKey: "savedUserLocationLongitude") as? Double
            
            let userLocation = CLLocation(latitude: latitude!, longitude: longitude!)
            
            return userLocation
        }
        
        return CLLocation()
    }
    
    func saveUserLocation(userLocation:CLLocation)->Void{
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "didGetUserLocation")
        userDefaults.set(userLocation.coordinate.latitude, forKey: "savedUserLocationLatitude")
        userDefaults.set(userLocation.coordinate.longitude, forKey: "savedUserLocationLongitude")
        
    }
    
    
    func getUserDistanceTo(toLocation:CLLocationCoordinate2D)->Double{
        
        return self.getUserDistanceFrom(fromLocation: self.mostRecentLocation!.coordinate, to: toLocation)
        
    }
    
    func getUserDistanceFrom(fromLocation:CLLocationCoordinate2D,to toLocation:CLLocationCoordinate2D)->Double{
        
        let originalLocation = CLLocation(latitude: fromLocation.latitude, longitude: fromLocation.longitude)
        
        let destinationLocation = CLLocation(latitude: toLocation.latitude, longitude: toLocation.longitude)
        
        let distanceBetweenLocations = destinationLocation.distance(from: originalLocation)
        
        return distanceBetweenLocations
        
    }
    
    /// Search and find out the top most presented view controller in application
    ///
    /// - Returns: top UIViewController
    private func topMostViewController()->UIViewController{
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
            // topController should now be your topmost view controller
        }
        
        return UIViewController()
    }
    
    
    func reverseGeoCodeUserLocation(location: CLLocation, fillData: ((_ addrees: String, _ userInfo: NSDictionary) -> Void)? ){
        
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            
            if placemarks!.count > 0 {
                
                let pm = placemarks![0]
                let addressDictionary = pm.addressDictionary
                //                print(addressDictionary)
                
                var city = ""
                var street = ""
                var country = ""
                
                //++ check if city,country or street is nil
                if addressDictionary![String(kABPersonAddressCityKey)] != nil{
                    city = addressDictionary![String(kABPersonAddressCityKey)]! as! String
                    
                }
                if addressDictionary![String(kABPersonAddressStreetKey)] != nil{
                    street = addressDictionary![String(kABPersonAddressStreetKey)]! as! String
                }
                if pm.country != nil{
                    country = pm.country!
                }
                
            
                
//
                //let locale = NSLocale(localeIdentifier: "en_US")
               
                
               
                //AA : To save the values of users location in nuser defaults
                let address = "\(city) , \(country) , \(street)"
                
                fillData?(address, ["city": city, "country": country,"street" : street, "latitude": location.coordinate.latitude, "longitude": location.coordinate.longitude] )
                
            }
            else {
                
                //                MBProgressHUD.hide(for: self.view, animated: true)
                
                print("Problem with the data received from geocoder")
            }
            
            
        })
        
    }
    
    
}
