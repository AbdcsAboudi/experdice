//
//  DTChatBaseViewController.swift
//  DTChatMoudule
//
//  Created by DTAboudi on 11/22/16.
//  Copyright © 2016 DreamTechs. All rights reserved.
//

import UIKit
import SlackTextViewController

class DTChatBaseViewController: SLKTextViewController {

    var tableViewReference: UITableView!
    
    var hasHeader : Bool = false
    var enableAutoCompletion : Bool = false

    var cellHeight : CGFloat = 0.0
    var commentCellHeight : CGFloat = 0.0
    var headerCellHeight : CGFloat = 0.0
    var numberOfSections : NSInteger = 0
    var numberOfRows : NSInteger = 0
    var numberOfRowsInEachPage : NSInteger = 0

    var dataSourceArray : NSMutableArray = []

    //AA : Blocks
    var addNewCommentBlock : () -> (Void) = {}
    var loadMoreCommentsBlock : () -> (Void) = {}
    var receivedNewCommentBlock : () -> (Void) = {}
    var populateCommentCellBlock : (Int,UITableView) -> (Any) = {_ in UITableViewCell()}
    var populateHeaderCellBlock : (Int,UITableView) -> (Any) = {_ in UITableViewCell()}
    var populateLoadMoreCommentsCellBlock : (Int,UITableView) -> (Any) = {_ in UITableViewCell()}

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //AA : To setup comments tableView
        self.setupCommentsTableView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //AA : To add observer to detect when new comment notification received
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(getNewCommentWithId(notification:)),
            name: NSNotification.Name(rawValue: "getNewCommentFromPushNotification"),
            object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helping Methods
    
    /**
     * @abstract    Initiate comments tableView with the needed properties
     
     * @author      Abd Aboudi
     */
    func setupCommentsTableView(withNib nibFiles:UINib ...) {
       
        self.tableView?.separatorStyle = .none
        
        for nib in nibFiles {
        
            self.tableView?.register(nib, forCellReuseIdentifier: "\(nib.classForCoder)")
        
        }
        
//        self.tableView?.register(UINib(nibName: "CommentTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "CommentTableViewCell")
//        self.tableView?.register(UINib(nibName: "LoadMoreCommentsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "LoadMoreCommentsTableViewCell")
//        self.tableView?.register(UINib(nibName: "PostTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "PostTableViewCell")
        
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.estimatedRowHeight = 140
        
        //AA : To check if to enable the auto completion by (#,@) or no
        if (enableAutoCompletion){
            //AA : To detect prefix (#,@)
            self.registerPrefixes(forAutoCompletion: ["@","#"])
        }
       
        //AA : To check if the tableView contains header cell or no
        if (hasHeader){
            //AA : To rotate the tableView to its normal state
            self.tableView?.transform = CGAffineTransform(a: 1.0, b: 0.0, c: 0.0, d: 1.0, tx: 0.0, ty: 0.0)
            
            numberOfSections = numberOfSections + 1
            numberOfRows = dataSourceArray.count
        }else{
            numberOfRows = dataSourceArray.count
        }
        
    }
    
    /**
     * @abstract    To add comment locally without waiting the response
     
     * @author      Abd Aboudi
     */
    func addCommentLocally() {
        
        //AA : To add comment with the desired object
        let localCommentObject = CommentObject()
//        localCommentObject.commentOwnerName = "Abd"
//        localCommentObject.commentText = self.textView.text
        
        self.dataSourceArray.insert(localCommentObject, at: 0)
        numberOfRows = numberOfRows + 1
        
        self.tableView?.reloadData()

    }
    
    // MARK: - New Comment Notification Receiver Method
    
    /**
     * @abstract    To request get comment API after receiving new comment notification
     
     * @author      Abd Aboudi
     */
    func getNewCommentWithId(notification:NSDictionary) {
        
    }
    
    // MARK: - APIClient Methods Handling

    /**
     * @abstract    To call this method when the response of add new comment API returned succesfully
     
     * @author      Abd Aboudi
     */
    func addCommentSuccessfully(_ commentObject : CommentObject) {
        
    }
    
    /**
     * @abstract    To call this method when the response of add new comment API returned with failure (to remove the last object)
     
     * @author      Abd Aboudi
     */
    func addCommentFailedWithError(_ error : NSError, andCommentObject commentObject:CommentObject) {
        
        //AA : To remove the last added object
        dataSourceArray.removeLastObject()
        
    }
    

    // MARK: - UITableView Delegate & DataSource Methods
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (hasHeader){
            
            if (indexPath.section == 0){
                return headerCellHeight
            }else{
                return UITableViewAutomaticDimension
            }
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (hasHeader){
            
            if (section == 0){
                return 1
            }else{
                return numberOfRows
 
            }
            
        }else{
            return numberOfRows
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        //AA : To check if the tableView has header
        if (hasHeader){
            
            if (indexPath.section == 0){
                
                //AA : To populate the Header cell
                return populateHeaderCellBlock(indexPath.row,tableView) as! UITableViewCell
            }else{
                
//                if (indexPath.row == numberOfRowsInEachPage - 1){
//                    
//                    //AA : To populate the load more cell
//                    return populateLoadMoreCommentsCellBlock(indexPath.row,tableView) as! UITableViewCell
//                }else{
                
                    //AA : To populate the comment cell
                    return populateCommentCellBlock(indexPath.row,tableView) as! UITableViewCell
//                }
              

            }
        }else{
//            
//            if (indexPath.row == numberOfRowsInEachPage - 1){
//                
                  //AA : To populate the load more cell
//                return populateLoadMoreCommentsCellBlock(indexPath.row,tableView) as! UITableViewCell
//            }else{
            
                //AA : To populate the comment cell
                return populateCommentCellBlock(indexPath.row,tableView) as! UITableViewCell

//            }
            
        }
        
         }
    
    
    // MARK: - SLKTextViewController Delegates
    
    override func didChangeAutoCompletionPrefix(_ prefix: String, andWord word: String) {
        
    }
    
    // MARK: - Actions

    override func didPressRightButton(_ sender: Any?) {
        
        //AA : To add the new comment locally
        self.addCommentLocally()
        
        //AA : To refresh the comment textField
        self.textView.refreshFirstResponder()
        super.didPressRightButton(sender)
    }
    
    /**
     * @abstract    To call this method when load more comments button pressed (this button shows when the number of comments reached numberOfRowsInEachPage comment) to load the next page
     
     * @author      Abd Aboudi
     */
    func loadMoreComments() {
        
    }
    


}

class CommentObject {
    
}
