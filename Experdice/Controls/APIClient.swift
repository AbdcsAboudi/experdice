//
//  APIClient.swift
//
//
//  Created by Saad Basha on 1/12/17.
//  Copyright © 2017 Saad Basha. All rights reserved.
//

import UIKit
import AFNetworking
import AFNetworkActivityLogger

let bURL                             = "http://numidiajo.com/Experdice/API/"

class APIClient:AFHTTPSessionManager {
    
    var isDisplayingError : Bool = false
    
    
    //MARK: - Life Cycle
    
    init(baseURL url: URL!) {
        super.init(baseURL: url, sessionConfiguration: nil)
        
        //Requst
        self.requestSerializer = AFJSONRequestSerializer() as AFJSONRequestSerializer
        
        //Response
        self.responseSerializer = AFJSONResponseSerializer() as AFJSONResponseSerializer
        
        self.requestSerializer.httpMethodsEncodingParametersInURI =  ["POST", "GET", "PUT", "DELETE"]
        self.requestSerializer.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        
        //self.requestSerializer.setValue("text/plain", forHTTPHeaderField: "Content-Type")
        self.responseSerializer.acceptableContentTypes = NSSet(array: ["application/xml", "text/xml", "text/plain","application/json", "text/html"]) as? Set<String>
        self.requestSerializer.timeoutInterval = 25
        
        responseSerializer.acceptableStatusCodes = NSIndexSet(index: 200) as IndexSet
        
        #if DEBUG
            let consoleLogger = AFNetworkActivityConsoleLogger()
            consoleLogger.level = .AFLoggerLevelDebug
            AFNetworkActivityLogger.shared().addLogger(consoleLogger)
            AFNetworkActivityLogger.shared().startLogging()
        #endif
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    struct Static{
        
        static var instance: APIClient?
    }
    
    class var sharedInstance: APIClient{
        
        if Static.instance == nil
        {
            Static.instance = APIClient(baseURL: URL(string: bURL))
        }
        
        return Static.instance!
    }
    
    func dispose(){
        
        APIClient.Static.instance = nil
        print("Disposed Singleton instance")
        
        _ = APIClient.sharedInstance
        
    }
    
    
    
    private func includeTokenAndHeaderData(_ includeToken:Bool){
        
        requestSerializer.setValue("dreamtechs", forHTTPHeaderField: "PHP_AUTH_USER")
        requestSerializer.setValue("dreamtechs@2017*", forHTTPHeaderField: "PHP_AUTH_PW")
        
    }
    
    
    func makeApiRequest(_ requestObject:APIRequestObject,withBlock block: @escaping (_ response:Any?,_ sucess:Bool) -> (Void)){
        
        switch (requestObject.requestType) {
            
        case .get:
            
            //FIXME: check request and response serializer types like below
            
            self.getWithParameters(postData: requestObject.requestParameters , withURL: requestObject.requestURL, andRequestID: requestObject.requestId, withCompletionBlock: { (reponse) -> (Void) in
                
                block(reponse, true)
                
            }, withFailureBlock: { (error) -> (Void) in
                
                block(error, false)

            })
            
            break;
            
        case .post:
            
            self.postWithParameters(postData: requestObject.requestParameters, withURL: requestObject.requestURL , andRequestID: requestObject.requestId, withCompletionBlock: { (reponse) -> (Void) in
                
                block(reponse, true)
                
            }, withFailureBlock: { (error) -> (Void) in
                
                block(error, false)
                
            })
            
            break;
            
        case .multipartPost:
            
            self.experdiseMultipartPostWithParameters(postData: requestObject.requestParameters, withURL: requestObject.requestURL, andRequestID: requestObject.requestId, withCompletionBlock: { (response) -> (Void) in
                
                block(response, true)
                
            }, withFailureBlock: { (error) -> (Void) in
                
                block(error, false)
            })
        
        case .multiPart:
            
            self.multipartPostWithParameters(postData: requestObject.requestParameters , withURL: requestObject.requestURL, andRequestID: requestObject.requestId, withCompletionBlock: { (reponse) -> (Void) in
                
                block(reponse, true)
                
            }, withFailureBlock: { (error) -> (Void) in
                
                block(error, false)
                
            })
            
            break;
            
        case .put:
            
            //FIXME: check request and response serializer types like below
            
            self.putWithParameters(postData: requestObject.requestParameters , withURL: requestObject.requestURL, andRequestID: requestObject.requestId, withCompletionBlock: { (reponse) -> (Void) in
                
                block(reponse, true)
                
            }, withFailureBlock: { (error) -> (Void) in
                
                block(error, false)
                
            })
            
            break;
            
        case .delete:
            
            //FIXME: check request and response serializer types like below
            
            self.deleteWithParameters(postData: requestObject.requestParameters , withURL: requestObject.requestURL, andRequestID: requestObject.requestId, withCompletionBlock: { (reponse) -> (Void) in
                
                block(reponse, true)
                
            }, withFailureBlock: { (error) -> (Void) in
                
                block(error, false)
                
            })
            
            break;
            
        case .image:
            
            //FIXME: check request and response serializer types like below
            
            self.uploadImageWithParameters(postData: requestObject.requestParameters , withURL: requestObject.requestURL, andRequestID: requestObject.requestId, withCompletionBlock: { (reponse) -> (Void) in
                
                block(reponse, true)
                
            }, withFailureBlock: { (error) -> (Void) in
                
                block(error, false)
                
            })
            
            break;

        }
    }
    
    
    
    private func getWithParameters(postData:Any?,withURL url:String,andRequestID requestId:ApiRequestID,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)){
    
        self.includeTokenAndHeaderData(true)
        
        ///- show activity indicator in status bar
        self.setNetworkActivityIndicatorVisible(setVisible: true)
        
        ///- log post method properties
        print("Sending GET Request with URL:\(url) and params:\(postData ?? "")");
        
        self.requestSerializer.setValue(DTLanguageManager.sharedInstance.getLanguageCode(), forHTTPHeaderField: "Accept-Language")
        
        self.get(url, parameters: postData, progress: nil, success: { (task, response) in
            
            ///- log the response
            print("\n[ApiClient] ************** STARTING \(requestId)WithParameters RESPONSE **************\n\(response!)\n************** ENDING \(requestId)WithParameters RESPONSE **************\n");
            
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
//            if self.shouldCallSuccessBlockForApiRequest(requestId, params: postData, response: response) {
            
                completionBlock(response)
                
//            }

            
        }) { (task, error) in
            
            ///- request failed, log error
            print("[ApiClient] \(requestId)WithParameters FAILED with error: \(error.localizedDescription)");
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
            ///- check if should call delegate and send handled error object
            if self.shouldCallFailureBlockForApiRequest(requestId, withParams: postData, error: error){
            
                failureBlock(error)
            
            }
            
        }
    
    }
    
    
    private func postWithParameters(postData:Any?,withURL url:String,andRequestID requestId:ApiRequestID,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)){
        
        self.includeTokenAndHeaderData(true)
        
        ///- show activity indicator in status bar
        self.setNetworkActivityIndicatorVisible(setVisible: true)
        
        ///- log post method properties
        print("Sending POST Request with URL:\(url) \nwith params:\(postData ?? "")");
        
        self.requestSerializer.setValue(DTLanguageManager.sharedInstance.getLanguageCode(), forHTTPHeaderField: "Accept-Language")
        
        self.post(url, parameters: postData, progress: nil, success: { (task, response) in
            
            ///- log the response
            print("\n[ApiClient] ************** STARTING \(requestId)WithParameters RESPONSE **************\n\(response!)\n************** ENDING \(requestId)WithParameters RESPONSE **************\n");
            
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
//            if self.shouldCallSuccessBlockForApiRequest(requestId, params: postData, response: response) {
            
                completionBlock(response)
                
//            }
            
            
        }) { (task, error) in
            
            ///- request failed, log error
//            print("[ApiClient] \(requestId)WithParameters FAILED with error: \(error.localizedDescription)");
            
            print("[ApiClient] \(requestId)WithParameters FAILED with error: \(error.localizedDescription) And error body :\(self.getErrorFromResponseBody(response: error))");
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
            ///- check if should call delegate and send handled error object
            if self.shouldCallFailureBlockForApiRequest(requestId, withParams: postData, error: error){
                
                failureBlock(error)
                
            }
            
        }
        
    }
    
    private func experdiseMultipartPostWithParameters(postData:Any?, withURL url:String, andRequestID requestId:ApiRequestID,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)){
        
        self.includeTokenAndHeaderData(true)
        
        ///- show activity indicator in status bar
        self.setNetworkActivityIndicatorVisible(setVisible: true)
        
        ///- log post method properties
        print("Sending POST Request with URL:/\(url) and params:\(postData ?? "")");
        
        var imagesDictionary = [String: Data]()
        
        if let postData = postData as? [String: Any] {
            
            for data in postData {
                
                if let imageData = data.value as? Data {
                    imagesDictionary[data.key] = imageData
                }
            }
        }
        
        
        ///- log post method properties
        print("Sending POST Request with URL:/\(url) and params:\(postData ?? "")");
        
        self.post(url, parameters: postData, constructingBodyWith: { (formData) in
            
            for image in imagesDictionary {
                formData.appendPart(withFileData: image.value, name: image.key, fileName: "\(image.key).jpg", mimeType: "image/jpeg")
            }
            
        }, progress: { (progress) in
            
            print(progress)
            
        }, success: { (task, response) in
            
            ///- log the response
            print("\n[ApiClient] ************** STARTING \(requestId)WithParameters RESPONSE **************\n\(response!)\n************** ENDING \(requestId)WithParameters RESPONSE **************\n");
            
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
            
            //            if self.shouldCallSuccessBlockForApiRequest(requestId, params: postData, response: response) {
            
            completionBlock(response)
            
            //            }
            
        }) { (task, error) in
            
            ///- request failed, log error
            print("[ApiClient] \(requestId)WithParameters FAILED with error: \(error.localizedDescription) And error body :\(self.getErrorFromResponseBody(response: error))");
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
            ///- check if should call delegate and send handled error object
            if self.shouldCallFailureBlockForApiRequest(requestId, withParams: postData, error: error){
                
                failureBlock(error)
                
            }
            
        }
    }

    
    private func multipartPostWithParameters(postData:Any?,withURL url:String,andRequestID requestId:ApiRequestID,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)){
        

        self.includeTokenAndHeaderData(true)
        
        ///- show activity indicator in status bar
        self.setNetworkActivityIndicatorVisible(setVisible: true)
        
        
        
    }
    func getErrorFromResponseBody(response:Any?)->NSDictionary{
        
        let error = response as! NSError
        let responseData : Data = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data ?? Data()
        
        // convert data to string
        let jsonErrorObject = try? JSONSerialization.jsonObject(with: responseData, options: .mutableLeaves) as? NSDictionary ?? NSDictionary()
        
        //SB i will say that there is no internet connection or the body of faliure has no body
        if jsonErrorObject == nil && error.code == -1009{
            
            let dictionary : NSDictionary = ["error":"The Internet connection appears to be offline"]
            
            return dictionary
            
        }else if jsonErrorObject == nil{
            
            let dictionary : NSDictionary = ["error":"Something went wrong"]
            
            return dictionary
            
        }
        
        
        return jsonErrorObject!
        
    }
    
    
    private func putWithParameters(postData:Any?,withURL url:String,andRequestID requestId:ApiRequestID,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)){
        
        self.includeTokenAndHeaderData(true)
        
        ///- show activity indicator in status bar
        self.setNetworkActivityIndicatorVisible(setVisible: true)
        
        ///- log post method properties
        print("Sending PUT Request with URL:\(url) and params:\(postData ?? "")");
        
        self.requestSerializer.setValue(DTLanguageManager.sharedInstance.getLanguageCode(), forHTTPHeaderField: "Accept-Language")
        
        self.put(url, parameters: postData, success: { (task, response) in
            
            ///- log the response
            print("\n[ApiClient] ************** STARTING \(requestId)WithParameters RESPONSE **************\n\(response!)\n************** ENDING \(requestId)WithParameters RESPONSE **************\n");
            
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
//            if self.shouldCallSuccessBlockForApiRequest(requestId, params: postData, response: response) {
            
                completionBlock(response)
                
//            }
            
            
        }) { (task, error) in
            
            ///- request failed, log error
            print("[ApiClient] \(requestId)WithParameters FAILED with error: \(error.localizedDescription)");
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
            ///- check if should call delegate and send handled error object
            if self.shouldCallFailureBlockForApiRequest(requestId, withParams: postData, error: error){
                
                failureBlock(error)
                
            }
            
        }
        
    }
    
    
    private func deleteWithParameters(postData:Any?,withURL url:String,andRequestID requestId:ApiRequestID,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)){
        
        self.includeTokenAndHeaderData(true)
        
        ///- show activity indicator in status bar
        self.setNetworkActivityIndicatorVisible(setVisible: true)
        
        ///- log post method properties
        print("Sending DELETE Request with URL:\(url) and params:\(postData ?? "")");
        
        self.requestSerializer.setValue(DTLanguageManager.sharedInstance.getLanguageCode(), forHTTPHeaderField: "Accept-Language")
        
        self.delete(url, parameters: postData, success: { (task, response) in
            
            ///- log the response
            print("\n[ApiClient] ************** STARTING \(requestId)WithParameters RESPONSE **************\n\(response!)\n************** ENDING \(requestId)WithParameters RESPONSE **************\n");
            
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
//            if self.shouldCallSuccessBlockForApiRequest(requestId, params: postData, response: response) {
            
                completionBlock(response)
                
//            }
            
            
        }) { (task, error) in
            
            ///- request failed, log error
            print("[ApiClient] \(requestId)WithParameters FAILED with error: \(error.localizedDescription)");
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
            ///- check if should call delegate and send handled error object
            if self.shouldCallFailureBlockForApiRequest(requestId, withParams: postData, error: error){
                
                failureBlock(error)
                
            }
            
        }
        
    }
    
    
    private func patchWithParameters(postData:Any?,withURL url:String,andRequestID requestId:ApiRequestID,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)){
        
        self.includeTokenAndHeaderData(true)
        
        ///- show activity indicator in status bar
        self.setNetworkActivityIndicatorVisible(setVisible: true)
        
        ///- log post method properties
        print("Sending PATCH Request with URL:\(url) and params:\(postData ?? "")");
        
        self.requestSerializer.setValue(DTLanguageManager.sharedInstance.getLanguageCode(), forHTTPHeaderField: "Accept-Language")
        
        self.patch(url, parameters: postData, success: { (task, response) in
            
            ///- log the response
            print("\n[ApiClient] ************** STARTING \(requestId)WithParameters RESPONSE **************\n\(response!)\n************** ENDING \(requestId)WithParameters RESPONSE **************\n");
            
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
//            if self.shouldCallSuccessBlockForApiRequest(requestId, params: postData, response: response) {
            
                completionBlock(response)
                
//            }
            
            
        }) { (task, error) in
            
            ///- request failed, log error
            print("[ApiClient] \(requestId)WithParameters FAILED with error: \(error.localizedDescription)");
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
            ///- check if should call delegate and send handled error object
            if self.shouldCallFailureBlockForApiRequest(requestId, withParams: postData, error: error){
                
                failureBlock(error)
                
            }
            
        }
        
    }
    
    
    
    private func uploadImageWithParameters(postData:Any?,withURL url:String,andRequestID requestId:ApiRequestID,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)){
        
        self.includeTokenAndHeaderData(true)
        
        ///- show activity indicator in status bar
        self.setNetworkActivityIndicatorVisible(setVisible: true)
        
        self.requestSerializer.setValue(DTLanguageManager.sharedInstance.getLanguageCode(), forHTTPHeaderField: "Accept-Language")
        
        ///- log post method properties
        print("Sending POST Upload Image Request with URL:\(url) and params:\(postData ?? "")");
        
//        let photo = postDictionary.object(forKey: "photo") as! UIImage
//        let imageData = UIImageJPEGRepresentation(photo, 0.5)
//        
//        let dictionary : NSMutableDictionary = ["id":postDictionary.object(forKey: "id")!]
        
        self.post(url, parameters: postData, constructingBodyWith: { (formData) in
            
//            formData.appendPart(withFileData: imageData!, name: "file", fileName: "photo.jpg", mimeType: "image/jpeg")
            
        }, progress: { (progress) in
            
            print(progress)
            
        }, success: { (task, response) in
            
            ///- log the response
            print("\n[ApiClient] ************** STARTING \(requestId)WithParameters RESPONSE **************\n\(response!)\n************** ENDING \(requestId)WithParameters RESPONSE **************\n");
            
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
//            if self.shouldCallSuccessBlockForApiRequest(requestId, params: postData, response: response) {
            
                completionBlock(response)
                
//            }
            
        }) { (task, error) in
            
            ///- request failed, log error
            print("[ApiClient] \(requestId)WithParameters FAILED with error: \(error.localizedDescription)");
            
            ///- stop network activity indicator
            self.setNetworkActivityIndicatorVisible(setVisible: false)
            
            ///- check if should call delegate and send handled error object
            if self.shouldCallFailureBlockForApiRequest(requestId, withParams: postData, error: error){
                
                failureBlock(error)
                
            }
            
        }
        
    }
    
    
    //MARK: success Handling
    
    func shouldCallSuccessBlockForApiRequest(_ requestID:ApiRequestID,params:Any?,response:Any?)->Bool{
        
        if (response as? [Dictionary<String, Any>]) != nil {
            
            return true
        }

//        if let responseDictionary = response as? [String: Any] {
//            
//            let success = (responseDictionary.object(forKey: "result") as AnyObject).intValue == 1
//            
//            if (success) {
//                
//                return true;
//                
//            } else {
//                
//                let statusCode = responseDictionary.value(forKey: "statusCode") as? Int ?? 0
//                let shouldCallDelegate = self.blockShouldBeCalledForErrorCode(statusCode, withParams: params, requestID: requestID)
//                
//                return shouldCallDelegate;
//                
//            }
//            
//        }
        
        return false
        
    }
    
    
    
    
    //MARK: Error Handling
    
    func shouldCallFailureBlockForApiRequest(_ requestID:ApiRequestID,withParams param:Any?,error:Error)->Bool{
        
        // Just to get code
        let errorCasted = error as NSError
        
        let shouldCallDelegate = self.blockShouldBeCalledForErrorCode(errorCasted.code, withParams: param, requestID: requestID)
        
        return shouldCallDelegate
    }
    
    func blockShouldBeCalledForErrorCode(_ errorCode:Int,withParams:Any?,requestID:ApiRequestID)->Bool {
        
        
        switch (errorCode) {
            
        case 401:
            
            ///- 401 is for unauthorized, so if the user was already logged in, this means we have an invalid token and should request a new one. Here we should send an extra dictionary of the API call that generated this error so that we can call it again in case token renewal is successful
            
            return false
            
            
        case NSURLErrorTimedOut:
            
            ///- error
            
            
            
            return true;
            
        default:
            return true;
        }
        
    }
    
    func errorHandlingForError(_ error:NSError)->NSError{
        
        ///- prepare handled error info dictionary
        let errorInfo = NSMutableDictionary()
        var myerror  = error
        
        switch (error.code) {
        case 998877:
            
            ///- handle the error by altering the message
            errorInfo.setValue("This error is now handled", forKey: NSLocalizedDescriptionKey)
            myerror = NSError(domain: "Restix", code: 998877, userInfo: errorInfo as NSDictionary? as? [AnyHashable: AnyObject] ?? [:])
            return error;
            
        case NSURLErrorTimedOut:
            
            ///- handle the time out error by altering the message
            errorInfo.setValue("Request timed out.. please try again later", forKey: NSLocalizedDescriptionKey)
            myerror = NSError(domain: "Restix", code: 112233, userInfo: errorInfo as NSDictionary? as? [AnyHashable: AnyObject] ?? [:])
            
            break;
            
            
        case -1004:
            
            ///- handle the time out error by altering the message
            errorInfo.setValue("There was a problem reaching the servers.. please try again later", forKey: NSLocalizedDescriptionKey)
            myerror = NSError(domain: "Restix", code: 112234, userInfo: errorInfo as NSDictionary? as? [AnyHashable: AnyObject] ?? [:])
            
            break;
            
            
        case -1009:
            
            ///- handle the time out error by altering the message
            errorInfo.setValue("Please check your internet connection and try again", forKey: NSLocalizedDescriptionKey)
            myerror = NSError(domain: "Restix", code: 112235, userInfo: errorInfo as NSDictionary? as? [AnyHashable: AnyObject] ?? [:])
            
            break;
            
            
        default:
            return myerror;
            
        }
        
        return myerror;
        
    }
    
    
    func makeAlertViewForError(_ error:NSError)->UIAlertController{
        
        let errorInfo : NSDictionary = error.userInfo as NSDictionary
        let message : String = errorInfo.value(forKey: NSLocalizedDescriptionKey) as! String
        let alertController = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
            self.isDisplayingError = false
            
        }
        alertController.addAction(ok)
        
        return alertController;
        
    }
    
    
    func shouldDisplayErrorMessageForResponseDictionary(_ reponseDictionary:NSMutableDictionary)->Bool{
        
        let statusCode = reponseDictionary.value(forKey: "statusCode") as! Int
        let shouldDisplay = self.shouldDisplayErrorMessageForErrorCode(statusCode)
        
        return shouldDisplay
        
    }
    
    func shouldDisplayErrorMessageForErrorCode(_ errorCode:Int)->Bool{
        
        
        switch errorCode {
        case 112233:
            if (self.isDisplayingError) {
                return false;
            } else {
                self.isDisplayingError = true;
                return true;
            }
        default:
            break;
        }
        
        return true
        
    }
    
    
    func makeAlertViewForResponseDictionary(response:Any?)->EXAlertViewController{
        
        let alertViewController = EXAlertViewController(title: nil, okay: nil)
        
        APIClient.sharedInstance.isDisplayingError = true
        
        let error = response as! NSError
        
        //SB No internet connection
        if error.code == -1009{
            
            alertViewController.alertTitle =  DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Error")
            alertViewController.alertMessage = DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "The Internet connection appears to be offline")
            alertViewController.modalPresentationStyle = .custom
            
            return alertViewController
            
        }
        
        if let responseData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data {
            
            
            let alertViewController = EXAlertViewController(title: DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Error"), twoButton: false, alertMessage: "", buttonTitle: "Ok",zeroHighView: false,centerTextAlertMessage: true,speratorViewEnable: true)
            alertViewController.alertMessage = ""
            
            do {
                
                // convert data to string
                let jsonErrorObject = try? JSONSerialization.jsonObject(with: responseData, options: .mutableLeaves) as? NSDictionary
                
                let errorTitle = DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Error")
                
                let errors = jsonErrorObject??.object(forKey: "Errors") as? [String?]
                let errorMessage = errors?.first as? String
                                
                alertViewController.alertTitle = errorTitle
                alertViewController.alertMessage = errorMessage ?? ""
                alertViewController.modalPresentationStyle = .custom
                
                return alertViewController
                
            }
            
        }else {
            
            
            let alertViewController = EXAlertViewController(title: DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Error"), okay: nil)
            alertViewController.alertMessage = DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Something went wrong")
            alertViewController.modalPresentationStyle = .custom
            
            return alertViewController
            
            
        }    }
    
    private func getErrorMessageForErrorCode(errorCode:Int)->String{
        
        let userLoggedIn = UserDefaults.standard.bool(forKey: "loggedIn")
        
        switch (errorCode) {
            
        case 401:
            
            if (!userLoggedIn) {
                return "Unautharized Access"
            }else {
                return ""
            }
            
        case 400:
            
            if (!userLoggedIn) {
                return ""
                //return @"Unautharized Access";
            }else {
                return ""
            }
            
        default:
            return ""
        }
    }

    
    //MARK: - Helping
    
    private func setNetworkActivityIndicatorVisible(setVisible:Bool){
        
        var NumberOfCallsToSetVisible = 0
        
        ///- fix for when forcing to hide indicator when app gets offline
        if (setVisible){
            NumberOfCallsToSetVisible += 1
        }else{
            NumberOfCallsToSetVisible -= 1
        }
        // The assertion helps to find programmer errors in activity indicator management.
        // Since a negative NumberOfCallsToSetVisible is not a fatal error,
        // it should probably be removed from production code.
        //NSAssert(NumberOfCallsToSetVisible >= 0, @"Network Activity Indicator was asked to hide more often than shown");
        
        // Display the indicator as long as our static counter is > 0.
        UIApplication.shared.isNetworkActivityIndicatorVisible = (NumberOfCallsToSetVisible > 0)

    }
    
    

}
