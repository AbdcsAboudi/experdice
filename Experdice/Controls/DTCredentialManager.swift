//
//  DTCredentialManager.swift
//  WeLoveReadingVolunteer
//
//  Created by Ayman Rawashdeh on 11/22/16.
//  Copyright © 2016 Saad Basha. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper


class DTCredentialManager {
    
    enum AuthenticationType {
        case email
        case facebook
        case twitter
        case google
        case instagram
        case username
    }
    
    static let credentialSharedInstance = DTCredentialManager()
    
    var user: Any? {
        get {
            if let data = UserDefaults.standard.object(forKey: "userData") as? NSData {
                return NSKeyedUnarchiver.unarchiveObject(with: data as Data)
            }
            return nil
        }
        set{
            if let value = newValue {
                
                UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: value), forKey: "userData")
            }
        }
    }
    
    static var userID: Int? {
        
        get {
            
            return KeychainWrapper.standard.integer(forKey: "UserID")
        }
        
        set {
            
            if let newValue = newValue {
                KeychainWrapper.standard.set(newValue, forKey: "UserID")
            }
        }
    }
    
    private init (){
        
    }
    
    static func saveUserCredentialsForUsername(username: String, password:String, withAuthentication authType:AuthenticationType){
        
        KeychainWrapper.standard.set(username, forKey: "USER_NAME")
        KeychainWrapper.standard.set(password, forKey: "USER_PASSWORD")
        
        DispatchQueue.global(qos: .background).async {
            UserDefaults.standard.set(authDescriptionFor(authType: authType), forKey: "LoginAuthType")
            UserDefaults.standard.set(true, forKey: "UserLoggedIn")
        }
    }
    
    static func getUserCredentials() -> NSDictionary {
        
        var username = KeychainWrapper.standard.string(forKey: "USER_NAME")
        var password = KeychainWrapper.standard.string(forKey: "USER_PASSWORD")
        var authType = UserDefaults.standard.string(forKey: "LoginAuthType")
        
        if username == nil {
            username = ""
        }
        
        if password == nil {
            password = ""
        }
        if authType == nil {
            authType = ""
        }
        
        let dictionary:NSDictionary = ["username": username! as String, "password": password! as String, "authType": authType! as String]
        
        return dictionary
        
    }
    
    static func resetUserCredentials(){
        _ = KeychainWrapper.standard.removeAllKeys()
        UserDefaults.standard.removeObject(forKey: "LoginAuthType")
        UserDefaults.standard.set(false, forKey: "UserLoggedIn")
        DTCredentialManager.credentialSharedInstance.user = nil
    }
    
    /**
     AY: Check User Authentication
     
     ````
     if isAuthenticated(username: userNameField.text, password: passwordField.text, authType: .email) {
     
     }
     ````
     - Parameters:
     - username: Username provided by user
     - password: Password provided by user
     - authType: The login authentication type
     
     - Returns: True/False if the username, password and authType in the saved user credintials
     */

    static func isAuthenticated(username:String, password:String, authType:AuthenticationType) -> Bool{
        
        let userCredential = getUserCredentials()
        
        if let savedUsername = userCredential["username"] as? String, let savedPassword = userCredential["password"] as? String, let savedAuthType = userCredential["authType"] as? String {
            if username == savedUsername && password == savedPassword && savedAuthType == authDescriptionFor(authType: authType){
                
                return true
            }
        }
        
        return false
        
    }
    
    // AY: Check if user is logged in
    static func isLoggedIn()->Bool{
        return UserDefaults.standard.bool(forKey: "UserLoggedIn")
    }
    
    static func authDescriptionFor(authType:AuthenticationType) -> String{
        switch authType {
        case .email:
            return "email"
        case .facebook:
            return "facebook"
        case .google:
            return "google"
        case .instagram:
            return "instagram"
        case .twitter:
            return "twitter"
        case .username:
            return "username"
        }
    }
    
    static func isFirstTimeOpened() -> Bool {
        
        let isFirstTime = KeychainWrapper.standard.bool(forKey: "IS_FIRST_TIME_OPEND")

        return isFirstTime ?? true
            
    }
    
    static func setFirstLogin(to isFistTime: Bool){
        
       KeychainWrapper.standard.set(isFistTime, forKey: "IS_FIRST_TIME_OPEND")
        
    }
    
    
}
