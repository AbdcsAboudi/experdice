//
//  DTValidationManager.swift
//  customizeTextField
//
//  Created by Yousef ALselawe on 7/25/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit


class DTValidationManager {
    
    // MARK: - Validation Methods
    static func isValidEmail(text: String!) -> Bool {
        
        // YA : email Validation
        let emailRegEx = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: text)
        return result
        
    }
    
    static func isValidName(text: String!) -> Bool {
        let nameRegex = "^[A-Za-zء-ي]{3,128}"
        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegex)
        let result = nameTest.evaluate(with: text)
        return result
        
    }
    static func isValidHeadLine(text: String!) -> Bool {
        let headLineRegex = "^[A-Za-zء-ي]{3,128}"
        let headLineTest = NSPredicate(format:"SELF MATCHES %@", headLineRegex)
        let result = headLineTest.evaluate(with: text)
        return result
        
    }
    
    static func isValidNumber(text: String!) -> Bool {
        
        //YA: number Validation
        
        let numberRegex = "^((\\+)|(00))([0-9]{3})\\s*((([0-9]{3})\\s+([0-9]{8}))|([0-9]{3,14}))$"
        let numberTest = NSPredicate(format: "SELF MATCHES %@", numberRegex)
        let result = numberTest.evaluate(with: text)
        return result
        
    }
    
    static func isValidPassword(text: String!) -> Bool {
        
        //Minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character:
        let passwordRegex = "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}"
        let numberTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        let result = numberTest.evaluate(with: text)
        return result
        
    }
    
    static func isVaildBusinessName(text: String!) -> Bool {
        
        let businessNameRegex = "^[\\sA-Za-zء-ي]{3,128}"
        let nameTest = NSPredicate(format:"SELF MATCHES %@", businessNameRegex)
        let result = nameTest.evaluate(with: text)
        return result
        
    }
    
    static func isVaildBusinessType(text: String!) -> Bool {
        
        let businessTypeRegex = "^[\\sA-Za-zء-ي]{3,128}"
        let nameTest = NSPredicate(format:"SELF MATCHES %@", businessTypeRegex)
        let result = nameTest.evaluate(with: text)
        return result
        
    }
    
    //MARK: - Validation
    //MARK: Create Project - Teambuilding Type
    
    static func isValidProjectTitle(text: String!) -> Bool {
        
        let projectTitle = "^[\\sA-Za-zء-ي]{3,128}"
        let projectTitleTest = NSPredicate(format:"SELF MATCHES %@", projectTitle)
        let result = projectTitleTest.evaluate(with: text)
        return result
        
    }
    
    static func isValidProjectObjective(text: String!) -> Bool {
        
        let projectObjective = "^[\\sA-Za-zء-ي]{3,128}"
        let projectObjectiveTest = NSPredicate(format:"SELF MATCHES %@", projectObjective)
        let result = projectObjectiveTest.evaluate(with: text)
        return result
        
    }
    
    static func isValidProjectGoalAchievements(text: String!) -> Bool {
        
        let projectGoalAchievements = "^[\\sA-Za-zء-ي]{3,128}"
        let projectGoalAchievementsTest = NSPredicate(format:"SELF MATCHES %@", projectGoalAchievements)
        let result = projectGoalAchievementsTest.evaluate(with: text)
        return result
        
    }
    
    static func isValidProjectGoalDescription(text: String!) -> Bool {
        
        let projectGoalDescription = "^[\\sA-Za-zء-ي]{3,128}"
        let projectGoalDescriptionTest = NSPredicate(format:"SELF MATCHES %@", projectGoalDescription)
        let result = projectGoalDescriptionTest.evaluate(with: text)
        return result
        
    }
    
    static func isValidProjectGoalNotes(text: String!) -> Bool {
        
        let projectGoalNotes = "^[\\sA-Za-zء-ي]{3,128}"
        let projectGoalNotesTest = NSPredicate(format:"SELF MATCHES %@", projectGoalNotes)
        let result = projectGoalNotesTest.evaluate(with: text)
        return result
        
    }
    
    static func isValidProjectBudgetRate(text: String!) -> Bool {
        
        let projectBudgetRate = "^[\\sA-Za-zء-ي]{3,128}"
        let projectBudgetRateTest = NSPredicate(format:"SELF MATCHES %@", projectBudgetRate)
        let result = projectBudgetRateTest.evaluate(with: text)
        return result
        
    }
    
    
    static func isValidStreetAddress(text: String!) -> Bool {
        
        let address = "^[\\sA-Za-zء-ي]{3,128}"
        let addressTest = NSPredicate(format:"SELF MATCHES %@", address)
        let result = addressTest.evaluate(with: text)
        return result
    }
    
    static func isValidZipCode(text: String!) -> Bool {
        
        let zipCode = "^[\\sA-Za-zء-ي]{3,128}"
        let zipCodeTest = NSPredicate(format:"SELF MATCHES %@", zipCode)
        let result = zipCodeTest.evaluate(with: text)
        return result
    }
    
    static func isValidPhoneNumber(text: String!) -> Bool {
        
        let phoneNumber = "^[\\sA-Za-zء-ي]{3,128}"
        let phoneNumberTest = NSPredicate(format:"SELF MATCHES %@", phoneNumber)
        let result = phoneNumberTest.evaluate(with: text)
        return result
    }
    
    static func isValidCityName(text: String!) -> Bool {
        
        let cityName = "^[\\sA-Za-zء-ي]{3,128}"
        let cityNameTest = NSPredicate(format:"SELF MATCHES %@", cityName)
        let result = cityNameTest.evaluate(with: text)
        return result
    }
    
    static func isValidCountryName(text: String!) -> Bool {
        
        let countryName = "^[\\sA-Za-zء-ي]{3,128}"
        let countryNameTest = NSPredicate(format:"SELF MATCHES %@", countryName)
        let result = countryNameTest.evaluate(with: text)
        return result
    }
    
    
    static func isValidStateName(text: String!) -> Bool {
        
        let stateName = "^[\\sA-Za-zء-ي]{3,128}"
        let stateNameTest = NSPredicate(format:"SELF MATCHES %@", stateName)
        let result = stateNameTest.evaluate(with: text)
        return result
    }
    
    
    
    static func isValidProjectExpectedNumber(text: String!) -> Bool {
        
        let projectExpectedNumber = "^[0-9]{1,128}"
        let projectExpectedNumberTest = NSPredicate(format:"SELF MATCHES %@", projectExpectedNumber)
        let result = projectExpectedNumberTest.evaluate(with: text)
        return result
        
    }
    
    static func isVaildDateRange(dateTo: String!,dateFrom: String!) -> Bool {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
       
        let dateToDateType = dateFormatter.date(from: dateTo)
        let dateFromDateType = dateFormatter.date(from: dateFrom)
        
        if dateToDateType?.compare(dateFromDateType!) == .orderedAscending {
            return true
        }
       return false
        
    }
    
    
}


