//
//  LanguageManager.swift
//
//  Created by Saad Basha on 10/10/16.
//  Copyright © 2016 Saad Basha. All rights reserved.
//

import UIKit
import VLRTextField

class DTLanguageManager {
    
    public enum Language : Int {
        
        case arabic = 0
        case english = 1
    }
    
    static let sharedInstance = DTLanguageManager()
    var hasSelectedLanguage = false
    var language : Language = .english
    
    //MARK: - Life Cycle
    
    /// Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
    private init(){
        
        self.getCurrentLanguage()
        
    }
    
    //MARK: - Language Managment
    
    /// This method will use the latest used language if found, or by default use the system language
    func getCurrentLanguage() {
        
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil {
            
            self.language = Language(rawValue: UserDefaults.standard.object(forKey: "currentLanguage") as? Int ?? 1)!
            
            self.hasSelectedLanguage = true
            
        }else{
            
            let defaultSystemLanguage = Locale.current.languageCode
            
            if let systemLanguage = defaultSystemLanguage{
                
                if systemLanguage == "ar" {
                    self.language = .arabic
                }else {
                    self.language = .english
                }
                
            }
            
            self.hasSelectedLanguage = true
            
        }
        
    }
    
    
    
    /// Reset Language Settings
    func removeCurrentLanguage(){
        
        UserDefaults.standard.setValue(nil, forKey: "currentLanguage")
        self.hasSelectedLanguage = false
        
    }
    
    /// Used to save language
    ///
    /// - Parameter lang: enum value of language ex: .arabic
    func saveCurrentLanguage(lang:Language){
        
        UserDefaults.standard.setValue(lang.rawValue, forKey: "currentLanguage")
        self.language = lang
        hasSelectedLanguage = true
        
    }
    
    //MARK: - Localizing Text and Nib
    
    func stringForLocalizedKey(key:String)->String{
        
        var languageCode : String?
        
        if (self.language == .english) {
            languageCode = "Base";
        } else if (self.language == .arabic) {
            languageCode = "ar";
        }
        
        var path = Bundle.main.path(forResource: languageCode, ofType: "lproj")
        if path == nil {
            path = Bundle.main.path(forResource: "Base", ofType: "lproj")
        }
        
        let languageBundle = Bundle(path: path!)
        let str = languageBundle?.localizedString(forKey: key, value: "", table: nil)
        
        return str!;
        
    }
    
    
    func nibForLocalizedKey(key:String)->Bundle{
        
        var languageCode : String?
        
        if (self.language == .english) {
            languageCode = "Base";
        } else if (self.language == .arabic) {
            languageCode = "ar";
        }
        
        let path = Bundle.main.path(forResource: languageCode, ofType: "lproj")
        let languageBundle = Bundle(path: path!)
        
        
        return languageBundle!
        
    }
    
    func changeLayoutTo(layoutDirection:UISemanticContentAttribute ,StartingFrom viewController : UIViewController) {
        
        UIView.appearance().semanticContentAttribute = layoutDirection
        UINavigationBar.appearance().semanticContentAttribute = layoutDirection
        
        //AA : To save the current language un the defaults
        if layoutDirection == .forceLeftToRight {
            saveCurrentLanguage(lang: .english)
        }else{
            saveCurrentLanguage(lang: .arabic)
        }
        
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func changeTheLayoutBasedOnTheSavedLangauge(from ViewController : UIViewController) {
        
        if (self.language == .english) {
            self.changeLayoutTo(layoutDirection: .forceLeftToRight, StartingFrom: ViewController)
            
        } else if (self.language == .arabic) {
            
            self.changeLayoutTo(layoutDirection: .forceRightToLeft, StartingFrom: ViewController)
        }
        
    }
    
    static func getLocale() -> Locale {
        
        return DTLanguageManager.sharedInstance.language == .english ? Locale(identifier: "en_US") : Locale(identifier: "ar_EH")
    }
    
    static func getLocaleDateFormatter(with date: Date, format: String ) -> String{
        
        let calendar = Calendar(identifier: .gregorian)
        var timeZone: TimeZone!
        
        if let settingsTimeZoneIdentifier = Settings.sharedSettings?.timeZoneIdentifier {
            
            timeZone = TimeZone(identifier: settingsTimeZoneIdentifier)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = DTLanguageManager.getLocale()
        dateFormatter.dateFormat = format
        dateFormatter.calendar = calendar
        dateFormatter.timeZone = timeZone
        
        return dateFormatter.string(from: date)
    }
    
    func getLanguageCode() -> String {
        
        if language == .arabic {
            
            return "ar"
        }
        
        return "en"
    }
    
    
}
