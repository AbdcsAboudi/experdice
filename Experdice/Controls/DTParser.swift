//
//  DTParser.swift
//
//
//  Created by Ahmad Sallam  on 1/24/17.
//  Copyright © 2017 Ahmad Sallam . All rights reserved.
//

import Foundation
import UIKit

class DTParser{
    
    
    //MARK: - Parse Languages Object From Response Array
    
    static func parseLanguagesObjectFromResponseArray(from response:[Any]) -> [LanguagesObject?]{
        
        var languagesObjectArray = [LanguagesObject]()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String:Any]  {
                
                languagesObjectArray.append(self.parseLanugaeObjectFromResponse(from: responseDictionary))
            }
            
        }
        return languagesObjectArray
    }
    
    static func parseLanugaeObjectFromResponse(from response:Any)->LanguagesObject{
        
        var languageObject = LanguagesObject()
        
        if let responseDictionary = response as? [String:Any] {
            
            languageObject = LanguagesObject(id:Int(responseDictionary["ID"] as? String ?? "0"),
                                             name: responseDictionary["NAME"] as? String ?? "")
        }
        
        return languageObject
        
    }
    
    //MARK: - Parse Payment Methods Object From Response Array
    
    static func parsePaymentMethodsFromResponseArray(from response:[Any]) -> [PaymentMethodsObject?]{
        
        var paymentMethodsObjectArray = [PaymentMethodsObject]()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String:Any]  {
                
                paymentMethodsObjectArray.append(self.parsePaymentMethodsObjectFromResponse(from: responseDictionary))
            }
            
        }
        return paymentMethodsObjectArray
    }
    
    static func parsePaymentMethodsObjectFromResponse(from response:Any)->PaymentMethodsObject{
        
        var paymentMethodsObject = PaymentMethodsObject()
        
        if let responseDictionary = response as? [String:Any] {
            
            paymentMethodsObject = PaymentMethodsObject(id: Int(responseDictionary["ID"] as? String ?? "0"),
                                                  name: responseDictionary["NAME"] as? String ?? "",
                                                  descriptionText: responseDictionary["DESCRIPTION"] as? String ?? "")
        }
        
        return paymentMethodsObject
        
    }
    
    //MARK: - Parse Professional Seniorities Object From Response Array
    
    static func parseProfessionalSenioritiesFromResponseArray(from response:[Any]) -> [ProfssionalSeniorityObject?]{
        
        var professionalSenioritiesObjectArray = [ProfssionalSeniorityObject]()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String:Any] {
                
            professionalSenioritiesObjectArray.append(self.parseProfessionalSenioritiesObjectFromResponse(from: responseDictionary))
            }
            
        }
        return professionalSenioritiesObjectArray
    }
    
    static func parseProfessionalSenioritiesObjectFromResponse(from response:Any)->ProfssionalSeniorityObject{
        
        var professionalSenioritiesObject = ProfssionalSeniorityObject()
        
        if let responseDictionary = response as? [String:Any] {
            
            professionalSenioritiesObject = ProfssionalSeniorityObject(id: Int(responseDictionary["ID"] as? String ?? "0"),
                                                                       name: responseDictionary["NAME"] as? String ?? "",
                                                                       seniorityDescription: responseDictionary["DESCRIPTION"] as? String ?? "")
        }
        
        return professionalSenioritiesObject
        
    }
  
    //MARK: - Parse Expert User And Business Object From Response Array
 
    static func parseExpertObjectFromResponseArray(from response:[Any]) -> ProfileObject?{
        
        var expertProfileObject = ProfileObject()

        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String: Any]{
                let experienceArray = self.parseExperiencesArray(response: responseDictionary)
                let educationsArray = self.parseEducationArray(response: responseDictionary)
                let certificationsArray = self.parseCertificationArray(response: responseDictionary)
                let badgesArray = self.parseBadgesArray(response: responseDictionary)
                let reviewsArray = self.parseReviewArray(response: responseDictionary)
                if let expertProfile = responseDictionary["Expert"] as? [String:Any]{
                    expertProfileObject = ProfileObject(withUserExpert: expertProfile["EXPERT_ID"] as? Int ?? 0,
                                                        profileFirstName: expertProfile["FIRST_NAME"] as? String ?? "",
                                                        profileSecondName: expertProfile["LAST_NAME"] as? String ?? "",
                                                        profileJopTitle: "",
                                                        expertCountry:"",
                                                        expertCity:"",
                                                        expertOverallRating:Int(expertProfile["EXPERT_OVERALL_RATING"] as? String ?? "0"),
                                                        expertStatusId: Int(expertProfile["EXPERT_STATUS_ID"] as? String ?? "0"),
                                                        linkedinURL: expertProfile["FIRST_NAME"] as? String ?? "",
                                                        nationalityId: Int(expertProfile["NATIONALITY_ID"] as? String ?? "0"),
                                                        nationalityName: expertProfile["NATIONALITY_NAME"] as? String ?? "",
                                                        earilyRegistered: expertProfile["EARLY_REGISTERED_FLAG"] as? String ?? "",
                                                        experienceObject: experienceArray,
                                                        educationObject: educationsArray,
                                                        certificationObject: certificationsArray,
                                                        reviews:reviewsArray,
                                                        badges:badgesArray)
                    if let countryName = expertProfile["Country"] as? [String:Any]{
                        expertProfileObject.expertCountry = countryName["NAME"] as? String
                    }
                    
                    if let cityName = expertProfile["City"] as? [String:Any]{
                        expertProfileObject.expertCountry = cityName["NAME"] as? String
                    }
                }
                expertProfileObject.experienceObject = experienceArray
                expertProfileObject.educationObject = educationsArray
                expertProfileObject.certificationObject = certificationsArray
                expertProfileObject.reviews = reviewsArray
                expertProfileObject.badges = badgesArray
            }
        }
        return expertProfileObject
    }
    
    class func parseExpertUserObjectFromResponseArray(response:[Any]) -> UserObject {
        
        var object = UserObject()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String: Any]{
                
                if let user = responseDictionary["User"] as? [String:Any] {
                    
                    object = UserObject(userID: user["ID"] as? Int ?? 0,
                                        profile: ProfileObject(),
                                        email: user["EMAIL"] as? String ?? "",
                                        password: user["PASSWORD"] as? String ?? "",
                                        phoneNumber: user["PHONE_NUMBER"] as? String ?? "",
                                        countryId: Int(user["COUNTRY_ID"] as? String ?? "\(0)"),
                                        countryName: "",
                                        cityId: Int(user["CITY_ID"] as? String ?? "\(0)"),
                                        cityName: "",
                                        stateId: Int(user["STATE_ID"] as? String ?? "\(0)"),
                                        stateName: user["STATE"] as? String ?? "",
                                        streetName: user["STREET"] as? String ?? "",
                                        zipCode: user["ZIP_CODE"] as? String ?? "",
                                        userType: user["USER_TYPE"] as? String ?? "",
                                        overviewText: user["OVERVIEW_TEXT"] as? String ?? "",
                                        overviewVideoLink: user["OVERVIEW_VIDEO_LINK"] as? String ?? "",
                                        bussinessIndustryID: user["BUSSINESS_INDUSTRY_ID"] as? String ?? "",
                                        invitedExpertName: user["NAME"] as? String ?? "",
                                        genderId:0,
                                        dateOfBirth:user["DATE_OF_BIRTH"] as? String ?? "",
                                        headLine:user["HEADLINE"] as? String ?? "",
                                        imageURL: user["IMAGE_URL"] as? String ?? "")
                    
                    if let countryName = user["Country"] as? [String:Any] {
                          object.countryName = countryName["NAME"] as? String
                    }
                    
                    if let cityName =  user["City"] as? [String:Any] {
                        object.cityName = cityName["NAME"] as? String
                    }
                    
                    if let stateName = user["State"] as? [String:Any] {
                        object.stateName = self.parseStateObjectArray(from: stateName).name
                    }
                  
                }
            }
        }
        
        
        object.profile = self.parseExpertObjectFromResponseArray(from: response)
        
        
        return object
        
    }
    
    class func parseBusinessUserObjectFromResponseArray(response:[Any]) -> UserObject {
        
        var object = UserObject()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String: Any]{
                
                if let user = responseDictionary["User"] as? [String:Any] {
                    
                    object = UserObject(userID: user["USER_ID"] as? Int ?? 0,
                                        profile: ProfileObject(),
                                        email: user["EMAIL"] as? String ?? "",
                                        password: user["PASSWORD"] as? String ?? "",
                                        phoneNumber: user["PHONE_NUMBER"] as? String ?? "",
                                        countryId: Int(user["COUNTRY_ID"] as? String ?? "\(0)"),
                                        countryName: "",
                                        cityId: Int(user["CITY_ID"] as? String ?? "\(0)"),
                                        cityName: "",
                                        stateId: Int(user["STATE_ID"] as? String ?? "\(0)"),
                                        stateName: user["STATE"] as? String ?? "",
                                        streetName: user["STREET"] as? String ?? "",
                                        zipCode: user["ZIP_CODE"] as? String ?? "",
                                        userType: user["USER_TYPE"] as? String ?? "",
                                        overviewText: user["OVERVIEW_TEXT"] as? String ?? "",
                                        overviewVideoLink: user["OVERVIEW_VIDEO_LINK"] as? String ?? "",
                                        bussinessIndustryID: "",
                                        invitedExpertName: "",
                                        genderId:Int(user["GENDER_ID"] as? String ?? "0") ?? 0,
                                        dateOfBirth:"",
                                        headLine:"",
                                        imageURL: user["IMAGE_URL"] as? String ?? "")
                    
                    object.countryName = self.parseCountryObject(from: user["Country"]).name
                    object.cityName = self.parseCityObject(response: user["City"]).name
                    object.stateName = self.parseStateObjectArray(from: user["State"]).name
                    
                }
            }
            
            object.profile = self.parseBusinessProfileFromResponse(from: responseDictionary as! [String : Any])
            
        }
        
        
        
        
        return object
        
    }
    
    
    
    // MARK: - Parse Object From Response
    
    class func parseUserObjectFromResponse(response:[String:Any]) -> UserObject {
        
        var object = UserObject()
        
        if let user = response["User"] as? [String :Any]{
            
            object = UserObject(userID: user["ID"] as? Int ?? 0,
                                profile: ProfileObject(),
                                email: user["EMAIL"] as? String ?? "",
                                password: user["PASSWORD"] as? String ?? "",
                                phoneNumber: user["PHONE_NUMBER"] as? String ?? "",
                                countryId: Int(user["COUNTRY_ID"] as? String ?? "\(0)"),
                                countryName: user["COUNTRY"] as? String ?? "",
                                cityId: Int(user["CITY_ID"] as? String ?? "\(0)"),
                                cityName: user["CITY"] as? String ?? "",
                                stateId: Int(user["STATE_ID"] as? String ?? "\(0)"),
                                stateName: user["STATE"] as? String ?? "",
                                streetName: user["STREET"] as? String ?? "",
                                zipCode: user["ZIP_CODE"] as? String ?? "",
                                userType: user["USER_TYPE"] as? String ?? "",
                                overviewText: user["OVERVIEW_TEXT"] as? String ?? "",
                                overviewVideoLink: user["OVERVIEW_VIDEO_LINK"] as? String ?? "",
                                bussinessIndustryID: user["BUSSINESS_INDUSTRY_ID"] as? String ?? "",
                                invitedExpertName: user["NAME"] as? String ?? "",
                                genderId:Int(user["GENDER_ID"] as? String ?? "0") ?? 0,
                                dateOfBirth:user["DATE_OF_BIRTH"] as? String ?? "",
                                headLine:user["HEADLINE"] as? String ?? "",
                                imageURL: user["IMAGE_URL"] as? String ?? "")
        }
        
        if object.userType == "1"{
            
            object.profile = self.parseExpertProfileFromResponse(from: response)
            
        }else{
            
            object.profile = self.parseBusinessProfileFromResponse(from: response)
        }
        
        
        return object
        
    }
    
    class func parseUserObjectFromInviteList(response:[Any]) -> UserObject {
        
        var object = UserObject()
        
        for expert in response {
            
            if let user = expert as? [String:Any] {
                
                object = UserObject(userID: user["ID"] as? Int ?? 0,
                                    profile: ProfileObject(),
                                    email: user["EMAIL"] as? String ?? "",
                                    password: user["PASSWORD"] as? String ?? "",
                                    phoneNumber: user["PHONE_NUMBER"] as? String ?? "",
                                    countryId: Int(user["COUNTRY_ID"] as? String ?? "0"),
                                    countryName: user["COUNTRY"] as? String ?? "",
                                    cityId: Int(user["CITY_ID"] as? String ?? "0"),
                                    cityName: user["CITY"] as? String ?? "",
                                    stateId: Int(user["STATE_ID"] as? String ?? "0"),
                                    stateName: user["STATE"] as? String ?? "",
                                    streetName: user["STREET"] as? String ?? "",
                                    zipCode: user["ZIP_CODE"] as? String ?? "",
                                    userType: user["USER_TYPE"] as? String ?? "",
                                    overviewText: user["OVERVIEW_TEXT"] as? String ?? "",
                                    overviewVideoLink: user["OVERVIEW_VIDEO_LINK"] as? String ?? "",
                                    bussinessIndustryID: user["BUSSINESS_INDUSTRY_ID"] as? String ?? "",
                                    invitedExpertName:user["NAME"] as? String ?? "",
                                    genderId:Int(user["GENDER_ID"] as? String ?? "0") ?? 0,
                                    dateOfBirth:user["NAME"] as? String ?? "",
                                    headLine:user["HEADLINE"] as? String ?? "",
                                    imageURL: user["IMAGE_URL"] as? String ?? "")
            }
        }
       
        return object
        
    }

    
    
    static func parseExpertProfileFromResponse(from response:[String:Any]) -> ProfileObject?{
        
        var expertProfileObject = ProfileObject()
        
            if let expertProfile = response["Expert"] as? [String:Any]{
                
                expertProfileObject = ProfileObject(withUserExpert:expertProfile["EXPERT_ID"] as? Int ?? 0,
                                                    profileFirstName: expertProfile["FIRST_NAME"] as? String ?? "",
                                                    profileSecondName: expertProfile["LAST_NAME"] as? String ?? "",
                                                    profileJopTitle: "",
                                                    expertCountry: "",
                                                    expertCity: "",
                                                    expertOverallRating:Int(expertProfile["EXPERT_OVERALL_RATING"] as? String ?? "0"),
                                                    expertStatusId: Int(expertProfile["EXPERT_STATUS_ID"] as? String  ?? ""),
                                                    linkedinURL: expertProfile["FIRST_NAME"] as? String,
                                                    nationalityId: Int(expertProfile["NATIONALITY_ID"] as? String  ?? ""),
                                                    nationalityName: expertProfile["NATIONALITY_NAME"] as? String  ?? "",
                                                    earilyRegistered: expertProfile["EARLY_REGISTERED_FLAG"] as? String  ?? "",
                                                    experienceObject: nil,
                                                    educationObject: nil,
                                                    certificationObject: nil,
                                                    reviews:nil,
                                                    badges:nil)
                
        }
      
        return expertProfileObject
    }
    
    static func parseBusinessProfileFromResponse(from response:[String:Any]) -> ProfileObject?{
        
        var businessProfileObject = ProfileObject()
        
        
        if let businessProfile = response["Client"] as? [String:Any]{
            
            businessProfileObject = ProfileObject(withUserBusiness: businessProfile["ID"] as? Int ?? 0,
                                                  businessName: businessProfile["COMPANY_NAME"] as? String ?? "",
                                                  businessIndustry: businessProfile[""] as? String ?? "",
                                                  businessHeadquarters: businessProfile[""] as? String ?? "",
                                                  aboutUs: businessProfile[""] as? String ?? "",
                                                  founded: businessProfile["FOUNDED"] as? String ?? "",
                                                  typeOfBusiness: businessProfile[""] as? String ?? "",
                                                  specialties: businessProfile[""] as? String ?? "")
        
        
        
        }
        
        return businessProfileObject
    }
    
    
    //MARK: - Parse Grades Object
    
    static func parseGradeOjectArray(from response:[Any]) -> [EducationGradeObject] {
        
        var gradesArray = [EducationGradeObject]()
        
        for respinseDictionary in response {
            
            if let responseDictionary = respinseDictionary as? [String:Any]{
                
                let grades = self.parseGradeOjectArray(from: responseDictionary)
                
                gradesArray.append(grades)
                
            }
            
        }
        
        return gradesArray
    }
    
    static func parseGradeOjectArray(from response:Any) -> EducationGradeObject {
        
        var object = EducationGradeObject()
        
        if let grades = response as? [String:Any] {
            
            object = EducationGradeObject(id: grades["ID"] as? Int, gradeName: grades["NAME"] as? String)
            
        }
        
        return object
        
    }
    
    
    //MARK: - Parse Degrees Object
    
    static func parseDegreesOjectArray(from response:[Any]) -> [EducationDegreeObject] {
        
        var degreesTypeArray = [EducationDegreeObject]()
        
        
        for respinseDictionary in response {
            
            if let responseDictionary = respinseDictionary as? [String:Any]{
                
                let degrees = self.parseDegreesOjectArray(from: responseDictionary)
                
                degreesTypeArray.append(degrees)
                
            }
            
        }
        
        return degreesTypeArray
    }
    
    static func parseDegreesOjectArray(from response:Any) -> EducationDegreeObject {
        
        var object = EducationDegreeObject()
        
        if let degrees = response as? [String:Any] {
            
            object = EducationDegreeObject(id: Int(degrees["ID"] as? String ?? "0"),
                                           degreeName: degrees["NAME"] as? String ?? "",
                                           descriptionText: degrees["DESCRIPTION"] as? String ?? "")
        }
        
        return object
        
    }
    
    

    //MARK: - Parse business Types Object 
    
    static func parseBusinessTypeOjectArray(from response:[Any]) -> [BusinessTypeObject] {
        
        var businessTypeArray = [BusinessTypeObject]()
        
        
        for respinseDictionary in response {
            
            if let responseDictionary = respinseDictionary as? [String:Any]{
                
                
                let business = self.parseBusinessTypeOject(from: responseDictionary)
                
                businessTypeArray.append(business)
              
            }
            
        }
        
        return businessTypeArray
    }
    

    static func parseBusinessTypeOject(from response:Any) -> BusinessTypeObject {
        
        var object = BusinessTypeObject()
        
        if let businessType = response as? [String:Any] {
            
            object = BusinessTypeObject(id: Int(businessType["ID"] as? String ?? ""),
                                        name: businessType["INDUSTRY"] as? String ?? "")
        }
        
        return object
        
    }
    
    //MARK: - parsing staetes Object
    
    static func parseStateObjectArray(from response:[Any])-> [StateObject] {
        
        var stateArray = [StateObject]()
        
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String:Any]{
                
                let staet = self.parseStateObjectArray(from: responseDictionary)
                
                stateArray.append(staet)
            }
        }
        
        return stateArray
    }
    
    
    class func parseStateObjectArray(from response:Any) -> StateObject {
        
        var object = StateObject()
        
        
        if let state = response as? [String:Any] {
            
            object = StateObject(id: Int(state["ID"] as? String ?? ""),
                                 name: state["NAME"] as? String ?? "",
                                 cityId: Int(state["CITY_ID"] as? String ?? ""))
            
        }
        
        return object
    
    }

    
    
    //MARK: - parsing Country Object

    static func parseCountryObjectArray(from response:[Any])-> [CountryObject] {
        
        var countryArray = [CountryObject]()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String:Any]{
                
                let country = self.parseCountryObject(from :responseDictionary)
                
                countryArray.append(country)
                
            }
        }
        
        
        return countryArray
        
    }
    
    
    class func parseCountryObject(from response:Any?) -> CountryObject {
        
        var object =  CountryObject()
        
        if let country = response as? [String:Any] {
            
            object = CountryObject(id: Int(country["ID"] as? String ?? ""),
                                   code: country["CODE"] as? String ?? "",
                                   name: country["NAME"] as? String ?? "")
        }
        
        return object
        
    }
    
    //MARK: - parsing city object
    
    static func parseCityObjectArray(from response:[Any])-> [CityObject] {
        
        var cityArray = [CityObject]()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String:Any]{
                
                let city = self.parseCityObject(response :responseDictionary)
                
                cityArray.append(city)
            
            }
        
        }
        
        return cityArray
        
    }
    

    class func parseCityObject(response:Any?) -> CityObject{
        
        var object = CityObject()
        
        if let city = response as? [String:Any] {
            
            object = CityObject(id: Int(city["ID"] as? String ?? "") ,
                                name: city["NAME"] as? String ?? "" ,
                                countryId: Int(city["COUNTRY_ID"] as? String ?? ""))
        }
        
        return object
    }
    

 
    
    
//     MARK: - List of bidrs Parse
    
    static func parseExpertBiderListFromResponseArray(from response:[Any]) -> [ProjectBidsObject?]{
        
        var projectBidObject = ProjectBidsObject()
        var projectBidArray = [ProjectBidsObject]()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String: Any]{
                
                projectBidObject = ProjectBidsObject(bidId: responseDictionary["ID"] as? String,
                                                     projectName:responseDictionary["PROJECT_NAME"] as? String ?? "Test",
                                                     expertInfo: nil,
                                                     bidAmount: responseDictionary["AMOUNT"] as? String,
                                                     bidStatus: responseDictionary["STATUS_ID"] as? String,
                                                     date: responseDictionary["DATE"] as? String)
                
                if let expertInfo = responseDictionary["Expert"] as? [String:Any]{
                    
                 projectBidObject.expertInfo = ExpertInfo(name: expertInfo["NAME"] as? String,
                                                          imageURL: expertInfo["IMAGE_URL"] as? String,
                                                          country: expertInfo["COUNTRY"] as? String,
                                                          city: expertInfo["CITY"] as? String,
                                                          expertRating: expertInfo["EXPERT_OVERALL_RATING"] as? String)
                    
                }
                
                
                
                
                
            }
            projectBidArray.append(projectBidObject)
        }
        return projectBidArray
    }
    
    
    
    
    
//     MARK: - Project Parser
    
    static func parseProjectObjectArray(from response:[Any])-> [ProjectObject?] {
        
        var projectArray = [ProjectObject]()
        
        for responseDictionary in response {
            
            if let responseDictionary = responseDictionary as? [String: Any]{
                
                let project = self.parseProjectObject(response: responseDictionary)
                
                projectArray.append(project)
                
            }
        }
        
        return projectArray
    }
    
    static func parseProjectDetailsObjectArray(from response:[Any])-> ProjectObject? {
        
        let projectObject = ProjectObject()
        
        for responseDictionary in response {
            
           // if let responseDictionary = responseDictionary as? [String: Any]{
                
                //let project = self.parseProjectObject(response: responseDictionary)
                
         //   }
        }
        
        return projectObject
    }
    
    class func parseProjectDetailsObject(response:[Any]) -> ProjectObject {
        
        var object = ProjectObject()
        var projectBidsObjectArray = [ProjectBidsObject]()
        
        for projectArray in response {
            
            if let project = projectArray as? [String : Any]{
                
                object = ProjectObject(id: project["ID"] as? Int ?? 0,
                                       title: project["TITLE"] as? String ?? "" ,
                                       projectType: "",
                                       businessID: Int(project["CLIENT_ID"] as? String ?? ""),
                                       businessName: "",
                                       imageURL: project["IMAGE_URL"] as? String ?? "",
                                       addingDate: project["ADDING_DATE"] as? String ?? "",
                                       openUntilDate: project["OPEN_UNTIL_DATE"] as? String ?? "",
                                       actualBeginDate: project["ACTUAL_BEGIN_DATE"] as? String ?? "",
                                       actualEndDate: project["ACTUAL_END_DATE"] as? String ?? "",
                                       budget: project["BUDGET"] as? String ?? "",
                                       bidNumber: project["BID_NUMBER"] as? Int ?? 0,
                                       bidAverage: project["BID_AVERAGE"] as? Double ?? 0.0,
                                       
                                       invitesExpert:[UserObject](),
                                       
                                       objectives: project["OBJECTIVE"] as? String ?? "",
                                       goalsAchievement: project["GOAL_ACHIEVEMENTS"] as? String ?? "",
                                       goalsDescription: project["GOAL_DESCRIPTION"] as? String ?? "",
                                       goalsNotes: project["GOAL_NOTES"] as? String ?? "",
                                       seccionDuration: project["SESSION_DURATION"] as? String ?? "",
                                       expectedNumber: project["EXPECTED_NUMBER"] as? String ?? "",
                                       projectStatusId: project["PROJECT_STATUS_ID"] as? String ?? "",
                                       preference: project["PREFERENCE"] as? String ?? "",
                                       paymentMethodID: project["PAYMENT_METHOD_ID"] as? String ?? "",
                                       sessionFrequencyRate: project["SESSION_FREQUENCY_RATE"] as? String ?? "",
                                       budgetRate: project["BUDGET_RATE"] as? String ?? "",
                                       experinceYear: project["EXPERIENCE_YEARS"] as? String ?? "",
                                       candidateDescription: project["CANDIDATE_DESCRIPTION"] as? String ?? "",
                                       proSeniority:nil,
                                       client: "",
                                       user: [UserObject()],
                                       projectBids: nil,
                                       projectCategory: nil,
                                       paymentMethod: nil,
                                       sessionLanguage: nil,
                                       contentLanguage: nil,
                                       trainerGender: nil,
                                       businessIndustry: nil,
                                       country: nil,
                                       city: nil,
                                       isOpend: false,
                                       isWatchList:false)
                
                if let sessionLanguage = project["SESSION_LANGUAGE"] as? [String:Any]{
                    
                    object.sessionLanguage = LanguagesObject(id: Int(sessionLanguage["ID"] as? String ?? "0"),
                                                             name: sessionLanguage["NAME"] as? String)
                }
                
                if let clientName = project["CLIENT"] as? [String:Any]{
                    
                   object.businessName = clientName["COMPANY_NAME"] as? String
                }
                
                if let contentLanguage = project["CONTENT_LANGUAGE"] as? [String:Any]{
                    
                    object.contentLanguage = LanguagesObject(id: Int(contentLanguage["ID"] as? String ?? "0"),
                                                             name: contentLanguage["NAME"] as? String)
                }
                
                if let trainerGender = project["TRAINER_GENDER"] as? [String:Any]{
                    
                    object.trainerGender = GenderObject(id: Int(trainerGender["ID"] as? String ?? "0"),
                                                        genderName: trainerGender["NAME"] as? String)
                }
                
                if let country = project["COUNTRY"] as? [String:Any]{
                    
                    object.country = CountryObject(id: Int(country["ID"] as? String ?? "0"),
                                                   code: country["CODE"] as? String,
                                                   name: country["NAME"] as? String)
                    
                }
                
                if let city = project["CITY"] as? [String:Any]{
                    
                    object.city = CityObject(id: Int(city["ID"] as? String ?? "0"),
                                             name: city["NAME"] as? String,
                                             countryId: Int(city["COUNTRY_ID"] as? String ?? "0"))
                    
                }
                
                if let businessIndustry = project["BUSINESS_INDUSTRY"] as? [String:Any]{
                    
                    object.businessIndustry = BusinessTypeObject(id: Int(businessIndustry["ID"] as? String ?? "0"),
                                                                 name:  businessIndustry["INDUSTRY"] as? String)
                }
                
                if let paymentMethod = project["PAYMENT_METHOD"] as? [String:Any]{
                    
                    object.paymentMethod = PaymentMethodsObject(id: Int(paymentMethod["ID"] as? String ?? "0"),
                                                                name: paymentMethod["NAME"] as? String,
                                                                descriptionText: paymentMethod["DESCRIPTION"] as? String)
                }
                
                if let projectCategory = project["PROJECT_CATEGORY"] as? [String:Any]{
                    
                    object.projectCategory = ProjectCategoryObject(id: Int(projectCategory["ID"] as? String ?? "0"),
                                                                   name: projectCategory["NAME"] as? String,
                                                                   projectTypeId: Int(projectCategory["PROJECT_TYPE_ID"] as? String ?? "0"))
                    
                }
                
                
                if let proSeniority = project["PRO_SENIORITY"] as? [String:Any]{
                    
                    object.proSeniority = ProfssionalSeniorityObject(id: Int(proSeniority["ID"] as? String ?? "0"),
                                                                     name: proSeniority["NAME"] as? String,
                                                                     seniorityDescription:proSeniority["DESCRIPTION"] as? String)
                }
                
                if let projectType = project["PROJECT_TYPE"] as? [String:Any]{
                    
                    object.projectType = projectType["TYPE"] as? String
                }
                
                if let projectBidsUser = project["PROJECT_BIDS"] as? [Any]{
                    
                    for userBiderArray in projectBidsUser {
                        
                        if let userBid = userBiderArray as? [String:Any]{
                            let bidObject = ProjectBidsObject(bidId: "\(userBid["ID"] as? Int ?? 0)",
                                projectName: "",
                                expertInfo: nil,
                                bidAmount: userBid["AMOUNT"] as? String,
                                bidStatus: userBid["STATUS_ID"] as? String,
                                date: userBid["DATE"] as? String)
                            if let expertInfo = userBid["EXPERT"] as? [String:Any]{
                                
                                bidObject.expertInfo = ExpertInfo(name: expertInfo["NAME"] as? String,
                                                                  imageURL: expertInfo["IMAGE_URL"] as? String,
                                                                  country:  expertInfo["COUNTRY"] as? String,
                                                                  city:  expertInfo["CITY"] as? String,
                                                                  expertRating:  expertInfo["EXPERT_OVERALL_RATING"] as? String)
                            }
                           
                            projectBidsObjectArray.append(bidObject)
                            
                        }
                        
                    }
                    object.projectBids = projectBidsObjectArray
                }
                
            }
        }
        
        return object
    }
    
    
    
    
    class func parseProjectObject(response:Any?) -> ProjectObject {
    
        var object = ProjectObject()
      //  var proSeniorityArray = [ProfssionalSeniorityObject]()
        
        
        if let project = response as? [String : Any]{
            
            object = ProjectObject(id: project["ID"] as? Int ?? 0,
                                   title: project["TITLE"] as? String ?? "" ,
                                   projectType: project["PROJECT_TYPE"] as? String ?? "",
                                   businessID: Int(project["CLIENT_ID"] as? String ?? ""),
                                   businessName: project["COMPANY_NAME"] as? String ?? "",
                                   imageURL: project["IMAGE_URL"] as? String ?? "",
                                   addingDate: project["ADDING_DATE"] as? String ?? "",
                                   openUntilDate: project["OPEN_UNTIL_DATE"] as? String ?? "",
                                   actualBeginDate: project["ACTUAL_BEGIN_DATE"] as? String ?? "",
                                   actualEndDate: project["ACTUAL_END_DATE"] as? String ?? "",
                                   budget: project["BUDGET"] as? String ?? "",
                                   bidNumber: project["BID_NUMBER"] as? Int ?? 0,
                                   bidAverage: project["BID_AVERAGE"] as? Double ?? 0.0,
                                   
                                   invitesExpert:[UserObject](),
                                   
                                   objectives: project["OBJECTIVE"] as? String ?? "",
                                   goalsAchievement: project["GOAL_ACHIEVEMENTS"] as? String ?? "",
                                   goalsDescription: project["GOAL_DESCRIPTION"] as? String ?? "",
                                   goalsNotes: project["GOAL_NOTES"] as? String ?? "",
                                   seccionDuration: project["SESSION_DURATION"] as? String ?? "",
                                   expectedNumber: project["EXPECTED_NUMBER"] as? String ?? "",
                                   projectStatusId: project["PROJECT_STATUS_ID"] as? String ?? "",
                                   preference: project["PREFERENCE"] as? String ?? "",
                                   paymentMethodID: project["PAYMENT_METHOD_ID"] as? String ?? "",
                                   sessionFrequencyRate: project["SESSION_FREQUENCY_RATE"] as? String ?? "",
                                   budgetRate: project["BUDGET_RATE"] as? String ?? "",
                                   experinceYear: project["EXPERIENCE_YEARS"] as? String ?? "",
                                   candidateDescription: project["CANDIDATE_DESCRIPTION"] as? String ?? "",
                                   proSeniority:nil,
                                   client: "",
                                   user: [UserObject()],
                                   projectBids: nil,
                                   projectCategory: nil,
                                   paymentMethod: nil,
                                   sessionLanguage: nil,
                                   contentLanguage: nil,
                                   trainerGender: nil,
                                   businessIndustry: nil,
                                   country: nil,
                                   city: nil,
                                   isOpend: false,
                                   isWatchList:false)
            
            if let projectCatgory = project["PROJECT_CATEGORY"] as? [Any]{
                
            }
            
            if let isInWatchList = project["IN_WATCHLIST"] as? Int{
                
                if isInWatchList == 1{
                    
                    object.isWatchList = true
                }
            }
            
//            if let city = project["CITY"] as? [Any]{
//
//                object.city?.append(self.parseCityObject(response: city))
//            }
            
//            if let proSeniority = project["PRO_SENIORITY"] as? [Any]{
//
//            }
            
            if let inviteList = project["INVITES"] as? [Any]{
                
                object.invitesExpert?.append(self.parseExpertUserObjectFromResponseArray(response:inviteList))

            }
            
        }
        
        return object
        
    }
   
    static func parseWatchListArray(response:[String:Any?])->[WatchListProjectObject]{
        
        var array = [WatchListProjectObject]()
        
        if let watchListArray = response["WatchList"] as? [String : Any]{
            
            var watchlist = WatchListProjectObject(id:watchListArray["ID"] as? Int ?? 0,
                                                   expertId: Int(watchListArray["ExpertId"] as? String ?? "0") ,
                                                   projectId: Int(watchListArray["ProjectId"] as? String ?? "0"))
            
            array.append(watchlist)
        }
        
        return array
        
    }
    
    
    
   
    // MARK: - Experiences Parser

    static func parseExperiencesArray(response:Any?)->[ExperienceObject]{
        
        var array = [ExperienceObject]()
        
        if let experiences = (response as? [String : Any])?["Experience"] as? NSArray {
            
            for experience in experiences {
                
                let experienceObject = self.parseExperienceObject(response: experience)
                array.append(experienceObject)
                
            }
            
        }
        
        return array
        
    }
    
    static func parseExperiencesListArray(response:Any?)->[ExperienceObject]{
        
        var array = [ExperienceObject]()
        
        if let experiences = response as? [[String:Any]] {
            
            for experience in experiences {
                
                let experienceObject = self.parseExperienceObject(response: experience)
                array.append(experienceObject)
                
            }
            
        }
        
        return array
        
    }
    
    static func parseExperienceObject(response:Any?)->ExperienceObject{
        
        var object = ExperienceObject()
        
        if let expereince = response as? [String:Any]{
            
            object = ExperienceObject(id: expereince["ID"] as? Int ?? 0 ,
                                                    experienceImage: nil,
                                                    experienceTitle: expereince["TITLE"] as? String ?? "",
                                                    experienceCompanyName: expereince["COMPANY"] as? String  ?? "",
                                                    experienceCountry:  nil,
                                                    experienceCity:  nil,
                                                    experienceDateFrom: expereince["DATE_FROM"] as? String  ?? "",
                                                    experienceDateTo: expereince["DATE_TO"] as? String  ?? "",
                                                    isWorkHere: expereince["CURRENTLY_WORKING"] as? String ?? "",
                                                    cityId: Int(expereince["CITY_ID"] as? String  ?? "0"),
                                                    countryId: Int(expereince["COUNTRY_ID"] as? String ?? "0"))
            
            let countryObject = self.parseCountryObject(from: expereince["Country"])
            let cityObject = self.parseCityObject(response: expereince["City"])
            object.experienceCountry = countryObject.name
            object.experienceCity = cityObject.name
            
        }
        
        
        return object
        
    }
    
    //MARK: - Education Parser
    
    static func parseEducationListArray(response:Any?)->[EducationObject]{
        
        var array = [EducationObject]()
        
        if let educations = response as? [[String:Any]] {
            
            for education in educations {
                
                let educationObject = self.parseEducationObject(response: education)
                array.append(educationObject)
                
            }
            
        }
        
        return array
        
    }
    
    static func parseEducationArray(response:Any?)->[EducationObject]{
        
        var array = [EducationObject]()
        
        if let educations = (response as? [String : Any])?["Education"] as? NSArray {
            
            for education in educations {
                
                let educationObject = self.parseEducationObject(response: education)
                array.append(educationObject)
                
            }
            
        }
        
        return array
        
    }
    
    static func parseEducationObject(response:Any?)->EducationObject{
        
        var object = EducationObject()
        
        if let education = response as? [String:Any]{
            
            var countryName: String?
            var cityName :String?
            var degreeName : String?
            
            if let countryResponse = education["Country"] as? [String: Any] {
                countryName = countryResponse["NAME"] as? String
            }
            
            if let cityResponse = education["City"] as? [String:Any]{
                cityName = cityResponse["NAME"] as? String
            }
            
            if let degreeResponse = education["Degree"] as? [String:Any]{
                degreeName = degreeResponse["NAME"] as? String
            }
            
            object = EducationObject(id: education["ID"] as? Int ?? 0,
                                     educationImage: "",
                                     educationSchoolName: education["SCHOOL"] as? String ?? "",
                                     educationCountry: countryName,
                                     educationCity: cityName,
                                     educationDegreeId: education["DEGREE_ID"] as? String ?? "",
                                     educationFieldOfStudy: education["FIELD_OF_STUDY"] as? String ?? "",
                                     educationDegree: degreeName,
                                     educationCompletionOn: education["YEAR"] as? String ?? "",
                                     gradeId: education["GRADE_ID"] as? Int,
                                     cityId: Int(education["CITY_ID"] as? String ?? "0"),
                                     countryId: Int(education["COUNTRY_ID"] as? String  ?? "0"))
         
        }
       
        return object
    }
    
    static func parseEducationObjectResponse(response:Any?)->EducationObject{
        
        var object = EducationObject()
        var countryName: String?
        var cityName :String?
        var gradeName:String?
        
        if let responseArray = response as? [String:Any] {
            
            if let education = responseArray["Education"] as? [String:Any] {
                
                if let countryResponse = education["country"] as? [String: Any] {
                    countryName = countryResponse["NAME"] as? String
                }
                if let cityResponse = education["city"] as? [String:Any]{
                    cityName = cityResponse["NAME"] as? String
                }
                
                if let gradeResponse = education["grade"] as? [String:Any]{
                    gradeName = gradeResponse["NAME"] as? String
                }
                
                object = EducationObject(id: education["ID"] as? Int ?? 0,
                                         educationImage: "",
                                         educationSchoolName: education["SCHOOL"] as? String ?? "",
                                         educationCountry: countryName,
                                         educationCity: cityName,
                                         educationDegreeId: education["DEGREE_ID"] as? String ?? "",
                                         educationFieldOfStudy: education["FIELD_OF_STUDY"] as? String ?? "",
                                         educationDegree: gradeName,
                                         educationCompletionOn: education["YEAR"] as? String ?? "",
                                         gradeId: Int(education["GRADE_ID"] as? String ?? ""),
                                         cityId: Int(education["CITY_ID"] as? String ?? "0"),
                                         countryId: Int(education["COUNTRY_ID"] as? String  ?? "0"))
                
               
            }
           
        }
        
        return object
    }
    
    
    //MARK: - Certification Parser
    
    static func parseCertificationArray(response:Any?)->[CertificationObject]{
        
        var array = [CertificationObject]()
        
        if let certifications = (response as? [String : Any])?["Certification"] as? NSArray {
            
            for certification in certifications {
                
                let certificationObject = self.parseCertificationObject(response: certification)
                array.append(certificationObject)
                
            }
            
        }
        
        return array
        
    }
    
    static func parseCertificationObject(response:Any?)->CertificationObject{
        
        var object = CertificationObject()
        
        if let certification = response as? [String:Any]{
            
            object = CertificationObject(id: certification["ID"] as? Int ?? 0,
                                         certificationName: certification["NAME"] as? String ?? "",
                                         certificationAuthority: certification["AUTHORITY"] as? String ?? "",
                                         certificationImage: "",
                                         certificationLicenseNo: certification["LICENSE_NO"] as? String ?? "",
                                         certificationDateFrom: certification["DATE_FROM"] as? String ?? "",
                                         certificationDateTo: certification["DATE_TO"] as? String ?? "",
                                         certificationURL: certification["URL"] as? String ?? "",
                                         expireFlag: certification["EXPIRE_FLAG"] as? String ?? "",
                                         approvedFlag: certification["APPROVED_FLAG"] as? String ?? "")
            
        }
        return object
    }
    
    //MARK: - Badges Parser
    
    static func parseBadgesArray(response:Any?)->[BadgeObject]{
        
        var array = [BadgeObject]()
        
        if let badges = (response as? [String : Any])?["Badge"] as? NSArray {
            
            for badge in badges {
                
                let badgeObject = self.parseBadgeObject(response: badge)
                array.append(badgeObject)
                
            }
            
        }
        
        return array
        
    }
    
    static func parseBadgeObject(response:Any?)->BadgeObject{
        
        var object = BadgeObject()
        
        if let badge = response as? [String:Any]{
            
            object = BadgeObject(experdiceBadgeId: badge["EXPERDICE_BADGE_ID"] as? Int  ?? 0,
                                 badgeName: badge["NAME"] as? String ?? "",
                                 badgeDescription: badge["DESCRIPTION"] as? String ?? "")
            
        }
        
        return object
    }
    
    
    //MARK: - Review Parser
    
    static func parseReviewArray(response:Any?)->[ReviewObject]{
        
        var array = [ReviewObject]()
      
        if let reviews = (response as? [String : Any])?["Review"] as? NSArray {
            
            for review in reviews {
                
                let reviewObject = self.parseReviewObject(response: review)
                array.append(reviewObject)
                
            }
        }
        return array
        
    }
    
    static func parseReviewObject(response:Any?)->ReviewObject{
        
        var object = ReviewObject()
        
        if let review = response as? [String:Any]{
            
            object = ReviewObject(id: review["ID"] as? Int ?? 0,
                                  reviewTitle: review["TITLE"] as? String  ?? "",
                                  reviewDescription: review["DESCRIPTION"] as? String ?? "",
                                  ratingId: review["RATING_ID"] as? Int ?? 0,
                                  ratingLevel: review["RATING_LEVEL"] as? String ?? "",
                                  businessId: review["CLIENT_ID"] as? Int ?? 0,
                                  businessName: review["COMPANY_NAME"] as? String ?? "")
        }
        
        return object
    }
    
    
    static func parseReviewObjectArray(response:[Any])->[ReviewObject]{
        
        var reviewArrayReturn = [ReviewObject]()
        var object = ReviewObject()
        
        for responseDictionary in response {
            
            if let reviewDictionary = responseDictionary as? [String:Any]{
                
                if let reviewArray = reviewDictionary["Review"] as? [Any]{
                    
                    for reviewObjectArray in reviewArray {
                        
                        if let reviewObject = reviewObjectArray as? [String:Any]{
                            object = ReviewObject(id: reviewObject["ID"] as? Int ?? 0,
                                                  reviewTitle: reviewObject["TITLE"] as? String  ?? "",
                                                  reviewDescription: reviewObject["DESCRIPTION"] as? String ?? "",
                                                  ratingId: reviewObject["RATING_ID"] as? Int ?? 0,
                                                  ratingLevel: reviewObject["RATING_LEVEL"] as? String ?? "",
                                                  businessId: reviewObject["CLIENT_ID"] as? Int ?? 0,
                                                  businessName: reviewObject["COMPANY_NAME"] as? String ?? "")
                        }
                        reviewArrayReturn.append(object)
                    }
                    
                }
            }
            
            
        }
      
        return reviewArrayReturn
    }
    






}
