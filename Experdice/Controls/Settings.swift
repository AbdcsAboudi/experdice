//
//  Settings.swift
//  OutAndAbout
//
//  Created by Ayman Rawashdeh on 6/1/17.
//  Copyright © 2017 Dream Techs. All rights reserved.
//

import Foundation

class Settings: NSObject, NSCoding{
   
    var weekStart = 0
    var timeZoneIdentifier = TimeZone.current.identifier
    
    var displayToDo = true
    
    static var sharedSettings: Settings? {
        get {
            if let data = UserDefaults.standard.object(forKey: "Hers is the userDefulat settings") as? NSData {
                return NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? Settings
            }
            return nil
        }
        set{
            if let value = newValue {
                UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: value), forKey: "TypeHereSettings")
            }
        }
    }

    override init() {
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        
        weekStart = aDecoder.decodeInteger(forKey: "weekStart")
        
        if let timeZoneIdentifier = aDecoder.decodeObject(forKey: "timeZoneIdentifier") as? String {
            
            self.timeZoneIdentifier = timeZoneIdentifier
        }

    }

    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(weekStart, forKey: "weekStart")
        aCoder.encode(timeZoneIdentifier, forKey: "timeZoneIdentifier")
    }
    
    
    
    

}
