//
//  DTLinkedInManger.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 8/8/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import Foundation
import UIKit

class DTLinkedInManger {
    
    
    static let sharedInstance = DTLinkedInManger()
    
    var showGoToAppStoreDialog:Bool?
    var result:[String:Any?]?
    
    
    func loginWithLinkdein(showGoToAppStoreDialog:Bool!, onComplete: (([String: Any?]?,_ isSuccess:(Bool?)) -> Void)? ){
        
        
        LISDKSessionManager.createSession(withAuth: [LISDK_BASIC_PROFILE_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: { (success) in
            
            let url = "https://api.linkedin.com/v1/people/~"
            
            
            if (LISDKSessionManager.hasValidSession()){
                
              
              LISDKAPIHelper.sharedInstance().getRequest(url, success: { (response) in
                    
                    self.result = self.convertToDictionary(text: (response?.data)!)!
                
                //AS: Completion block for ensure does not complete this until is request and return data
                onComplete?(self.result,true)
                
              }, error: { (error) in
                print(error ?? "")
                onComplete?(self.result,false)
              })
            }
            
        }) { (error) in
            print("ERROR\(String(describing: error))")
            
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    

   
 
    
}

