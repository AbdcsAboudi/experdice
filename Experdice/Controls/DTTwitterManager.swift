////
////  DTTwitterManager.swift
////  Enatni
////
////  Created by Saad Basha on 11/2/16.
////  Copyright © 2016 Saad Basha. All rights reserved.
////
//
//
//import UIKit
//import TwitterKit
//
//class DTTwitterManager {
//    
//    
//    static let sharedInstance = DTTwitterManager()
//    
//    private init(){
//        
//        
//    }
//    
//    
//    func loginWithTwitter(viewcontroller:UIViewController,completionBlock:@escaping (_ result:NSDictionary)->(Void),failureBlock:@escaping (_ error:Error)->(Void)){
//      
//        
//        Twitter.sharedInstance().logIn(with: viewcontroller, completion: { session, error in
//            if (session != nil) {
//                
//                let client = TWTRAPIClient.withCurrentUser()
//                let request = client.urlRequest(withMethod: "GET",
//                                                url: "https://api.twitter.com/1.1/account/verify_credentials.json",
//                                                parameters: ["include_email": "true", "skip_status": "true"],
//                                                error: nil)
//                
//                client.sendTwitterRequest(request) { response, data, connectionError in
//                    
//                    print("response : \(response)");
//                    
//                    if error != nil {
//                        
//                        failureBlock(error!)
//                        
//                    }else {
//                        
//                        do {
//                            let json = try JSONSerialization.jsonObject(with: data!, options: [])
//                            
//                            print("json: \(json)")
//                            
//                            let jsonDictionary = json as! NSDictionary
//                            
//                            let id : Int = jsonDictionary.object(forKey: "id") as! Int
//                            
//                            var email = ""
//                            if let twitterEmail  = jsonDictionary.object(forKey:"email") as? String {
//                                
//                                email = twitterEmail
//                                
//                            }else {
//                                
//                                // havent recieved email
//                                //
////                                failureBlock(NSError(domain: "emailNotFound", code: 1, userInfo: nil))
////                                return
//                            }
//                            
//                            let name : String = jsonDictionary.object(forKey:"name") as! String
//                            let username : String = jsonDictionary.object(forKey:"screen_name") as! String
//                            let profile_image_url : String = jsonDictionary.object(forKey:"profile_image_url") as! String
//                            
//                            let readyResult : NSDictionary =  ["picture":profile_image_url,"email":email,"userID":session!.userID,"name":name,"username":username,"id":id,"token":session!.authToken]
//                            
//                            
//                            completionBlock(readyResult)
//                            
//                            
//                        } catch  {
//                            
//                            // error
//                            failureBlock(NSError(domain: "jsonError", code: 2, userInfo: nil))
//                            
//                        }
//                        
//                        
//                    }
//                    
//                }
//                
//                
//            } else {
//                print("error: \(error?.localizedDescription)");
//                failureBlock(error!)
//
//            }
//        })
//        
//
//        
//        
//    }
//    
//    
//    
//    
//}
