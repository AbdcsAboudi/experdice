//
//  DTFacebookManager.swift
//  Enatni
//
//  Created by Saad Basha on 11/2/16.
//  Copyright © 2016 Saad Basha. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class DTFacebookManager {
    
    
    static let sharedInstance = DTFacebookManager()
    var didAskFacebookPermission : Bool?
    
    private init(){
    
        didAskFacebookPermission = checkFacebookAccessPermission()
    
    }
    
  
    private func checkFacebookAccessPermission()->Bool{
    
        let accessToken = FBSDKAccessToken.current()
        let permissions = accessToken?.permissions
        
        if let _ = permissions?.contains("public_profile") {
        
            return true

        }else {
        
            return false
        
        }
        
    
    }
    
    func loginWithFacebook(viewcontroller:UIViewController,completionBlock:@escaping (_ result:NSDictionary)->(Void),failureBlock:@escaping (_ error:Bool)->(Void)){
    
        DispatchQueue.main.async {
            
            
            let login = FBSDKLoginManager()
            login.loginBehavior = .browser
            
            login.logIn(withReadPermissions: ["public_profile", "email", "user_friends", "user_events"], from: viewcontroller) { (result, error) in
                
                if (error != nil) {
                    failureBlock(true) // Process error
                } else if (result?.isCancelled)! {
                    failureBlock(true) // Cancelled
                } else {
                    
                    print("Logged in")
                    
                    print("RESULT%@",result!)
                    
                    // check token
                    if FBSDKAccessToken.current() != nil {
                        
                        print("Token is available : %@",FBSDKAccessToken.current().tokenString)
                        
                        // FBSDKGraphRequest handler and data parser**
                        
                        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, link, first_name, last_name, picture.type(large), email, birthday ,location ,friends ,hometown , friendlists , gender"]).start(completionHandler: { (connection, result, error) -> Void in
                            
                            if (error == nil){
                                
                                print("RESULT%@",result!)
                                
                                let result : NSDictionary = result as! NSDictionary
                                
                                let picture  = result.object(forKey: "picture") as! NSDictionary
                                let data  = picture.object(forKey: "data") as! NSDictionary
                                let imageURL = data.object(forKey: "url") as? String ?? ""
                                let email = result.object(forKey: "email") as? String ?? ""
                                let birthdate = result.object(forKey: "birthday") as? String ?? ""
                                let gender = result.object(forKey: "gender") as? String ?? ""
                                let first_name = result.object(forKey: "first_name") as? String ?? ""
                                let last_name = result.object(forKey: "last_name") as? String ?? ""
                                let id = result.object(forKey: "id") as? String ?? ""
                                
                                
                                let readyResult : NSDictionary =  ["picture": imageURL,"email":email,"birthdate":birthdate,"gender":gender,"first_name":first_name,"last_name":last_name,"id":id,"token":FBSDKAccessToken.current().tokenString]
                                
                                completionBlock(readyResult)

                                
                            }else {
                                
                                failureBlock(true) // Error
                                print("Error %@",error as Any)
                                
                            }
                        })
                        // FBSDKGraphRequest handler and data parser**
                        
                    }
                    
                }
                
            }

            
        }
    
        
    }
    
    
    
    
}
