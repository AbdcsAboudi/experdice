//
//  DTExtensions.swift
//  Experdice
//
//  Created by Ahmad Sallam  on 7/31/17.
//  Copyright © 2017 DreamTech. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import QuartzCore
import Foundation
import CoreLocation
import MapKit
import AFNetworking
import MBProgressHUD


//MARK: - enum FontType
public enum FontType : Int {
    case bold
    case extraBold
    case light
    case regular
    
}



//MARK: - Navigation Extentions

public extension UIWindow {
    
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController  = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(rootViewController)
        }
        return nil
    }
    
    class func getVisibleViewControllerFrom(_ vc:UIViewController) -> UIViewController {
        
        if vc.isKind(of: UINavigationController.self) {
            
            let navigationController = vc as! UINavigationController
            return UIWindow.getVisibleViewControllerFrom( navigationController.visibleViewController!)
            
        } else if vc.isKind(of: UITabBarController.self) {
            
            let tabBarController = vc as! UITabBarController
            return UIWindow.getVisibleViewControllerFrom(tabBarController.selectedViewController!)
            
        } else {
            
            if let presentedViewController = vc.presentedViewController {
                
                return UIWindow.getVisibleViewControllerFrom(presentedViewController)
                
            } else {
                
                return vc;
            }
        }
    }
    
    
}

//MARK: - Arrays Extentions

public extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}


//MARK: - Strings and Fonts Extentions

public extension Character
{
    func unicodeScalarCodePoint() -> UInt32
    {
        let characterString = String(self)
        let scalars = characterString.unicodeScalars
        
        return scalars[scalars.startIndex].value
    }
}

public extension String {
    
    enum AttributedStringType {
        case title
        case titleRegular
        case description
        case storyDescription
        case message
    }
    public func htmlAttributedString(stringType: AttributedStringType,fontName:String,sizeFont: CGFloat,withColor color:UIColor) -> NSAttributedString? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil) else { return nil }
        
        var attributes:[String: Any]?
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        
        switch stringType {
            
        case .title:
            
            attributes = [NSFontAttributeName: UIFont(name: "\(fontName)-Bold", size: sizeFont) ?? UIFont.systemFont(ofSize: sizeFont), NSForegroundColorAttributeName: color, NSParagraphStyleAttributeName: paragraphStyle ]
            
        case .titleRegular:
            
            attributes = [NSFontAttributeName: UIFont(name: "\(fontName)-Regular", size: sizeFont) ?? UIFont.systemFont(ofSize: sizeFont), NSForegroundColorAttributeName: color , NSParagraphStyleAttributeName: paragraphStyle]
            
        case .description:
            
            attributes = [NSFontAttributeName: UIFont(name: "\(fontName)-Regular", size: sizeFont) ?? UIFont.systemFont(ofSize: sizeFont), NSForegroundColorAttributeName: color, NSParagraphStyleAttributeName: paragraphStyle ]
            
        case .storyDescription:
            
            attributes = [NSFontAttributeName: UIFont(name: "\(fontName)-Bold", size: sizeFont) ?? UIFont.systemFont(ofSize: sizeFont), NSForegroundColorAttributeName: color, NSParagraphStyleAttributeName: paragraphStyle ]
            
        case .message:
            
            attributes = [NSFontAttributeName: UIFont(name: "\(fontName)-Bold", size: sizeFont) ?? UIFont.systemFont(ofSize: sizeFont), NSForegroundColorAttributeName: color, NSParagraphStyleAttributeName: paragraphStyle ]
        }
        
        
        html.addAttributes(attributes!, range:NSMakeRange(0, html.length))
        
        
        return html
    }
    
    
    public func htmlAttributedString(sizeFont: CGFloat, direction: NSTextAlignment, withColor color:UIColor) -> NSAttributedString? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil) else { return nil }
        
        var attributes:[String: Any]?
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = direction
        
        
        attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: sizeFont), NSForegroundColorAttributeName: color, NSParagraphStyleAttributeName: paragraphStyle ]
        
        
        html.addAttributes(attributes!, range:NSMakeRange(0, html.length))
        
        
        return html
    }

}

//MARK: - UILabel Extention
public extension UILabel {
    
    func setSizeFont (sizeFont: Double ,fontType:FontType,fontName:String) {
        
        switch fontType {
        case .bold:
            self.font =  UIFont(name: "\(fontName)-Bold", size: CGFloat(sizeFont))!
            break;
            
        case .extraBold:
            self.font =  UIFont(name: "\(fontName)-ExtraBold", size: CGFloat(sizeFont))!
            break;
            
        case .light:
            self.font =  UIFont(name:  "\(fontName)-Light", size: CGFloat(sizeFont))!
            break;
            
        case .regular:
            self.font =  UIFont(name:  "\(fontName)-Regular", size: CGFloat(sizeFont))!
            break;
            
        }
    }
    
  
    
}

//MARK: - UITextView
public extension UITextView {
  
    func setSizeFont (sizeFont: Double ,fontType:FontType,fontName:String) {
        
        switch fontType {
        case .bold:
            self.font =  UIFont(name: "\(fontName)-Bold", size: CGFloat(sizeFont))!
            break;
            
        case .extraBold:
            self.font =  UIFont(name: "\(fontName)-ExtraBold", size: CGFloat(sizeFont))!
            break;
            
        case .light:
            self.font =  UIFont(name:  "\(fontName)-Light", size: CGFloat(sizeFont))!
            break;
            
        case .regular:
            self.font =  UIFont(name:  "\(fontName)-Regular", size: CGFloat(sizeFont))!
            break;
            
        }
    }
    
   
}

    


public extension String {
    func convertHtmlSymbols() throws -> String? {
        guard let data = data(using: .utf8) else { return nil }
        return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil).string
    }
}


//MARK: - Image Extentions

public extension UIImage {
    
    func toBase64() -> String? {
        
        let imageData : NSData = UIImageJPEGRepresentation(self, 1.0)! as NSData
        return imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
    }
    
    var circleMask: UIImage {
        
        let square = CGSize(width: min(size.width, size.height), height: min(size.width, size.height))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 2
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    var circleMaskBlue: UIImage {
        
        let square = CGSize(width: min(size.width, size.height), height: min(size.width, size.height))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.borderColor = ColorManager.sharedInstance.getColorImageBorder().cgColor
        imageView.layer.borderWidth = 2
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    
}

public extension UIImage{
    
    func resizeToBoundingSquare(boundingSquareSideLength : CGFloat) -> UIImage{
        
        let imgScale = self.size.width > self.size.height ? boundingSquareSideLength / self.size.width : boundingSquareSideLength / self.size.height
        let newWidth = self.size.width * imgScale
        let newHeight = self.size.height * imgScale
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContext(newSize)
        
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext();
        
        return resizedImage!
    }
    
}


//MARK: - UIView Extentions

public extension UIView {
    
    public func round() {
            let width = bounds.width < bounds.height ? bounds.width : bounds.height
        let mask = CAShapeLayer()
       // mask.path = UIBezierPath(ovalInRect: CGRectMake(bounds.midX - width / 2, bounds.midY - width / 2, width, width)).cgPath
        self.layer.mask = mask
    }
    
    
    /**
     
     To cornarize any uiview
     @param corenerRadius the value of the radius
     @param borderWidth the value of the border width
     
     @author Abd Aboudi.
     
     */
    func makeCorneredViewWith(corenerRadius:CGFloat,and borderWidth:CGFloat) {
        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = corenerRadius
        self.clipsToBounds = true
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = UIColor.white.cgColor
    }
    
    func makeCorneredViewWith(corenerRadius:CGFloat,and borderWidth:CGFloat, borderColor:UIColor) {
        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = corenerRadius
        self.clipsToBounds = true
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
}



public extension UITextField {
    
    func setTextFieldPlaceHolderColor(title:String,color:UIColor){
        
        self.attributedPlaceholder = NSAttributedString(string:title, attributes: [NSForegroundColorAttributeName: color])
    }
    
    
    func setTextFieldTextColor(){
        
        self.textColor = ColorManager.sharedInstance.getTextFiledTextColor()
    }
    
    func setToolBarforTextField(title:String){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "     \(title)", style: UIBarButtonItemStyle.plain, target: self, action: #selector(resignTextField))]
        numberToolbar.sizeToFit()
        self.inputAccessoryView = numberToolbar
        
    }
    
    func setToolBarforTextField(title:String,tintColor:UIColor){
        
        let barButton = UIBarButtonItem(title: "     \(title)", style: UIBarButtonItemStyle.plain, target: self, action: #selector(resignTextField))
        barButton.tintColor = tintColor
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            barButton]
        numberToolbar.sizeToFit()
        self.inputAccessoryView = numberToolbar
        
    }
    
    func resignTextField(){
        
        self.resignFirstResponder()
        
    }
    
    
    func makeRoundedCorners(bottom:Bool,orTop:Bool) {
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        
        if bottom && orTop{
            
            rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .topRight ,.bottomLeft , .bottomRight], cornerRadii: CGSize(width: 7, height: 7)).cgPath
            
        }else if bottom {
            
            rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft , .bottomRight ], cornerRadii: CGSize(width: 7, height: 7)).cgPath
            
            
        }else if orTop{
            
            rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .topRight ], cornerRadii: CGSize(width: 7, height: 7)).cgPath
            
        }
        
        self.layer.backgroundColor = UIColor.white.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        self.layer.mask = rectShape
        
        let frameLayer = CAShapeLayer()
        frameLayer.frame = bounds
        frameLayer.path = rectShape.path
        frameLayer.strokeColor = UIColor.lightGray.cgColor
        frameLayer.fillColor = nil
        
        self.layer.addSublayer(frameLayer)
        
    }
    
}

public extension UITextView {
    
    func setToolBarforTextField(title:String){
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "     \(title)", style: UIBarButtonItemStyle.plain, target: self, action: #selector(resignTextField))]
        numberToolbar.sizeToFit()
        self.inputAccessoryView = numberToolbar
        
    }
    
    func resignTextField(){
        
        self.resignFirstResponder()
        
    }
    
}

public extension UIImage {
    
    func grayscaled() -> UIImage {
        
        let filter = CIFilter(name: "CIPhotoEffectTonal")
        
        // convert UIImage to CIImage and set as input
        
        let ciInput = CIImage(image: self)
        filter?.setValue(ciInput, forKey: "inputImage")
        
        // get output CIImage, render as CGImage first to retain proper UIImage scale
        
        let ciOutput = filter?.outputImage
        let ciContext = CIContext()
        let cgImage = ciContext.createCGImage(ciOutput!, from: (ciOutput?.extent)!)
        
        return UIImage(cgImage: cgImage!)
    }
    
    
}



extension UIImageView {
    
    func addImage(at point:CGPoint,with addedImage:UIImage){
        
        UIGraphicsBeginImageContextWithOptions((self.image?.size)!, false, 0.0)
        self.image!.draw(in: CGRect(x:0,y: 0, width:self.image!.size.width,height: self.image!.size.height))
        
        addedImage.draw(in: CGRect(x: point.x - (addedImage.size.width / 2), y: point.y - (addedImage.size.height / 2), width: addedImage.size.width, height: addedImage.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.image = newImage!
        
    }
    
     func makeCorneredImageViewWith(corenerRadius:CGFloat,and borderWidth:CGFloat, borderColor:UIColor) {
        
        self.layer.masksToBounds = false
        self.layer.cornerRadius = corenerRadius
        self.clipsToBounds = true
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }

    
}


extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}


public extension CLLocationCoordinate2D {
    
    public func transform(using latitudinalMeters: CLLocationDistance, longitudinalMeters: CLLocationDistance) -> CLLocationCoordinate2D {
        let region = MKCoordinateRegionMakeWithDistance(self, latitudinalMeters, longitudinalMeters)
        return CLLocationCoordinate2D(latitude: latitude + region.span.latitudeDelta, longitude: longitude + region.span.longitudeDelta)
    }
    
}

public extension UIColor {
    
    convenience init(hex: Int, alpha: Double = 1.0) {
        
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green:CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha:  CGFloat(255 * alpha) / 255)
    }
}

public extension Date {
    
    
    public func firstDayOfMonth() -> Date? {
        
        let components = Calendar.current.dateComponents([.year, .month, .timeZone], from: self)
        
        return Calendar.current.date(from: components)
    }
    
    public func lastDayOfMonth() -> Date? {
        
        let firstDay = firstDayOfMonth() ?? self
        
        return Calendar.current.date(byAdding: DateComponents(calendar: nil, timeZone: nil, era: nil, year: nil, month: 1, day: -1, hour: nil, minute: nil, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil), to: firstDay)
    }
}



