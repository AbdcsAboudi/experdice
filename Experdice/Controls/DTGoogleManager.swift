//
//  DTGoogleManager.swift
//
//
//  Created by Saad Basha on 1/31/17.
//  Copyright © 2017 Saad Basha. All rights reserved.
//

import Foundation
import GoogleAPIClientForREST
import GoogleSignIn
import GGLCore
import Google
//import DTHelper

 @objc protocol DTGoogleLoginDelegate :NSObjectProtocol{
    
     @objc optional func didSignInSuccessfully(_ responseDictionary:NSDictionary)
     @objc optional func didSignInWithError(_ error:NSError)
     @objc optional func didFetchGoogleEvents(_ eventsArray:[Any])
    
}

class DTGoogleManager:NSObject,GIDSignInDelegate,GIDSignInUIDelegate {
    
    private let scopes = [kGTLRAuthScopeCalendarReadonly]
    private let service = GTLRCalendarService()

    static let sharedInstance = DTGoogleManager()
    var refferenceVC = UIViewController()
    weak var delegate : DTGoogleLoginDelegate?
    
    private override init(){
        
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
    }
    
    
    func loginWithGoogle(viewcontroller:UIViewController){
    
        self.refferenceVC = viewcontroller
        
        var configureError : NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        
       
        if (configureError == nil) {
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().scopes = scopes
            GIDSignIn.sharedInstance().signIn()
        }
        
    }
    
    
    func silentLogin(viewcontroller:UIViewController){
        
        self.refferenceVC = viewcontroller
        
        var configureError : NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        
//        print("In keychain ", GIDSignIn.sharedInstance().hasAuthInKeychain())
//        print("Logged In User ", GIDSignIn.sharedInstance().currentUser ?? "No User")
        
        
        if (configureError == nil) {
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().scopes = scopes
            GIDSignIn.sharedInstance().signInSilently()
        }

    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //MARK: Google Login Delegate                                                          Google Login Delegate
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
       
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID  ?? ""                // For client-side use only!
            let idToken = user.authentication.idToken ?? ""// Safe to send to the server
            let fullName = user.profile.name ?? ""
            let givenName = user.profile.givenName ?? ""
            //let familyName = user.profile.familyName
            let email = user.profile.email ?? ""
            let userImage = NSString(format: "%@", user.profile.imageURL(withDimension: 500) as CVarArg)
            
            self.service.authorizer = user.authentication.fetcherAuthorizer()

            let readyResult : NSDictionary =  ["picture":userImage,"email":"\(email)","userID":"\(userId)","name":"\(fullName)","username":"\(givenName)","token":"\(idToken)"]

            if self.delegate != nil && self.delegate!.responds(to: #selector(DTGoogleLoginDelegate.didSignInSuccessfully(_:))){
                self.delegate!.didSignInSuccessfully!(readyResult)
            }
            
            // ...
        } else {
            
            self.service.authorizer = nil
            print("\(error.localizedDescription)")
            
            if self.delegate != nil && self.delegate!.responds(to: #selector(DTGoogleLoginDelegate.didSignInWithError(_:))){
                self.delegate!.didSignInWithError!(error as NSError)
            }
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {

        // Perform any operations when the user disconnects from app here.
        // ...
        if (error != nil) {
            print("error = %@",error.localizedDescription)
            
            if self.delegate != nil && self.delegate!.responds(to: #selector(DTGoogleLoginDelegate.didSignInWithError(_:))){
                self.delegate!.didSignInWithError!(error as NSError)
            }
            
        }
    }
    
    
    // Construct a query and get a list of upcoming events from the user calendar
    func fetchEvents(startDate: Date, endDate: Date) {
        
        let query = GTLRCalendarQuery_EventsList.query(withCalendarId: "primary")
       // query.maxResults = 10
//        
//        let startDate = Date().firstDayOfMonth()
//        let endDate: Date? =  Date().lastDayOfMonth()
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MM/yyyy dd"
//        
//        print("Start Date ", dateFormatter.string(from: startDate ?? Date()))
//        print("End Date ",   dateFormatter.string(from: endDate ?? Date()))
        
        query.timeMin = GTLRDateTime(date: startDate)
        query.timeMax = GTLRDateTime(date: endDate)
        
        query.singleEvents = true
        query.orderBy = kGTLRCalendarOrderByStartTime
        
//        service.executeQuery(
//            query,
//            delegate: self,
//            didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        if (error != nil) {
            print("error = %@",error.localizedDescription)
            
            if self.delegate != nil && self.delegate!.responds(to: #selector(DTGoogleLoginDelegate.didSignInWithError(_:))){
                self.delegate!.didSignInWithError!(error as NSError)
            }
            
        }
        
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        refferenceVC.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        refferenceVC.dismiss(animated: true, completion: nil)
    }
    
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
        
    }

    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        refferenceVC.present(viewController, animated: true, completion: nil)
    }
    
    
    
}

