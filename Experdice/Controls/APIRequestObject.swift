//
//  APIObject.swift
//  CoreDataTemplateApp
//
//  Created by Saad Basha on 1/16/17.
//  Copyright © 2017 DreamTechs. All rights reserved.
//

import UIKit

class APIRequestObject {
    
    var requestId : ApiRequestID!
    var requestParameters :Any?
    var requestType : ApiRequestType = .get
    var requestURL = ""
    var entityTableName = ""
    
    init(requestId:ApiRequestID,requestParameters:Any?,requestType:ApiRequestType,requestURL:String,entityTableName:String) {
        
        self.requestId = requestId
        self.requestParameters = requestParameters
        self.requestType = requestType
        self.requestURL = requestURL
        self.entityTableName = entityTableName
        
    }
    
}
