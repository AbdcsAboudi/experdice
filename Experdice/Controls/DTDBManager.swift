//
//  DTDBManager.swift
//  CoreDataTemplateApp
//
//  Created by DTAboudi on 1/15/17.
//  Copyright © 2017 DreamTechs. All rights reserved.
//

import UIKit
import CoreData
import Foundation

struct EntityTableNames {
    
    static let none                               = "None"
    static let person                             = "Person"
    
}

class DTDBManager: NSObject {

    static let sharedInstance = DTDBManager()
    var managedContextContainer = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    
    private override init(){
        
        //guard let appDelegate =
        //    UIApplication.shared.delegate as? AppDelegate else {
        //        return
        //}
        
        //AA : To get the DB
        managedContextContainer = CoreDataManager.sharedInstance.managedObjectContext
        
    }
    
    //MARK: - Get Cashed Gate
    
    func getCachedValuesForAPIRequest(_ requestObject:APIRequestObject,withBlock block: @escaping (_ response:Any?,_ sucess:Bool) -> (Void)){
        
        switch (requestObject.requestType) {
            
        case .get,.post:
            
            self.getCachedValues(entityName: requestObject.entityTableName, withCompletionBlock: { (fetchedData) -> (Void) in
                
                block(fetchedData, true)
                
            }, withFailureBlock: { (error) -> (Void) in
                
                block(error, false)

            })
            
            break;
        default:
            break;
        }
    }
    
    
    func getCachedValues(entityName:String,withCompletionBlock completionBlock: @escaping (_ response:Any?) -> (Void),withFailureBlock failureBlock: @escaping (_ error:Error) -> (Void)) {
        
        //AA : The query to fetch the data in the passed table
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(entityName)")
        
        var fetchedArray = [NSManagedObject]()
        
        //AA : To execute the fetch query
        do {
            fetchedArray = try managedContextContainer.fetch(fetchRequest)
            completionBlock(fetchedArray)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            failureBlock(error)
        }
        
    }
    
    
    //MARK: - Save Cashed Gate
    
    
    func saveValue(object : Any , entityName : String) {
        
        //AA : To Execute the insert query
        do {
            try managedContextContainer.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }

    }
    
    
    //MARK: - Delete Cashed Gate
    
    func deleteValue(object: Any,for entityName: String,Id : Any){
    
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
//        let objectId = self.getById(id: Id as! NSManagedObjectID)
        fetchRequest.predicate = NSPredicate(format: "SELF = %@", Id as! CVarArg)
        fetchRequest.includesPropertyValues = false
        
        if let result = try? managedContextContainer.fetch(fetchRequest) {
            for object in result {
                managedContextContainer.delete(object)
            }
        }
        
        //SB Apply change to coredata
        self.saveValue(object: "", entityName: entityName)
    
    }
    
    func getById(id: NSManagedObjectID) -> Any? {
        return managedContextContainer.object(with: id)
    }
    
    
    //MARK: - Helping Methods 
    
    func getEntity(forEntityName name:String)->NSEntityDescription{
        
        return NSEntityDescription.entity(forEntityName: name, in: self.managedContextContainer)!
        
    }
    


    
}
