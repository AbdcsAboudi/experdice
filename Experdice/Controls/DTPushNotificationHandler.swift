

//  DTPushNotification.swift
//  WeLoveReadingVolunteer
//
//  Created by Ayman Rawashdeh on 12/11/16.
//  Revised and improved by Saad Basha on 12/11/16.
//  Copyright © 2016 Saad Basha. All rights reserved.
//

import Foundation
import UIKit


class DTPushNotificationHandler: NSObject,EXAlertViewControllerDelegate {
    
    static let sharedInstance = DTPushNotificationHandler()
    static var currentViewController: AnyObject?
    var pushReceivedWhileClosed = false
    var pushReceivedWhileInBackground = false
    
    var alertViewReference = EXAlertViewController(title: "", okay: "")
    var bookingId = 0
    var reminderId = 0
    
    
    //MARK: -  Life Cycle
    
    private override init(){
    }
    
    //MARK: -  Interface
    
    func inAppNotificationReceived(with userInfo: NSDictionary) {
        
        //AA : To handle the notification
        self.showPushNotificationWithUserInfo(userInfo: userInfo)
    }
    
    func opendAppFromPushNotification(with userInfo: NSDictionary){
        
        //AA : To handle the notification from out of app
        self.handleTheClickOnNotification(with: userInfo)
    }
    
    func showPushNotificationWithUserInfo(userInfo : NSDictionary) {
        
        print("DTPushNotificationHandler ----> ",userInfo)
        
        var userInfoDictionary:NSDictionary? = userInfo
        
        if #available(iOS 10, *) {
            
            if userInfo.object(forKey: "aps") != nil {
                userInfoDictionary = userInfo.object(forKey: "aps") as? NSDictionary
            }
        }
        
        if let alertDict = userInfoDictionary?["alert"] as? NSDictionary {
            
            let message = alertDict.value(forKey: "body") as? String ?? ""
            showNotification(userInfo: userInfo, title: "", message: message)
            
        }
        
    }
    
    func showNotification(userInfo: NSDictionary, title:String, message: String){
        
        guard DTCredentialManager.isLoggedIn() else {
            return
        }
        
        //AA : To get booking id if it exists
        if let parsedBookingId = userInfo.value(forKey: "bookingId") as? String{
            
            bookingId = Int(parsedBookingId)!
            
        }
        
        //AA : To get reminder id if it exists
        if let parsedReminderId = userInfo.value(forKey: "reminderId") as? String{
            
            reminderId = Int(parsedReminderId)!
            
        }
        let notificationAlert = EXAlertViewController(title: "Notification", okay: "ok")
        
        
        notificationAlert.delegate = self
        //notificationAlert.alertType = .notification
        
        notificationAlert.cancelButton?.setTitle(DTLanguageManager.sharedInstance.stringForLocalizedKey(key: "Cancel Appointment"), for: .normal)
       // notificationAlert.autoDismiss = false
        
        //AA : To get a reference from confirmation alertView
        alertViewReference = notificationAlert
        
        topViewController()?.present(notificationAlert, animated: true, completion: nil)
        
    }
    
    
    func handleTheClickOnNotification(with userInfo: NSDictionary) {
        
        //AA : Get the notification from notification payload
        print("Now At handleTheClickOnNotificationWithUserInfo with user info ", userInfo)
        
        var userInfoDictionary:NSDictionary? = userInfo
        
        if #available(iOS 10, *) {
            
            if userInfo.object(forKey: "userInfo") != nil {
                userInfoDictionary = userInfo.object(forKey: "userInfo") as? NSDictionary
            }
        }
    }
 
    // MARK: - Alert Delegates
    
    func alertDidConfirm(alertViewController: EXAlertViewController) {
        
    }
    
    func alertDidCancel(alertViewController: EXAlertViewController) {
       
    }
    
    //MARK: - View Controller Helping Methods
    
    private func topViewController() -> UIViewController?{
        
        return self.topViewControllerWithRootViewController(UIApplication.shared.keyWindow?.rootViewController)
    }
    
    private func topViewControllerWithRootViewController(_ rootViewController: UIViewController?) -> UIViewController?{
        
        if let navigationController = rootViewController as? UINavigationController {
            return topViewControllerWithRootViewController(navigationController.visibleViewController)
        } else if let tabBarController = rootViewController as? UITabBarController {
            return topViewControllerWithRootViewController(tabBarController.selectedViewController)
        } else if let presentedViewController = rootViewController?.presentedViewController {
            return topViewControllerWithRootViewController(presentedViewController)
        }
        
        return rootViewController
    }
    
    private func presentOnTopViewController(viewToPresent: UIViewController?){
        if let view = viewToPresent {
            self.topViewController()?.present(view, animated: false, completion: nil)
        }
    }
    
   
    //MARK: - API Methods
    
    
    
    
    
}

