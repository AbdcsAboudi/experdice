//
//  DTDataSource.swift
//  CoreDataTemplateApp
//
//  Created by Saad Basha on 1/16/17.
//  Copyright © 2017 DreamTechs. All rights reserved.
//

import UIKit

class DTDataSource {
    
    var isCashedEnabled = false
    
    //MARK: - Life Cycle
    
    private init(){
    
    
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    struct Static{
        
        static var instance: DTDataSource?
    }
    
    class var sharedInstance: DTDataSource{
        
        if Static.instance == nil
        {
            Static.instance = DTDataSource()
        }
        
        return Static.instance!
    }
    
    func dispose(){
        
        DTDataSource.Static.instance = nil
        print("Disposed Singleton instance")
        
        _ = DTDataSource.sharedInstance
        
    }
    
    //MARK: - Gate To Everything
    
    
    func executeAPIRequest(requestObject:APIRequestObject,withCachedCompletionBlock cacheCompletionBlock: @escaping (_ response:Any?,_ success:Bool) -> (Void),withAPICompletionBlock apiCompletionBlock: @escaping (_ response:Any?,_ success:Bool) -> (Void)) {
        
        

        if self.isCashedEnabled {

            switch requestObject.requestType {
            case .get,.post:
                
               DTDBManager.sharedInstance.getCachedValuesForAPIRequest(requestObject, withBlock: { (response, success) -> (Void) in
                
                   cacheCompletionBlock(response, success)
                
               })
               break;
            default:
                break;
            }
        
        }
        
        //- Request API
        
        APIClient.sharedInstance.makeApiRequest(requestObject) { (response, success) -> (Void) in
            
            if success == true && self.isCashedEnabled == true{
            
                // update database cache
                
                
            
            }
            
            apiCompletionBlock(response, success)
            
        }
        
        
    }
    
    
    
}
