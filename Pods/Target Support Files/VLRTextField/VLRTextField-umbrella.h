#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "NSString+VLRTextField.h"
#import "VLRFormService.h"
#import "VLRMultiDelegates.h"
#import "VLRTextField.h"
#import "VLRTextFieldMacros.h"

FOUNDATION_EXPORT double VLRTextFieldVersionNumber;
FOUNDATION_EXPORT const unsigned char VLRTextFieldVersionString[];

